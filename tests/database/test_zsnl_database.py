# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import uuid
from sqlalchemy.dialects.postgresql import UUID as pgUUID
from sqlalchemy.engine.base import Engine
from sqlalchemy.types import CHAR
from zsnl_domains.database import database_engine, schema
from zsnl_domains.database.types import GUID, JSONEncodedDict


class TestEngine:
    """Test database engine creation"""

    def test_database_engine_no_echo(self):
        """Test database_engine method"""

        configuration = {
            "ZaaksysteemDB": {
                "connect_info": {
                    "sqlalchemy.url": "sqlite://",
                    "sqlalchemy.echo": False,
                }
            }
        }

        engine = database_engine(config=configuration)

        assert isinstance(engine, Engine)
        assert not engine.echo

    def test_database_engine_with_echo(self):
        """Test database_engine method"""

        configuration = {
            "ZaaksysteemDB": {
                "connect_info": {
                    "sqlalchemy.url": "sqlite://",
                    "sqlalchemy.echo": True,
                }
            }
        }

        engine = database_engine(config=configuration)

        assert isinstance(engine, Engine)
        assert engine.echo


class TestSchema:
    """Test database schema"""

    def test_queue(self):
        fields = [
            "id",
            "object_id",
            "status",
            "type",
            "label",
            "data",
            "date_created",
            "date_started",
            "date_finished",
            "parent_id",
            "priority",
            "_metadata",
        ]

        for field in fields:
            assert hasattr(schema.Queue, field)

    def test_case(self):
        fields = [
            "id",
            "pid",
            "uuid",
            "coordinator",
            "behandelaar",
            "route_ou",
            "route_role",
            "behandelaar_gm_id",
            "coordinator_gm_id",
            "archival_state",
            "status",
            "last_modified",
            "aanvraag_trigger",
            "onderwerp",
            "resultaat",
            "besluit",
            "aanvrager",
            "locatie_zaak",
            "locatie_correspondentie",
            "deleted",
            "vervolg_van",
            "aanvrager_gm_id",
            "payment_status",
            "payment_amount",
            "confidentiality",
            "onderwerp_extern",
            "duplicate_prevention_token",
            "resultaat_id",
            "urgency",
            "preset_client",
            "prefix",
        ]

        for field in fields:
            assert hasattr(schema.Case, field)

    def test_case_authorisation(self):
        fields = [
            "id",
            "zaak_id",
            "capability",
            "entity_id",
            "entity_type",
            "scope",
        ]

        for field in fields:
            assert hasattr(schema.CaseAuthorisation, field)

    def test_case_meta(self):
        fields = [
            "id",
            "zaak_id",
            "verlenging",
            "opschorten",
            "gerelateerd",
            "afhandeling",
            "stalled_since",
            "unread_communication_count",
            "unaccepted_attribute_update_count",
            "unaccepted_files_count",
            "pending_changes",
        ]

        for field in fields:
            assert hasattr(schema.CaseMeta, field)

    def test_object_acl_entry(self):
        fields = [
            "uuid",
            "object_uuid",
            "entity_type",
            "entity_id",
            "capability",
            "scope",
            "groupname",
        ]

        for field in fields:
            assert hasattr(schema.ObjectAclEntry, field)

    def test_object_data(self):
        fields = [
            "uuid",
            "object_class",
            "object_id",
            "class_uuid",
            "acl_groupname",
            "invalid",
            "text_vector",
            "index_hstore",
        ]

        for field in fields:
            assert hasattr(schema.ObjectData, field)

    def test_group(self):
        fields = [
            "id",
            "path",
            "name",
            "description",
            "date_created",
            "date_modified",
            "uuid",
            "roles",
        ]

        for field in fields:
            assert hasattr(schema.Group, field)

    def test_role(self):
        fields = [
            "id",
            "parent_group_id",
            "parent_group",
            "name",
            "description",
            "system_role",
            "date_created",
            "date_modified",
            "uuid",
        ]

        for field in fields:
            assert hasattr(schema.Role, field)

    def test_subject(self):
        fields = [
            "id",
            "uuid",
            "subject_type",
            "properties",
            "settings",
            "username",
            "last_modified",
            "role_ids",
            "group_ids",
            "nobody",
            "system",
        ]

        for field in fields:
            assert hasattr(schema.Subject, field)

    def test_logging(self):
        fields = [
            "id",
            "zaak_id",
            "betrokkene_id",
            "aanvrager_id",
            "is_bericht",
            "component",
            "component_id",
            "seen",
            "onderwerp",
            "bericht",
            "created",
            "last_modified",
            "deleted_on",
            "event_type",
            "event_data",
            "created_by",
            "modified_by",
            "deleted_by",
            "created_for",
            "created_by_name_cache",
            "object_uuid",
            "restricted",
        ]

        for field in fields:
            assert hasattr(schema.Logging, field)

    def test_directory(self):
        fields = ["id", "name", "case_id", "original_name", "path", "uuid"]

        for field in fields:
            assert hasattr(schema.Directory, field)

    def test_file(self):
        fields = [
            "id",
            "filestore_id",
            "name",
            "extension",
            "root_file_id",
            "version",
            "case_id",
            "metadata_id",
            "subject_id",
            "directory_id",
            "creation_reason",
            "accepted",
            "rejection_reason",
            "reject_to_queue",
            "is_duplicate_name",
            "publish_pip",
            "publish_website",
            "date_created",
            "created_by",
            "date_modified",
            "modified_by",
            "date_deleted",
            "deleted_by",
            "destroyed",
            "scheduled_jobs_id",
            "intake_owner",
            "active_version",
            "is_duplicate_of",
            "queue",
            "document_status",
            "generator",
            "lock_timestamp",
            "lock_subject_id",
            "lock_subject_name",
            "uuid",
            "confidential",
            "search_term",
            "search_index",
            "object_type",
            "searchable_id",
            "search_order",
            "intake_group_id",
            "intake_role_id",
            "rejected_by_display_name",
        ]

        for field in fields:
            assert hasattr(schema.File, field)

    def test_filestore(self):
        fields = [
            "id",
            "uuid",
            "original_name",
            "size",
            "mimetype",
            "md5",
            "date_created",
            "storage_location",
            "is_archivable",
            "virus_scan_status",
        ]

        for field in fields:
            assert hasattr(schema.Filestore, field)

    def test_bibliotheek_categorie(self):
        fields = ["id", "uuid", "pid", "naam", "created", "last_modified"]

        for field in fields:
            assert hasattr(schema.BibliotheekCategorie, field)

    def test_bibliotheek_sjabloon(self):
        fields = [
            "id",
            "uuid",
            "bibliotheek_categorie_id",
            "created",
            "last_modified",
            "deleted",
            "naam",
            "filestore_id",
            "help",
            "interface_id",
            "template_external_name",
            "magic_strings",
        ]

        for field in fields:
            assert hasattr(schema.BibliotheekSjabloon, field)

    def test_bibliotheek_sjabloon_magic_string(self):
        fields = ["id", "bibliotheek_sjablonen_id", "value"]

        for field in fields:
            assert hasattr(schema.BibliotheekSjabloonMagicString, field)

    def test_bibliotheek_notificatie_kenmerk(self):
        fields = [
            "id",
            "bibliotheek_notificatie_id",
            "bibliotheek_kenmerken_id",
        ]

        for field in fields:
            assert hasattr(schema.BibliotheekNotificatieKenmerk, field)

    def test_bibliotheek_notificatie(self):
        fields = [
            "id",
            "uuid",
            "bibliotheek_categorie_id",
            "created",
            "last_modified",
            "deleted",
            "label",
            "subject",
            "search_term",
            "message",
            "sender",
            "sender_address",
            "attachments",
        ]

        for field in fields:
            assert hasattr(schema.BibliotheekNotificatie, field)

    def test_bibliotheek_kenmerk(self):
        fields = [
            "id",
            "uuid",
            "bibliotheek_categorie_id",
            "created",
            "last_modified",
            "deleted",
            "naam",
            "naam_public",
            "magic_string",
            "value_type",
            "value_default",
            "help",
            "document_categorie",
            "file_metadata_id",
            "type_multiple",
            "properties",
            "attachment_to",
        ]

        for field in fields:
            assert hasattr(schema.BibliotheekKenmerk, field)

    def test_bibliotheek_kenmerk_values(self):
        fields = [
            "id",
            "bibliotheek_kenmerken_id",
            "value",
            "active",
            "sort_order",
        ]

        for field in fields:
            assert hasattr(schema.BibliotheekKenmerkenValues, field)

    def test_file_metadata(self):
        fields = [
            "id",
            "description",
            "trust_level",
            "origin",
            "document_category",
            "origin_date",
            "pronom_format",
            "appearance",
            "structure",
        ]

        for field in fields:
            assert hasattr(schema.FileMetaData, field)

    def test_object_bibliotheek_entry(self):
        fields = ["id", "bibliotheek_categorie_id", "object_uuid", "name"]

        for field in fields:
            assert hasattr(schema.ObjectBibliotheekEntry, field)

    def test_zaaktype(self):
        fields = [
            "id",
            "bibliotheek_categorie_id",
            "zaaktype_node_id",
            "version",
            "created",
            "last_modified",
            "deleted",
            "active",
            "zaaktype_nodes",
        ]

        for field in fields:
            assert hasattr(schema.Zaaktype, field)

    def test_zaaktype_node(self):
        fields = [
            "id",
            "created",
            "last_modified",
            "deleted",
            "titel",
            "code",
            "trigger",
            "version",
            "active",
            "logging_id",
            "uuid",
            "zaaktype_id",
            "zaaktype",
            "zaaktype_definitie_id",
            "properties",
            "is_public",
            "zaaktype_trefwoorden",
            "zaaktype_omschrijving",
            "uuid",
            "webform_toegang",
            "webform_authenticatie",
            "adres_relatie",
            "aanvrager_hergebruik",
            "automatisch_aanvragen",
            "automatisch_behandelen",
            "toewijzing_zaakintake",
            "toelichting",
            "online_betaling",
            "adres_andere_locatie",
            "adres_aanvrager",
            "bedrijfid_wijzigen",
            "zaaktype_vertrouwelijk",
            "extra_relaties_in_aanvraag",
            "contact_info_intake",
            "prevent_pip",
            "contact_info_email_required",
            "contact_info_phone_required",
            "contact_info_mobile_phone_required",
            "moeder_zaaktype_id",
        ]

        for field in fields:
            assert hasattr(schema.ZaaktypeNode, field)

    def test_zaaktype_status(self):
        fields = [
            "id",
            "created",
            "last_modified",
            "zaaktype_node_id",
            "status",
            "status_type",
            "naam",
            "fase",
            "ou_id",
            "role_id",
            "checklist",
            "role_set",
        ]

        for field in fields:
            assert hasattr(schema.ZaaktypeStatus, field)

    def test_zaaktype_kenmerk(self):
        fields = [
            "id",
            "uuid",
            "created",
            "last_modified",
            "zaaktype_node_id",
            "zaak_status_id",
            "bibliotheek_kenmerken_id",
            "object_id",
            "value_mandatory",
            "label",
            "help",
            "pip",
            "zaakinformatie_view",
            "bag_zaakadres",
            "value_default",
            "pip_can_change",
            "publish_public",
            "referential",
            "is_systeemkenmerk",
            "required_permissions",
            "version",
            "help_extern",
            "object_metadata",
            "label_multiple",
            "properties",
            "is_group",
        ]

        for field in fields:
            assert hasattr(schema.ZaaktypeKenmerk, field)

    def test_zaaktype_sjabloon(self):
        fields = [
            "id",
            "created",
            "last_modified",
            "zaaktype_node_id",
            "zaak_status_id",
            "bibliotheek_sjablonen_id",
            "bibliotheek_kenmerken_id",
            "automatisch_genereren",
            "target_format",
        ]

        for field in fields:
            assert hasattr(schema.ZaaktypeSjabloon, field)

    def test_zaaktype_notificatie(self):
        fields = [
            "id",
            "created",
            "last_modified",
            "zaaktype_node_id",
            "zaak_status_id",
            "bibliotheek_notificatie_id",
            "label",
            "rcpt",
            "onderwerp",
            "bericht",
            "intern_block",
            "email",
            "behandelaar",
            "automatic",
            "cc",
            "bcc",
            "betrokkene_role",
        ]

        for field in fields:
            assert hasattr(schema.ZaaktypeNotificatie, field)

    def test_zaaktype_relatie(self):
        fields = [
            "id",
            "created",
            "last_modified",
            "zaaktype_node_id",
            "zaaktype_status_id",
            "relatie_zaaktype_id",
            "relatie_type",
            "start_delay",
            "status",
            "kopieren_kenmerken",
            "ou_id",
            "role_id",
            "automatisch_behandelen",
            "required",
            "subject_role",
            "copy_subject_role",
            "betrokkene_authorized",
            "betrokkene_notify",
            "betrokkene_id",
            "betrokkene_role",
            "betrokkene_role_set",
            "betrokkene_prefix",
            "eigenaar_type",
            "eigenaar_role",
            "eigenaar_id",
            "show_in_pip",
            "pip_label",
        ]

        for field in fields:
            assert hasattr(schema.ZaaktypeRelatie, field)

    def test_zaak_betrokkenen(self):
        fields = [
            "id",
            "zaak_id",
            "deleted",
            "betrokkene_type",
            "betrokkene_id",
            "gegevens_magazijn_id",
            "uuid",
            "subject_id",
            "naam",
            "rol",
            "magic_string_prefix",
            "verificatie",
            "pip_authorized",
            "authorisation",
        ]
        for field in fields:
            assert hasattr(schema.ZaakBetrokkenen, field)

    def test_message(self):
        fields = [
            "id",
            "message",
            "subject_id",
            "logging_id",
            "is_read",
            "is_archived",
        ]
        for field in fields:
            assert hasattr(schema.Message, field)

    def test_zaaktype_resultaten(self):
        fields = [
            "id",
            "zaaktype_node_id",
            "zaaktype_status_id",
            "resultaat",
            "ingang",
            "bewaartermijn",
            "created",
            "last_modified",
            "dossiertype",
            "label",
            "selectielijst",
            "archiefnominatie",
            "comments",
            "external_reference",
            "trigger_archival",
            "selectielijst_brondatum",
            "selectielijst_einddatum",
            "properties",
            "standaard_keuze",
        ]
        for field in fields:
            assert hasattr(schema.ZaaktypeResultaten, field)

    def test_zaaktype_regel(self):
        fields = [
            "id",
            "zaaktype_node_id",
            "zaak_status_id",
            "naam",
            "created",
            "last_modified",
            "settings",
            "active",
            "is_group",
        ]
        for field in fields:
            assert hasattr(schema.ZaaktypeRegel, field)

    def test_zaaktype_standaard_betrokkenen(self):
        fields = [
            "id",
            "uuid",
            "zaaktype_node_id",
            "zaak_status_id",
            "betrokkene_type",
            "betrokkene_identifier",
            "naam",
            "rol",
            "magic_string_prefix",
            "gemachtigd",
            "notify",
        ]
        for field in fields:
            assert hasattr(schema.ZaaktypeStandaardBetrokkenen, field)

    def test_file_case_document(self):
        fields = {
            "id",
            "file_id",
            "magic_string",
            "bibliotheek_kenmerken_id",
            "case_id",
        }
        for field in fields:
            assert hasattr(schema.FileCaseDocument, field)

    def test_company(self):
        fields = [
            "uuid",
            "id",
            "object_type",
            "handelsnaam",
            "email",
            "telefoonnummer",
            "werkzamepersonen",
            "dossiernummer",
            "subdossiernummer",
            "hoofdvestiging_dossiernummer",
            "hoofdvestiging_subdossiernummer",
            "vorig_dossiernummer",
            "vorig_subdossiernummer",
            "rechtsvorm",
            "kamernummer",
            "faillisement",
            "surseance",
            "contact_naam",
            "contact_aanspreektitel",
            "contact_voorvoegsel",
            "contact_voorletters",
            "contact_geslachtsnaam",
            "contact_geslachtsaanduiding",
            "vestiging_adres",
            "vestiging_straatnaam",
            "vestiging_huisnummer",
            "vestiging_huisnummertoevoeging",
            "vestiging_postcodewoonplaats",
            "vestiging_postcode",
            "vestiging_woonplaats",
            "correspondentie_straatnaam",
            "correspondentie_huisnummer",
            "correspondentie_huisnummertoevoeging",
            "correspondentie_postcodewoonplaats",
            "correspondentie_postcode",
            "correspondentie_woonplaats",
            "correspondentie_adres",
            "hoofdactiviteitencode",
            "nevenactiviteitencode1",
            "nevenactiviteitencode2",
            "authenticated",
            "authenticatedby",
            "fulldossiernummer",
            "import_datum",
            "deleted_on",
            "verblijfsobject_id",
            "system_of_record",
            "system_of_record_id",
            "vestigingsnummer",
            "vestiging_huisletter",
            "correspondentie_huisletter",
            "vestiging_adres_buitenland1",
            "vestiging_adres_buitenland2",
            "vestiging_adres_buitenland3",
            "vestiging_landcode",
            "correspondentie_adres_buitenland1",
            "correspondentie_adres_buitenland2",
            "correspondentie_adres_buitenland3",
            "correspondentie_landcode",
            "search_term",
        ]
        for field in fields:
            assert hasattr(schema.Bedrijf, field)

    def test_natural_person(self):
        fields = [
            "uuid",
            "id",
            "object_type",
            "voornamen",
            "voorvoegsel",
            "geslachtsnaam",
            "active",
            "naamgebruik",
            "adellijke_titel",
            "search_term",
            "burgerservicenummer",
            "a_nummer",
            "voorletters",
            "voornamen",
            "geslachtsnaam",
            "voorvoegsel",
            "geslachtsaanduiding",
            "nationaliteitscode1",
            "nationaliteitscode2",
            "nationaliteitscode3",
            "geboorteplaats",
            "geboorteland",
            "geboortedatum",
            "aanhef_aanschrijving",
            "voorletters_aanschrijving",
            "voornamen_aanschrijving",
            "naam_aanschrijving",
            "voorvoegsel_aanschrijving",
            "burgerlijke_staat",
            "indicatie_geheim",
            "land_waarnaar_vertrokken",
            "import_datum",
            "adres_id",
            "authenticated",
            "authenticatedby",
            "deleted_on",
            "verblijfsobject_id",
            "datum_overlijden",
            "aanduiding_naamgebruik",
            "onderzoek_persoon",
            "onderzoek_huwelijk",
            "onderzoek_overlijden",
            "onderzoek_verblijfplaats",
            "partner_a_nummer",
            "partner_burgerservicenummer",
            "partner_voorvoegsel",
            "partner_geslachtsnaam",
            "datum_huwelijk",
            "datum_huwelijk_ontbinding",
            "in_gemeente",
            "landcode",
        ]
        for field in fields:
            assert hasattr(schema.NatuurlijkPersoon, field)

    def test_natural_person_address(self):
        fields = [
            "id",
            "straatnaam",
            "huisnummer",
            "huisnummertoevoeging",
            "nadere_aanduiding",
            "postcode",
            "woonplaats",
            "landcode",
            "natuurlijk_persoon_id",
            "gemeentedeel",
            "functie_adres",
            "datum_aanvang_bewoning",
            "woonplaats_id",
            "gemeente_code",
            "hash",
            "import_datum",
            "deleted_on",
            "adres_buitenland1",
            "adres_buitenland2",
            "adres_buitenland3",
        ]
        for field in fields:
            assert hasattr(schema.Adres, field)

    def test_gm_bedrijf(self):
        fields = [
            "id",
            "handelsnaam",
            "email",
            "telefoonnummer",
            "werkzamepersonen",
            "dossiernummer",
            "subdossiernummer",
            "hoofdvestiging_dossiernummer",
            "hoofdvestiging_subdossiernummer",
            "vorig_dossiernummer",
            "vorig_subdossiernummer",
            "rechtsvorm",
            "kamernummer",
            "faillisement",
            "surseance",
            "contact_naam",
            "contact_aanspreektitel",
            "contact_voorvoegsel",
            "contact_voorletters",
            "contact_geslachtsnaam",
            "contact_geslachtsaanduiding",
            "vestiging_adres",
            "vestiging_straatnaam",
            "vestiging_huisnummer",
            "vestiging_huisnummertoevoeging",
            "vestiging_postcodewoonplaats",
            "vestiging_postcode",
            "vestiging_woonplaats",
            "correspondentie_straatnaam",
            "correspondentie_huisnummer",
            "correspondentie_huisnummertoevoeging",
            "correspondentie_postcodewoonplaats",
            "correspondentie_postcode",
            "correspondentie_woonplaats",
            "correspondentie_adres",
            "hoofdactiviteitencode",
            "nevenactiviteitencode1",
            "nevenactiviteitencode2",
            "authenticated",
            "authenticatedby",
            "import_datum",
            "verblijfsobject_id",
            "system_of_record",
            "system_of_record_id",
            "vestigingsnummer",
            "vestiging_huisletter",
            "correspondentie_huisletter",
            "vestiging_adres_buitenland1",
            "vestiging_adres_buitenland2",
            "vestiging_adres_buitenland3",
            "vestiging_landcode",
            "correspondentie_adres_buitenland1",
            "correspondentie_adres_buitenland2",
            "correspondentie_adres_buitenland3",
            "correspondentie_landcode",
        ]
        for field in fields:
            assert hasattr(schema.GmBedrijf, field)

    def test_contact_data(self):
        fields = [
            "id",
            "gegevens_magazijn_id",
            "betrokkene_type",
            "mobiel",
            "telefoonnummer",
            "email",
            "created",
            "last_modified",
            "note",
        ]
        for field in fields:
            assert hasattr(schema.ContactData, field)

    def test_gm_natuurlijk_persoon(self):
        fields = [
            "id",
            "voornamen",
            "gegevens_magazijn_id",
            "voorvoegsel",
            "geslachtsnaam",
            "naamgebruik",
            "adellijke_titel",
            "burgerservicenummer",
            "a_nummer",
            "voorletters",
            "voornamen",
            "geslachtsnaam",
            "voorvoegsel",
            "geslachtsaanduiding",
            "nationaliteitscode1",
            "nationaliteitscode2",
            "nationaliteitscode3",
            "geboorteplaats",
            "geboorteland",
            "geboortedatum",
            "aanhef_aanschrijving",
            "voorletters_aanschrijving",
            "voornamen_aanschrijving",
            "naam_aanschrijving",
            "voorvoegsel_aanschrijving",
            "burgerlijke_staat",
            "indicatie_geheim",
            "import_datum",
            "adres_id",
            "authenticated",
            "authenticatedby",
            "verblijfsobject_id",
            "datum_overlijden",
            "aanduiding_naamgebruik",
            "aanduiding_naamgebruik",
            "onderzoek_persoon",
            "onderzoek_huwelijk",
            "onderzoek_overlijden",
            "onderzoek_verblijfplaats",
            "partner_a_nummer",
            "partner_burgerservicenummer",
            "partner_voorvoegsel",
            "partner_geslachtsnaam",
            "datum_huwelijk",
            "datum_huwelijk_ontbinding",
            "landcode",
            "naamgebruik",
            "adellijke_titel",
        ]
        for field in fields:
            assert hasattr(schema.GmNatuurlijkPersoon, field)

    def test_gm_adres(self):
        fields = [
            "id",
            "straatnaam",
            "huisnummer",
            "huisnummertoevoeging",
            "nadere_aanduiding",
            "postcode",
            "woonplaats",
            "landcode",
            "natuurlijk_persoon_id",
            "gemeentedeel",
            "functie_adres",
            "datum_aanvang_bewoning",
            "woonplaats_id",
            "gemeente_code",
            "hash",
            "import_datum",
            "deleted_on",
            "adres_buitenland1",
            "adres_buitenland2",
            "adres_buitenland3",
        ]
        for field in fields:
            assert hasattr(schema.GmAdres, field)

    def test_config(self):
        fields = [
            "id",
            "uuid",
            "definition_id",
            "parameter",
            "value",
            "advanced",
        ]
        for field in fields:
            assert hasattr(schema.Config, field)

    def test_zaaktype_definitie(self):
        fields = [
            "id",
            "openbaarheid",
            "handelingsinitiator",
            "grondslag",
            "procesbeschrijving",
            "afhandeltermijn",
            "afhandeltermijn_type",
            "servicenorm",
            "servicenorm_type",
            "pdc_voorwaarden",
            "pdc_description",
            "pdc_meenemen",
            "pdc_tarief",
            "omschrijving_upl",
            "aard",
            "extra_informatie",
            "preset_client",
            "extra_informatie",
            "extra_informatie_extern",
        ]
        for field in fields:
            assert hasattr(schema.ZaaktypeDefinitie, field)

    def test_zaak_kenmerk(self):
        fields = ["id", "zaak_id", "bibliotheek_kenmerken_id", "value"]
        for field in fields:
            assert hasattr(schema.ZaakKenmerk, field)

    def test_zaaktype_betrokkenen(self):
        fields = [
            "id",
            "zaaktype_node_id",
            "betrokkene_type",
            "created",
            "last_modified",
        ]
        for field in fields:
            assert hasattr(schema.ZaaktypeBetrokkenen, field)

    def test_case_action(self):
        fields = [
            "id",
            "case_id",
            "casetype_status_id",
            "type",
            "label",
            "automatic",
            "data",
            "state_tainted",
            "data_tainted",
        ]
        for field in fields:
            assert hasattr(schema.CaseAction, field)

    def test_zaaktype_status_checklist_item(self):
        fields = ["id", "casetype_status_id", "label", "external_reference"]
        for field in fields:
            assert hasattr(schema.ZaaktypeChecklistItem, field)

    def test_checklist(self):
        fields = ["id", "case_id", "case_milestone"]
        for field in fields:
            assert hasattr(schema.Checklist, field)

    def test_checklist_item(self):
        fields = [
            "id",
            "uuid",
            "checklist_id",
            "label",
            "state",
            "sequence",
            "user_defined",
            "deprecated_answer",
            "due_date",
            "description",
            "assignee_id",
        ]
        for field in fields:
            assert hasattr(schema.ChecklistItem, field)

    def test_user_entity(self):
        fields = [
            "id",
            "uuid",
            "source_interface_id",
            "source_identifier",
            "subject_id",
            "date_created",
            "date_deleted",
            "properties",
            "password",
            "active",
        ]
        for field in fields:
            assert hasattr(schema.UserEntity, field)

    def test_interface(self):
        fields = [
            "id",
            "uuid",
            "name",
            "active",
            "multiple",
            "module",
            "date_deleted",
            "interface_config",
        ]

        for field in fields:
            assert hasattr(schema.Interface, field)

    def test_object_relationship(self):
        fields = [
            "uuid",
            "object1_uuid",
            "object2_uuid",
            "type1",
            "type2",
            "object1_type",
            "object2_type",
            "blocks_deletion",
            "title1",
            "title2",
            "owner_object_uuid",
        ]

        for field in fields:
            assert hasattr(schema.ObjectRelationship, field)

    def test_case_relation(self):
        fields = [
            "id",
            "case_id_a",
            "case_id_b",
            "order_seq_a",
            "order_seq_b",
            "type_a",
            "type_b",
            "uuid",
        ]

        for field in fields:
            assert hasattr(schema.CaseRelation, field)

    def test_zaaktype_authorisation(self):
        fields = [
            "id",
            "recht",
            "created",
            "last_modified",
            "deleted",
            "role_id",
            "ou_id",
            "zaaktype_id",
        ]

        for field in fields:
            assert hasattr(schema.ZaaktypeAuthorisation, field)

    def test_subject_position_matrix(self):
        fields = ["subject_id", "group_id", "role_id", "position"]

        for field in fields:
            assert hasattr(schema.SubjectPositionMatrix, field)

    def test_case_acl(self):
        fields = [
            "case_id",
            "case_uuid",
            "permission",
            "subject_id",
            "subject_uuid",
            "casetype_id",
        ]
        for field in fields:
            assert hasattr(schema.CaseAcl, field)

    def test_file_derivative(self):
        fields = [
            "id",
            "file_id",
            "filestore_id",
            "max_width",
            "max_height",
            "date_generated",
            "type",
        ]
        for field in fields:
            assert hasattr(schema.FileDerivative, field)


class TestJSONEncodedDictType:
    """Test custom database types"""

    def test_JSONEncodedDict_process_bind_param(self):
        g = JSONEncodedDict()

        assert g.process_bind_param(None, None) is None
        assert g.process_bind_param({"test": 1}, None) == '{"test": 1}'

    def test_JSONEncodedDict_process_result_value(self):
        g = JSONEncodedDict()

        assert g.process_result_value(None, None) is None
        assert g.process_result_value('{"test":1}', None) == {"test": 1}


class TestGUIDType:
    """Test custom database types"""

    def _get_fake_dialect(self):
        class FakeDialect:
            name = "postgresql"

            def type_descriptor(self, descriptor):
                self.descriptor = descriptor
                return "XXX"

        return FakeDialect()

    def test_GUID_load_dialect_impl(self):
        g = GUID()

        fake_dialect = self._get_fake_dialect()

        rv = g.load_dialect_impl(fake_dialect)

        assert rv == "XXX"
        assert isinstance(fake_dialect.descriptor, pgUUID)

        fake_dialect.name = "theirsql"
        rv = g.load_dialect_impl(fake_dialect)

        assert rv == "XXX"
        assert isinstance(fake_dialect.descriptor, CHAR)
        assert not isinstance(fake_dialect.descriptor, pgUUID)

        assert fake_dialect.descriptor.length == 32

    def test_GUID_process_bind_param(self):
        g = GUID()

        fake_dialect = self._get_fake_dialect()

        assert g.process_bind_param(None, fake_dialect) is None

        # PostgreSQL dialect: use UUID type
        assert (
            g.process_bind_param(
                uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95"), fake_dialect
            )
            == "3eb921de-b6d0-4fce-9565-785c6017fd95"
        )

        # Other dialects: use 32 byte hex strings
        fake_dialect.name = "theirsql"

        assert (
            g.process_bind_param(
                "3eb921de-b6d0-4fce-9565-785c6017fd95", fake_dialect
            )
            == "3eb921deb6d04fce9565785c6017fd95"
        )
        assert (
            g.process_bind_param(
                uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95"), fake_dialect
            )
            == "3eb921deb6d04fce9565785c6017fd95"
        )

    def test_GUID_process_result_value(self):
        g = GUID()

        assert g.process_result_value(None, self._get_fake_dialect()) is None
        assert g.process_result_value(
            uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95"),
            self._get_fake_dialect(),
        ) == uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95")
        assert g.process_result_value(
            "3eb921de-b6d0-4fce-9565-785c6017fd95", self._get_fake_dialect()
        ) == uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95")
