# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from uuid import uuid4
from zsnl_domains.communication.entities import Contact


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestContactEntity:
    contact_id = 123
    contact_uuid = uuid4()
    contact_name = "test"
    type = "person"

    def setup(self):
        self.contact = Contact(
            id=self.contact_id,
            uuid=self.contact_uuid,
            type="employee",
            name="contact name",
            address="amsterdam",
        )

    def test_contact_initialisation(self):
        assert self.contact.id == self.contact_id
        assert self.contact.uuid == self.contact_uuid
        assert self.contact.entity_id == self.contact_uuid
