# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from collections import namedtuple
from datetime import datetime, timezone
from dateutil.tz import tzoffset, tzutc
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import TimelineEntry


class Test_AsUser_GetListOfCustomObjectEventLogs(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

    def test_get_custom_object_event_logs(self):
        mock_custom_object_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "2"
        page_size = "10"

        mock_custom_object_events[0].configure_mock(
            id=10,
            type="custom_object/created",
            created=created1,
            description="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            user="auser",
            uuid=uuid1,
            entity_meta_summary="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            entity_id=uuid1,
            case_uuid=uuid4(),
            zaak_id=10,
        )
        mock_custom_object_events[1].configure_mock(
            id=11,
            type="custom_object/updated",
            created=created2,
            description="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            user="buser",
            uuid=uuid2,
            entity_meta_summary="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            entity_id=uuid2,
            case_uuid=uuid4(),
            zaak_id=1,
        )

        self.session.execute().fetchall.return_value = (
            mock_custom_object_events
        )

        custom_object_uuid = uuid4()

        mock_custom_object_event_list = self.qry.get_custom_object_event_logs(
            page=page,
            page_size=page_size,
            custom_object_uuid=str(custom_object_uuid),
        )

        assert isinstance(mock_custom_object_event_list[0], TimelineEntry)

        mock_custom_object_events = list(mock_custom_object_event_list)
        assert len(mock_custom_object_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()
        assert (
            str(query)
            == 'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id \n'
            "FROM logging \n"
            "WHERE logging.component = :component_1 AND (CAST(logging.event_data AS JSON) ->> :param_1) = :param_2 ORDER BY logging.created DESC\n"
            " LIMIT :param_3 OFFSET :param_4"
        )
        assert query.params["param_3"] == 10
        assert query.params["param_4"] == 10

    def test_get_custom_object_event_logs_with_period(self):
        mock_custom_object_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "2"
        page_size = "10"

        mock_custom_object_events[0].configure_mock(
            id=10,
            type="custom_object/created",
            created=created1,
            description="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            user="auser",
            uuid=uuid1,
            entity_meta_summary="Object TestObject1 van objecttype ObjectType1 is aangemaakt.",
            entity_id=uuid1,
            case_uuid=uuid4(),
            zaak_id=10,
        )
        mock_custom_object_events[1].configure_mock(
            id=11,
            type="custom_object/updated",
            created=created2,
            description="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            user="buser",
            uuid=uuid2,
            entity_meta_summary="Object TestObject2 van objecttype ObjectType2 is aangepast.",
            entity_id=uuid2,
            case_uuid=uuid4(),
            zaak_id=1,
        )

        self.session.execute().fetchall.return_value = (
            mock_custom_object_events
        )

        custom_object_uuid = uuid4()

        mock_custom_object_event_list = self.qry.get_custom_object_event_logs(
            page=page,
            page_size=page_size,
            custom_object_uuid=str(custom_object_uuid),
            period_start="2021-02-03T12:53:00+01:00",
            period_end="2021-02-04T12:53:00Z",
        )

        assert isinstance(mock_custom_object_event_list[0], TimelineEntry)

        mock_custom_object_events = list(mock_custom_object_event_list)
        assert len(mock_custom_object_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()
        assert (
            str(query)
            == 'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id \n'
            "FROM logging \n"
            "WHERE logging.component = :component_1 AND (CAST(logging.event_data AS JSON) ->> :param_1) = :param_2 AND logging.created >= :created_1 AND logging.created <= :created_2 ORDER BY logging.created DESC\n"
            " LIMIT :param_3 OFFSET :param_4"
        )
        assert query.params["created_1"] == datetime(
            2021, 2, 3, 12, 53, tzinfo=tzoffset(None, 3600)
        )
        assert query.params["created_2"] == datetime(
            2021, 2, 4, 12, 53, tzinfo=tzutc()
        )
        assert query.params["param_3"] == 10
        assert query.params["param_4"] == 10


class Test_AsUser_GetListOfContactEventLogs(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

    def test_get_contact_event_logs(self):
        mock_contact_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )

        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )

        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "1"
        page_size = "10"

        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        mock_contact_events[0].configure_mock(
            id=10,
            component="zaak",
            type="case/attribute/update",
            created=created1,
            description='Kenmerk "numeric field" gewijzigd',
            user="auser",
            uuid=uuid1,
            entity_meta_summary='Kenmerk "numeric field" gewijzigd',
            entity_id=uuid1,
            case_uuid=uuid4(),
            zaak_id=11,
            attribute_value="20",
        )
        mock_contact_events[1].configure_mock(
            id=11,
            type="case/document/create",
            created=created2,
            description='Document "testdoc-1" toegevoegd',
            user="auser",
            uuid=uuid2,
            entity_meta_summary='Document "testdoc-1" toegevoegd',
            entity_id=uuid2,
            case_uuid=uuid4(),
            zaak_id=1,
        )
        self.session.execute().fetchone.return_value = namedtuple(
            "ResultProxy", "type id"
        )(type="medewerker", id=1)

        self.session.execute().fetchall.return_value = mock_contact_events
        contact_uuid = uuid4()
        self.session.reset_mock()
        mock_contact_event_list = self.qry.get_contact_event_logs(
            page=page,
            page_size=page_size,
            contact_type="medewerker",
            contact_uuid=str(contact_uuid),
            period_start="2021-02-03T12:53:00Z",
            period_end="2021-02-03T12:53:00+01:00",
        )

        assert isinstance(mock_contact_event_list[0], TimelineEntry)

        mock_contact_events = list(mock_contact_event_list)
        assert len(mock_contact_events) == 2

        call_list = self.session.execute.call_args_list

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        # Time line events query

        assert (
            str(compiled_select) == "WITH anon_1 AS \n"
            '(SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, %(param_2)s AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value \n'
            "FROM logging \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_1_1)s, %(event_type_1_2)s, %(event_type_1_3)s, %(event_type_1_4)s, %(event_type_1_5)s, %(event_type_1_6)s, %(event_type_1_7)s, %(event_type_1_8)s, %(event_type_1_9)s, %(event_type_1_10)s, %(event_type_1_11)s, %(event_type_1_12)s, %(event_type_1_13)s, %(event_type_1_14)s, %(event_type_1_15)s, %(event_type_1_16)s, %(event_type_1_17)s, %(event_type_1_18)s, %(event_type_1_19)s, %(event_type_1_20)s, %(event_type_1_21)s, %(event_type_1_22)s, %(event_type_1_23)s, %(event_type_1_24)s, %(event_type_1_25)s, %(event_type_1_26)s, %(event_type_1_27)s, %(event_type_1_28)s, %(event_type_1_29)s, %(event_type_1_30)s, %(event_type_1_31)s, %(event_type_1_32)s, %(event_type_1_33)s, %(event_type_1_34)s, %(event_type_1_35)s, %(event_type_1_36)s, %(event_type_1_37)s, %(event_type_1_38)s, %(event_type_1_39)s) AND ((CAST(logging.event_data AS JSON) ->> %(param_4)s) = %(param_5)s OR logging.created_for = %(created_for_1)s) UNION SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_6)s AS comment, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_7)s AS attribute_value \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id JOIN zaak_betrokkenen ON zaak.aanvrager = zaak_betrokkenen.id \n"
            "WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_2_1)s, %(event_type_2_2)s, %(event_type_2_3)s, %(event_type_2_4)s, %(event_type_2_5)s, %(event_type_2_6)s, %(event_type_2_7)s, %(event_type_2_8)s, %(event_type_2_9)s, %(event_type_2_10)s, %(event_type_2_11)s, %(event_type_2_12)s, %(event_type_2_13)s, %(event_type_2_14)s, %(event_type_2_15)s, %(event_type_2_16)s, %(event_type_2_17)s, %(event_type_2_18)s, %(event_type_2_19)s, %(event_type_2_20)s, %(event_type_2_21)s, %(event_type_2_22)s, %(event_type_2_23)s, %(event_type_2_24)s, %(event_type_2_25)s, %(event_type_2_26)s, %(event_type_2_27)s, %(event_type_2_28)s, %(event_type_2_29)s, %(event_type_2_30)s, %(event_type_2_31)s, %(event_type_2_32)s, %(event_type_2_33)s, %(event_type_2_34)s, %(event_type_2_35)s, %(event_type_2_36)s, %(event_type_2_37)s, %(event_type_2_38)s, %(event_type_2_39)s) AND zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))))\n"
            ' SELECT anon_1.uuid, anon_1.type, anon_1.description, anon_1."user", anon_1.created, anon_1.zaak_id, anon_1.comment, anon_1.case_uuid, anon_1.attribute_value \n'
            "FROM anon_1 \n"
            "WHERE created >= %(created_1)s AND created <= %(created_2)s ORDER BY created DESC \n"
            " LIMIT %(param_8)s OFFSET %(param_9)s"
        )

        assert compiled_select.params["created_1"].tzinfo == timezone.utc
        assert compiled_select.params["created_2"].tzinfo == timezone.utc

        # Timeline events with only period start filter
        mock_contact_event_list = self.qry.get_contact_event_logs(
            page=page,
            page_size=page_size,
            contact_type="medewerker",
            contact_uuid=str(contact_uuid),
            period_start="2021-02-03T12:53:00Z",
        )

        assert isinstance(mock_contact_event_list[0], TimelineEntry)

        mock_contact_events = list(mock_contact_event_list)
        assert len(mock_contact_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        # Time line events query
        assert (
            str(query) == "WITH anon_1 AS \n"
            '(SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, %(param_2)s AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value \n'
            "FROM logging \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_1_1)s, %(event_type_1_2)s, %(event_type_1_3)s, %(event_type_1_4)s, %(event_type_1_5)s, %(event_type_1_6)s, %(event_type_1_7)s, %(event_type_1_8)s, %(event_type_1_9)s, %(event_type_1_10)s, %(event_type_1_11)s, %(event_type_1_12)s, %(event_type_1_13)s, %(event_type_1_14)s, %(event_type_1_15)s, %(event_type_1_16)s, %(event_type_1_17)s, %(event_type_1_18)s, %(event_type_1_19)s, %(event_type_1_20)s, %(event_type_1_21)s, %(event_type_1_22)s, %(event_type_1_23)s, %(event_type_1_24)s, %(event_type_1_25)s, %(event_type_1_26)s, %(event_type_1_27)s, %(event_type_1_28)s, %(event_type_1_29)s, %(event_type_1_30)s, %(event_type_1_31)s, %(event_type_1_32)s, %(event_type_1_33)s, %(event_type_1_34)s, %(event_type_1_35)s, %(event_type_1_36)s, %(event_type_1_37)s, %(event_type_1_38)s, %(event_type_1_39)s) AND ((CAST(logging.event_data AS JSON) ->> %(param_4)s) = %(param_5)s OR logging.created_for = %(created_for_1)s) UNION SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_6)s AS comment, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_7)s AS attribute_value \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id JOIN zaak_betrokkenen ON zaak.aanvrager = zaak_betrokkenen.id \n"
            "WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_2_1)s, %(event_type_2_2)s, %(event_type_2_3)s, %(event_type_2_4)s, %(event_type_2_5)s, %(event_type_2_6)s, %(event_type_2_7)s, %(event_type_2_8)s, %(event_type_2_9)s, %(event_type_2_10)s, %(event_type_2_11)s, %(event_type_2_12)s, %(event_type_2_13)s, %(event_type_2_14)s, %(event_type_2_15)s, %(event_type_2_16)s, %(event_type_2_17)s, %(event_type_2_18)s, %(event_type_2_19)s, %(event_type_2_20)s, %(event_type_2_21)s, %(event_type_2_22)s, %(event_type_2_23)s, %(event_type_2_24)s, %(event_type_2_25)s, %(event_type_2_26)s, %(event_type_2_27)s, %(event_type_2_28)s, %(event_type_2_29)s, %(event_type_2_30)s, %(event_type_2_31)s, %(event_type_2_32)s, %(event_type_2_33)s, %(event_type_2_34)s, %(event_type_2_35)s, %(event_type_2_36)s, %(event_type_2_37)s, %(event_type_2_38)s, %(event_type_2_39)s) AND zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))))\n"
            ' SELECT anon_1.uuid, anon_1.type, anon_1.description, anon_1."user", anon_1.created, anon_1.zaak_id, anon_1.comment, anon_1.case_uuid, anon_1.attribute_value \n'
            "FROM anon_1 \n"
            "WHERE created >= %(created_1)s ORDER BY created DESC \n"
            " LIMIT %(param_8)s OFFSET %(param_9)s"
        )
        # Timeline events with only period end filter
        mock_contact_event_list = self.qry.get_contact_event_logs(
            page=page,
            page_size=page_size,
            contact_type="medewerker",
            contact_uuid=str(contact_uuid),
            period_end="2021-02-03T12:53:00+01:00",
        )

        assert isinstance(mock_contact_event_list[0], TimelineEntry)

        mock_contact_events = list(mock_contact_event_list)
        assert len(mock_contact_events) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        # Time line events query
        assert (
            str(query) == "WITH anon_1 AS \n"
            '(SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, %(param_2)s AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value \n'
            "FROM logging \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_1_1)s, %(event_type_1_2)s, %(event_type_1_3)s, %(event_type_1_4)s, %(event_type_1_5)s, %(event_type_1_6)s, %(event_type_1_7)s, %(event_type_1_8)s, %(event_type_1_9)s, %(event_type_1_10)s, %(event_type_1_11)s, %(event_type_1_12)s, %(event_type_1_13)s, %(event_type_1_14)s, %(event_type_1_15)s, %(event_type_1_16)s, %(event_type_1_17)s, %(event_type_1_18)s, %(event_type_1_19)s, %(event_type_1_20)s, %(event_type_1_21)s, %(event_type_1_22)s, %(event_type_1_23)s, %(event_type_1_24)s, %(event_type_1_25)s, %(event_type_1_26)s, %(event_type_1_27)s, %(event_type_1_28)s, %(event_type_1_29)s, %(event_type_1_30)s, %(event_type_1_31)s, %(event_type_1_32)s, %(event_type_1_33)s, %(event_type_1_34)s, %(event_type_1_35)s, %(event_type_1_36)s, %(event_type_1_37)s, %(event_type_1_38)s, %(event_type_1_39)s) AND ((CAST(logging.event_data AS JSON) ->> %(param_4)s) = %(param_5)s OR logging.created_for = %(created_for_1)s) UNION SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_6)s AS comment, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_7)s AS attribute_value \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id JOIN zaak_betrokkenen ON zaak.aanvrager = zaak_betrokkenen.id \n"
            "WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_2_1)s, %(event_type_2_2)s, %(event_type_2_3)s, %(event_type_2_4)s, %(event_type_2_5)s, %(event_type_2_6)s, %(event_type_2_7)s, %(event_type_2_8)s, %(event_type_2_9)s, %(event_type_2_10)s, %(event_type_2_11)s, %(event_type_2_12)s, %(event_type_2_13)s, %(event_type_2_14)s, %(event_type_2_15)s, %(event_type_2_16)s, %(event_type_2_17)s, %(event_type_2_18)s, %(event_type_2_19)s, %(event_type_2_20)s, %(event_type_2_21)s, %(event_type_2_22)s, %(event_type_2_23)s, %(event_type_2_24)s, %(event_type_2_25)s, %(event_type_2_26)s, %(event_type_2_27)s, %(event_type_2_28)s, %(event_type_2_29)s, %(event_type_2_30)s, %(event_type_2_31)s, %(event_type_2_32)s, %(event_type_2_33)s, %(event_type_2_34)s, %(event_type_2_35)s, %(event_type_2_36)s, %(event_type_2_37)s, %(event_type_2_38)s, %(event_type_2_39)s) AND zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))))\n"
            ' SELECT anon_1.uuid, anon_1.type, anon_1.description, anon_1."user", anon_1.created, anon_1.zaak_id, anon_1.comment, anon_1.case_uuid, anon_1.attribute_value \n'
            "FROM anon_1 \n"
            "WHERE created <= %(created_1)s ORDER BY created DESC \n"
            " LIMIT %(param_8)s OFFSET %(param_9)s"
        )


class Test_GetCaseEventLogs(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

    def test_get_case_event_logs(self):
        mock_case_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2021, month=10, day=1, hour=13, minute=44, second=27
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="zaak-component",
            type="zaak/created",
            created=created1,
            description="Descripition",
            user="auser",
            uuid=uuid1,
            entity_meta_summary="Summary",
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="",
        )
        mock_case_events[1].configure_mock(
            id=11,
            component="zaak",
            type="zaak/updated",
            created=created2,
            description="Descripition2",
            user="buser",
            uuid=uuid2,
            entity_meta_summary="Summary2",
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=10,
            attribute_value="",
        )

        self.session.execute().fetchall.return_value = mock_case_events

        mock_case_event_list = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )
        assert isinstance(mock_case_event_list[0], TimelineEntry)

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
        )

        assert len(case_event_logs) == 2
        execute_calls = self.session.execute.call_args_list

        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_2)s AS attribute_value \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE logging.restricted IS false AND zaak.uuid = %(uuid_1)s AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_get_case_event_logs_with_period(self):
        mock_case_events = [mock.Mock(), mock.Mock()]
        created1 = datetime(
            year=2021, month=2, day=3, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2021, month=2, day=3, hour=13, minute=44, second=27
        )
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "1"
        page_size = "10"
        case_uuid = uuid4()

        mock_case_events[0].configure_mock(
            id=10,
            component="zaak",
            type="case/attribute/update",
            created=created1,
            description='Kenmerk "numeric field" gewijzigd',
            user="auser",
            uuid=uuid1,
            entity_meta_summary='Kenmerk "numeric field" gewijzigd',
            entity_id=uuid1,
            case_uuid=case_uuid,
            zaak_id=10,
            event_data={"comment": "reason"},
            attribute_value="20",
        )
        mock_case_events[1].configure_mock(
            id=11,
            component="zaak",
            type="custom_object/updated",
            created=created2,
            description="Descripition",
            user="buser",
            uuid=uuid2,
            entity_meta_summary="Descripition",
            entity_id=uuid2,
            case_uuid=case_uuid,
            zaak_id=1,
            attribute_value="",
        )

        self.session.execute().fetchall.return_value = mock_case_events

        mock_case_event_list = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
            period_start="2021-02-03T12:53:00Z",
            period_end="2021-02-04T12:53:00Z",
        )
        assert isinstance(mock_case_event_list[0], TimelineEntry)
        assert "reden" in mock_case_event_list[0].description

        self.session.reset_mock()
        case_event_logs = self.qry.get_case_event_logs(
            page=page,
            page_size=page_size,
            case_uuid=str(case_uuid),
            period_start="2021-02-03T12:53:00Z",
            period_end="2021-02-04T12:53:00Z",
        )

        assert len(case_event_logs) == 2
        execute_calls = self.session.execute.call_args_list

        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", logging.created AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_2)s AS attribute_value \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE logging.restricted IS false AND zaak.uuid = %(uuid_1)s AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND logging.created >= %(created_1)s AND logging.created <= %(created_2)s ORDER BY logging.created DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )
        assert compiled_select.params["created_1"] == datetime(
            2021, 2, 3, 12, 53, tzinfo=timezone.utc
        )
        assert compiled_select.params["created_2"] == datetime(
            2021, 2, 4, 12, 53, tzinfo=timezone.utc
        )
