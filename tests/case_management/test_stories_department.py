# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from minty.entity import EntityCollection
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_GetListOfDepartments(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

    def test_get_departments(self):
        parent_department = mock.Mock()
        child_department = mock.Mock()
        parent_uuid = uuid4()
        child_uuid = uuid4()

        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )
        child_department.configure_mock(
            uuid=child_uuid,
            name="Child",
            description="A group",
            parent_uuid=parent_uuid,
            parent_name="Parent",
        )

        mock_departments = [parent_department, child_department]

        self.session.execute().fetchall.return_value = mock_departments

        departments = self.qry.get_departments()
        assert isinstance(departments, EntityCollection)

        departments = list(departments)
        assert len(departments) == 2

        assert departments[0].name == "Parent"
        assert departments[1].name == "Child"
        assert departments[1].parent.name == "Parent"

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query)
            == "SELECT groups.uuid, groups.name, groups.description, groups_1.uuid AS parent_uuid, groups_1.name AS parent_name \n"
            "FROM groups LEFT OUTER JOIN groups AS groups_1 ON array_length(groups.path, :array_length_1) > :array_length_2 AND groups.path[(array_upper(groups.path, :array_upper_1) - :array_upper_2)] = groups_1.id"
        )
