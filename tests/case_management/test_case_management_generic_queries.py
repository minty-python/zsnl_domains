# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from zsnl_domains.shared.util import (
    get_country_name_from_landcode,
    get_type_of_business_from_entity_code,
)


class TestGenericQueries:
    def test_get_country_name_from_landcode(self):
        landcode = 12345
        res = get_country_name_from_landcode(landcode)
        assert res is None

        landcode = 5005
        res = get_country_name_from_landcode(landcode)
        assert res == "Malawi"

    def test_get_type_of_business_from_entity_code(self):
        legal_entity_code = 00
        res = get_type_of_business_from_entity_code(legal_entity_code)
        assert res is None

        legal_entity_code = 66
        res = get_type_of_business_from_entity_code(legal_entity_code)
        assert res == "Coöperatie B.A. blijkens statuten structuurcoöperatie"
