# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import CaseSummary


class Test_AsUser_GetListOfCaseSummaryByUUID(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

    def test_get_case_summaries_by_uuid(self):
        case_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_case_summary_rows = [mock.Mock()]
        mock_case_summary_rows[0].configure_mock(
            uuid=case_uuid,
            id=73,
            casetype_title="Parking permit",
            progress_status=0.2,
            summary="Parking permit in Amsterdam",
            result="Resultaat",
            assignee={"uuid": assignee_uuid, "name": "beheerder"},
            event_service=mock.MagicMock(),
        )

        self.session.execute().fetchall.return_value = mock_case_summary_rows
        self.session.execute.reset_mock()
        mock_case_summary_list = self.qry.get_case_summaries_by_uuid(
            case_uuids=[str(case_uuid)]
        )

        assert isinstance(mock_case_summary_list["result"][0], CaseSummary)
        assert mock_case_summary_list["result"][0].entity_id == case_uuid
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]

        assert isinstance(select_statement, sql.selectable.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak.uuid, zaak.id, zaaktype_node.titel AS casetype_title, zaak.onderwerp AS summary, zaak.resultaat AS result, CASE WHEN (subject.uuid IS NOT NULL) THEN json_build_object(%(json_build_object_1)s, subject.uuid, %(json_build_object_2)s, CAST(subject.properties AS JSON) -> %(param_1)s) END AS assignee, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress_status \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN subject ON subject.id = zaak.behandelaar_gm_id \n"
            "WHERE zaak.uuid IN (%(uuid_1_1)s)"
        )
