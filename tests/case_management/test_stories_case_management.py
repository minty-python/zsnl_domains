# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import pytest
import tests.case_management.test_entity_util as entity_util
from collections import defaultdict, namedtuple
from datetime import date, datetime
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import entities


class Test_AsUser_create_Case(TestBase):
    def setup(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, custom_fields, rules=[]):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": custom_fields,
                "checklist_items": [],
                "rules": rules,
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"

        return mock_case_type

    def __mock_case_type_attributes(self, case_type_uuid):
        case_type_attributes_for_magic_strings = mock.MagicMock()
        case_type_attributes_for_magic_strings = {
            "id": 1,
            "uuid": case_type_uuid,
            "case_type_json": {
                "case.casetype.id": 3,
                "case.casetype.goal": "Target test",
                "case.casetype.name": "test_case_type ",
                "case.casetype.wkpb": "Ja",
                "case.casetype.eform": "e-form-test",
                "case.casetype.node.id": 143,
                "case.casetype.penalty": "Ja",
                "case.casetype.version": 72,
                "case.casetype.keywords": "test keyword",
                "case.casetype.extension": "Ja",
                "case.casetype.motivation": "Reason is testing",
                "case.casetype.price.post": "600",
                "case.casetype.supervisor": "Test supervisor",
                "case.casetype.suspension": "Ja",
                "case.casetype.description": "test case type",
                "case.casetype.price.email": "400",
                "case.casetype.publication": "Ja",
                "case.casetype.price.counter": "200",
                "case.casetype.adjourn_period": "3",
                "case.casetype.identification": "Testid",
                "case.casetype.price.employee": "500",
                "case.casetype.price.telephone": "300",
                "case.casetype.principle_local": "test-2",
                "case.casetype.extension_period": "2",
                "case.casetype.initiator_source": "email",
                "case.casetype.registration_bag": "Ja",
                "case.casetype.supervisor_relation": "Test relation",
                "case.casetype.objection_and_appeal": "Nee",
                "case.casetype.text_for_publication": "Publicatietekst",
                "case.casetype.lex_silencio_positivo": "Ja",
                "case.casetype.version_date_of_creation": "2021-10-21T12:36:46.345679+00:00",
                "case.casetype.version_date_of_expiration": None,
                "case.casetype.archive_classification_code": "123",
                "case.casetype.designation_of_confidentiality": "Openbaar",
            },
        }
        return case_type_attributes_for_magic_strings

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Checkbox",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_checkbox",
                "field_type": "checkbox",
                "is_hidden_field": False,
                "field_options": ["test", "test1"],
                "date_field_limit": {},
            },
            {
                "uuid": uuid4(),
                "is_required": False,
                "field_magic_string": "test_option",
                "field_type": "option",
                "label": "Custom label for option",
                "field_options": ["step1", "step2"],
                "name": "Option name",
            },
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        requestor_uuid = uuid4()
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": requestor_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            None,
            None,
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="email",
            requestor={"type": "employee", "id": str(requestor_uuid)},
            custom_fields={
                "test_checkbox": [["test", "test1"]],
                "test_option": ["step1", "step2"],
            },
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={"mobile_number": "+3106123456789"},
            options={"allow_missing_required_fields": True},
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query) == "UPDATE zaak SET route_ou=(SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_1), route_role=(SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_2), last_modified=:last_modified, onderwerp=:onderwerp, onderwerp_extern=:onderwerp_extern WHERE zaak.uuid = :uuid_3"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_incorrect_custom_fields(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject_field",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            },
            {
                "name": "Test checkbox",
                "uuid": field_type_uuids[1],
                "field_magic_string": "att_checkbox",
                "field_type": "checkbox",
                "is_hidden_field": True,
                "is_required": True,
                "field_options": ["option_a", "option_b"],
                "date_field_limit": {},
            },
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        department_uuid = uuid4()
        subject_uuid = str(self.cmd.user_uuid)

        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        val = [
            {
                "case_id": 247,
                "case_type_id": 1,
                "case_type_node_id": 83,
                "case_cordinator_id": 1,
                "case_assignee_id": 1,
                "department_id": 1,
                "role_id": 1,
            }
        ]

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            val,
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            None,
            None,
        ]

        case_uuid = str(uuid4())
        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="internal",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": subject_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )
        assert excinfo.value.args == (
            "Custom fields 'test_subject' is/are not allowed",
            "case/custom_fields_not_allowed",
        )

        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            case_type_custom_fields = [
                {
                    "name": "Test Subject",
                    "uuid": field_type_uuids[0],
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                    "is_hidden_field": False,
                    "field_options": [],
                    "date_field_limit": {},
                    "is_required": True,
                },
                {
                    "name": "Test Name",
                    "uuid": field_type_uuids[0],
                    "field_magic_string": "test_name",
                    "field_type": "text",
                    "is_hidden_field": False,
                    "field_options": [],
                    "date_field_limit": {},
                    "is_required": True,
                },
            ]
            mock_case_type = self.__mock_case_type(
                case_type_uuid, case_type_custom_fields
            )
            self.session.query().filter().one.side_effect = [mock_subject]

            self.session.execute().fetchone.side_effect = [
                mock_case_type_node,
                [mock_case_type],
                True,
                [case_type_attributes_for_magic_strings],
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                parent_department,
                mock_role,
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                val,
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                None,
                None,
            ]
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="internal",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": subject_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": False},
            )
        assert excinfo.value.args == (
            "Custom fields 'test_name' is/are required",
            "case/required_custom_fields_missing",
        )

        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.session.query().filter().one.side_effect = [mock_subject]

            self.session.execute().fetchone.side_effect = [
                mock_case_type_node,
                [mock_case_type],
                True,
                [case_type_attributes_for_magic_strings],
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                parent_department,
                mock_role,
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                val,
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                namedtuple("query_result", "department_id role_id")(
                    department_id=1, role_id=1
                ),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                [mock_subject],
                namedtuple("case_id", "id")(id=247),
                None,
                None,
            ]
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": "test"},
                confidentiality="internal",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": subject_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )
        assert excinfo.value.args == (
            "Custom field values 'test' for 'test_subject' is not a list",
            "case/custom_field_values_not_list",
        )

        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            mock_case_type["phases"] = []
            mock_role = mock.Mock()
            mock_role.configure_mock(
                uuid=str(uuid4()),
                name="Role name",
                description="A role",
                parent_uuid=uuid4(),
                parent_name="Parent group",
            )
            self.session.execute().fetchone.side_effect = [
                mock_case_type_node,
                [mock_case_type],
                True,
                [case_type_attributes_for_magic_strings],
                [mock_subject],
                namedtuple("query_result", "department")(
                    department="department"
                ),
                parent_department,
                mock_role,
                val,
            ]
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(subject_uuid)},
                custom_fields={"test_subject": "test"},
                confidentiality="internal",
                assignment={
                    "role": {"id": str(uuid4()), "type": "role"},
                    "department": {
                        "id": str(department_uuid),
                        "type": "department",
                    },
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )
        assert excinfo.value.args == (
            f"Case type '{mock_case_type['uuid']}' has no phases in it.",
            "case_type/no_phases",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_rules(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_kenmerk":"contactchannel","active":"1","voorwaarden":"1","is_group":0,"voorwaarde_1_value_checkbox":"1","bibliotheek_kenmerken_id":null,"condition_type":"and","acties":"1","properties":{"map_case_location":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"text_content":null,"map_wms_layer_id":null,"map_wms_feature_attribute_label":null,"show_on_map":null,"skip_change_approval":null},"actie_1_value":"internal","actie_1_attribute_type":"none","mijlsort":"2","voorwaarde_1_value":"behandelaar","actie_1":"change_confidentiality","naam":"condition contact channel"}',
                "active": True,
                "is_group": False,
            },
            {
                "name": "condition without condition?",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"help":"","zaaktype_titel":"Basiszaaktype dun","id":"11249","pip":null,"bibliotheek_kenmerken_id":null,"mijlsort":"1","zaak_status_id":16459,"active":null,"zaaktype_node_id":7336,"naam":"Regels voor afhandelen zaak","is_group":"1","properties":{"skip_change_approval":null,"text_content":null,"map_wms_feature_attribute_label":null,"map_case_location":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null}}',
                "active": True,
                "is_group": False,
            },
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[15][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "internal"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_rules_with_or_condition(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_value_checkbox":"1","condition_type":"or","acties":"1","is_group":0,"properties":{"map_wms_feature_attribute_label":null,"map_case_location":null,"text_content":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null,"skip_change_approval":null,"show_on_map":null},"voorwaarde_1_kenmerk":"contactchannel","bibliotheek_kenmerken_id":null,"mijlsort":"1","ander_2_value":"public","active":"1","actie_1":"change_confidentiality","ander_2":"change_confidentiality","actie_1_value":"internal","voorwaarden":"1","voorwaarde_1_value":"behandelaar","naam":"condition contact channel","ander_2_attribute_type":"none","actie_1_attribute_type":"none","anders":"2"}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="post",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[15][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_nomatching_rules(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_value_checkbox":"1","condition_type":"and","acties":"1","is_group":0,"properties":{"map_wms_feature_attribute_label":null,"map_case_location":null,"text_content":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null,"skip_change_approval":null,"show_on_map":null},"voorwaarde_1_kenmerk":"contactchannel","bibliotheek_kenmerken_id":null,"mijlsort":"1","ander_2_value":"public","active":"1","actie_1":"change_confidentiality","ander_2":"change_confidentiality","actie_1_value":"internal","voorwaarden":"1","voorwaarde_1_value":"behandelaar","naam":"condition contact channel","ander_2_attribute_type":"none","actie_1_attribute_type":"none","anders":"2"}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="post",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list
        insert_case_stmt = db_execute_calls[15][0][0].compile()
        assert insert_case_stmt.params["confidentiality"] == "public"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_errors(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_value_checkbox":"1","condition_type":"and","acties":"1","is_group":0,"properties":{"map_wms_feature_attribute_label":null,"map_case_location":null,"text_content":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null,"skip_change_approval":null,"show_on_map":null},"voorwaarde_1_kenmerk":"aanvrager","bibliotheek_kenmerken_id":null,"mijlsort":"1","ander_2_value":"public","active":"1","actie_1":"change_confidentiality","ander_2":"change_confidentiality","actie_1_value":"internal","voorwaarden":"1","voorwaarde_1_value":"behandelaar","naam":"condition contact channel","ander_2_attribute_type":"none","actie_1_attribute_type":"none","anders":"2"}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        with pytest.raises(NotImplementedError) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="post",
                requestor={"type": "employee", "id": str(assignee_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": False,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )

        assert excinfo.value.args == (
            "Condition 'aanvrager' is not implemented in v2 rule_engine",
            "rule_engine/condition_not_implemeneted",
        )

        rules = [
            {
                "name": "condition contact channel",
                "created": "2021-04-26T14:55:56.617921",
                "last_modified": "2021-04-26T14:55:56.617921",
                "settings": '{"voorwaarde_1_value_checkbox":"1","condition_type":"and","acties":"1","is_group":0,"properties":{"map_wms_feature_attribute_label":null,"map_case_location":null,"text_content":null,"relationship_subject_role":null,"map_wms_feature_attribute_id":null,"map_wms_layer_id":null,"skip_change_approval":null,"show_on_map":null},"voorwaarde_1_kenmerk":"contactchannel","bibliotheek_kenmerken_id":null,"mijlsort":"1","ander_2_value":"public","active":"1","actie_1":"set_value","ander_2":"change_confidentiality","actie_1_value":"internal","voorwaarden":"1","voorwaarde_1_value":"behandelaar","naam":"condition contact channel","ander_2_attribute_type":"none","actie_1_attribute_type":"none","anders":"2"}',
                "active": True,
                "is_group": False,
            }
        ]
        mock_case_type = self.__mock_case_type(
            case_type_uuid, custom_fields, rules
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]
        with pytest.raises(NotImplementedError) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="behandelaar",
                requestor={"type": "employee", "id": str(assignee_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": False,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )

        assert excinfo.value.args == (
            "Action 'set_value' is not implemented in v2 rule_engine",
            "rule_engine/action_not_implemeneted",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_case_summaries_containing_magic_strings(
        self, mock_ids
    ):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type[
            "case_summary"
        ] = "Case is [[j('$.attributes.status')]] with confidentiality '[[j('$.attributes.confidentiality')]]' and case_type is '[[j('$.relationships.case_type.name')]]'"
        mock_case_type[
            "case_public_summary"
        ] = "Value of 'test_subject' using j style magic string is '[[j('$.attributes.custom_fields.test_subject.value')]]' and with legacy magic string is '[[test_subject]]'"

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="internal",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[15][0][0].compile()
        assert (
            insert_case_stmt.params["onderwerp"]
            == "Case is new with confidentiality 'internal' and case_type is 'test_case_type '"
        )
        assert (
            insert_case_stmt.params["onderwerp_extern"]
            == "Value of 'test_subject' using j style magic string is 'test' and with legacy magic string is 'test'"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_empty_case_summaries(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]

        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        (
            mock_case_type["case_summary"],
            mock_case_type["case_public_summary"],
        ) = (None, None)

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.Mock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }

        self.session.query().filter().one.side_effect = [mock_subject]
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            [mock_subject],
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
            namedtuple("case_id", "id")(id=247),
        ]

        case_uuid = str(uuid4())

        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="behandelaar",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="internal",
            assignment={
                "employee": {
                    "type": "employee",
                    "id": assignee_uuid,
                    "use_employee_department": False,
                }
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        db_execute_calls = self.session.execute.call_args_list

        insert_case_stmt = db_execute_calls[15][0][0].compile()
        assert insert_case_stmt.params["onderwerp"] == ""
        assert insert_case_stmt.params["onderwerp_extern"] == ""

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_with_department(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        mock_role.configure_mock(
            uuid=str(uuid4()),
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        # Mock subject
        mock_subject = mock.MagicMock()
        department_uuid = uuid4()
        assignee_uuid = str(uuid4())
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
            mock_role,
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("query_result", "department_id role_id")(
                department_id=1, role_id=1
            ),
            namedtuple("case_id", "id")(id=247),
            namedtuple("group_id", "id")(id=1),
        ]

        case_uuid = str(uuid4())
        role_uuid = str(uuid4())
        self.cmd.create_case(
            case_uuid=case_uuid,
            case_type_version_uuid=str(case_type_uuid),
            contact_channel="email",
            requestor={"type": "employee", "id": str(assignee_uuid)},
            custom_fields={"test_subject": ["test"]},
            confidentiality="public",
            assignment={
                "role": {"id": str(role_uuid), "type": "role"},
                "department": {
                    "id": str(department_uuid),
                    "type": "department",
                },
            },
            contact_information={},
            options={"allow_missing_required_fields": True},
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query) == "UPDATE zaak SET route_ou=(SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_1), route_role=(SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_2), last_modified=:last_modified, onderwerp=:onderwerp, onderwerp_extern=:onderwerp_extern WHERE zaak.uuid = :uuid_3"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_number_prefix_from_config"
    )
    def test_create_case_conflict_not_active(self, mock_ids):
        mock_ids.return_value = ""
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        field_type_uuids = [str(uuid4()) for _ in range(4)]
        custom_fields = [
            {
                "name": "Test Subject",
                "uuid": field_type_uuids[0],
                "field_magic_string": "test_subject",
                "field_type": "text",
                "is_hidden_field": False,
                "field_options": [],
                "date_field_limit": {},
            }
        ]
        mock_case_type = self.__mock_case_type(case_type_uuid, custom_fields)
        mock_case_type["active"] = False
        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        # Mock subject
        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(self.cmd.user_uuid)
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )

        mock_subject = {
            "id": "1",
            "uuid": assignee_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": department_uuid,
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            parent_department,
        ]

        case_uuid = str(uuid4())
        with pytest.raises(Conflict) as excinfo:
            self.cmd.create_case(
                case_uuid=case_uuid,
                case_type_version_uuid=str(case_type_uuid),
                contact_channel="email",
                requestor={"type": "employee", "id": str(assignee_uuid)},
                custom_fields={"test_subject": ["test"]},
                confidentiality="public",
                assignment={
                    "employee": {
                        "type": "employee",
                        "id": assignee_uuid,
                        "use_employee_department": True,
                    }
                },
                contact_information={},
                options={"allow_missing_required_fields": True},
            )

        assert excinfo.value.args == (
            "Case type is not active.",
            "case_type/not_active",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_set_registration_date(self, mock_version):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2019-01-31"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        department_uuid = uuid4()
        subject_uuid = str(uuid4())
        advocaat_uuid = str(uuid4())

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )

        mock_requestor = mock.MagicMock()
        mock_requestor = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        mock_coordinator = mock.MagicMock()
        mock_coordinator = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Coordinator",
            "magic_string_prefix": "coordinator",
        }
        mock_advocaat = mock.MagicMock()
        mock_advocaat = {
            "uuid": advocaat_uuid,
            "type": "natuurlijk_persoon",
            "role": "Advocaat",
            "magic_string_prefix": "advocaat",
        }
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "type": "person",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": None,
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": True,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": 1,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }

        self.session.query().filter().one.side_effect = [
            uuid4(),
            namedtuple("Department", "department")(department="Development"),
        ]

        subject_id = str(uuid4())
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": [
                {
                    "betrokkene_type": "medewerker",
                    "rol": "advocaat",
                    "subject_id": subject_id,
                }
            ],
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": [
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": uuid4(),
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": uuid4(),
                        },
                    ],
                }
            ],
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "case_location": "Test locataion",
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "manage"],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee, mock_coordinator, mock_advocaat]],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [person],
            mock_case_type_node,
            [mock_case_type],
            None,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()
        self.cmd.set_case_registration_date(
            case_uuid=case_uuid, target_date=target_date
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 19

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

        update_statement = call_list[18][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_update) == (
            "UPDATE zaak SET registratiedatum=%(registratiedatum)s, last_modified=now(), onderwerp=%(onderwerp)s, onderwerp_extern=%(onderwerp_extern)s WHERE zaak.uuid = %(uuid_1)s"
        )
        assert compiled_update.params["registratiedatum"] == "2019-01-31"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_set_registration_date_conflict(self, mock_version):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2020-01-31"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        self.session.query().filter().one.side_effect = [uuid4()]

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 10),
            "streefafhandeldatum": date(2020, 1, 5),
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "case_location": "Test locataion",
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            mock.MagicMock(),
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]
        with pytest.raises(Conflict) as excinfo:
            self.cmd.set_case_registration_date(
                case_uuid=case_uuid, target_date=target_date
            )

        assert excinfo.value.args == (
            "Not allowed to set 'registration_date' after 'target_completion_date'.",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_set_registration_date_conflict_target_completion_date(
        self, mock_version
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2020-10-20"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        self.session.query().filter().one.side_effect = [uuid4()]
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": date(2019, 1, 31),
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 10),
            "streefafhandeldatum": date(2019, 1, 5),
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            mock.MagicMock(),
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.set_case_registration_date(
                case_uuid=case_uuid, target_date=target_date
            )

        assert excinfo.value.args == (
            "Not allowed to set 'registration_date' after 'completion_date'.",
        )

    def test_assign_case_to_user_not_found(self):
        case_uuid = str(uuid4())
        user_uuid = str(uuid4())
        self.session.execute().fetchone.side_effect = [None]
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.assign_case_to_user(
                case_uuid=case_uuid, user_uuid=user_uuid
            )

        assert excinfo.value.args == (
            f"Employee with uuid '{user_uuid}' not found.",
            "employee/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_set_case_target_completion_date(self, mock_version):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        target_date = "2019-01-31"
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        self.session.query().filter().one.side_effect = [uuid4()]
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": date(2020, 1, 5),
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "case_location": "Test locataion",
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            mock.MagicMock(),
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]

        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.cmd.set_case_registration_date(
            case_uuid=case_uuid, target_date=target_date
        )

        assert mock_case["registratiedatum"] == date(2020, 1, 1)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 14

        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Function"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Select"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_enqueue_case_assignee_email(self, mock_version):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor = mock.MagicMock()
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )
        self.session.query().filter().one.side_effect = [uuid4()]

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=uuid4(),
            parent_name="Parent group",
        )
        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": date(2020, 1, 5),
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "case_location": "Test locataion",
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            mock_case_type_node,
            [mock_case_type],
            True,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )

        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()
        self.cmd.enqueue_case_assignee_email(case_uuid=case_uuid)
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 12

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Select"
        insert_statement = call_list[11][0][0]
        compiled_select = insert_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "INSERT INTO queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) VALUES (%(id)s, %(object_id)s, %(status)s, %(type)s, %(label)s, %(data)s, %(date_created)s, %(date_started)s, %(date_finished)s, %(parent_id)s, %(priority)s, %(metadata)s)"
        )

    def test_reassign_case_messages(self):
        case_uuid = str(uuid4())
        case_id = 1
        subject_uuid = str(uuid4())

        mock_message = mock.Mock()
        mock_message.configure_mock(
            case_id=case_id,
            case_uuid=case_uuid,
            user=None,
        )

        mock_subject = mock.Mock()
        department_uuid = uuid4()
        assignee_uuid = str(uuid4())
        mock_subject.configure_mock(
            id=1,
            uuid=assignee_uuid,
            subject_type="employee",
            properties={},
            settings={},
            username="Subject",
            last_modified=None,
            role_ids=[],
            group_ids=[],
            nobody=False,
            system=True,
            department_uuid=department_uuid,
            department_name="department",
        )
        self.session.execute().fetchone.side_effect = [
            mock_subject,
            mock_message,
        ]
        self.session.reset_mock()

        self.cmd.reassign_case_messages(
            case_uuid=case_uuid, subject_uuid=subject_uuid
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT subject.uuid, subject.id, subject.subject_type, subject.properties, subject.settings, subject.username, subject.last_modified, subject.role_ids, subject.group_ids, subject.nobody, subject.system, groups.uuid AS department_uuid, groups.name AS department_name \n"
            "FROM subject, groups \n"
            "WHERE subject.uuid = %(uuid_1)s AND groups.id = subject.group_ids[%(group_ids_1)s]"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "UPDATE message SET subject_id=concat(%(concat_1)s, (SELECT id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s)) WHERE message.logging_id IN (SELECT logging.id \n"
            "FROM logging \n"
            "WHERE logging.zaak_id = %(zaak_id_1)s)"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_pause_case(self, mock_version):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        # department entity
        parent_department = mock.Mock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()
        department_uuid = uuid4()
        subject_uuid = str(uuid4())
        advocaat_uuid = str(uuid4())

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )

        mock_requestor = mock.MagicMock()
        mock_requestor = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        mock_coordinator = mock.MagicMock()
        mock_coordinator = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Coordinator",
            "magic_string_prefix": "coordinator",
        }
        mock_advocaat = mock.MagicMock()
        mock_advocaat = {
            "uuid": advocaat_uuid,
            "type": "natuurlijk_persoon",
            "role": "Advocaat",
            "magic_string_prefix": "advocaat",
        }
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "type": "person",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": None,
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": True,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": 1,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }

        self.session.query().filter().one.side_effect = [
            uuid4(),
            namedtuple("Department", "department")(department="Development"),
        ]

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "extension_reason": None,
            "suspension_reason": None,
            "related_to": "{}",
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": date(2020, 1, 1),
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": [
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": uuid4(),
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": uuid4(),
                        },
                    ],
                }
            ],
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "case_location": "Test locataion",
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee, mock_coordinator, mock_advocaat]],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [person],
            mock_case_type_node,
            [mock_case_type],
            None,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()
        mock_related_case_1.case_a_id = 10
        mock_related_case_1.status = "resolved"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 10
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        self.cmd.pause_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            suspension_reason="suspension",
            suspension_term_value=6,
            suspension_term_type="weeks",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 21

        update_statement = call_list[19][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE zaak SET status=%(status)s, stalled_until=%(stalled_until)s, last_modified=now(), onderwerp=%(onderwerp)s, onderwerp_extern=%(onderwerp_extern)s WHERE zaak.uuid = %(uuid_1)s"
        )
        assert compiled_update.params["status"] == "stalled"


class TestAs_A_User_I_Want_To_Get_case(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.qry.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, custom_fields, rules=[]):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": custom_fields,
                "checklist_items": [],
                "rules": rules,
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"

        return mock_case_type

    def __mock_case_type_attributes(self, case_type_uuid):
        case_type_attributes_for_magic_strings = mock.MagicMock()
        case_type_attributes_for_magic_strings = {
            "id": 1,
            "uuid": case_type_uuid,
            "case_type_json": {
                "case.casetype.id": 3,
                "case.casetype.goal": "Target test",
                "case.casetype.name": "test_case_type ",
                "case.casetype.wkpb": "Ja",
                "case.casetype.eform": "e-form-test",
                "case.casetype.node.id": 143,
                "case.casetype.penalty": "Ja",
                "case.casetype.version": 72,
                "case.casetype.keywords": "test keyword",
                "case.casetype.extension": "Ja",
                "case.casetype.motivation": "Reason is testing",
                "case.casetype.price.post": "600",
                "case.casetype.supervisor": "Test supervisor",
                "case.casetype.suspension": "Ja",
                "case.casetype.description": "test case type",
                "case.casetype.price.email": "400",
                "case.casetype.publication": "Ja",
                "case.casetype.price.counter": "200",
                "case.casetype.adjourn_period": "3",
                "case.casetype.identification": "Testid",
                "case.casetype.price.employee": "500",
                "case.casetype.price.telephone": "300",
                "case.casetype.principle_local": "test-2",
                "case.casetype.extension_period": "2",
                "case.casetype.initiator_source": "email",
                "case.casetype.registration_bag": "Ja",
                "case.casetype.supervisor_relation": "Test relation",
                "case.casetype.objection_and_appeal": "Nee",
                "case.casetype.text_for_publication": "Publicatietekst",
                "case.casetype.lex_silencio_positivo": "Ja",
                "case.casetype.version_date_of_creation": "2021-10-21T12:36:46.345679+00:00",
                "case.casetype.version_date_of_expiration": None,
                "case.casetype.archive_classification_code": "123",
                "case.casetype.designation_of_confidentiality": "Openbaar",
            },
        }
        return case_type_attributes_for_magic_strings

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_get_case(self, mock_version):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        subject_uuid = str(uuid4())
        advocaat_uuid = str(uuid4())
        department_uuid = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        # department entity
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )

        self.session.query().filter().one.side_effect = [
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_requestor = mock.Mock()
        mock_requestor = {
            "uuid": uuid4(),
            "type": "bedrijf",
            "role": "Aanvrager",
            "magic_string_prefix": "aanvrager",
        }
        mock_assignee = mock.MagicMock()
        mock_assignee = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Behandelaar",
            "magic_string_prefix": "behandelaar",
        }
        mock_coordinator = mock.MagicMock()
        mock_coordinator = {
            "uuid": subject_uuid,
            "type": "medewerker",
            "role": "Coordinator",
            "magic_string_prefix": "coordinator",
        }
        mock_advocaat = mock.MagicMock()
        mock_advocaat = {
            "uuid": advocaat_uuid,
            "type": "natuurlijk_persoon",
            "role": "Advocaat",
            "magic_string_prefix": "advocaat",
        }
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": subject_uuid,
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "type": "person",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": "M",
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": None,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": None,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        correspondence_info = {
            "correspondentie_woonplaats": "Amsterdam",
            "correspondentie_straatnaam": "Overamstel",
            "correspondentie_postcode": "1234ER",
            "correspondentie_adres_buitenland1": None,
            "correspondentie_adres_buitenland2": None,
            "correspondentie_adres_buitenland3": None,
            "correspondentie_landcode": 6030,
            "correspondentie_huisnummer": 50,
            "correspondentie_huisletter": "M",
            "correspondentie_huisnummertoevoeging": "SS",
        }
        org = mock.MagicMock()
        org = {
            "uuid": uuid4(),
            "id": "3",
            "object_type": None,
            "handelsnaam": "test company",
            "email": "test@test.nl",
            "telefoonnummer": "1234456",
            "werkzamepersonen": None,
            "dossiernummer": "1234",
            "subdossiernummer": None,
            "hoofdvestiging_dossiernummer": None,
            "hoofdvestiging_subdossiernummer": None,
            "vorig_dossiernummer": None,
            "vorig_subdossiernummer": None,
            "rechtsvorm": "102",
            "kamernummer": None,
            "faillisement": None,
            "surseance": None,
            "contact_naam": None,
            "contact_aanspreektitel": None,
            "contact_voorvoegsel": None,
            "contact_voorletters": None,
            "contact_geslachtsnaam": None,
            "contact_geslachtsaanduiding": None,
            "vestiging_adres": None,
            "vestiging_straatnaam": None,
            "vestiging_huisnummer": None,
            "vestiging_huisnummertoevoeging": None,
            "vestiging_postcodewoonplaats": None,
            "vestiging_postcode": None,
            "vestiging_woonplaats": None,
            "hoofdactiviteitencode": None,
            "nevenactiviteitencode1": None,
            "nevenactiviteitencode2": None,
            "authenticated": None,
            "authenticatedby": None,
            "fulldossiernummer": None,
            "import_datum": None,
            "deleted_on": None,
            "verblijfsobject_id": None,
            "system_of_record": None,
            "system_of_record_id": None,
            "vestigingsnummer": None,
            "vestiging_huisletter": None,
            "vestiging_adres_buitenland1": None,
            "vestiging_adres_buitenland2": None,
            "vestiging_adres_buitenland3": None,
            "vestiging_landcode": None,
            "correspondence_info": correspondence_info,
            "contact_data": contact_data,
        }

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        case_actions = {
            "status": 1,
            "type": "case",
            "data": json.dumps(
                {
                    "id": 4,
                    "zaaktype_node_id": 6,
                    "zaaktype_status_id": 11,
                    "relatie_zaaktype_id": 2,
                    "relatie_type": "deelzaak",
                    "start_delay": "0",
                    "status": 1,
                    "kopieren_kenmerken": None,
                    "ou_id": 1,
                    "role_id": 12,
                    "automatisch_behandelen": None,
                    "required": "2",
                    "betrokkene_authorized": None,
                    "betrokkene_notify": None,
                    "betrokkene_id": "",
                    "betrokkene_role": None,
                    "betrokkene_role_set": "Advocaat",
                    "betrokkene_prefix": "",
                    "eigenaar_type": "aanvrager",
                    "eigenaar_role": "Advocaat",
                    "eigenaar_id": "",
                    "show_in_pip": False,
                    "pip_label": None,
                    "created": "2019-09-16T13:23:59.745873 +00:00",
                    "last_modified": "2019-09-16T13:23:59.745873 +00:00",
                }
            ),
            "automatic": True,
        }

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2019, 2, 1),
                "last_modified": date(2020, 1, 1),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": date(2020, 1, 1),
            },
            "case_actions": [case_actions],
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "user_is_admin": False,
            "user_is_requestor": True,
            "user_is_assignee": True,
            "user_is_coordinator": True,
            "authorizations": ["read"],
        }
        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [[mock_requestor, mock_assignee, mock_coordinator, mock_advocaat]],
            [org],
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [mock_subject],
            namedtuple("query_result", "department")(department="department"),
            [person],
            mock_case_type_node,
            [mock_case_type],
            None,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        result = self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert isinstance(result, entities.Case)
        assert result.entity_meta_authorizations == {"search", "read", "write"}

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 17

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.id, view_case_v2.uuid, view_case_v2.onderwerp, view_case_v2.route_ou, view_case_v2.route_role, view_case_v2.vernietigingsdatum, view_case_v2.archival_state, view_case_v2.status, view_case_v2.contactkanaal, view_case_v2.created, view_case_v2.registratiedatum, view_case_v2.streefafhandeldatum, view_case_v2.afhandeldatum, view_case_v2.stalled_until, view_case_v2.milestone, view_case_v2.last_modified, view_case_v2.behandelaar_gm_id, view_case_v2.coordinator_gm_id, view_case_v2.aanvrager, view_case_v2.aanvraag_trigger, view_case_v2.onderwerp_extern, view_case_v2.resultaat, view_case_v2.resultaat_id, view_case_v2.payment_amount, view_case_v2.payment_status, view_case_v2.confidentiality, view_case_v2.behandelaar, view_case_v2.coordinator, view_case_v2.urgency, view_case_v2.preset_client, view_case_v2.prefix, view_case_v2.active_selection_list, view_case_v2.case_status, view_case_v2.case_meta, view_case_v2.case_role, view_case_v2.type_of_archiving, view_case_v2.period_of_preservation, view_case_v2.result_description, view_case_v2.result_explanation, view_case_v2.result_selection_list_number, view_case_v2.result_process_type_number, view_case_v2.result_process_type_name, view_case_v2.result_process_type_description, view_case_v2.result_process_type_explanation, view_case_v2.result_process_type_generic, view_case_v2.result_origin, view_case_v2.result_process_term, view_case_v2.aggregation_scope, view_case_v2.number_parent, view_case_v2.number_master, view_case_v2.number_previous, view_case_v2.price, view_case_v2.result_process_type_object, view_case_v2.custom_fields, view_case_v2.file_custom_fields, view_case_v2.case_actions, view_case_v2.case_department, view_case_v2.case_subjects, view_case_v2.destructable, view_case_v2.suspension_rationale, view_case_v2.premature_completion_rationale, view_case_v2.days_left, view_case_v2.lead_time_real, view_case_v2.progress_days, %(param_1)s AS user_is_admin, array((SELECT case_acl.permission \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = view_case_v2.id AND case_acl.subject_uuid = %(subject_uuid_1)s GROUP BY case_acl.permission)) AS authorizations, CASE WHEN (view_case_v2.aanvrager_type = %(aanvrager_type_1)s AND view_case_v2.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s)) THEN %(param_2)s ELSE %(param_3)s END AS user_is_requestor, CASE WHEN (view_case_v2.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s)) THEN %(param_4)s ELSE %(param_5)s END AS user_is_assignee, CASE WHEN (view_case_v2.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s)) THEN %(param_6)s ELSE %(param_7)s END AS user_is_coordinator \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid = %(uuid_2)s AND view_case_v2.status != %(status_1)s AND view_case_v2.deleted IS NULL AND view_case_v2.status != %(status_2)s"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "array((SELECT json_build_object(%(json_build_object_2)s, zaak_betrokkenen.subject_id, %(json_build_object_3)s, zaak_betrokkenen.betrokkene_type, %(json_build_object_4)s, coalesce(zaak_betrokkenen.rol, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_1)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_2)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_3)s END), %(json_build_object_5)s, coalesce(zaak_betrokkenen.magic_string_prefix, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN %(param_4)s WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN %(param_5)s WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN %(param_6)s END)) AS json_build_object_1 \n"
            "FROM zaak JOIN zaak_betrokkenen ON zaak.id = zaak_betrokkenen.zaak_id AND zaak_betrokkenen.deleted IS NULL \n"
            "WHERE zaak.uuid = %(uuid_1)s))"
        )

        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT json_build_object(%(json_build_object_2)s, bedrijf.id, %(json_build_object_3)s, bedrijf.uuid, %(json_build_object_4)s, bedrijf.dossiernummer, %(json_build_object_5)s, bedrijf.handelsnaam, %(json_build_object_6)s, bedrijf.rechtsvorm, %(json_build_object_7)s, bedrijf.vestiging_straatnaam, %(json_build_object_8)s, bedrijf.vestigingsnummer, %(json_build_object_9)s, bedrijf.vestiging_huisnummer, %(json_build_object_10)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_11)s, bedrijf.vestiging_postcode, %(json_build_object_12)s, json_build_object(%(json_build_object_13)s, bedrijf.correspondentie_straatnaam, %(json_build_object_14)s, bedrijf.correspondentie_huisnummer, %(json_build_object_15)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_16)s, bedrijf.correspondentie_postcode, %(json_build_object_17)s, bedrijf.correspondentie_woonplaats, %(json_build_object_18)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_19)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_20)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_21)s, bedrijf.correspondentie_adres, %(json_build_object_22)s, bedrijf.correspondentie_postcodewoonplaats, %(json_build_object_23)s, bedrijf.correspondentie_huisletter, %(json_build_object_24)s, bedrijf.correspondentie_landcode), %(json_build_object_25)s, bedrijf.vestiging_huisletter, %(json_build_object_26)s, bedrijf.vestiging_landcode, %(json_build_object_27)s, bedrijf.subdossiernummer, %(json_build_object_28)s, bedrijf.hoofdvestiging_dossiernummer, %(json_build_object_29)s, bedrijf.hoofdvestiging_subdossiernummer, %(json_build_object_30)s, bedrijf.vorig_dossiernummer, %(json_build_object_31)s, bedrijf.vorig_subdossiernummer, %(json_build_object_32)s, bedrijf.kamernummer, %(json_build_object_33)s, bedrijf.faillisement, %(json_build_object_34)s, bedrijf.surseance, %(json_build_object_35)s, bedrijf.telefoonnummer, %(json_build_object_36)s, bedrijf.email, %(json_build_object_37)s, bedrijf.vestiging_adres, %(json_build_object_38)s, bedrijf.vestiging_postcodewoonplaats, %(json_build_object_39)s, bedrijf.vestiging_woonplaats, %(json_build_object_40)s, bedrijf.hoofdactiviteitencode, %(json_build_object_41)s, bedrijf.nevenactiviteitencode1, %(json_build_object_42)s, bedrijf.nevenactiviteitencode2, %(json_build_object_43)s, bedrijf.nevenactiviteitencode2, %(json_build_object_44)s, bedrijf.contact_naam, %(json_build_object_45)s, bedrijf.contact_aanspreektitel, %(json_build_object_46)s, bedrijf.contact_voorletters, %(json_build_object_47)s, bedrijf.contact_voorvoegsel, %(json_build_object_48)s, bedrijf.contact_geslachtsnaam, %(json_build_object_49)s, bedrijf.contact_geslachtsaanduiding, %(json_build_object_50)s, bedrijf.authenticated, %(json_build_object_51)s, bedrijf.authenticatedby, %(json_build_object_52)s, bedrijf.import_datum, %(json_build_object_53)s, bedrijf.verblijfsobject_id, %(json_build_object_54)s, bedrijf.vestigingsnummer, %(json_build_object_55)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_56)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_57)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_58)s, json_build_object(%(json_build_object_59)s, contact_data.mobiel, %(json_build_object_60)s, contact_data.telefoonnummer, %(json_build_object_61)s, contact_data.email, %(json_build_object_62)s, contact_data.note)) AS json_build_object_1 \n"
            "FROM bedrijf LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s \n"
            "WHERE bedrijf.uuid = %(uuid_1)s"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_get_case_for_secret_person_as_requestor(
        self,
        mock_version,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        # department entity
        department_uuid = uuid4()
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )

        self.session.query().filter().one.side_effect = [
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_requestor = mock.MagicMock()
        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )

        address = mock.MagicMock()
        address.configure_mock(
            straatnaam="Vrijzicht",
            huisnummer=50,
            huisnummertoevoeging="ABC",
            woonplaats="Amsterdam",
            postcode="1234ER",
            adres_buitenland1=None,
            adres_buitenland2=None,
            adres_buitenland3=None,
        )
        contact_data = mock.MagicMock()
        contact_data.configure_mock(mobiel="064535353", email="edwin@ed.com")
        person = mock.MagicMock()
        person.configure_mock(
            uuid=str(uuid4()),
            id=1,
            voornamen="Edwin",
            voorvoegsel="T",
            geslachtsnaam="Jaison",
            naamgebruik="T Jaison",
            geslachtsaanduiding="M",
            burgerservicenummer="1234567",
            geboorteplaats="Amsterdam",
            geboorteland="Amsterdam",
            geboortedatum=None,
            landcode=6030,
            active=True,
            adellijke_titel="Mr.",
            datum_huwelijk_ontbinding=None,
            onderzoek_persoon=None,
            datum_huwelijk=None,
            a_nummer=None,
            indicatie_geheim="7",
        )

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id

        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.query().outerjoin().outerjoin().filter().one.side_effect = [
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": {},
            "prefix": "1",
            "route_ou": 1,
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": "2021-10-26T09:24:06.494135",
                "last_modified": "2021-10-26T09:24:06.494135",
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "user_is_admin": True,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "manage"],
        }

        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            mock_case_type_node,
            [mock_case_type],
            None,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]

        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]
        self.session.reset_mock()

        case = self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert case.entity_meta_authorizations == {
            "search",
            "read",
            "write",
            "manage",
        }

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_get_case_for_person_as_requestor(
        self,
        mock_version,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        # department entity
        department_uuid = uuid4()
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )
        mock_requestor = mock.MagicMock()
        subject_uuid = str(uuid4())
        person_uuid = str(uuid4())
        mock_requestor.configure_mock(
            uuid=person_uuid,
            type="natuurlijk_persoon",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )
        address = mock.MagicMock()
        address.configure_mock(
            straatnaam="Vrijzicht",
            huisnummer=50,
            huisnummertoevoeging="ABC",
            woonplaats="Amsterdam",
            postcode="1234ER",
            adres_buitenland1=None,
            adres_buitenland2=None,
            adres_buitenland3=None,
        )
        contact_data = mock.MagicMock()
        contact_data.configure_mock(mobiel="064535353", email="edwin@ed.com")
        person = mock.MagicMock()
        person.configure_mock(
            uuid=str(uuid4()),
            id=1,
            voornamen="Edwin",
            voorvoegsel="T",
            geslachtsnaam="Jaison",
            naamgebruik="T Jaison",
            geslachtsaanduiding="M",
            burgerservicenummer="1234567",
            geboorteplaats="Amsterdam",
            geboorteland="Amsterdam",
            geboortedatum=None,
            landcode=6030,
            active=True,
            adellijke_titel="Mr.",
            datum_huwelijk_ontbinding=None,
            onderzoek_persoon=None,
            datum_huwelijk=None,
            a_nummer=None,
            indicatie_geheim=None,
            address=address,
            contact_data=contact_data,
        )

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )
        self.session.query().outerjoin().outerjoin().filter().one.side_effect = [
            namedtuple("query_result", "NatuurlijkPersoon Adres ContactData")(
                person, address, contact_data
            ),
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": {},
            "prefix": "1",
            "route_ou": 1,
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": "2021-10-26T09:24:06.494135",
                "last_modified": "2021-10-26T09:24:06.494135",
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            mock_case_type_node,
            [mock_case_type],
            None,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]
        self.session.reset_mock()
        self.qry.get_case_by_uuid(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_get_case_for_organization_as_requestor(
        self,
        mock_version,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()

        mock_case_type_node = mock.MagicMock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )
        # department entity
        department_uuid = uuid4()
        parent_department = mock.MagicMock()
        parent_uuid = uuid4()
        parent_department.configure_mock(
            uuid=parent_uuid,
            name="Parent",
            description="A parentgroup",
            parent_uuid=None,
            parent_name=None,
        )

        mock_role = mock.MagicMock()
        role_uuid = uuid4()

        mock_role.configure_mock(
            uuid=role_uuid,
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )
        mock_requestor = mock.MagicMock()
        subject_uuid = str(uuid4())
        org_uuid = str(uuid4())
        mock_requestor.configure_mock(
            uuid=org_uuid,
            type="bedrijf",
            role="Aanvrager",
            magic_string_prefix="aanvrager",
        )
        mock_assignee = mock.MagicMock()
        mock_assignee.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Behandelaar",
            magic_string_prefix="behandelaar",
        )
        mock_coordinator = mock.MagicMock()
        mock_coordinator.configure_mock(
            uuid=subject_uuid,
            type="medewerker",
            role="Coordinator",
            magic_string_prefix="coordinator",
        )
        contact_data = mock.MagicMock()
        contact_data.configure_mock(mobiel="064535353", email="edwin@ed.com")
        org = mock.MagicMock()
        org.configure_mock(
            uuid=org_uuid,
            id=1,
            handelsnaam="Mintlab",
            correspondentie_woonplaats="Amsterdam",
            correspondentie_straatnaam="Overamstel",
            correspondentie_postcode="1234ER",
            vestiging_straatnaam="Overamstel",
            correspondentie_huisnummer=50,
            correspondentie_huisletter="M",
            correspondentie_huisnummertoevoeging="SS",
            vestiging_huisnummer=23,
            vestiging_huisletter="P",
            vestiging_huisnummertoevoeging="MM",
            vestiging_postcode="1234ER",
            correspondentie_adres_buitenland1=None,
            correspondentie_adres_buitenland2=None,
            correspondentie_adres_buitenland3=None,
            dossiernummer=50,
            rechtsvorm="Internet banking",
            vestigingsnummer=3,
            correspondentie_landcode=6030,
            vestiging_landcode=6030,
            contact_data=contact_data,
        )

        subject_id = str(uuid4())
        subject = mock.MagicMock()
        subject.betrokkene_type = "medewerker"
        subject.rol = "advocaat"
        subject.subject_id = subject_id
        case_type_attributes_for_magic_strings = (
            self.__mock_case_type_attributes(case_type_uuid)
        )

        self.session.query().filter().outerjoin().one.side_effect = [
            namedtuple("query_result", "Bedrijf ContactData")(
                org, contact_data
            ),
            namedtuple("Department", "department")(department="Development"),
        ]

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "case_department": {
                "uuid": str(uuid4()),
                "name": "Backoffice",
                "description": "Default backoffice",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_role": {
                "uuid": str(uuid4()),
                "name": "Behandelaar",
                "description": "Systeemrol: Behandelaar",
                "parent_uuid": str(uuid4()),
                "parent_name": "Development",
            },
            "case_subjects": None,
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": {},
            "prefix": "1",
            "route_ou": 1,
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": "2021-10-26T09:24:06.494135",
                "last_modified": "2021-10-26T09:24:06.494135",
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [mock_requestor, mock_assignee, mock_coordinator],
            mock_case_type_node,
            [mock_case_type],
            None,
            [case_type_attributes_for_magic_strings],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
            parent_department,
            mock_role,
            mock_role,
            None,
            parent_department,
            None,
            None,
        ]
        mock_related_case_1 = mock.MagicMock()

        mock_related_case_1.case_a_id = 10
        mock_related_case_1.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "parent"
        mock_related_case_2 = mock.MagicMock()
        mock_related_case_2.case_a_id = 11
        mock_related_case_2.case_a_uuid = uuid4()
        mock_related_case_1.case_type = "child"
        mock_file = mock.MagicMock()
        mock_file.configure_mock(
            name="testdoc",
            extension=".odt",
            status="Original",
            case_document_id=101,
        )
        self.session.execute().fetchall.side_effect = [
            [mock_related_case_1, mock_related_case_2],
            [mock_related_case_1],
            [mock_related_case_1],
            [mock_file],
            [subject],
        ]

        self.session.reset_mock()

        self.qry.get_case_by_uuid(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 11

        assert call_list[1][0][0].__class__.__name__ == "Function"
        assert call_list[2][0][0].__class__.__name__ == "Select"
        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_get_case_requestor_employee_not_found(
        self,
        mock_version,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        role_id = 10
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        subject_uuid = str(uuid4())
        subject = [
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Aanvrager",
                "magic_string_prefix": "aanvrager",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Behandelaar",
                "magic_string_prefix": "behandelaar",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Coordinator",
                "magic_string_prefix": "coordinator",
            },
        ]

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "department": None,
            "role": "mock.Mock()",
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": {},
            "prefix": "1",
            "route_ou": 1,
            "route_role": role_id,
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": "2021-10-26T09:24:06.494135",
                "last_modified": "2021-10-26T09:24:06.494135",
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [subject],
            None,
            None,
            None,
            mock_case_type_node,
            [mock_case_type],
            namedtuple("query_result", "case_location")(
                case_location="H.J.E. Wenckebachweg 90, 1114AD Amsterdam-Duivendrecht"
            ),
        ]

        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert excinfo.value.args == (
            f"Employee with uuid '{subject_uuid}' not found.",
            "employee/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_get_case_requestor_person_not_found(
        self,
        mock_version,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        role_id = 10
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        subject_uuid = str(uuid4())
        subject = [
            {
                "uuid": subject_uuid,
                "type": "natuurlijk_persoon",
                "role": "Aanvrager",
                "magic_string_prefix": "natuurlijk_persoon",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Behandelaar",
                "magic_string_prefix": "behandelaar",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Coordinator",
                "magic_string_prefix": "coordinator",
            },
        ]

        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "department": None,
            "role": "mock.Mock()",
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": {},
            "prefix": "1",
            "route_ou": 1,
            "route_role": role_id,
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": "2021-10-26T09:24:06.494135",
                "last_modified": "2021-10-26T09:24:06.494135",
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "case_location": "Test locataion",
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [subject],
            None,
            None,
            None,
            mock_case_type_node,
            [mock_case_type],
        ]

        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert excinfo.value.args == (
            f"Person with uuid '{subject_uuid}' not found.",
            "natuurlijk_persoon/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.get_case_type_version_uuid"
    )
    def test_get_case_requestor_organization_not_found(
        self,
        mock_version,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = uuid4()
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        role_id = 10
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=40, uuid=uuid4())
        mock_version.return_value = uuid4()
        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            custom_fields=[
                {
                    "uuid": uuid4(),
                    "name": "custom_field_1",
                    "field_magic_string": "test_subject",
                    "field_type": "text",
                },
                {
                    "uuid": type_uuid_1,
                    "name": "custom_field_2",
                    "field_magic_string": "object_relation_1",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
                {
                    "uuid": type_uuid_2,
                    "name": "custom_field_3",
                    "field_magic_string": "object_relation_2",
                    "field_type": "relationship",
                    "relationship_type": "custom_object",
                },
            ],
        )

        subject_uuid = str(uuid4())
        subject = [
            {
                "uuid": subject_uuid,
                "type": "bedrijf",
                "role": "Aanvrager",
                "magic_string_prefix": "bedrijf",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Behandelaar",
                "magic_string_prefix": "behandelaar",
            },
            {
                "uuid": subject_uuid,
                "type": "medewerker",
                "role": "Coordinator",
                "magic_string_prefix": "coordinator",
            },
        ]
        mock_case = mock.MagicMock()
        mock_case = {
            "id": 16,
            "uuid": case_uuid,
            "department": None,
            "role": "mock.Mock()",
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": {},
            "prefix": "1",
            "route_ou": 1,
            "route_role": role_id,
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "milestone": 1,
            "case_status": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": "2021-10-26T09:24:06.494135",
                "last_modified": "2021-10-26T09:24:06.494135",
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
            "destructable": False,
            "aggregation_scope": "Dossier",
            "number_parent": None,
            "number_master": 737,
            "relations": ",738",
            "number_previous": None,
            "premature_completion_rationale": None,
            "price": 100,
            "type_of_archiving": "Bewaren (B)",
            "period_of_preservation": "4 weken",
            "suspension_rationale": None,
            "result_description": "closing the case",
            "result_explanation": "closing the case",
            "result_selection_list_number": "10",
            "result_process_type_number": "11",
            "result_process_type_name": "test-process-type",
            "result_process_type_description": "This is test process type",
            "result_process_type_explanation": "This is test process type",
            "result_process_type_object": "Procestype-object",
            "result_process_type_generic": "Generiek",
            "result_origin": "Systeemanalyse",
            "result_process_term": "A",
            "active_selection_list": "test list",
            "days_left": 10,
            "lead_time_real": None,
            "progress_days": 10,
            "case_location": "Test locataion",
            "user_is_admin": False,
            "user_is_requestor": False,
            "user_is_assignee": False,
            "user_is_coordinator": False,
            "authorizations": ["read", "write"],
        }
        self.session.execute().fetchone.side_effect = [
            mock_case,
            [subject],
            None,
            None,
            None,
            mock_case_type_node,
            [mock_case_type],
        ]

        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_case_by_uuid(case_uuid=case_uuid)

        assert excinfo.value.args == (
            f"Organization with uuid '{subject_uuid}' not found.",
            "organization/not_found",
        )


class Test_AsUser_transition_Case(TestBase):
    def setup(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, custom_fields, rules=[]):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": custom_fields,
                "checklist_items": [],
                "rules": rules,
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"

        return mock_case_type

    def __mock_case_type_attributes(self, case_type_uuid):
        case_type_attributes_for_magic_strings = mock.MagicMock()
        case_type_attributes_for_magic_strings = {
            "id": 1,
            "uuid": case_type_uuid,
            "case_type_json": {
                "case.casetype.id": 3,
                "case.casetype.goal": "Target test",
                "case.casetype.name": "test_case_type ",
                "case.casetype.wkpb": "Ja",
                "case.casetype.eform": "e-form-test",
                "case.casetype.node.id": 143,
                "case.casetype.penalty": "Ja",
                "case.casetype.version": 72,
                "case.casetype.keywords": "test keyword",
                "case.casetype.extension": "Ja",
                "case.casetype.motivation": "Reason is testing",
                "case.casetype.price.post": "600",
                "case.casetype.supervisor": "Test supervisor",
                "case.casetype.suspension": "Ja",
                "case.casetype.description": "test case type",
                "case.casetype.price.email": "400",
                "case.casetype.publication": "Ja",
                "case.casetype.price.counter": "200",
                "case.casetype.adjourn_period": "3",
                "case.casetype.identification": "Testid",
                "case.casetype.price.employee": "500",
                "case.casetype.price.telephone": "300",
                "case.casetype.principle_local": "test-2",
                "case.casetype.extension_period": "2",
                "case.casetype.initiator_source": "email",
                "case.casetype.registration_bag": "Ja",
                "case.casetype.supervisor_relation": "Test relation",
                "case.casetype.objection_and_appeal": "Nee",
                "case.casetype.text_for_publication": "Publicatietekst",
                "case.casetype.lex_silencio_positivo": "Ja",
                "case.casetype.version_date_of_creation": "2021-10-21T12:36:46.345679+00:00",
                "case.casetype.version_date_of_expiration": None,
                "case.casetype.archive_classification_code": "123",
                "case.casetype.designation_of_confidentiality": "Openbaar",
            },
        }
        return case_type_attributes_for_magic_strings


class Test_AsUser_set_result(TestBase):
    def setup(self):
        self.load_command_instance(case_management)

        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )
        self.cmd.user_info = self.user_info

    def _compile_query(self, query) -> str:
        return str(query.compile(dialect=postgresql.dialect()))

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_not_found(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())

        mock_find_case.side_effect = NotFound("")

        with pytest.raises(NotFound):
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_type_result_uuid_not_found(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        mock_find_case.return_value = case
        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as exception:
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )
        assert (
            exception.value.args[0]
            == f"case_type_result not found {case_type_result_uuid} for case_uuid {case_uuid}"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.destruction_date == date(2019, 11, 22)
        assert case.result == entities.CaseResult(
            result="aangegaan", result_id=1, archival_attributes=None
        )
        assert case.archival_state == "vernietigen"

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 5

        assert self._compile_query(call_list[0][0][0]) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s AND zaak_1.status != %(status_1)s AND zaaktype_resultaten_1.uuid = %(uuid_2)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s"
        )

        assert self._compile_query(call_list[1][0][0]) == (
            "SELECT zaaktype_resultaten.id \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.uuid = %(uuid_1)s"
        )

        assert (
            self._compile_query(call_list[2][0][0])
            == "UPDATE zaak SET vernietigingsdatum=%(vernietigingsdatum)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s"
        )

        assert (
            self._compile_query(call_list[3][0][0])
            == "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s"
        )

        assert (
            self._compile_query(call_list[4][0][0])
            == "UPDATE zaak SET resultaat=%(resultaat)s, resultaat_id=%(resultaat_id)s WHERE zaak.uuid = %(uuid_1)s"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_destruction_date_month(
        self, mock_find_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 maanden",
            preservation_term_unit="month",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.destruction_date == date(2020, 2, 25)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_destruction_date_year(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 jaar",
            preservation_term_unit="year",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.destruction_date == date(2023, 10, 25)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_destruction_invalid_unit(
        self, mock_find_case
    ):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 decade",
            preservation_term_unit="decade",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        with pytest.raises(NotImplementedError):
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_trigger_active(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        case.destruction_date = date(2022, 3, 1)
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=True,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.destruction_date is None
        assert case.result == entities.CaseResult(
            result="aangegaan", result_id=1, archival_attributes=None
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        assert self._compile_query(call_list[0][0][0]) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s AND zaak_1.status != %(status_1)s AND zaaktype_resultaten_1.uuid = %(uuid_2)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s"
        )

        assert self._compile_query(call_list[1][0][0]) == (
            "SELECT zaaktype_resultaten.id \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.uuid = %(uuid_1)s"
        )

        assert (
            self._compile_query(call_list[2][0][0])
            == "UPDATE zaak SET vernietigingsdatum=%(vernietigingsdatum)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s"
        )

        assert (
            self._compile_query(call_list[3][0][0])
            == "UPDATE zaak SET resultaat=%(resultaat)s, resultaat_id=%(resultaat_id)s WHERE zaak.uuid = %(uuid_1)s"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_no_completion_date(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        case.completion_date = None
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid, case_type_result_uuid=case_type_result_uuid
        )

        assert case.result == entities.CaseResult(
            result="aangegaan", result_id=1, archival_attributes=None
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        assert self._compile_query(call_list[0][0][0]) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s AND zaak_1.status != %(status_1)s AND zaaktype_resultaten_1.uuid = %(uuid_2)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s"
        )

        assert self._compile_query(call_list[1][0][0]) == (
            "SELECT zaaktype_resultaten.id \n"
            "FROM zaaktype_resultaten \n"
            "WHERE zaaktype_resultaten.uuid = %(uuid_1)s"
        )

        assert (
            self._compile_query(call_list[2][0][0])
            == "UPDATE zaak SET archival_state=%(archival_state)s, last_modified=now() WHERE zaak.uuid = %(uuid_1)s"
        )

        assert (
            self._compile_query(call_list[3][0][0])
            == "UPDATE zaak SET resultaat=%(resultaat)s, resultaat_id=%(resultaat_id)s WHERE zaak.uuid = %(uuid_1)s"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_case_status_invalid(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        case.status = "resolved"
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        with pytest.raises(Conflict):
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_trigger_invalid_unit(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="Bewaren",
            preservation_term_unit=None,
            preservation_term_unit_amount=None,
        )

        self.session.execute().fetchone.return_value = mock_result_type
        self.session.reset_mock()

        self.cmd.set_case_result(
            case_uuid=case_uuid,
            case_type_result_uuid=case_type_result_uuid,
        )

        # assert no changes on desctruction_date due to invalue perservation_term_unit
        assert case.destruction_date == date(2019, 1, 1)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_set_result_no_result_id(self, mock_find_case):
        case_uuid = str(uuid4())
        case_type_result_uuid = str(uuid4())
        case = entity_util.create_case(
            event_service=self.cmd.event_service, uuid=UUID(case_uuid)
        )
        mock_find_case.return_value = case

        mock_result_type = mock.MagicMock()
        mock_result_type.configure_mock(
            uuid=case_type_result_uuid,
            name="What a great result",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="Bewaren",
            preservation_term_unit=None,
            preservation_term_unit_amount=None,
        )

        self.session.execute().fetchone.side_effect = [mock_result_type, None]
        self.session.reset_mock()

        with pytest.raises(NotFound):
            self.cmd.set_case_result(
                case_uuid=case_uuid,
                case_type_result_uuid=case_type_result_uuid,
            )
