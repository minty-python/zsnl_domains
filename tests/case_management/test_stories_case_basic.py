# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions
import pytest
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_Case_Basic_Queries(TestBase):
    def setup(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_get_case_basic(self):
        case_uuid = str(uuid4())

        mock_db_row = mock.Mock(
            id=3,
            uuid=uuid4(),
            custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring1",
                    "value": ["some value"],
                    "type": "text",
                },
                {
                    "name": "some json type",
                    "magic_string": "magicstring3",
                    "value": ['{"json": "JSON!"}'],
                    "type": "geojson",
                },
                {
                    "name": "some bad json",
                    "magic_string": "magicstring4",
                    "value": ["definitely not JSON"],
                    "type": "geojson",
                },
            ],
            file_custom_fields=[
                {
                    "name": "some name",
                    "magic_string": "magicstring2",
                    "type": "file",
                    "value": [
                        {
                            "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                            "size": 3,
                            "uuid": uuid4(),
                            "filename": "hello.txt",
                            "mimetype": "text/plain",
                            "is_archivable": True,
                            "original_name": "hello.txt",
                            "thumbnail_uuid": uuid4(),
                        },
                    ],
                }
            ],
            case_type_uuid=uuid4(),
            case_type_version_uuid=uuid4(),
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        case_basic = self.qry.get_case_basic_by_uuid(case_uuid=case_uuid)

        assert case_basic.entity_id == case_basic.uuid == mock_db_row.uuid
        assert case_basic.number == 3
        assert case_basic.case_type_uuid == mock_db_row.case_type_uuid
        assert (
            case_basic.case_type_version_uuid
            == mock_db_row.case_type_version_uuid
        )
        assert case_basic.custom_fields == {
            "magicstring1": {"type": "text", "value": ["some value"]},
            "magicstring2": {
                "type": "file",
                "value": [mock_db_row.file_custom_fields[0]["value"][0]],
            },
            "magicstring3": {"type": "geojson", "value": {"json": "JSON!"}},
            "magicstring4": {"type": "geojson", "value": {}},
        }

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_7)s, bibliotheek_kenmerken.naam, %(json_build_object_8)s, bibliotheek_kenmerken.magic_string, %(json_build_object_9)s, bibliotheek_kenmerken.value_type, %(json_build_object_10)s, array((SELECT json_build_object(%(json_build_object_12)s, filestore.md5, %(json_build_object_13)s, filestore.size, %(json_build_object_14)s, filestore.uuid, %(json_build_object_15)s, file.name || file.extension, %(json_build_object_16)s, filestore.mimetype, %(json_build_object_17)s, filestore.is_archivable, %(json_build_object_18)s, filestore.original_name, %(json_build_object_19)s, filestore.thumbnail_uuid) AS json_build_object_11 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_6 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype.uuid AS case_type_uuid, zaaktype_node.uuid AS case_type_version_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id \n"
            "WHERE zaak_1.uuid = %(uuid_1)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s)))"
        )

    def test_get_case_basic_notfound(self):
        case_uuid = str(uuid4())

        mock_db_row = None

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.get_case_basic_by_uuid(case_uuid=case_uuid)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_7)s, bibliotheek_kenmerken.naam, %(json_build_object_8)s, bibliotheek_kenmerken.magic_string, %(json_build_object_9)s, bibliotheek_kenmerken.value_type, %(json_build_object_10)s, array((SELECT json_build_object(%(json_build_object_12)s, filestore.md5, %(json_build_object_13)s, filestore.size, %(json_build_object_14)s, filestore.uuid, %(json_build_object_15)s, file.name || file.extension, %(json_build_object_16)s, filestore.mimetype, %(json_build_object_17)s, filestore.is_archivable, %(json_build_object_18)s, filestore.original_name, %(json_build_object_19)s, filestore.thumbnail_uuid) AS json_build_object_11 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_6 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype.uuid AS case_type_uuid, zaaktype_node.uuid AS case_type_version_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id \n"
            "WHERE zaak_1.uuid = %(uuid_1)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s)))"
        )
