# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import copy
import pytest
from .test_case_management_repositories import InfraFactoryMock
from collections import namedtuple
from datetime import date
from minty import cqrs
from minty import exceptions as minty_exc
from sqlalchemy import dialects
from sqlalchemy import exc as sql_exc
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.case_management import entities
from zsnl_domains.case_management.entities import Department
from zsnl_domains.case_management.repositories.task import (
    TaskRepository,
    task_query,
)
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories import case_acl


class TestTaskRepository:
    @mock.patch("sqlalchemy.orm.Session")
    def setup(self, mock_session):
        self.session = mock_session
        infra = InfraFactoryMock(infra={"database": self.session})
        self.task_repo = TaskRepository(
            infrastructure_factory=infra,
            context=None,
            event_service=mock.MagicMock(),
        )
        self.event = cqrs.Event(
            uuid=uuid4(),
            created_date="2019-12-11 20:45:15",
            correlation_id=uuid4(),
            domain="case_management",
            context="localhost.testing",
            user_uuid=uuid4(),
            entity_type="Task",
            entity_id=uuid4(),
            event_name="TaskCreated",
            changes={},
            entity_data={},
        )
        self.case = entities.Case(
            id=1,
            uuid=uuid4(),
            department=Department(
                uuid=uuid4(), name="Dept", description="Dept"
            ),
            role=None,
            destruction_date=date(2019, 7, 8),
            archival_state=False,
            status="open",
            created_date=date(2019, 12, 11),
            registration_date=None,
            target_completion_date=None,
            completion=None,
            completion_date=None,
            stalled_until_date=None,
            milestone=2,
            suspension_reason=None,
            stalled_since_date=None,
            last_modified_date=None,
            coordinator=None,
            assignee=None,
        )
        self.task = entities.Task(
            uuid=uuid4(),
            title="some title",
            description="some description",
            case={
                "uuid": uuid4(),
                "milestone": 2,
                "status": "open",
                "id": 448,
            },
            case_type=None,
            due_date=None,
            completed=False,
            user_defined=True,
            assignee=None,
            phase=2,
            department=None,
        )

        self.assignee = entities.Employee(
            uuid=uuid4(),
            id=2,
            type="employee",
            first_name="Admin",
            source="test",
            status="active",
            name="Admin",
            surname="",
            department={"uuid": uuid4(), "name": "dept"},
            roles=[],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )

        self.department = entities.Department(
            uuid=uuid4(),
            name="Backoffice",
        )

    def test_create_task(self):
        case = copy.deepcopy(self.case)
        title = "new title"
        task_uuid = uuid4()
        phase = 23
        task = self.task_repo.create_task(
            case=case, title=title, uuid=task_uuid, phase=phase
        )
        assert task.title == title
        assert task.phase == phase
        assert task.uuid == task_uuid
        assert task.case == {
            "uuid": case.uuid,
            "milestone": case.milestone,
            "status": case.status,
            "id": case.id,
        }
        task.event_service is self.task_repo.event_service

    def test_get_task(self):
        task_uuid = str(uuid4())
        permission = "read"
        user_uuid = str(uuid4())
        self.session.reset_mock()
        self.task_repo.get_task(
            task_uuid=task_uuid, permission=permission, user_uuid=user_uuid
        )
        where_clause = sql.and_(
            schema.ChecklistItem.uuid == task_uuid,
            case_acl.allowed_cases_subquery(
                db=self.session, user_uuid=user_uuid, permission=permission
            ),
        )
        stmt = task_query
        # out of 3 calls to session.execute we need to assert the 2th
        assert str(self.session.execute.call_args_list[1][0][0]) == str(
            stmt.where(where_clause)
        )

    def test_get_task_forbidden(self):
        task_uuid = str(uuid4())
        permission = "read"
        user_uuid = str(uuid4())
        self.session.execute().fetchone.return_value = None
        with pytest.raises(minty_exc.Forbidden):
            self.task_repo.get_task(
                task_uuid=task_uuid, permission=permission, user_uuid=user_uuid
            )

    def test_get_task_list_simple(self):
        user_uuid = uuid4()

        stmt = task_query
        where_clause = sql.and_(
            case_acl.allowed_cases_subquery(
                db=self.task_repo.session,
                user_uuid=user_uuid,
                permission="read",
            ),
        )
        page = 1
        page_size = 10
        self.task_repo.get_task_list(
            user_uuid=user_uuid, page=page, page_size=page_size
        )
        offset = (page * page_size) - page_size
        assert str(self.session.execute.call_args_list[2][0][0]) == str(
            stmt.where(where_clause)
            .order_by(
                schema.Checklist.case_milestone,
                schema.ChecklistItem.sequence,
                schema.ChecklistItem.id,
            )
            .limit(page_size)
            .offset(offset)
        )

    def test_get_task_list_full(self):
        case_uuid = uuid4()
        case_type_uuid = uuid4()
        assignee_uuid = uuid4()
        user_uuid = uuid4()
        department_uuid = uuid4()
        phase = 2
        state = True

        stmt = task_query
        where_clause = sql.and_(
            case_acl.allowed_cases_subquery(
                db=self.task_repo.session,
                user_uuid=user_uuid,
                permission="read",
            ),
            schema.Case.uuid == case_uuid,
            schema.Zaaktype.uuid == case_type_uuid,
            schema.Group.uuid == department_uuid,
            schema.Checklist.case_milestone == phase,
            schema.ChecklistItem.state == state,
        )
        page = 1
        page_size = 10
        offset = (page * page_size) - page_size

        self.task_repo.get_task_list(
            user_uuid=user_uuid,
            assignee_uuids=[assignee_uuid],
            case_uuids=[case_uuid],
            case_ids=[1, 2, 3],
            case_type_uuids=[case_type_uuid],
            department_uuids=[department_uuid],
            title_words=["wo%rd", "second_word"],
            keyword="%with_escapes~",
            phase=phase,
            completed=state,
            page=page,
            page_size=page_size,
            sort="-attributes.due_date",
        )

        call_list = self.session.execute.call_args_list

        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak.uuid AS case_uuid, zaak.id AS case_id, zaak.milestone AS case_milestone, zaak.uuid AS case_status, checklist.case_milestone AS phase, checklist_item.uuid, checklist_item.label, checklist_item.description, checklist_item.due_date, checklist_item.state, checklist_item.user_defined, subject.uuid AS assignee_uuid, subject.properties AS assignee_properties, groups.name AS department_name, groups.uuid AS department_uuid, zaaktype_node.titel AS case_type_title, zaaktype.uuid AS case_type_uuid \n"
            "FROM checklist JOIN checklist_item ON checklist.id = checklist_item.checklist_id JOIN zaak ON zaak.id = checklist.case_id JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id LEFT OUTER JOIN subject ON checklist_item.assignee_id = subject.id JOIN groups ON zaak.route_ou = groups.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.uuid IN (%(uuid_2_1)s) AND zaak.id IN (%(id_1_1)s, %(id_1_2)s, %(id_1_3)s) AND zaaktype.uuid IN (%(uuid_3_1)s) AND subject.uuid IN (%(uuid_4_1)s) AND groups.uuid IN (%(uuid_5_1)s) AND (checklist_item.label ILIKE %(label_1)s ESCAPE '~' OR checklist_item.label ILIKE %(label_2)s ESCAPE '~') AND checklist_item.label ILIKE %(label_3)s ESCAPE '~' AND checklist.case_milestone = %(case_milestone_1)s AND checklist_item.state = true ORDER BY checklist_item.due_date DESC, checklist.case_milestone, checklist_item.sequence, checklist_item.id \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
        assert compiled_select.params["label_1"] == "%wo~%rd%"
        assert compiled_select.params["label_2"] == "%second~_word%"
        assert compiled_select.params["label_3"] == "%~%with~_escapes~~%"

        self.session.reset_mock()
        self.task_repo.get_task_list(
            user_uuid=user_uuid,
            case_uuids=[case_uuid],
            case_type_uuids=[case_type_uuid],
            department_uuids=[department_uuid],
            phase=phase,
            assignee_uuids=[assignee_uuid],
            page=page,
            page_size=page_size,
        )

        where_clause = sql.and_(
            case_acl.allowed_cases_subquery(
                db=self.task_repo.session,
                user_uuid=user_uuid,
                permission="read",
            ),
            schema.Case.uuid.in_([case_uuid]),
            schema.Zaaktype.uuid.in_([case_type_uuid]),
            schema.Subject.uuid.in_([assignee_uuid]),
            schema.Group.uuid.in_([department_uuid]),
            schema.Checklist.case_milestone == phase,
        )
        assert str(self.session.execute.call_args_list[1][0][0]) == str(
            stmt.where(where_clause)
            .order_by(
                schema.Checklist.case_milestone,
                schema.ChecklistItem.sequence,
                schema.ChecklistItem.id,
            )
            .limit(page_size)
            .offset(offset)
        )

    def test__transform_to_entity(self):
        result_row = namedtuple(
            "Result",
            [
                "case_uuid",
                "case_status",
                "case_milestone",
                "case_id",
                "uuid",
                "label",
                "description",
                "due_date",
                "state",
                "user_defined",
                "phase",
                "assignee_uuid",
                "assignee_properties",
                "department_name",
                "department_uuid",
                "case_type_uuid",
                "case_type_title",
            ],
        )

        result = result_row(
            case_id=23,
            case_uuid=uuid4(),
            case_status="open",
            case_milestone=2,
            case_type_uuid=uuid4(),
            case_type_title="Case Type Title",
            assignee_uuid=None,
            assignee_properties=None,
            uuid=uuid4(),
            label="some_title",
            description="some descr",
            due_date="2019-12-11",
            state=False,
            user_defined=True,
            phase=2,
            department_name="Backoffice",
            department_uuid=uuid4(),
        )
        task = self.task_repo._transform_to_entity(row=result)
        assert task.uuid == result.uuid
        assert task.title == result.label
        assert task.description == result.description
        assert task.due_date == result.due_date
        assert task.completed == result.state
        assert task.user_defined == result.user_defined
        assert task.assignee is None
        assert task.phase == result.phase
        assert task.case == {
            "uuid": result.case_uuid,
            "milestone": result.case_milestone,
            "status": result.case_status,
            "id": result.case_id,
        }
        assert task.department == {
            "type": "department",
            "uuid": result.department_uuid,
            "display_name": result.department_name,
        }

    def test__extract_assignee(self):
        result_row = namedtuple("Result", "assignee_uuid assignee_properties")
        result = result_row(assignee_uuid=None, assignee_properties=None)
        assignee = self.task_repo._extract_assignee(result)
        assert assignee is None
        result = result_row(
            assignee_uuid=uuid4(),
            assignee_properties={
                "displayname": "AD Min",
                "firstname": "Ad",
                "Lastname": "lastname",
            },
        )
        assignee = self.task_repo._extract_assignee(result)
        assert assignee == {
            "uuid": result.assignee_uuid,
            "type": "employee",
            "display_name": "AD Min",
        }

    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._insert_task"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._delete_task"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._update_task"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._set_completion"
    )
    def test_save(
        self,
        _set_completion_mock,
        _update_task_mock,
        _delete_task_mock,
        _insert_task_mock,
    ):
        event = copy.deepcopy(self.event)
        event.event_name = "TaskCreated"
        self.task_repo.event_service.get_events_by_type.return_value = [event]
        self.task_repo.save()
        _insert_task_mock.assert_called_once()

        event.event_name = "TaskCompletionSet"
        self.task_repo.save()
        _set_completion_mock.assert_called_once()

        event.event_name = "TaskUpdated"
        self.task_repo.save()
        _update_task_mock.assert_called_once()

        event.event_name = "TaskDeleted"
        self.task_repo.save()
        _delete_task_mock.assert_called_once()

    def test__set_completion(self):
        event = copy.deepcopy(self.event)
        event.changes = [
            {"key": "completed", "old_value": False, "new_value": True}
        ]
        self.task_repo._set_completion(event=event)
        values = {"state": True}
        update_stmt = (
            sql.update(schema.ChecklistItem)
            .where(schema.ChecklistItem.uuid == event.entity_id)
            .values(values)
        )
        self.session.execute.assert_called()
        assert str(self.session.execute.call_args_list[0][0][0]) == str(
            update_stmt
        )

    def test__update_task(self):
        event = copy.deepcopy(self.event)
        event.changes = [
            {"key": "title", "old_value": None, "new_value": "title"},
            {"key": "description", "old_value": None, "new_value": "desc"},
            {"key": "due_date", "old_value": None, "new_value": "2019-12-11"},
            {"key": "assignee", "old_value": None, "new_value": None},
        ]
        self.task_repo._update_task(event=event)
        self.session.execute.assert_called()
        assignee_uuid = str(uuid4())
        event.changes = [
            {"key": "title", "old_value": None, "new_value": "title"},
            {"key": "description", "old_value": None, "new_value": "desc"},
            {"key": "due_date", "old_value": None, "new_value": "2019-12-11"},
            {
                "key": "assignee",
                "old_value": None,
                "new_value": {
                    "uuid": assignee_uuid,
                    "entity_type": "employee",
                },
            },
        ]
        self.task_repo._update_task(event=event)
        self.session.execute.assert_called()
        values = {
            "label": "title",
            "due_date": "2019-12-11",
            "description": "desc",
            "assignee_id": assignee_uuid,
        }
        update_stmt = (
            sql.update(schema.ChecklistItem)
            .where(schema.ChecklistItem.uuid == event.entity_id)
            .values(values)
        )
        assert str(self.session.execute.call_args_list[0][0][0]) == str(
            update_stmt
        )

    def test__delete_task(self):
        event = copy.deepcopy(self.event)
        self.session.reset_mock()

        self.task_repo._delete_task(event=event)

        self.session.execute.assert_called()

        assert str(self.session.execute.call_args_list[0][0][0]) == str(
            sql.delete(schema.ChecklistItem).where(
                schema.ChecklistItem.uuid == event.entity_id
            )
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._get_phase_id"
    )
    def test__insert_task_IntegrityError(self, _get_phase_id_mock):
        _get_phase_id_mock.return_value = 456
        event = copy.deepcopy(self.event)
        event.changes = [
            {
                "key": "case",
                "old_value": None,
                "new_value": {
                    "uuid": uuid4(),
                    "status": "open",
                    "milestone": 1,
                },
            },
            {"key": "title", "old_value": None, "new_value": "title"},
            {"key": "description", "old_value": None, "new_value": "desc"},
            {"key": "user_defined", "old_value": None, "new_value": True},
            {"key": "completed", "old_value": None, "new_value": False},
            {"key": "phase", "old_value": None, "new_value": 1},
        ]

        self.session.execute.side_effect = sql_exc.IntegrityError(
            None, None, None
        )
        with pytest.raises(minty_exc.Conflict):
            self.task_repo._insert_task(event=event)

    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._get_phase_id"
    )
    def test__insert_task(self, _get_phase_id_mock):
        _get_phase_id_mock.return_value = 456
        event = copy.deepcopy(self.event)
        event.changes = [
            {
                "key": "case",
                "old_value": None,
                "new_value": {
                    "uuid": uuid4(),
                    "status": "open",
                    "milestone": 1,
                },
            },
            {"key": "title", "old_value": None, "new_value": "title"},
            {"key": "description", "old_value": None, "new_value": "desc"},
            {"key": "user_defined", "old_value": None, "new_value": True},
            {"key": "completed", "old_value": None, "new_value": False},
            {"key": "phase", "old_value": None, "new_value": 1},
        ]
        self.task_repo._insert_task(event=event)
        values = {
            "checklist_id": 456,
            "label": "title",
            "state": True,
            "sequence": None,
            "user_defined": False,
            "uuid": event.entity_id,
            "due_date": None,
            "description": "descr",
            "assignee_id": None,
        }
        assert str(self.session.execute.call_args_list[0][0][0]) == str(
            sql.insert(schema.ChecklistItem, values=values)
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._insert_phase"
    )
    def test__get_phase_id(self, _insert_phase_mock):
        case_uuid = str(uuid4())
        phase = 2
        result = namedtuple("result", "id")(id=1234)
        self.session.execute().fetchone.return_value = result
        phase_id = self.task_repo._get_phase_id(
            case_uuid=case_uuid, phase=phase
        )
        assert phase_id == 1234

        _insert_phase_mock.return_value = 4321
        self.session.execute().fetchone.return_value = None
        phase_id_2 = self.task_repo._get_phase_id(
            case_uuid=case_uuid, phase=phase
        )
        assert phase_id_2 == 4321

    def test__insert_phase(self):
        case_uuid = str(uuid4())
        phase = 2
        self.session.execute().inserted_primary_key = [1234]
        phase_id = self.task_repo._insert_phase(
            case_uuid=case_uuid, phase=phase
        )
        assert phase_id == 1234
        self.session.execute.assert_called()

    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._get_case_assignee_contact_id"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._insert_logging"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._insert_notification"
    )
    def test_create_task_status_notification_for_case_assignee(
        self, _insert_notification, _insert_logging, _get_case_assignee
    ):
        task = copy.deepcopy(self.task)
        user_uuid = uuid4()
        subject = {"legacy_id": "betrokkene-medewerker-23", "uuid": uuid4()}
        _get_case_assignee.return_value = subject
        _insert_logging.return_value = 876
        self.task_repo.create_task_status_notification_for_case_assignee(
            task=task, user_uuid=user_uuid
        )

        _insert_logging.assert_called_with(
            case_id=448,
            subject=f'Taak "{task.title}" {"afgerond" if task.completed else "heropend"}.',
            event_type="case/task/set_completion",
            user_uuid=user_uuid,
        )

        _insert_notification.assert_called_with(
            logging_id=876,
            subject=f'Taak "{task.title}" {"afgerond" if task.completed else "heropend"}.',
            assignee_uuid=subject["uuid"],
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._insert_logging"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository._insert_notification"
    )
    def test_create_assigned_notification_for_task_assignee(
        self, _insert_notification, _insert_logging
    ):
        task = copy.deepcopy(self.task)
        task.title = "do some task"
        assignee = namedtuple("Employee", "uuid")(uuid=uuid4())
        user_uuid = uuid4()
        _insert_logging.return_value = 876

        self.task_repo.create_assigned_notification_for_task_assignee(
            task=task, user_uuid=user_uuid, assignee=assignee
        )
        _insert_logging.assert_called_with(
            case_id=448,
            subject=f'Taak "{task.title}" toegewezen.',
            event_type="case/task/new_assignee",
            user_uuid=user_uuid,
        )

        _insert_notification.assert_called_with(
            logging_id=876,
            subject=f'Taak "{task.title}" toegewezen.',
            assignee_uuid=assignee.uuid,
        )

    def test__insert_logging(self):
        case_id = 123
        subject = "some subject"
        user_uuid = uuid4()
        event_type = "case/task/new_assignee"
        self.task_repo._insert_logging(
            case_id=case_id,
            subject=subject,
            user_uuid=user_uuid,
            event_type=event_type,
        )
        restricted_case = sql.case(
            [(schema.Case.confidentiality == "confidential", True)],
            else_=False,
        )

        restricted = (
            sql.select([restricted_case])
            .where(schema.Case.id == case_id)
            .scalar_subquery()
        )

        created_by_name = (
            sql.select(
                [
                    sql.cast(
                        schema.Subject.properties, dialects.postgresql.JSON
                    )["displayname"]
                ]
            )
            .where(schema.Subject.uuid == user_uuid)
            .scalar_subquery()
        )

        insert_values_logging = {
            "zaak_id": case_id,
            "component": "zaak",
            "onderwerp": subject,
            "event_type": event_type,
            "event_data": {"case_id": f"{case_id}", "content": ""},
            "restricted": restricted,
            "created_by_name_cache": created_by_name,
        }
        assert str(self.session.execute.call_args_list[0][0][0]) == str(
            sql.insert(schema.Logging, values=insert_values_logging)
        )

    def test__insert_notification(self):
        logging_id = 123
        subject = "some subject"
        assignee_uuid = uuid4()
        self.task_repo._insert_notification(
            logging_id=logging_id, subject=subject, assignee_uuid=assignee_uuid
        )
        insert_values_notification = {
            "logging_id": logging_id,
            "message": subject,
            "subject_id": sql.select(
                [
                    "betrokkene-medewerker-"
                    + sql.cast(schema.Subject.id, sqltypes.VARCHAR),
                ]
            )
            .where(schema.Subject.uuid == assignee_uuid)
            .scalar_subquery(),
            "is_read": False,
            "is_archived": False,
        }
        assert str(self.session.execute.call_args_list[0][0][0]) == str(
            sql.insert(schema.Message, values=insert_values_notification)
        )

    def test___get_case_assignee_contact_id(self):
        case_id = 123
        self.session.execute().fetchone.return_value = None
        case_assignee = self.task_repo._get_case_assignee_contact_id(
            case_id=case_id
        )
        assert case_assignee == {"legacy_id": None, "uuid": None}

        res = namedtuple("res", "betrokkene_type betrokkene_id uuid")(
            "medewerker", 41, uuid4()
        )
        self.session.execute().fetchone.return_value = res
        case_assignee = self.task_repo._get_case_assignee_contact_id(
            case_id=case_id
        )
        assert case_assignee == {
            "legacy_id": "betrokkene-medewerker-41",
            "uuid": res.uuid,
        }
