# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import get_query_instance
from zsnl_domains.case_management.entities import _shared


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context, event_service):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCaseDomainQueries:
    def setup(self):
        repo_factory = MockRepositoryFactory(infra_factory={})
        repo_factory.repositories["case"] = mock.MagicMock()
        repo_factory.repositories["case_basic"] = mock.MagicMock()
        repo_factory.repositories["case_type"] = mock.MagicMock()
        repo_factory.repositories["organization"] = mock.MagicMock()
        repo_factory.repositories["employee"] = mock.MagicMock()
        repo_factory.repositories["person"] = mock.MagicMock()
        repo_factory.repositories["subject_relation"] = mock.MagicMock()
        repo_factory.repositories["case_relation"] = mock.MagicMock()
        repo_factory.repositories["task"] = mock.MagicMock()
        repo_factory.repositories["department"] = mock.MagicMock()
        repo_factory.repositories["export_file"] = mock.MagicMock()
        repo_factory.repositories["country"] = mock.MagicMock()

        self.user_uuid = "d3887763-675c-4988-9347-c5e1448fd20d"
        self.user_info = UserInfo(
            user_uuid=self.user_uuid, permissions={"admin": True}
        )
        self.querymock = get_query_instance(
            repository_factory=repo_factory,
            context=None,
            user_uuid=self.user_uuid,
        )
        self.case_entity = mock.MagicMock()
        self.case_type_version_entity = mock.MagicMock()
        self.organization_entity = mock.MagicMock()
        self.employee_entity = mock.MagicMock()
        self.person_entity = mock.MagicMock()
        self.subject_relation_entity = mock.MagicMock()
        self.export_file_entity = mock.MagicMock()

        repo_factory.repositories[
            "case"
        ].find_case_by_uuid.return_value = self.case_entity
        repo_factory.repositories[
            "case_basic"
        ].find_case_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "case_type"
        ].find_case_type_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "organization"
        ].find_organization_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "employee"
        ].find_employee_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "person"
        ].find_person_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "case_relation"
        ].get_case_relations.return_value = None
        repo_factory.repositories[
            "export_file"
        ].get_case_relations.return_value = self.export_file_entity

    def test_get_case_type_version_by_uuid(self):
        version_uuid = uuid4()
        case_type_repo = self.querymock.repository_factory.repositories[
            "case_type"
        ]
        case_type_repo.find_case_type_version_by_uuid.return_value = (
            self.case_type_version_entity
        )

        res = self.querymock.get_case_type_version_by_uuid(
            version_uuid=str(version_uuid)
        )
        case_type_repo.find_case_type_version_by_uuid.assert_called_once_with(
            version_uuid=version_uuid
        )
        assert isinstance(res, dict)
        assert res == {"result": self.case_type_version_entity}

    def test_get_case_type_active_version(self):
        case_type_uuid = uuid4()
        case_type_repo = self.querymock.repository_factory.repositories[
            "case_type"
        ]
        case_type_repo.find_active_version_by_uuid.return_value = (
            self.case_type_version_entity
        )

        res = self.querymock.get_case_type_active_version(
            case_type_uuid=str(case_type_uuid)
        )
        case_type_repo.find_active_version_by_uuid.assert_called_once_with(
            case_type_uuid=case_type_uuid
        )
        assert isinstance(res, dict)
        assert res == {"result": self.case_type_version_entity}

    def test_get_subject_relations_for_case(self):
        case_uuid = uuid4()
        repo = self.querymock.repository_factory.repositories[
            "subject_relation"
        ]
        user_info = mock.MagicMock()

        user_info = UserInfo(
            user_uuid="d3887763-675c-4988-9347-c5e1448fd20d",
            permissions={"admin": True},
        )

        repo.find_subject_relations_for_case.return_value = [
            self.subject_relation_entity
        ]
        self.querymock.user_info = user_info
        res = self.querymock.get_subject_relations_for_case(
            case_uuid=str(case_uuid)
        )
        repo.find_subject_relations_for_case.assert_called_once_with(
            case_uuid=case_uuid, user_info=user_info
        )
        assert res == {"result": [self.subject_relation_entity]}

    def test_get_case_relations(self):
        case_uuid = uuid4()
        repo = self.querymock.repository_factory.repositories["case_relation"]
        repo.get_case_relations.return_value = ["fake_value"]
        user_info = mock.MagicMock()

        user_info = UserInfo(
            user_uuid="d3887763-675c-4988-9347-c5e1448fd20d",
            permissions={"admin": True},
        )
        self.querymock.user_info = user_info
        res = self.querymock.get_case_relations(case_uuid=case_uuid)
        repo.get_case_relations.assert_called_once_with(
            authorization=_shared.CaseAuthorizationLevel.search,
            case_uuid=case_uuid,
            user_info=user_info,
        )
        assert res == {"result": ["fake_value"]}

    def test_get_task_list(self):
        assignee_uuid = uuid4()
        case_uuid = uuid4()
        case_type_uuid = uuid4()
        department_uuid = uuid4()

        phase = 2
        user_uuid = self.querymock.user_uuid
        completed = True
        page = 1
        page_size = 10

        task_repo = self.querymock.repository_factory.repositories["task"]

        task_repo.get_task_list.return_value = ["tasks", "tasks"]

        res = self.querymock.get_task_list(
            page=page,
            page_size=page_size,
        )
        task_repo.get_task_list.assert_called_with(
            user_uuid=user_uuid,
            page=int(page),
            page_size=int(page_size),
        )
        assert res == ["tasks", "tasks"]

        self.querymock.get_task_list(
            case_uuids=[str(case_uuid)],
            case_ids=[1, 2, 3, 4],
            phase=phase,
            completed=completed,
            assignee_uuids=[str(assignee_uuid)],
            case_type_uuids=[str(case_type_uuid)],
            department_uuids=[str(department_uuid)],
            title_words=["some", "words"],
            keyword="key",
            page=page,
            page_size=page_size,
            sort="attributes.due_date",
        )
        task_repo.get_task_list.assert_called_with(
            user_uuid=user_uuid,
            case_ids=[1, 2, 3, 4],
            case_uuids=[case_uuid],
            assignee_uuids=[assignee_uuid],
            case_type_uuids=[case_type_uuid],
            department_uuids=[department_uuid],
            title_words=["some", "words"],
            keyword="key",
            phase=phase,
            completed=completed,
            page=int(page),
            page_size=int(page_size),
            sort="attributes.due_date",
        )

    def test_get_export_file_list(self):
        user_info = mock.MagicMock()

        user_info = UserInfo(
            user_uuid="d3887763-675c-4988-9347-c5e1448fd20d",
            permissions={"admin": True},
        )
        self.querymock.user_info = user_info
        page = 1
        page_size = 10

        export_file_repo = self.querymock.repository_factory.repositories[
            "export_file"
        ]

        export_file_repo.get_export_file_list.return_value = [
            "export_file",
            "export_file",
        ]

        res = self.querymock.get_export_file_list(
            page=page, page_size=page_size
        )
        export_file_repo.get_export_file_list.assert_called_with(
            user_uuid=user_info.user_uuid,
            page=int(page),
            page_size=int(page_size),
        )
        assert res == ["export_file", "export_file"]

        self.querymock.get_export_file_list(page=page, page_size=page_size)
        export_file_repo.get_export_file_list.assert_called_with(
            user_uuid=user_info.user_uuid,
            page=int(page),
            page_size=int(page_size),
        )

    def test_get_countries_list(self):
        country_repo = self.querymock.repository_factory.repositories[
            "country"
        ]

        country_repo.get_countries_list.return_value = [
            "countries_1",
            "countries_2",
        ]

        res = self.querymock.get_countries_list()
        country_repo.get_countries_list.assert_called_once()
        assert res == ["countries_1", "countries_2"]


class TestCaseManagementGeneral:
    def test_get_query_instance(self):
        repo = {}
        context = None
        qry = case_management.get_query_instance(
            repository_factory=repo,
            context=context,
            user_uuid="0b71102a-2714-421d-9fe1-e4b085a0eee8",
        )
        assert isinstance(qry, minty.cqrs.DomainQueryContainer)

    def test_get_command_instance(self):
        repo = {}
        context = None
        cmd = case_management.get_command_instance(
            repository_factory=repo,
            context=context,
            user_uuid="0b71102a-2714-421d-9fe1-e4b085a0eee8",
            event_service="event_service",
        )
        assert isinstance(cmd, minty.cqrs.DomainCommandContainer)


class Test_Case_Type_Result(TestBase):
    def setup(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_get_case_available_result_types(self):
        case_uuid = str(uuid4())

        mock_db_row = [
            mock.Mock(),
            mock.Mock(),
        ]
        mock_db_row[0].configure_mock(
            uuid=uuid4(),
            name="naam1",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )
        mock_db_row[1].configure_mock(
            uuid=uuid4(),
            name="naam2",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="6 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=6,
        )

        self.session.execute().fetchall.return_value = mock_db_row
        self.session.reset_mock()

        result_types = self.qry.get_case_available_result_types(
            case_uuid=case_uuid
        )

        assert len(result_types.entities) == 2
        assert result_types.entities[0].name == "naam1"
        assert result_types.entities[0].result == "aangegaan"
        assert not result_types.entities[0].trigger_archival
        assert result_types.entities[0].preservation_term_label == "4 weken"
        assert result_types.entities[0].preservation_term_unit == "week"
        assert result_types.entities[0].preservation_term_unit_amount == 4

        assert result_types.entities[1].name == "naam2"
        assert not result_types.entities[1].trigger_archival
        assert result_types.entities[1].preservation_term_label == "6 weken"
        assert result_types.entities[1].preservation_term_unit == "week"
        assert result_types.entities[1].preservation_term_unit_amount == 6

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s AND zaak_1.status != %(status_1)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s)))"
        )
