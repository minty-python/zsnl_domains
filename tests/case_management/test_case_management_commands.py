# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import datetime
import pytest
from collections import namedtuple
from minty.cqrs import UserInfo
from minty.exceptions import Conflict, Forbidden, NotFound, ValidationError
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.case_management import get_command_instance
from zsnl_domains.case_management.entities import (
    Employee,
    Organization,
    Person,
    Task,
)
from zsnl_domains.case_management.entities._shared import RelatedCustomObject


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context, event_service):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCaseDomainCommands:
    @mock.patch(
        "zsnl_domains.case_management.repositories.employee.EmployeeRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.organization.OrganizationRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.person.PersonRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.subject_relation.SubjectRelationRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_relation.CaseRelationRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.department.DepartmentRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.role.RoleRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_messages.CaseMessageListRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.entities.subject.Subject", autospec=True
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.subject.SubjectRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.entities.case_type_version.CaseTypeVersionEntity",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.task.TaskRepository",
        autospec=True,
    )
    def setup(
        self,
        taskRepositoryMock,
        caseTypeRepoMock,
        caseTypeVersionEntityMock,
        caseRepoMock,
        caseEntityMock,
        subjectRepoMock,
        subjectEntityMock,
        CaseMessageListRepoMock,
        roleRepoMock,
        departmentRepoMock,
        case_relation_repo_mock,
        relatedSubjectRepoMock,
        personRepoMock,
        organizationRepoMock,
        employeeRepoMock,
    ):
        repo_factory = MockRepositoryFactory(infra_factory={})
        repo_factory.repositories["case_relation"] = case_relation_repo_mock
        repo_factory.repositories["case"] = caseRepoMock
        repo_factory.repositories["subject"] = subjectRepoMock
        repo_factory.repositories[
            "case_message_list"
        ] = CaseMessageListRepoMock
        repo_factory.repositories["case_type"] = caseTypeRepoMock
        repo_factory.repositories["role"] = roleRepoMock
        repo_factory.repositories["department"] = departmentRepoMock

        repo_factory.repositories["subject_relation"] = relatedSubjectRepoMock
        repo_factory.repositories["person"] = personRepoMock
        repo_factory.repositories["organization"] = organizationRepoMock
        repo_factory.repositories["employee"] = employeeRepoMock

        repo_factory.repositories["task"] = taskRepositoryMock

        self.commandmock = get_command_instance(
            repository_factory=repo_factory,
            context=None,
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
            event_service={},
        )
        self.case_entity = caseEntityMock
        self.subject_entity = subjectEntityMock
        caseRepoMock.find_case_by_uuid.return_value = caseEntityMock
        subjectRepoMock.find_subject_by_uuid.return_value = subjectEntityMock
        caseTypeRepoMock.find_case_type_version_by_uuid.return_value = (
            caseTypeVersionEntityMock
        )
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid, permissions={"admin": True}
        )

    def test_set_case_target_completion_date(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.set_case_target_completion_date(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            target_date="2019-01-30",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.set_target_completion_date.call_args
        kwargs["target_completion_date"] == "2019-01-30"
        repo.save.assert_called_once()

    def test_change_case_set_case_completion_date(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.set_case_completion_date(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            target_date="2019-01-29",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.set_completion_date.call_args
        kwargs["completion_date"] == "2019-01-29"
        repo.save.assert_called_once()

    def test_pause_case(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.pause_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            suspension_reason="suspension",
            suspension_term_value=6,
            suspension_term_type="weeks",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.pause.call_args
        kwargs["suspension_reason"] == "suspension"
        kwargs["suspension_term_value"] == "6"
        kwargs["suspension_term_type"] == "weeks"
        repo.save.assert_called_once()

    def test_pause_case_indefinite_date(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.pause_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            suspension_reason="suspension",
            suspension_term_type="indefinite",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.pause.call_args
        kwargs["suspension_reason"] == "suspension"
        kwargs["suspension_term_value"] is None
        kwargs["suspension_term_type"] == "indefinite"
        repo.save.assert_called_once()

    def test_pause_case_non_existing_interval(self):
        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
                suspension_reason="suspension",
                suspension_term_type="Test non existing term type",
            )

    def test_resume_case(self):
        repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        self.commandmock.resume_case(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            stalled_since_date="2019-01-31",
            stalled_until_date="2019-02-15",
            resume_reason="resume_reason",
        )
        repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        args, kwargs = self.case_entity.resume.call_args
        kwargs["stalled_since_date"] == "2019-01-31"
        kwargs["stalled_until_date"] == "2019-02-15"
        kwargs["resume_reason"] == "resume_reason"
        repo.save.assert_called_once()

    def test_resume_case_validation(self):
        with pytest.raises(ValidationError):
            self.commandmock.resume_case(
                case_uuid=1,
                stalled_until_date="2019-01-31",
                stalled_since_date="2019-01-01",
            )

        with pytest.raises(ValidationError):
            self.commandmock.resume_case(
                case_uuid=str(uuid4()),
                stalled_since_date="31-01-2019",
                stalled_until_date="2019-01-22",
            )

        with pytest.raises(ValidationError):
            self.commandmock.resume_case(
                case_uuid=str(uuid4()),
                stalled_until_date="2019-01-01",
                stalled_since_date="23-02-2019",
            )

    def test_pause_case_validation(self):
        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid=1,
                stalled_until_date="2019-01-31",
                stalled_since_date="2019-01-31",
                suspension_reason="reason",
            )

        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
                stalled_until_date="01-01-2019",
                stalled_since_date="2019-01-31",
                suspension_reason="reason",
            )

        with pytest.raises(ValidationError):
            self.commandmock.pause_case(
                case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
                stalled_until_date="2019-01-31",
                stalled_since_date="2019-01-31",
                suspension_reason=1,
            )

    def test_assign_case_to_self(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]

        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info
        self.commandmock.assign_case_to_self(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2"
        )
        case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )

        # Called twice -- once in "reassign_case_messages"
        assert case_repo.get_contact_employee.call_args_list == [
            mock.call(uuid=UUID("8ee6f9de-d93b-453d-9246-536c83a27318"))
        ]
        self.case_entity.set_assignee.assert_called_once()
        self.case_entity.set_coordinator.assert_called_once()
        self.case_entity.set_status.assert_called_once()
        case_repo.save.assert_called_once()

    def test_assign_case_to_department(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        department_repo = self.commandmock.repository_factory.repositories[
            "department"
        ]
        role_repo = self.commandmock.repository_factory.repositories["role"]

        self.case_entity.department = None
        self.case_entity.role = None
        self.commandmock.user_info = self.user_info
        self.commandmock.assign_case_to_department(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            department_uuid="a40577cd-21c9-4caa-8d5f-972b8a67c633",
            role_uuid="bffaab57-26fc-4c6b-b321-5f6b6f352b2c",
        )

        case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )
        department_repo.find.assert_called_once_with(
            UUID("a40577cd-21c9-4caa-8d5f-972b8a67c633")
        )
        role_repo.find.assert_called_once_with(
            UUID("bffaab57-26fc-4c6b-b321-5f6b6f352b2c")
        )
        self.case_entity.clear_assignee.assert_called_once()
        self.case_entity.set_allocation.assert_called_once()
        case_repo.save.assert_called_once()

    def test_assign_case_to_user(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.case_entity.assignee = None
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info
        self.commandmock.assign_case_to_user(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )
        case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )
        # Called twice -- once in "reassign_case_messages"
        assert case_repo.get_contact_employee.call_args_list == [
            mock.call(uuid=UUID("8ee6f9de-d93b-453d-9246-536c83a27318"))
        ]
        self.case_entity.set_assignee.assert_called_once()
        self.case_entity.set_coordinator.assert_called_once()
        self.case_entity.set_status.assert_called_once()
        case_repo.save.assert_called_once()

    def test_change_case_coordinator(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.case_entity.coordinator = None
        self.commandmock.user_info = self.user_info
        self.commandmock.change_case_coordinator(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            coordinator_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
        )

        case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2"),
            permission="write",
            user_info=self.user_info,
        )
        case_repo.get_contact_employee.assert_called_once_with(
            uuid=UUID("8ee6f9de-d93b-453d-9246-536c83a27318")
        )
        self.case_entity.set_coordinator.assert_called_once()
        case_repo.save.assert_called_once()

    def test_reassign_case_messages(self):
        cml_repo = self.commandmock.repository_factory.repositories[
            "case_message_list"
        ]
        subject_repo = self.commandmock.repository_factory.repositories[
            "subject"
        ]

        self.commandmock.reassign_case_messages(
            case_uuid="58347e47-9b26-4e31-a7b3-ace95bad91b2",
            subject_uuid="59068c82-7194-11e9-b1c4-d761253cfb1c",
        )
        cml_repo.get_messages_for_case.assert_called_once_with(
            case_uuid=UUID("58347e47-9b26-4e31-a7b3-ace95bad91b2")
        )
        subject_repo.find_subject_by_uuid.assert_called_once_with(
            subject_uuid=UUID("59068c82-7194-11e9-b1c4-d761253cfb1c")
        )

        cml_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_enqueue_subcases(self, mock_case):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_subcase = mock.MagicMock()
        mock_case = mock.MagicMock()
        mock_subcase.data = {"test": 1}
        mock_case.id = 42
        mock_case.data = {}
        mock_case.enqueue_subcase.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_case
        self.commandmock.enqueue_subcases(case_uuid=case_uuid)
        mock_case.enqueue_subcases.assert_called_once()
        case_repo.save.assert_called_once()

    def test_create_subcases(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_subcase = mock.MagicMock()
        mock_case = mock.MagicMock()
        mock_subcase.data = {"test": 1}
        mock_case.id = 42
        mock_case.data = {}
        mock_case.create_subcases.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_case
        self.commandmock.create_subcases(
            case_uuid=case_uuid, queue_ids=["fake_uuid"]
        )
        mock_case.create_subcases.assert_called_once()
        case_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_enqueue_documents(self, mock_case):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_document = mock.MagicMock()
        mock_case = mock.MagicMock()
        mock_document.data = {"test": 1}
        mock_case.id = 42
        mock_case.data = {}
        mock_case.enqueue_subcase.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_case
        self.commandmock.enqueue_documents(case_uuid=case_uuid)
        mock_case.enqueue_documents.assert_called_once()
        case_repo.save.assert_called_once()

    def test_generate_documents(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_subcase = mock.MagicMock()
        mock_document = mock.MagicMock()
        mock_subcase.data = {"test": 1}
        mock_document.id = 42
        mock_document.data = {}
        mock_document.generate_documents.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_document
        self.commandmock.generate_documents(
            case_uuid=case_uuid, queue_ids=["fake_uuid"]
        )
        mock_document.generate_documents.assert_called_once()
        case_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_enqueue_emails(self, mock_case):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_email = mock.MagicMock()
        mock_case = mock.MagicMock()
        mock_email.data = {"test": 1}
        mock_case.id = 42
        mock_case.data = {}
        mock_case.enqueue_subcase.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_case
        self.commandmock.enqueue_emails(case_uuid=case_uuid)
        mock_case.enqueue_emails.assert_called_once()
        case_repo.save.assert_called_once()

    def test_generate_emails(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_subcase = mock.MagicMock()
        mock_email = mock.MagicMock()
        mock_subcase.data = {"test": 1}
        mock_email.id = 42
        mock_email.data = {}
        mock_email.generate_emails.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_email
        self.commandmock.generate_emails(
            case_uuid=case_uuid, queue_ids=["fake_uuid"]
        )
        mock_email.generate_emails.assert_called_once()
        case_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_enqueue_subjects(self, mock_case):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_subject = mock.MagicMock()
        mock_case = mock.MagicMock()
        mock_subject.data = {"test": 1}
        mock_case.id = 42
        mock_case.data = {}
        mock_case.enqueue_subcase.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_case
        self.commandmock.enqueue_subjects(case_uuid=case_uuid)
        mock_case.enqueue_subjects.assert_called_once()
        case_repo.save.assert_called_once()

    def test_generate_subjects(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        mock_subcase = mock.MagicMock()
        mock_subject = mock.MagicMock()
        mock_subcase.data = {"test": 1}
        mock_subject.id = 42
        mock_subject.data = {}
        mock_subject.generate_subjects.return_value = None
        case_repo.unsafe_find_case_by_uuid.return_value = mock_subject
        self.commandmock.generate_subjects(
            case_uuid=case_uuid, queue_ids=["fake_uuid"]
        )
        mock_subject.generate_subjects.assert_called_once()
        case_repo.save.assert_called_once()

    def test_transition_case(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())

        self.commandmock.transition_case(case_uuid=case_uuid)
        case_repo.unsafe_find_case_by_uuid.assert_called_once()
        case_repo.save.assert_called_once()

    def test_set_case_parent(self):
        self.commandmock.user_info = self.user_info
        case_repo = self.commandmock.repository_factory.repositories["case"]
        case_uuid = str(uuid4())
        case_1_uuid = str(uuid4())

        self.commandmock.set_case_parent(
            case_uuid=case_uuid, parent_uuid=case_1_uuid
        )
        case_repo.find_case_by_uuid.assert_called_once()
        case_repo.save.assert_called_once()

    def test_create_subject_relation(self):
        case_uuid = uuid4()
        subject_uuid = uuid4()
        subject = {"type": "person", "id": str(subject_uuid)}
        person_repo = self.commandmock.repository_factory.repositories[
            "person"
        ]
        self.commandmock.user_info = self.user_info
        person_repo.find_person_by_uuid.return_value = Person(
            type="person",
            uuid=subject_uuid,
            name="beheerder",
            first_names="beheerder",
            surname="beheerder",
            contact_information={},
            authenticated=True,
            family_name=None,
            noble_title=None,
            date_of_birth=None,
            date_of_death=None,
            surname_prefix=None,
            gender="Male",
            inside_municipality=False,
            foreign_address=False,
            residence_address=None,
            correspondence_address=None,
            related_custom_object_uuid=str(uuid4()),
            has_valid_address=True,
        )
        self.commandmock.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Advocaat",
            magic_string_prefix="advocaat",
            authorized=True,
            send_confirmation_email=True,
            permission=None,
        )

        subject = {"type": "organization", "id": str(subject_uuid)}
        organization_repo = self.commandmock.repository_factory.repositories[
            "organization"
        ]

        related_custom_object_uuid = uuid4()
        organization_repo.find_organization_by_uuid.return_value = (
            Organization(
                entity_id=subject_uuid,
                uuid=subject_uuid,
                name="beheerder",
                source="source",
                coc_number="12345678",
                coc_location_number="012345678912",
                date_founded=None,
                date_registered=None,
                date_ceased=None,
                rsin=None,
                oin=None,
                main_activity=None,
                secondary_activities=[],
                organization_type=None,
                location_address=None,
                correspondence_address=None,
                contact_information={},
                related_custom_object=RelatedCustomObject(
                    entity_id=related_custom_object_uuid,
                    uuid=related_custom_object_uuid,
                ),
                has_valid_address=True,
                authenticated=True,
            )
        )
        self.commandmock.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Advocaat",
            magic_string_prefix="advocaat",
            authorized=True,
            send_confirmation_email=True,
            permission=None,
        )

        subject = {"type": "employee", "id": str(subject_uuid)}
        employee_repo = self.commandmock.repository_factory.repositories[
            "employee"
        ]

        employee_repo.find_employee_by_uuid.return_value = Employee(
            id=123,
            uuid=subject_uuid,
            name="beheerder",
            status="active",
            first_name="beheerder",
            surname="",
            source=None,
            department={"uuid": uuid4(), "name": "dept"},
            roles=[],
            contact_information={},
            related_custom_object_uuid=str(uuid4()),
        )
        self.commandmock.create_subject_relation(
            case_uuid=str(case_uuid),
            subject=subject,
            role="Advocaat",
            magic_string_prefix="advocaat",
            authorized=True,
            send_confirmation_email=True,
            permission="read",
        )

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_enqueue_subject_relation_email(self, mock_subject_relation):
        subject_relation_uuid = uuid4()
        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )
        self.commandmock.user_info = self.user_info
        self.commandmock.enqueue_subject_relation_email(
            subject_relation_uuid=str(subject_relation_uuid)
        )
        mock_subject_relation.enqueue_email.assert_called_once()
        subject_relation_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_send_subject_relation_email(self, mock_subject_relation):
        subject_relation_uuid = uuid4()
        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        self.commandmock.user_info = self.user_info
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        self.commandmock.send_subject_relation_email(
            subject_relation_uuid=str(subject_relation_uuid)
        )
        mock_subject_relation.send_email.assert_called_once()
        subject_relation_repo.save.assert_called_once()

    def test_create_case_relation(self):
        case_relation_repo = self.commandmock.repository_factory.repositories[
            "case_relation"
        ]
        case_relation_repo.create_case_relation = mock.MagicMock()
        uuid1 = str(uuid4())
        uuid2 = str(uuid4())
        user_info = mock.MagicMock()

        user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True},
        )
        self.commandmock.user_info = user_info

        self.commandmock.create_case_relation(uuid1=uuid1, uuid2=uuid2)
        case_relation_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_enqueue_case_assignee_email(self, mock_case):
        case_uuid = str(uuid4())
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        mock_case = mock.MagicMock()
        case_repo.find_case_by_uuid.return_value = mock_case

        self.commandmock.enqueue_case_assignee_email(case_uuid=case_uuid)
        mock_case.enqueue_assignee_email.assert_called_once()
        case_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.case.Case", autospec=True
    )
    def test_send_case_assignee_email(self, mock_subject_relation):
        case_uuid = str(uuid4())
        queue_id = str(uuid4())
        case_repo = self.commandmock.repository_factory.repositories["case"]
        user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True},
        )
        self.commandmock.user_info = user_info
        mock_case = mock.MagicMock()
        case_repo.find_case_by_uuid.return_value = mock_case

        self.commandmock.send_case_assignee_email(
            case_uuid=case_uuid, queue_id=queue_id
        )
        mock_case.send_assignee_email.assert_called_once_with(
            queue_id=queue_id
        )
        case_repo.save.assert_called_once()

    def test_create_task_conflict(self):
        task_repo = self.commandmock.repository_factory.repositories["task"]
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        task_uuid = str(uuid4())
        case_uuid = str(uuid4())
        phase = 1

        case = namedtuple("CaseMock", "uuid status")(
            uuid=case_uuid, status="resolved"
        )
        case_repo.find_case_by_uuid.return_value = case

        t = Task(
            uuid=task_uuid,
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee={
                "type": "employee",
                "uuid": uuid4(),
                "display_name": "Ad Min",
            },
            case={"uuid": uuid4(), "status": "resolved", "milestone": 2},
            case_type=None,
            phase=2,
            department={
                "type": "department",
                "uuid": uuid4(),
                "display_name": "Backoffice",
            },
        )
        task_repo.create_task.return_value = t

        with pytest.raises(Conflict):
            self.commandmock.create_task(
                case_uuid=case_uuid,
                task_uuid=task_uuid,
                phase=phase,
                title="Some Title",
            )

    def test_create_task(self):
        task_repo = self.commandmock.repository_factory.repositories["task"]
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        task_uuid = str(uuid4())
        case_uuid = str(uuid4())
        phase = 1

        case = namedtuple("CaseMock", "uuid status")(
            uuid=case_uuid, status="resolved"
        )
        case_repo.find_case_by_uuid.return_value = case

        t = Task(
            uuid=task_uuid,
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee={
                "type": "employee",
                "uuid": uuid4(),
                "display_name": "Ad Min",
            },
            case={"uuid": uuid4(), "status": "open", "milestone": 2},
            case_type=None,
            phase=2,
            department={
                "type": "department",
                "uuid": uuid4(),
                "display_name": "Backoffice",
            },
        )
        task_repo.create_task.return_value = t
        self.commandmock.create_task(
            case_uuid=case_uuid,
            task_uuid=task_uuid,
            phase=phase,
            title="Some Title",
        )
        task_repo.save.assert_called_once()

    def test_create_task_case_not_found(self):
        case_repo = self.commandmock.repository_factory.repositories["case"]
        self.commandmock.user_info = self.user_info
        task_uuid = str(uuid4())
        case_uuid = str(uuid4())
        phase = 1

        case_repo.find_case_by_uuid.side_effect = NotFound

        with pytest.raises(Forbidden):
            self.commandmock.create_task(
                case_uuid=case_uuid,
                task_uuid=task_uuid,
                phase=phase,
                title="Some Title",
            )

    def test_update_task_conflict(self):
        task_repo = self.commandmock.repository_factory.repositories["task"]
        task_uuid = str(uuid4())
        assignee_uuid = str(uuid4())
        t = Task(
            uuid=task_uuid,
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee={
                "type": "employee",
                "uuid": uuid4(),
                "display_name": "Ad Min",
            },
            case={"uuid": uuid4(), "status": "open", "milestone": 2},
            case_type=None,
            phase=2,
            department={
                "type": "department",
                "uuid": uuid4(),
                "display_name": "Backoffice",
            },
        )
        t.event_service = mock.MagicMock()
        task_repo.get_task.return_value = t
        t.phase = 1
        with pytest.raises(Conflict):
            self.commandmock.update_task(
                task_uuid=task_uuid,
                title="some new title",
                description="some new descr",
                due_date="2019-02-23",
                assignee=assignee_uuid,
            )

    def test_update_task(self):
        task_repo = self.commandmock.repository_factory.repositories["task"]
        employee_repo = self.commandmock.repository_factory.repositories[
            "employee"
        ]
        task_uuid = str(uuid4())
        assignee_uuid = uuid4()
        t = Task(
            uuid=task_uuid,
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee={
                "type": "employee",
                "uuid": uuid4(),
                "display_name": "Ad Min",
            },
            case={"uuid": uuid4(), "status": "open", "milestone": 2},
            case_type=None,
            phase=2,
            department={
                "type": "department",
                "uuid": uuid4(),
                "display_name": "Backoffice",
            },
        )
        employee = Employee(
            uuid=assignee_uuid,
            source="source",
            status="active",
            name="firstname lastname",
            first_name="firstname",
            surname="lastname",
            department={"uuid": uuid4(), "name": "dept"},
            roles=[
                {
                    "uuid": uuid4(),
                    "name": "role",
                    "parent": {"uuid": uuid4(), "name": "dept"},
                }
            ],
            contact_information={},
            related_custom_object=None,
        )
        employee_repo.find_employee_by_uuid.return_value = employee
        t.event_service = mock.MagicMock()
        task_repo.get_task.return_value = t
        self.commandmock.update_task(
            task_uuid=task_uuid,
            title="some new title",
            description="some new descr",
            due_date="2019-02-23",
            assignee=str(assignee_uuid),
        )
        assert t.title == "some new title"
        assert t.description == "some new descr"
        assert t.due_date == datetime.date.fromisoformat("2019-02-23")
        assert t.assignee == {
            "display_name": "",
            "type": "employee",
            "uuid": assignee_uuid,
        }

    def test_set_completion_on_task(self):
        task_repo = self.commandmock.repository_factory.repositories["task"]
        task_uuid = str(uuid4())
        t = Task(
            uuid=task_uuid,
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee={
                "type": "employee",
                "uuid": uuid4(),
                "display_name": "Ad Min",
            },
            case={"uuid": uuid4(), "status": "open", "milestone": 2},
            case_type=None,
            phase=2,
            department={
                "type": "department",
                "uuid": uuid4(),
                "display_name": "Backoffice",
            },
        )
        t.event_service = mock.MagicMock()

        task_repo.get_task.return_value = t
        t.case["status"] = "resolved"
        # raise error since task is not editable
        with pytest.raises(Conflict):
            self.commandmock.set_completion_on_task(
                task_uuid=task_uuid, completed=True
            )

        t.case["status"] = "open"
        self.commandmock.set_completion_on_task(
            task_uuid=task_uuid, completed=True
        )
        assert t.completed is True
        task_repo.save.assert_called_once()

    def test_delete_task(self):
        task_repo = self.commandmock.repository_factory.repositories["task"]
        task_uuid = str(uuid4())
        t = Task(
            uuid=task_uuid,
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee={
                "type": "employee",
                "uuid": uuid4(),
                "display_name": "Ad Min",
            },
            case={"uuid": uuid4(), "status": "open", "milestone": 2},
            case_type=None,
            phase=2,
            department={
                "type": "department",
                "uuid": uuid4(),
                "display_name": "Backoffice",
            },
        )
        t.event_service = mock.MagicMock()
        t.event_service.event_list = []
        task_repo.get_task.return_value = t
        t.phase = 1
        # raise error since task is not editable
        with pytest.raises(Conflict):
            self.commandmock.delete_task(task_uuid=task_uuid)

        t.phase = 2
        self.commandmock.delete_task(task_uuid=task_uuid)
        task_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_update_subject_relation(self, mock_subject_relation):
        relation_uuid = str(uuid4())

        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        self.commandmock.user_info = self.user_info
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        self.commandmock.update_subject_relation(
            relation_uuid=relation_uuid,
            role="Role",
            magic_string_prefix="role",
            authorized=None,
            permission="write",
        )
        mock_subject_relation.update.assert_called_once_with(
            role="Role",
            magic_string_prefix="role",
            authorized=None,
            permission="write",
        )
        subject_relation_repo.save.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.entities.subject_relation.SubjectRelation",
        autospec=True,
    )
    def test_delete_subject_relation(self, mock_subject_relation):
        relation_uuid = str(uuid4())

        subject_relation_repo = (
            self.commandmock.repository_factory.repositories[
                "subject_relation"
            ]
        )
        mock_subject_relation = mock.MagicMock()
        self.commandmock.user_info = self.user_info
        subject_relation_repo.find_subject_relation_by_uuid.return_value = (
            mock_subject_relation
        )

        self.commandmock.delete_subject_relation(relation_uuid=relation_uuid)

        mock_subject_relation.delete.assert_called_once_with()
        subject_relation_repo.save.assert_called_once()
