# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime, timedelta
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.entity import EntityCollection
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_GetRelatedCasesForContact(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

        self.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        self.qry.user_info = self.user_info

    def test_get_contact_related_cases(self):
        mock_cases = [mock.Mock(), mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]

        contact_uuid = uuid4()

        mock_cases[0].configure_mock(
            uuid=case_uuids[0],
            case_id=5,
            status="open",
            progress=0.5,
            summary="summary 1",
            result="result 1",
            case_type_version_uuid=case_type_uuids[0],
            case_type_version_title="Casetype 1",
            days_left=timedelta(days=10),
            betrokkenen_cache={
                str(contact_uuid): {
                    "roles": {
                        "1084": {
                            "role": "Aanvrager",
                            "authorisation": None,
                            "pip_authorized": False,
                        }
                    },
                    "pip_authorized": False,
                }
            },
            roles=["Aanvrager"],
            archival_state="vernietigen",
            vernietigingsdatum="2021-04-26T14:55:56.617921",
        )
        mock_cases[1].configure_mock(
            uuid=case_uuids[1],
            case_id=2,
            status="open",
            progress=0,
            summary="summary 2",
            result="result 2",
            case_type_version_uuid=case_type_uuids[1],
            case_type_version_title="Casetype 2",
            days_left=timedelta(days=-8),
            betrokkenen_cache={},
            archival_state="vernietigen",
            vernietigingsdatum="2021-04-26T14:55:56.617921",
        )

        self.session.execute().fetchall.return_value = mock_cases
        self.session.reset_mock()

        result = self.qry.get_contact_related_cases(
            contact_uuid=str(contact_uuid),
            page="1",
            page_size="10",
            include_cases_on_location="true",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 2

        assert cases[0].uuid == case_uuids[0]
        assert cases[0].entity_id == mock_cases[0].uuid
        assert cases[0].number == mock_cases[0].case_id
        assert cases[0].status == mock_cases[0].status
        assert cases[0].progress == mock_cases[0].progress
        assert cases[0].case_type.uuid == case_type_uuids[0]
        assert cases[0].case_type.entity_id == case_type_uuids[0]
        assert cases[0].case_type.entity_meta_summary == "Casetype 1"
        assert cases[0].days_left.amount == 10
        assert cases[0].days_left.unit == "days"
        assert cases[0].roles == ["Aanvrager"]

        assert cases[1].uuid == case_uuids[1]
        assert cases[1].entity_id == mock_cases[1].uuid
        assert cases[1].number == mock_cases[1].case_id
        assert cases[1].status == mock_cases[1].status
        assert cases[1].progress == mock_cases[1].progress
        assert cases[1].case_type.uuid == case_type_uuids[1]
        assert cases[1].case_type.entity_id == case_type_uuids[1]
        assert cases[1].case_type.entity_meta_summary == "Casetype 2"
        assert cases[1].days_left.amount == -8
        assert cases[1].days_left.unit == "days"
        assert cases[1].roles == []

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(dialect=postgresql.dialect())

        assert str(query) == (
            "SELECT 'natuurlijk_persoon' AS type, natuurlijk_persoon.id, adres.bag_id AS location \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN adres ON natuurlijk_persoon.adres_id = adres.id \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s UNION ALL SELECT 'bedrijf' AS type, bedrijf.id, CAST(bedrijf.vestiging_bag_id AS VARCHAR) AS location \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_2)s UNION ALL SELECT 'medewerker' AS type, subject.id, NULL AS location \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s AND subject.subject_type = %(subject_type_1)s"
        )

        (args, kwargs) = self.session.execute.call_args_list[1]
        query = args[0].compile(dialect=postgresql.dialect())

        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) ORDER BY case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

    def test_get_contact_related_cases_with_status_filter(self):
        mock_cases = [mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]
        contact_uuid = str(uuid4())

        mock_cases[0].configure_mock(
            uuid=case_uuids[0],
            case_id=5,
            status="open",
            progress=0.5,
            summary="summary 1",
            result="result 1",
            case_type_version_uuid=case_type_uuids[0],
            case_type_version_title="Casetype 1",
            days_left=timedelta(days=10),
            betrokkenen_cache={
                contact_uuid: {
                    "roles": {
                        "1014": {
                            "role": "Aanvrager",
                            "authorisation": None,
                            "pip_authorized": False,
                        }
                    },
                    "pip_authorized": False,
                }
            },
            archival_state="vernietigen",
            vernietigingsdatum="2021-04-26T14:55:56.617921",
        )

        self.session.execute().fetchall.return_value = mock_cases

        result = self.qry.get_contact_related_cases(
            contact_uuid=contact_uuid,
            page="1",
            page_size="10",
            status="open",
            sort="attributes.days_left",
            roles_to_include="Aanvrager",
            keyword="5",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        query = call_list[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak_meta, zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.status IN (%(status_3_1)s) AND zaak.aanvrager_gm_id = %(aanvrager_gm_id_1)s AND zaak.aanvrager_type = %(aanvrager_type_2)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) AND zaak.id = zaak_meta.zaak_id AND zaak_meta.text_vector @@ to_tsquery(%(text_vector_1)s) ORDER BY days_left ASC, case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        query = call_list[3][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak_meta, zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.status IN (%(status_2_1)s) AND zaak.aanvrager_gm_id = %(aanvrager_gm_id_1)s AND zaak.aanvrager_type = %(aanvrager_type_2)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) AND zaak.id = zaak_meta.zaak_id AND zaak_meta.text_vector @@ to_tsquery(%(text_vector_1)s)"
        )

    def test_get_contact_related_cases_without_filter(self):
        mock_cases = [mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]
        contact_uuid = str(uuid4())

        mock_cases[0].configure_mock(
            uuid=case_uuids[0],
            case_id=5,
            status="open",
            progress=0.5,
            summary="summary 1",
            result="result 1",
            case_type_version_uuid=case_type_uuids[0],
            case_type_version_title="Casetype 1",
            days_left=timedelta(days=10),
            betrokkenen_cache={
                contact_uuid: {
                    "roles": {
                        "1014": {
                            "role": "Aanvrager",
                            "authorisation": None,
                            "pip_authorized": False,
                        }
                    },
                    "pip_authorized": False,
                }
            },
            archival_state="vernietigen",
            vernietigingsdatum="2021-04-26T14:55:56.617921",
        )
        self.session.execute().fetchall.return_value = mock_cases

        result = self.qry.get_contact_related_cases(
            contact_uuid=contact_uuid,
            page="1",
            page_size="10",
            sort="attributes.days_left",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        query = call_list[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) ORDER BY days_left ASC, case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        query = call_list[3][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s)"
        )

    def test_get_contact_related_with_roles_filter(self):
        mock_cases = [mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]
        contact_uuid = str(uuid4())

        mock_cases[0].configure_mock(
            uuid=case_uuids[0],
            case_id=5,
            status="open",
            progress=0.5,
            summary="summary 1",
            result="result 1",
            case_type_version_uuid=case_type_uuids[0],
            case_type_version_title="Casetype 1",
            days_left=timedelta(days=10),
            betrokkenen_cache={
                contact_uuid: {
                    "roles": {
                        "1014": {
                            "role": "Aanvrager",
                            "authorisation": None,
                            "pip_authorized": False,
                        }
                    },
                    "pip_authorized": False,
                }
            },
            archival_state="vernietigen",
            vernietigingsdatum="2021-04-26T14:55:56.617921",
        )

        self.session.execute().fetchall.return_value = mock_cases

        result = self.qry.get_contact_related_cases(
            contact_uuid=contact_uuid,
            page="1",
            page_size="10",
            roles_to_exclude="Aanvrager",
            sort="attributes.days_left",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        query = call_list[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND NOT (zaak.aanvrager_gm_id = %(aanvrager_gm_id_1)s AND zaak.aanvrager_type = %(aanvrager_type_2)s) AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) ORDER BY days_left ASC, case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        query = call_list[3][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND NOT (zaak.aanvrager_gm_id = %(aanvrager_gm_id_1)s AND zaak.aanvrager_type = %(aanvrager_type_2)s) AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s)"
        )

        result = self.qry.get_contact_related_cases(
            contact_uuid=str(uuid4()),
            page="1",
            page_size="10",
            roles_to_exclude="Coordinator",
            sort="attributes.days_left",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 7

        query = call_list[5][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id != %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) ORDER BY days_left ASC, case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        query = call_list[6][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id != %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s)"
        )

        result = self.qry.get_contact_related_cases(
            contact_uuid=str(uuid4()),
            page="1",
            page_size="10",
            roles_to_exclude="Behandelaar",
            sort="attributes.days_left",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 10

        query = call_list[8][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id != %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) ORDER BY days_left ASC, case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        query = call_list[9][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id != %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s)"
        )

    def test_get_contact_related_behandelaar_role(self):
        mock_cases = [mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]
        contact_uuid = str(uuid4())

        mock_cases[0].configure_mock(
            uuid=case_uuids[0],
            case_id=5,
            status="open",
            progress=0.5,
            summary="summary 1",
            result="result 1",
            case_type_version_uuid=case_type_uuids[0],
            case_type_version_title="Casetype 1",
            days_left=timedelta(days=10),
            betrokkenen_cache={
                contact_uuid: {
                    "roles": {
                        "1014": {
                            "role": "Behandelaar",
                            "authorisation": None,
                            "pip_authorized": False,
                        }
                    },
                    "pip_authorized": False,
                }
            },
            archival_state="vernietigen",
            vernietigingsdatum="2021-04-26T14:55:56.617921",
        )

        self.session.execute().fetchall.return_value = mock_cases

        result = self.qry.get_contact_related_cases(
            contact_uuid=contact_uuid,
            page="1",
            page_size="10",
            roles_to_include="Behandelaar",
            sort="attributes.days_left",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        query = call_list[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id = %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) ORDER BY days_left ASC, case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        query = call_list[3][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id = %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s)"
        )

    def test_get_contact_related_coordinator_role(self):
        mock_cases = [mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]
        contact_uuid = str(uuid4())

        mock_cases[0].configure_mock(
            uuid=case_uuids[0],
            case_id=5,
            status="open",
            progress=0.5,
            summary="summary 1",
            result="result 1",
            case_type_version_uuid=case_type_uuids[0],
            case_type_version_title="Casetype 1",
            days_left=timedelta(days=10),
            betrokkenen_cache={
                contact_uuid: {
                    "roles": {
                        "1014": {
                            "role": "Coordinator",
                            "authorisation": None,
                            "pip_authorized": False,
                        }
                    },
                    "pip_authorized": False,
                }
            },
            archival_state="vernietigen",
            vernietigingsdatum=datetime.now(),
        )
        self.session.execute().fetchall.return_value = mock_cases

        result = self.qry.get_contact_related_cases(
            contact_uuid=contact_uuid,
            page="1",
            page_size="10",
            roles_to_include="Coordinator",
            sort="attributes.days_left",
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        query = call_list[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT zaak.uuid, zaak.id AS case_id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, zaak.betrokkenen_cache, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaak.archival_state AS archival_state, zaak.vernietigingsdatum AS vernietigingsdatum, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, zaak_bag.bag_id, zaak_bag.bag_type, CASE WHEN (zaak.status = %(status_1)s) THEN NULL ELSE CASE WHEN (zaak.streefafhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.streefafhandeldatum END - CASE WHEN (zaak.afhandeldatum IS NULL) THEN CURRENT_DATE ELSE zaak.afhandeldatum END END AS days_left \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id = %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s) ORDER BY days_left ASC, case_id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        query = call_list[3][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN zaak_bag ON zaak.locatie_zaak = zaak_bag.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.behandelaar_gm_id = %(behandelaar_gm_id_1)s AND (zaak.betrokkenen_cache ? %(betrokkenen_cache_1)s)"
        )
