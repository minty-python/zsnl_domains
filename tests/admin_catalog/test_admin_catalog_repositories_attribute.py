# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from minty.cqrs.events import Event, EventService
from minty.exceptions import Conflict, NotFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.entities.attribute import Attribute
from zsnl_domains.admin.catalog.repositories.attribute import (
    AttributeRepository,
)


class TestAttributeRepository:
    def setup(self):
        self.mock_infra = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        self.attribute_repo = AttributeRepository(
            infrastructure_factory=self.mock_infra,
            context="context",
            event_service=event_service,
        )

    @mock.patch.object(AttributeRepository, "_transform_to_entity")
    def test_get_attribute_details_by_uuid(self, mock_transform_to_entity):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        mock_transform_to_entity.return_value = "attribute_entity"

        res = self.attribute_repo.get_attribute_details_by_uuid(uuid=uuid4())
        assert res == "attribute_entity"

    def test_get_attribute_details_by_uuid_not_found(self):
        uuid = str(uuid4())
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        mock_ses.query().outerjoin().outerjoin().filter().one.side_effect = (
            NoResultFound
        )

        with pytest.raises(NotFound) as excinfo:
            self.attribute_repo.get_attribute_details_by_uuid(uuid=uuid)

        assert excinfo.value.args == (
            f"Attribute with uuid {uuid} not found",
            "attribute/not_found",
        )

    def test__transform_to_entity_type_file(self):

        mock_attribute = mock.MagicMock()
        mock_attribute.value_type = "file"
        mock_attribute.properties = {"sensitive_field": "on"}

        mock_category = mock.MagicMock()
        mock_category.naam = "category_name"
        mock_category.uuid = uuid4()

        mock_file_metadata = mock.MagicMock()
        mock_file_metadata.origin = "Intern"
        mock_file_metadata.document_category = "Advies"

        query_result = (mock_attribute, mock_category, mock_file_metadata)

        res = self.attribute_repo._transform_to_entity(
            query_result=query_result, attribute_values=[]
        )

        assert isinstance(res, Attribute)
        assert res.attribute_type == "file"
        assert res.sensitive_field is True
        assert res.attribute_values == []

        assert res.category_name == "category_name"
        assert res.category_uuid == mock_category.uuid

        assert res.document_origin == "Intern"
        assert res.document_category == "Advies"

    def test__transform_to_entity_type_checkbox(self):

        uuid = uuid4()
        mock_attribute = mock.MagicMock()
        mock_attribute.value_type = "checkbox"
        mock_attribute.properties = {}
        mock_attribute.uuid = uuid

        mock_category = mock.MagicMock()
        mock_category.naam = "category_name"
        mock_category.id = uuid4()

        query_result = (mock_attribute, mock_category, None)

        res = self.attribute_repo._transform_to_entity(
            query_result=query_result, attribute_values="attribute_values"
        )

        assert isinstance(res, Attribute)
        assert res.entity_id == uuid
        assert res.attribute_type == "checkbox"
        assert res.category_name == "category_name"
        assert res.category_uuid == mock_category.uuid
        assert res.attribute_values == "attribute_values"

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.attribute_values_query"
    )
    def test__get_attribute_values(self, mock_attribute_values_query):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        mock_attribute_value = mock.MagicMock()
        mock_attribute_value.id = 1
        mock_attribute_value.active = True
        mock_attribute_value.sort_order = 0

        mock_ses.execute().fetchall.return_value = [mock_attribute_value]

        res = self.attribute_repo._get_attribute_values(
            attibute_id=uuid4(), attribute_type="checkbox"
        )
        assert isinstance(res[0], dict)
        assert res[0]["active"] is True
        assert res[0]["sort_order"] == 0

    @mock.patch.object(AttributeRepository, "get_attribute_details_by_uuid")
    def test__edit_attribute(self, mock_get_attribute_details):
        self.attribute_repo.event_service.log_event(
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "name",
                    "old_value": "old_name",
                    "new_value": "new_name",
                },
                {
                    "key": "sensitive_field",
                    "old_value": False,
                    "new_value": True,
                },
            ],
            entity_data={"attribute_type": "text"},
        )

        mock_get_attribute_details.id.return_value = 1
        self.attribute_repo.save()

    @mock.patch.object(AttributeRepository, "_is_valid_magic_string")
    def test__insert_attribute(self, mock_is_valid_magic_string):
        mock_is_valid_magic_string.return_value = True

        self.attribute_repo.event_service.log_event(
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeCreated",
            changes=[
                {
                    "key": "name",
                    "old_value": "old_name",
                    "new_value": "new_name",
                },
                {
                    "key": "sensitive_field",
                    "old_value": False,
                    "new_value": True,
                },
                {
                    "key": "magic_string",
                    "old_value": None,
                    "new_value": "doc_variable",
                },
                {
                    "key": "attribute_type",
                    "old_value": None,
                    "new_value": "text",
                },
            ],
            entity_data={"attribute_type": "text"},
        )
        self.attribute_repo.save()

    @mock.patch.object(AttributeRepository, "_is_valid_magic_string")
    def test_insert_attribute_conflict_uuid(self, mock_is_valid_magic_string):
        uuid = uuid4()
        category_uuid = uuid4()
        mock_is_valid_magic_string.return_value = True

        self.attribute_repo.event_service.log_event(
            entity_type="Attribute",
            entity_id=uuid,
            event_name="AttributeCreated",
            changes=[
                {"key": "uuid", "old_value": None, "new_value": str(uuid)},
                {
                    "key": "name",
                    "old_value": "old_name",
                    "new_value": "new_name",
                },
                {
                    "key": "category_uuid",
                    "old_value": None,
                    "new_value": category_uuid,
                },
                {
                    "key": "attribute_type",
                    "old_value": None,
                    "new_value": "text",
                },
            ],
            entity_data={"attribute_type": "text"},
        )

        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        mock_ses.execute.side_effect = IntegrityError(
            statement=None, params={}, orig=Exception
        )

        with pytest.raises(Conflict) as excinfo:
            self.attribute_repo.save()

        assert excinfo.value.args == (
            f"Attribute with uuid {uuid} already exists",
            "attribute/already_exists_with_uuid",
        )

    @mock.patch.object(AttributeRepository, "_is_valid_magic_string")
    def test_insert_attribute_conflict_magic_string(
        self, mock_is_valid_magic_string
    ):
        uuid = uuid4()
        category_uuid = uuid4()
        self, mock_is_valid_magic_string
        mock_is_valid_magic_string.return_value = False

        self.attribute_repo.event_service.log_event(
            entity_type="Attribute",
            entity_id=uuid,
            event_name="AttributeCreated",
            changes=[
                {"key": "uuid", "old_value": None, "new_value": str(uuid)},
                {
                    "key": "name",
                    "old_value": "old_name",
                    "new_value": "new_name",
                },
                {
                    "key": "category_uuid",
                    "old_value": None,
                    "new_value": category_uuid,
                },
                {
                    "key": "magic_string",
                    "old_value": None,
                    "new_value": "doc_varibale_1",
                },
            ],
            entity_data={"attribute_type": "text"},
        )

        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        mock_ses.execute.side_effect = IntegrityError(
            statement=None, params={}, orig=Exception
        )

        with pytest.raises(Conflict) as excinfo:
            self.attribute_repo.save()

        assert excinfo.value.args == (
            "Magic string 'doc_varibale_1' is invalid.",
            "attribute/invalid_magic_string",
        )

    def test_create_new_attribute(self):
        uuid = uuid4()
        category_uuid = uuid4()
        res = self.attribute_repo.create_new_attribute(
            uuid=uuid,
            fields={
                "name": "xyz",
                "magic_string": "doc_varibale_1",
                "category_uuid": category_uuid,
                "attribute_type": "text",
            },
        )

        assert res.uuid == uuid
        assert res.id is None
        assert res.name == "xyz"
        assert res.category_uuid == category_uuid
        assert res.magic_string == "doc_varibale_1"
        assert res.attribute_type == "text"

    def test_create_attribute_of_type_address_v2(self):
        uuid = uuid4()
        category_uuid = uuid4()
        res = self.attribute_repo.create_new_attribute(
            uuid=uuid,
            fields={
                "name": "test address_v2 type",
                "magic_string": "test_address_v2",
                "category_uuid": category_uuid,
                "attribute_type": "address_v2",
            },
        )

        assert res.uuid == uuid
        assert res.id is None
        assert res.name == "test address_v2 type"
        assert res.category_uuid == category_uuid
        assert res.magic_string == "test_address_v2"
        assert res.attribute_type == "address_v2"

    def test_create_attribute_of_type_appointment_v2(self):
        uuid = uuid4()
        category_uuid = uuid4()
        interface_uuid = uuid4()
        res = self.attribute_repo.create_new_attribute(
            uuid=uuid,
            fields={
                "name": "test appointment_v2 type",
                "magic_string": "test_appointment_v2",
                "category_uuid": category_uuid,
                "attribute_type": "appointment_v2",
                "appointment_interface_uuid": str(interface_uuid),
            },
        )

        assert res.uuid == uuid
        assert res.id is None
        assert res.name == "test appointment_v2 type"
        assert res.category_uuid == category_uuid
        assert res.magic_string == "test_appointment_v2"
        assert res.attribute_type == "appointment_v2"
        assert res.appointment_interface_uuid == str(interface_uuid)

    def test__get_bibliotheek_category(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        mock_category = mock.MagicMock()
        mock_category.id = 2

        mock_ses.query().filter().one.return_value = mock_category

        res = self.attribute_repo._get_bibliotheek_category(
            category_uuid=uuid4()
        )

        assert res.id == 2

    def test__get_bibliotheek_category_not_found(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        category_uuid = uuid4()
        mock_ses.query().filter().one.side_effect = NoResultFound

        with pytest.raises(NotFound) as excinfo:
            self.attribute_repo._get_bibliotheek_category(
                category_uuid=category_uuid
            )

        assert excinfo.value.args == (
            f"Bibliotheek category with uuid {category_uuid} not found",
            "bibliotheek_category/not_found",
        )

    def test_generate_magic_string(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        mock_ses.query().filter().all.return_value = []

        res = self.attribute_repo.generate_magic_string(
            string_input="doc_varibale;+ "
        )
        assert res == "doc_varibale_"

    def test_generate_magic_string_invalid_magic_string(self):
        class MockDataBaseQuery:
            def __init__(self, fail_after=1):
                self.count = 0
                self.fail_after = fail_after

            def __call__(self):
                called = self.count
                self.count = self.count + 1
                if called > self.fail_after:
                    return []
                return ["doc_varibale"]

        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        mock_ses.query().filter().all = MockDataBaseQuery(fail_after=2)

        res = self.attribute_repo.generate_magic_string(
            string_input="doc_varibale 1"
        )

        assert res == "doc_varibale_4"

        mock_ses.query().filter().all = MockDataBaseQuery(fail_after=2)

        res = self.attribute_repo.generate_magic_string(
            string_input="doc_varibale"
        )

        assert res == "doc_varibale3"

    def test_generate_magic_string_timeout(self):
        class MockDataBaseQuery:
            def __init__(self, fail_after=1):
                self.count = 0
                self.fail_after = fail_after

            def __call__(self):
                called = self.count
                self.count = self.count + 1
                if called > self.fail_after:
                    return []
                return ["doc_varibale"]

        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        mock_ses.query().filter().all = MockDataBaseQuery(fail_after=26)

        with pytest.raises(TimeoutError) as excinfo:
            self.attribute_repo.generate_magic_string(
                string_input="doc_varibale_1"
            )

        assert excinfo.value.args == (
            "Cannot generate magic_string. Try with different magic_string",
            "attribute/magic_string_cannot_be_generated",
        )

    def test_generate_magic_string_empty_string(self):
        class MockDataBaseQuery:
            def __init__(self, fail_after=1):
                self.count = 0
                self.fail_after = fail_after

            def __call__(self):
                called = self.count
                self.count = self.count + 1
                if called > self.fail_after:
                    return []
                return ["doc_varibale"]

        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        mock_ses.query().filter().all = MockDataBaseQuery(fail_after=2)

        res = self.attribute_repo.generate_magic_string(string_input="")
        assert res == "default_magic_string3"

    def test__is_valid_magic_string_empty_string(self):
        res = self.attribute_repo._is_valid_magic_string(string_input="")
        assert res is False

    def test_save_attribute_values(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        changes = [
            {
                "key": "attribute_values",
                "old_value": None,
                "new_value": [
                    {"id": 1, "value": "a", "active": True, "sort_order": 0}
                ],
            }
        ]

        self.attribute_repo._save_attribute_values(1, changes)

    def test_edit_attribute_file(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        event = Event(
            uuid=uuid4(),
            created_date="2019-05-31",
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "document_trust_level",
                    "old_value": None,
                    "new_value": "trust_level",
                }
            ],
            entity_data={"attribute_type": "file"},
        )

        self.attribute_repo._edit_attribute(event)

    def test_edit_attribute_checkbox(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        event = Event(
            uuid=uuid4(),
            created_date="2019-05-31",
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "attribute_values",
                    "old_value": None,
                    "new_value": [
                        {
                            "id": 1,
                            "value": "a",
                            "active": True,
                            "sort_order": 0,
                        }
                    ],
                }
            ],
            entity_data={"attribute_type": "checkbox"},
        )

        self.attribute_repo._edit_attribute(event)

    def test_insert_attribute_file(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        event = Event(
            uuid=uuid4(),
            created_date="2019-05-31",
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "document_origin",
                    "old_value": None,
                    "new_value": "origin",
                }
            ],
            entity_data={"attribute_type": "file"},
        )

        self.attribute_repo._insert_attribute(event)

    def test_insert_attribute_checkbox(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        event = Event(
            uuid=uuid4(),
            created_date="2019-05-31",
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "attribute_values",
                    "old_value": None,
                    "new_value": [
                        {
                            "id": 1,
                            "value": "a",
                            "active": True,
                            "sort_order": 0,
                        }
                    ],
                }
            ],
            entity_data={"attribute_type": "checkbox"},
        )

        self.attribute_repo._insert_attribute(event)

    def test_edit_attribute_appointment(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        event = Event(
            uuid=uuid4(),
            created_date="2019-05-31",
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "appointment_location_id",
                    "old_value": None,
                    "new_value": "1",
                },
                {
                    "key": "appointment_interface_uuid",
                    "old_value": None,
                    "new_value": str(uuid4()),
                },
            ],
            entity_data={"attribute_type": "appointment"},
        )

        self.attribute_repo._edit_attribute(event)

    def test_insert_attribute_appointemnt(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        event = Event(
            uuid=uuid4(),
            created_date="2019-05-31",
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "appointment_location_id",
                    "old_value": None,
                    "new_value": "1",
                },
                {
                    "key": "appointment_interface_uuid",
                    "old_value": None,
                    "new_value": str(uuid4()),
                },
            ],
            entity_data={"attribute_type": "appointment"},
        )

        self.attribute_repo._insert_attribute(event)

    def test_edit_attribute_appointment_no_active_integrations(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        event = Event(
            uuid=uuid4(),
            created_date="2019-05-31",
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Attribute",
            entity_id=uuid4(),
            event_name="AttributeEdited",
            changes=[
                {
                    "key": "appointment_location_id",
                    "old_value": None,
                    "new_value": "1",
                },
                {
                    "key": "appointment_interface_uuid",
                    "old_value": str(uuid4()),
                    "new_value": None,
                },
            ],
            entity_data={"attribute_type": "appointment"},
        )

        with pytest.raises(Conflict) as excinfo:
            self.attribute_repo._edit_attribute(event)

        excinfo.value.args == (
            "No active appointment integrations found",
            "attribute/no_active_appointment_integrations",
        )

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.attribute.attribute_search_query"
    )
    def test_search_attribute_by_name(self, mock_search_attribute):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        qry_res = namedtuple(
            "QueryResult", "id uuid name magic_string value_type"
        )
        m_uuid = uuid4()
        db_return = [
            qry_res(
                name="name1",
                id=1,
                uuid=m_uuid,
                magic_string="name1_1",
                value_type="file",
            )
        ]

        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        search_string = "Name"
        attr_type = "file"

        mock_ses.execute().fetchall.return_value = db_return

        results = self.attribute_repo.search_attribute_details_by_name(
            search_string=search_string, attribute_type=attr_type
        )

        mock_search_attribute.assert_called_once_with(
            search_string=search_string.lower(), attr_type=attr_type
        )

        assert results[0].id == 1
        assert results[0].uuid == m_uuid
        assert results[0].name == "name1"
        assert results[0].magic_string == "name1_1"
        assert results[0].value_type == "file"

    def test_delete_attribute(self):
        attribute_uuid = str(uuid4())

        res = self.attribute_repo.delete_attribute(
            uuid=attribute_uuid, reason="No more used"
        )

        assert res.uuid == attribute_uuid
        assert res.commit_message == "No more used"

    @mock.patch.object(
        AttributeRepository, "_get_case_types_related_to_attribute"
    )
    def test__delete_attribute(self, mock_get_case_types):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses
        attribute_uuid = uuid4()
        self.attribute_repo.event_service.log_event(
            entity_type="Attribute",
            entity_id=attribute_uuid,
            event_name="AttributeDeleted",
            changes=[
                {
                    "key": "deleted",
                    "old_value": None,
                    "new_value": "2019-07-08 11:09:02.885741",
                },
                {
                    "key": "commit_message",
                    "old_value": None,
                    "new_value": "Deleting the unsed attribute",
                },
            ],
            entity_data={},
        )
        mock_get_case_types.return_value = 0

        self.attribute_repo.save()
        mock_get_case_types.assert_called_once_with(uuid=attribute_uuid)

        with pytest.raises(Conflict) as excinfo:
            mock_get_case_types.return_value = 5
            self.attribute_repo.save()

        assert excinfo.value.args == (
            "Attribute is used in case types",
            "attribute/used_in_case_types",
        )

    def test__get_case_types_related_to_attribute(self):
        mock_ses = mock.MagicMock()
        self.attribute_repo.session = mock_ses

        mock_ses.execute().fetchone.return_value = [3]
        res = self.attribute_repo._get_case_types_related_to_attribute(
            uuid=uuid4()
        )
        assert res == 3

        mock_ses.execute().fetchone.return_value = [0]
        res = self.attribute_repo._get_case_types_related_to_attribute(
            uuid=uuid4()
        )
        assert res == 0

    def test__verify_correct_relationship_data(self):
        mock_ses = mock.MagicMock()
        mock_ses.session.query().one_or_none.return_value = {
            "name": "test_ralation"
        }
        update_values = {}
        update_values["relationship_type"] = "case"
        update_values["relationship_uuid"] = uuid4()
        update_values["relationship_name"] = "test_relation"
        self.attribute_repo._verify_correct_relationship_data(update_values)
        assert update_values["relationship_uuid"] is None
        assert update_values["relationship_name"] is None

        update_values["relationship_type"] = "document"
        update_values["relationship_uuid"] = uuid4()
        update_values["relationship_name"] = "test_relation"
        self.attribute_repo._verify_correct_relationship_data(update_values)
        assert update_values["relationship_uuid"] is None
        assert update_values["relationship_name"] is None

        update_values["relationship_type"] = "subject"
        update_values["relationship_uuid"] = uuid4()
        update_values["relationship_name"] = "test_relation"
        self.attribute_repo._verify_correct_relationship_data(update_values)
        assert update_values["relationship_uuid"] is None
        assert update_values["relationship_name"] is None

        update_values["relationship_type"] = "custom_object"
        update_values["relationship_uuid"] = None
        update_values["relationship_name"] = "test_relation"

        with pytest.raises(Conflict) as excinfo:
            self.attribute_repo._verify_correct_relationship_data(
                update_values
            )

        assert excinfo.value.args == (
            "Attribute with type 'relationship' has no uuid",
            "attribute/no_relationship_uuid_set",
        )

        update_values["relationship_type"] = None
        with pytest.raises(Conflict) as excinfo:
            self.attribute_repo._verify_correct_relationship_data(
                update_values
            )

        assert excinfo.value.args == (
            "Attribute with type 'relationship' has no type",
            "attribute/no_relationship_type_set",
        )
