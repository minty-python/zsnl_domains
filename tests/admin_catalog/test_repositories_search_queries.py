# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from zsnl_domains.admin.catalog.repositories import search_queries


class TestSearchQueries:
    def test_attribute_search_query(self):
        search_string = "document"
        attr_type = "file"
        qry = search_queries.attribute_search_query(
            search_string=search_string, attr_type=attr_type
        )
        qry_check = (
            "SELECT bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.id, "
            "bibliotheek_kenmerken.naam AS name, bibliotheek_kenmerken.magic_string, "
            "bibliotheek_kenmerken.value_type \nFROM bibliotheek_kenmerken"
            " \nWHERE lower(bibliotheek_kenmerken.naam) LIKE lower(:naam_1) AND "
            "bibliotheek_kenmerken.deleted IS NULL AND "
            "bibliotheek_kenmerken.value_type = :value_type_1"
        )
        assert str(qry) == qry_check

        qry2 = search_queries.attribute_search_query(search_string, None)
        qry2_check = (
            "SELECT bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.id, "
            "bibliotheek_kenmerken.naam AS name, bibliotheek_kenmerken.magic_string,"
            " bibliotheek_kenmerken.value_type \nFROM bibliotheek_kenmerken \nWHERE"
            " lower(bibliotheek_kenmerken.naam) LIKE lower(:naam_1) AND bibliotheek_kenmerken.deleted IS NULL"
        )

        assert str(qry2) == qry2_check
