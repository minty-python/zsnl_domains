# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import sqlalchemy
from collections import namedtuple
from datetime import datetime
from minty.exceptions import NotFound
from sqlalchemy.sql.elements import BinaryExpression, BooleanClauseList, Null
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog import entities
from zsnl_domains.admin.catalog.repositories.catalog import (
    Catalog,
    _inflate_folder_entry,
)
from zsnl_domains.admin.catalog.repositories.database_queries import (
    case_types_related_to_attribute,
    case_types_related_to_case_type,
    case_types_related_to_document_template,
    case_types_related_to_email_template,
    get_folder_contents_query,
    search_catalog_query,
)
from zsnl_domains.database import schema


class TestHelpers:
    def test__inflate_folder_entry(self):
        test_uuid = str(uuid4())
        row = mock.Mock()
        row.configure_mock(
            type="folder", uuid=test_uuid, name="Testfolder", active=True
        )
        fe = _inflate_folder_entry(row)

        assert isinstance(fe, entities.FolderEntry)
        assert fe.entry_type == row.type
        assert fe.uuid == row.uuid
        assert fe.name == row.name
        assert fe.active == row.active

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.folder_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.case_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.attribute_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.email_template_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.document_template_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.object_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.custom_object_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.union_all"
    )
    def test__get_folder_contents_query(
        self,
        mock_union,
        mock_custom_object_type_query,
        mock_object_type_query,
        mock_document_template_query,
        mock_email_template_query,
        mock_attribute_query,
        mock_case_type_query,
        mock_folder_query,
    ):
        union_all = mock.MagicMock()
        union_all.order_by.return_value = "order_by"
        mock_union.return_value = union_all
        get_folder_contents_query(123)

        args, kwargs = mock_folder_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.BibliotheekCategorie.pid
        assert args[0].right.value == 123

        args, kwargs = mock_case_type_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.Zaaktype.bibliotheek_categorie_id
        assert args[0].right.value == 123

        args, kwargs = mock_attribute_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert (
            args[0].left == schema.BibliotheekKenmerk.bibliotheek_categorie_id
        )
        assert args[0].right.value == 123

        args, kwargs = mock_email_template_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert (
            args[0].left
            == schema.BibliotheekNotificatie.bibliotheek_categorie_id
        )
        assert args[0].right.value == 123

        args, kwargs = mock_document_template_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert (
            args[0].left == schema.BibliotheekSjabloon.bibliotheek_categorie_id
        )
        assert args[0].right.value == 123

        args, kwargs = mock_object_type_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert (
            args[0].left
            == schema.ObjectBibliotheekEntry.bibliotheek_categorie_id
        )
        assert args[0].right.value == 123

        args, kwargs = mock_custom_object_type_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.CustomObjectType.catalog_folder_id
        assert args[0].right.value == 123

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.folder_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.case_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.attribute_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.document_template_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.email_template_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.object_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.custom_object_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.union_all"
    )
    def test__search_catalog_query(
        self,
        mock_union,
        mock_custom_object_type_query,
        mock_object_type_query,
        mock_email_template_query,
        mock_document_template_query,
        mock_attribute_query,
        mock_case_type_query,
        mock_folder_query,
    ):
        union_all = mock.MagicMock()
        union_all.order_by.return_value = "ordered_result"
        mock_union.return_value = union_all
        res = search_catalog_query("testing catalog")
        assert res.order_by.return_value == "ordered_result"

        args, kwargs = mock_folder_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.BibliotheekCategorie.naam
        assert args[0].right.value == f"%{'testing catalog'}%"

        args, kwargs = mock_case_type_query.where.call_args
        first_or_clause = "lower(zaaktype_node.titel) LIKE lower(:titel_1) "
        second_or_clause = "OR lower(zaaktype_node.zaaktype_trefwoorden) LIKE lower(:zaaktype_trefwoorden_1)"
        assert isinstance(args[0], BooleanClauseList)
        assert str(args[0]) == first_or_clause + second_or_clause

        args, kwargs = mock_attribute_query.where.call_args
        assert isinstance(args[0], BooleanClauseList)

        args, kwargs = mock_document_template_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.BibliotheekSjabloon.naam
        assert args[0].right.value == f"%{'testing catalog'}%"

        args, kwargs = mock_email_template_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.BibliotheekNotificatie.label
        assert args[0].right.value == f"%{'testing catalog'}%"

        args, kwargs = mock_object_type_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.ObjectBibliotheekEntry.name
        assert args[0].right.value == f"%{'testing catalog'}%"

        args, kwargs = mock_custom_object_type_query.where.call_args
        assert isinstance(args[0], BinaryExpression)
        assert args[0].left == schema.CustomObjectTypeVersion.name
        assert args[0].right.value == f"%{'testing catalog'}%"

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.folder_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.case_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.attribute_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.document_template_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.email_template_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.object_type_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.custom_object_type_query"
    )
    def test__search_catalog_query_with_type_filter(
        self,
        mock_custom_object_type_query,
        mock_object_type_query,
        mock_email_template_query,
        mock_document_template_query,
        mock_attribute_query,
        mock_case_type_query,
        mock_folder_query,
    ):
        # Search catalog by filter folder
        mock_folder_query.where().order_by.return_value = ["test folder"]
        res = search_catalog_query("test", type="folder")

        assert res == ["test folder"]
        args, kwargs = mock_folder_query.where.call_args
        assert args[0].left == schema.BibliotheekCategorie.naam
        assert args[0].right.value == f"%{'test'}%"

        # Search catalog by filter case_type
        mock_case_type_query.where().order_by.return_value = ["test case_type"]
        res = search_catalog_query("test", type="case_type")

        assert res == ["test case_type"]
        args, kwargs = mock_case_type_query.where.call_args
        assert args[0].clauses[0].left == schema.ZaaktypeNode.titel
        assert args[0].clauses[0].right.value == f"%{'test'}%"
        assert (
            args[0].clauses[1].left == schema.ZaaktypeNode.zaaktype_trefwoorden
        )
        assert args[0].clauses[1].right.value == f"%{'test'}%"

        # Search catalog by filter attribute
        mock_attribute_query.where().order_by.return_value = ["test attribute"]
        res = search_catalog_query("test", type="attribute")

        assert res == ["test attribute"]
        args, kwargs = mock_attribute_query.where.call_args
        assert args[0].clauses[0].left == schema.BibliotheekKenmerk.naam
        assert args[0].clauses[0].right.value == f"%{'test'}%"
        assert args[0].clauses[1].left == schema.BibliotheekKenmerk.deleted
        assert isinstance(args[0].clauses[1].right, Null)

        # Search catalog by filter document_template
        mock_document_template_query.where().order_by.return_value = [
            "test document_template"
        ]
        res = search_catalog_query("test", type="document_template")

        assert res == ["test document_template"]
        args, kwargs = mock_document_template_query.where.call_args
        assert args[0].left == schema.BibliotheekSjabloon.naam
        assert args[0].right.value == f"%{'test'}%"

        # Search catalog by filter email_template
        mock_email_template_query.where().order_by.return_value = [
            "test email_template"
        ]
        res = search_catalog_query("test", type="email_template")

        assert res == ["test email_template"]
        args, kwargs = mock_email_template_query.where.call_args
        assert args[0].left == schema.BibliotheekNotificatie.label
        assert args[0].right.value == f"%{'test'}%"

        # Search catalog by filter object_type
        mock_object_type_query.where().order_by.return_value = [
            "test object_type"
        ]
        res = search_catalog_query("test", type="object_type")

        assert res == ["test object_type"]
        args, kwargs = mock_object_type_query.where.call_args
        assert args[0].left == schema.ObjectBibliotheekEntry.name
        assert args[0].right.value == f"%{'test'}%"

        # Search catalog by filter custom_object_type
        mock_custom_object_type_query.where().order_by.return_value = [
            "test custom_object_type"
        ]
        res = search_catalog_query("test", type="custom_object_type")

        assert res == ["test custom_object_type"]
        args, kwargs = mock_custom_object_type_query.where.call_args
        assert args[0].left == schema.CustomObjectTypeVersion.name
        assert args[0].right.value == f"%{'test'}%"


class TestCatalog:
    def test__expect_one(self):
        mock_self = mock.MagicMock()

        assert Catalog._expect_one(mock_self, ["a"], "foo") == "a"

        with pytest.raises(KeyError):
            Catalog._expect_one(mock_self, [], "foo")
        with pytest.raises(KeyError):
            Catalog._expect_one(mock_self, ["a", "b"], "foo")

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.get_folder_contents_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.catalog._inflate_folder_entry"
    )
    def test_catalog_get_folder_contents(
        self, mock_inflate, mock_get_folder_contents
    ):
        nt = namedtuple("Folder", "id")
        mock_self = mock.MagicMock()
        mock_self._expect_one.return_value = nt(id=42)
        mock_self._get_infrastructure().execute().fetchall.return_value = [
            1,
            2,
            3,
        ]
        mock_self.reset_mock(return_value=False)

        mock_inflate.return_value = "inflated"

        res = Catalog.get_folder_contents(mock_self)
        mock_self._get_infrastructure.assert_called_once_with("database")

        mock_self._expect_one.assert_not_called()

        assert res == ["inflated", "inflated", "inflated"]

        # And with a folder UUID:
        mock_self.reset_mock(return_value=False)
        folder_uuid = str(uuid4())
        res = Catalog.get_folder_contents(mock_self, folder_uuid=folder_uuid)
        mock_self._get_infrastructure.assert_called_once_with("database")

        assert res == ["inflated", "inflated", "inflated"]

        # And now with no results:
        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError
        with pytest.raises(NotFound):
            Catalog.get_folder_contents(mock_self, folder_uuid=folder_uuid)

    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.database_queries.search_catalog_query"
    )
    @mock.patch(
        "zsnl_domains.admin.catalog.repositories.catalog._inflate_folder_entry"
    )
    def test_search_catalog(self, mock_inflate, mock_search_catalog_query):
        nt = namedtuple("Folder", "id")
        mock_self = mock.MagicMock()
        mock_self._expect_one.return_value = nt(id=42)
        mock_self._get_infrastructure().execute().fetchall.return_value = [
            1,
            2,
            3,
        ]
        mock_self.reset_mock(return_value=False)

        mock_inflate.return_value = "inflated"

        res = Catalog.search_catalog(mock_self)
        mock_self._get_infrastructure.assert_called_once_with("database")

        mock_self._expect_one.assert_not_called()

        assert res == ["inflated", "inflated", "inflated"]

        # And with a search keyword:
        mock_self.reset_mock(return_value=False)
        keyword = "search keyword"
        res = Catalog.search_catalog(mock_self, keyword=keyword)
        mock_self._get_infrastructure.assert_called_once_with("database")

        assert res == ["inflated", "inflated", "inflated"]

        # And now with no results:
        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError
        with pytest.raises(KeyError):
            Catalog.search_catalog(mock_self, keyword=keyword)

    def test_case_types_related_to_case_type(self):
        res = case_types_related_to_case_type(42)

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)
        assert res.subquery().columns.keys() == [
            "uuid",
            "active",
            "version",
            "name",
            "is_current_version",
        ]

    def test_case_types_related_to_attribute(self):
        res = case_types_related_to_attribute(42)

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)
        assert res.subquery().columns.keys() == [
            "uuid",
            "active",
            "version",
            "name",
            "is_current_version",
        ]

    def test_case_types_related_to_email_template(self):
        res = case_types_related_to_email_template(42)

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)
        assert res.subquery().columns.keys() == [
            "uuid",
            "active",
            "version",
            "name",
            "is_current_version",
        ]

    def test_case_types_related_to_document_template(self):
        res = case_types_related_to_document_template(42)

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)
        assert res.subquery().columns.keys() == [
            "uuid",
            "active",
            "version",
            "name",
            "is_current_version",
        ]

    def prepare__transform_to_entity(
        self, mock_self, self_uuid, parent_uuid, now
    ):
        mock_self._transform_to_entity.return_value = entities.Folder(
            id=None,
            uuid=self_uuid,
            name="name",
            parent_name="parent_folder",
            parent_uuid=parent_uuid,
            last_modified=now,
        )

        return mock_self

    def test_get_folder_details(self):
        nt = namedtuple(
            "Folder", "name uuid parent_name parent_uuid last_modified"
        )
        mock_self = mock.MagicMock()

        # Root folder doesn't have a lot of interesting details
        res = Catalog.get_folder_details(mock_self, None)
        assert isinstance(res, entities.Folder)
        assert res.name == "Catalog"
        assert res.uuid is None
        assert res.parent_name is None
        assert res.parent_uuid is None
        assert res.last_modified is None

        # Try again, with an actual folder UUID
        mock_self.reset_mock(return_value=False)

        now = datetime.now()
        self_uuid = str(uuid4())
        parent_uuid = str(uuid4())
        mock_self._expect_one.return_value = nt(
            name="name",
            uuid=self_uuid,
            parent_name="parent_folder",
            parent_uuid=parent_uuid,
            last_modified=now,
        )

        mock_self = self.prepare__transform_to_entity(
            mock_self, self_uuid, parent_uuid, now
        )

        res = Catalog.get_folder_details(mock_self, self_uuid)
        assert isinstance(res, entities.Folder)
        assert res.name == "name"
        assert res.uuid == self_uuid
        assert res.parent_name == "parent_folder"
        assert res.parent_uuid == parent_uuid
        assert res.last_modified == now

        # And again, with a "non-existent" folder UUID
        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError

        with pytest.raises(NotFound):
            Catalog.get_folder_details(mock_self, self_uuid)

    def test_get_folder_details_by(self):
        nt = namedtuple(
            "Folder", "name uuid parent_name parent_uuid last_modified"
        )
        mock_self = mock.MagicMock()

        # Root folder doesn't have a lot of interesting details
        res = Catalog.get_folder_details(mock_self, None)
        assert isinstance(res, entities.Folder)
        assert res.name == "Catalog"
        assert res.uuid is None
        assert res.parent_name is None
        assert res.parent_uuid is None
        assert res.last_modified is None

        # Try again, with an actual folder UUID
        mock_self.reset_mock(return_value=False)

        now = datetime.now()
        self_uuid = str(uuid4())
        parent_uuid = str(uuid4())
        mock_self._expect_one.return_value = nt(
            name="name",
            uuid=self_uuid,
            parent_name="parent_folder",
            parent_uuid=parent_uuid,
            last_modified=now,
        )
        mock_self = self.prepare__transform_to_entity(
            mock_self, self_uuid, parent_uuid, now
        )

        res = Catalog.get_folder_details(mock_self, self_uuid)
        assert isinstance(res, entities.Folder)
        assert res.name == "name"
        assert res.uuid == self_uuid
        assert res.parent_name == "parent_folder"
        assert res.parent_uuid == parent_uuid
        assert res.last_modified == now

        # And again, with a "non-existent" folder UUID
        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError

        with pytest.raises(NotFound):
            Catalog.get_folder_details(mock_self, self_uuid)

    def test_get_object_type_details(self):
        nt = namedtuple("ObjectType", "name uuid folder_uuid folder_name")

        mock_self = mock.MagicMock()

        self_uuid = str(uuid4())
        folder_uuid = uuid4()
        mock_self._expect_one.return_value = nt(
            name="name",
            uuid=self_uuid,
            folder_uuid=folder_uuid,
            folder_name="folder_name",
        )

        res = Catalog.get_object_type_details(mock_self, self_uuid)
        assert isinstance(res, entities.ObjectTypeDetail)
        assert res.name == "name"
        assert res.uuid == self_uuid

        # And again, with a "non-existent" folder UUID
        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError

        with pytest.raises(NotFound):
            Catalog.get_object_type_details(mock_self, self_uuid)

    def test_get_custom_object_type_details(self):
        nt = namedtuple(
            "CustomObjectTypeDetail",
            "name uuid folder_uuid folder_name version_independent_uuid title status version date_created last_modified external_reference",
        )

        mock_self = mock.MagicMock()

        self_uuid = str(uuid4())
        folder_uuid = uuid4()

        mock_self._expect_one.return_value = nt(
            name="name",
            uuid=self_uuid,
            folder_uuid=folder_uuid,
            folder_name="folder_name",
            version_independent_uuid=str(uuid4()),
            title="Custom Object Title",
            status="active",
            version=1,
            date_created=datetime.now(),
            last_modified=datetime.now(),
            external_reference="External Ref",
        )

        mock_self._thin_transform_to_entity.return_value = entities.Folder(
            id=5,
            uuid=folder_uuid,
            name="folder_name",
            parent_uuid=None,
            parent_name=None,
            last_modified=None,
        )

        res = Catalog.get_custom_object_type_details(mock_self, self_uuid)
        assert isinstance(res, entities.CustomObjectTypeDetail)
        assert res.name == "name"

        # And again, with a "non-existent" folder UUID
        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError

        with pytest.raises(NotFound):
            Catalog.get_custom_object_type_details(mock_self, self_uuid)

    def test_get_attribute_details(self):
        attribute_nt = namedtuple(
            "Attribute",
            " ".join(
                [
                    "id",
                    "name",
                    "uuid",
                    "magic_string",
                    "value_type",
                    "type_multiple",
                    "last_modified",
                    "folder_uuid",
                    "folder_name",
                ]
            ),
        )
        case_type_nt = namedtuple(
            "Casetype",
            " ".join(
                ["name", "uuid", "active", "version", "is_current_version"]
            ),
        )

        mock_self = mock.MagicMock()
        attribute_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        folder_uuid = str(uuid4())
        now = datetime.now()

        mock_self._expect_one.return_value = attribute_nt(
            id=42,
            name="Attribute Name",
            uuid=attribute_uuid,
            magic_string="abracadabra",
            value_type="richtext",
            type_multiple=False,
            last_modified=now,
            folder_uuid=folder_uuid,
            folder_name="folder_name",
        )
        mock_self._get_infrastructure().execute().fetchall.return_value = [
            case_type_nt(
                uuid=case_type_uuid,
                name="Casetype",
                active=True,
                version=8,
                is_current_version=True,
            )
        ]

        # Root folder doesn't have a lot of interesting details
        res = Catalog.get_attribute_details(mock_self, 123)
        assert isinstance(res, entities.AttributeDetail)
        assert res.name == "Attribute Name"
        assert res.uuid == attribute_uuid
        assert res.last_modified == now
        assert res.magic_string == "abracadabra"
        assert res.value_type == "richtext"
        assert res.is_multiple is False
        assert len(res.used_in_case_types) == 1

        assert res.used_in_case_types[0].uuid == case_type_uuid
        assert res.used_in_case_types[0].name == "Casetype"
        assert res.used_in_case_types[0].active is True
        assert res.used_in_case_types[0].version == 8
        assert res.used_in_case_types[0].is_current_version is True

        assert res.used_in_object_types == []

        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError

        with pytest.raises(NotFound):
            Catalog.get_attribute_details(mock_self, attribute_uuid)

    def test_get_email_template_details(self):
        email_template_nt = namedtuple(
            "EmailTemplateDetail",
            " ".join(
                [
                    "id",
                    "name",
                    "uuid",
                    "last_modified",
                    "folder_uuid",
                    "folder_name",
                ]
            ),
        )
        case_type_nt = namedtuple(
            "Casetype",
            " ".join(
                ["name", "uuid", "active", "version", "is_current_version"]
            ),
        )

        mock_self = mock.MagicMock()
        email_template_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        folder_uuid = str(uuid4())
        now = datetime.now()

        mock_self._expect_one.return_value = email_template_nt(
            id=9,
            name="Email Template Name",
            uuid=email_template_uuid,
            last_modified=now,
            folder_uuid=folder_uuid,
            folder_name="folder_name",
        )
        mock_self._get_infrastructure().execute().fetchall.return_value = [
            case_type_nt(
                uuid=case_type_uuid,
                name="Casetype",
                active=True,
                version=8,
                is_current_version=True,
            )
        ]

        # Root folder doesn't have a lot of interesting details
        res = Catalog.get_email_template_details(mock_self, 123)
        assert isinstance(res, entities.EmailTemplateDetail)
        assert res.name == "Email Template Name"
        assert res.uuid == email_template_uuid
        assert res.last_modified == now

        assert res.used_in_case_types[0].uuid == case_type_uuid
        assert res.used_in_case_types[0].name == "Casetype"
        assert res.used_in_case_types[0].active is True
        assert res.used_in_case_types[0].version == 8
        assert res.used_in_case_types[0].is_current_version is True

        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError

        with pytest.raises(NotFound):
            Catalog.get_email_template_details(mock_self, email_template_uuid)

    def test_get_document_template_details(self):
        document_template_nt = namedtuple(
            "DocumentTemplate",
            " ".join(
                [
                    "id",
                    "name",
                    "original_name",
                    "uuid",
                    "interface_id",
                    "last_modified",
                    "folder_uuid",
                    "folder_name",
                ]
            ),
        )
        case_type_nt = namedtuple(
            "Casetype",
            " ".join(
                ["name", "uuid", "active", "version", "is_current_version"]
            ),
        )

        mock_self = mock.MagicMock()
        document_template_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        folder_uuid = str(uuid4())
        now = datetime.now()

        mock_self._expect_one.return_value = document_template_nt(
            id=9,
            name="Document Template Name",
            original_name="a_file.odt",
            uuid=document_template_uuid,
            interface_id=1,
            last_modified=now,
            folder_uuid=folder_uuid,
            folder_name="folder_name",
        )
        mock_self._get_infrastructure().execute().fetchall.return_value = [
            case_type_nt(
                uuid=case_type_uuid,
                name="Casetype",
                active=True,
                version=8,
                is_current_version=True,
            )
        ]

        # Root folder doesn't have a lot of interesting details
        res = Catalog.get_document_template_details(mock_self, 123)
        assert isinstance(res, entities.DocumentTemplateDetail)
        assert res.name == "Document Template Name"
        assert res.uuid == document_template_uuid
        assert res.filename == "a_file.odt"
        assert res.has_default_integration is False
        assert res.last_modified == now

        assert res.used_in_case_types[0].uuid == case_type_uuid
        assert res.used_in_case_types[0].name == "Casetype"
        assert res.used_in_case_types[0].active is True
        assert res.used_in_case_types[0].version == 8
        assert res.used_in_case_types[0].is_current_version is True

        mock_self.reset_mock(return_value=False)

        mock_self._get_infrastructure().execute.side_effect = KeyError

        with pytest.raises(NotFound):
            Catalog.get_document_template_details(
                mock_self, document_template_uuid
            )

    def test__transform_to_entity(self):
        folder_row = namedtuple(
            "Folder", "name uuid parent_name parent_uuid last_modified"
        )

        mock_self = mock.MagicMock()

        now = datetime.now()
        folder_uuid = uuid4()
        parent_uuid = uuid4()
        return_value = folder_row(
            name="name",
            uuid=folder_uuid,
            parent_uuid=parent_uuid,
            parent_name="folder_name",
            last_modified=now,
        )

        res = Catalog._transform_to_entity(mock_self, return_value)
        assert isinstance(res, entities.Folder)
        assert res.name == "name"
        assert res.uuid == folder_uuid
        assert res.parent_uuid == parent_uuid
        assert res.parent_name == "folder_name"
        assert res.last_modified == now

    def test__thin_transform_to_entity(self):
        mock_self = mock.MagicMock()
        folder_uuid = uuid4()
        folder_name = "name"
        res = Catalog._thin_transform_to_entity(
            mock_self, folder_uuid, folder_name
        )
        assert isinstance(res, entities.Folder)
        assert res.name == folder_name
        assert res.uuid == folder_uuid
