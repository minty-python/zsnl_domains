# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog import get_command_instance, get_query_instance
from zsnl_domains.admin.catalog.entities.attribute import (
    AttributeDetailSearchResult,
)
from zsnl_domains.admin.catalog.queries import Queries


class TestQueryFactory:
    @mock.patch("zsnl_domains.admin.catalog.queries.Queries", autospec=True)
    def test_get_query_instance(self, mock_queries):
        q = get_query_instance("repository_factory", "context", "user_uuid")
        assert q.repository_factory == "repository_factory"
        assert q.context == "context"
        assert q.user_uuid == "user_uuid"

    @mock.patch("zsnl_domains.admin.catalog.commands.Commands", autospec=True)
    def test_get_command_instance(self, mock_commands):
        q = get_command_instance(
            "repository_factory", "context", "user_uuid", "event_service"
        )
        assert q.repository_factory == "repository_factory"
        assert q.context == "context"
        assert q.user_uuid == "user_uuid"


class TestQueryClass:
    def test_get_folder_contents(self):
        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository().get_folder_contents.return_value = (
            "test"
        )
        # Reset call counters, so the checks below see only one call
        mock_repo_factory.reset_mock(return_value=False)

        q = Queries(
            repository_factory=mock_repo_factory, context=None, user_uuid=None
        )

        result = q.get_folder_contents("7d94876e-4973-45af-952b-dfda6553f1ea")

        assert result == "test"

        mock_repo_factory.get_repository.assert_called_once_with(
            "catalog", context=None
        )
        mock_repo_factory.get_repository().get_folder_contents.assert_called_once_with(
            "7d94876e-4973-45af-952b-dfda6553f1ea"
        )

    def test_search_catalog(self):
        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository().search_catalog.return_value = "test"
        # Reset call counters, so the checks below see only one call
        mock_repo_factory.reset_mock(return_value=False)

        q = Queries(
            repository_factory=mock_repo_factory, context=None, user_uuid=None
        )

        result = q.search_catalog("search keyword")

        assert result == "test"

        mock_repo_factory.get_repository.assert_called_once_with(
            "catalog", context=None
        )
        mock_repo_factory.get_repository().search_catalog.assert_called_once_with(
            keyword="search keyword", type=None
        )

    def test_get_case_type_history(self):
        case_type_uuid = str(uuid4())
        mock_self = mock.MagicMock()

        mock_self.get_repository().get_case_type_versions_history_by_uuid.return_value = [
            "list of versions"
        ]

        rv = Queries.get_case_type_history(mock_self, uuid=case_type_uuid)

        assert rv == ["list of versions"]
        mock_self.get_repository().get_case_type_versions_history_by_uuid.assert_called_once_with(
            case_type_uuid
        )

    def test_get_folder_details(self):
        folder_uuid = str(uuid4())
        mock_self = mock.MagicMock()
        mock_self.repository.get_folder_details.return_value = "folder details"

        rv = Queries.get_folder_details(mock_self, folder_uuid)

        assert rv == "folder details"
        mock_self.repository.get_folder_details.assert_called_once_with(
            folder_uuid
        )

    def test_get_case_type_details(self):
        case_type_uuid = str(uuid4())
        mock_self = mock.MagicMock()
        mock_case_type = mock.MagicMock()
        mock_case_type.folder_id = 2
        mock_self.get_repository().get_case_type.return_value = mock_case_type

        rv = Queries.get_case_type_details(mock_self, case_type_uuid)

        assert rv is mock_case_type
        mock_self.get_repository().get_case_type.assert_called_once_with(
            uuid=case_type_uuid
        )

    def test_get_object_type_details(self):
        object_type_uuid = str(uuid4())
        mock_self = mock.MagicMock()
        details = mock.MagicMock()
        details.folder_id = 2
        mock_self.repository.get_object_type_details.return_value = details

        rv = Queries.get_object_type_details(mock_self, object_type_uuid)

        assert rv == details
        mock_self.repository.get_object_type_details.assert_called_once_with(
            object_type_uuid
        )

    def test_get_custom_object_type_details(self):
        object_type_uuid = str(uuid4())
        mock_self = mock.MagicMock()
        details = mock.MagicMock()
        details.folder_id = 2
        mock_self.repository.get_custom_object_type_details.return_value = (
            details
        )

        rv = Queries.get_custom_object_type_details(
            mock_self, object_type_uuid
        )

        assert rv == details
        mock_self.repository.get_custom_object_type_details.assert_called_once_with(
            object_type_uuid
        )

    def test_get_attribute_details(self):
        attribute_uuid = str(uuid4())
        mock_self = mock.MagicMock()
        details = mock.MagicMock()
        details.folder_id = 2
        mock_self.repository.get_attribute_details.return_value = details

        rv = Queries.get_attribute_details(mock_self, attribute_uuid)

        assert rv == details
        mock_self.repository.get_attribute_details.assert_called_once_with(
            attribute_uuid
        )

    def test_get_email_template_details(self):
        email_template_uuid = str(uuid4())
        mock_self = mock.MagicMock()
        details = mock.MagicMock()
        details.folder_id = 2
        mock_self.repository.get_email_template_details.return_value = details

        rv = Queries.get_email_template_details(mock_self, email_template_uuid)

        assert rv == details
        mock_self.repository.get_email_template_details.assert_called_once_with(
            email_template_uuid
        )

    def test_get_document_template_details(self):
        document_template_uuid = str(uuid4())
        mock_self = mock.MagicMock()
        details = mock.MagicMock()
        details.folder_id = 2
        mock_self.repository.get_document_template_details.return_value = (
            details
        )

        rv = Queries.get_document_template_details(
            mock_self, document_template_uuid
        )

        assert rv == details
        mock_self.repository.get_document_template_details.assert_called_once_with(
            document_template_uuid
        )

    def test_get_attribute_details_for_edit(self):
        mock_self = mock.MagicMock()
        mock_self.get_repository(
            "attribute"
        ).get_attribute_details_by_uuid.return_value = "attribute details"

        res = Queries.get_attribute_details_for_edit(mock_self, uuid=uuid4())
        assert res == "attribute details"

    def test_generate_magic_string(self):
        mock_self = mock.MagicMock()
        mock_self.get_repository(
            "attribute"
        ).generate_magic_string.return_value = "doc_varibale_1"

        res = Queries.generate_magic_string(
            mock_self, string_input="doc_variable"
        )
        assert res == "doc_varibale_1"

    def test_search_attribute_by_name(self):
        search_result = AttributeDetailSearchResult(
            name="result",
            id=1,
            uuid=uuid4(),
            magic_string="result_1",
            value_type="file",
        )

        mock_self = mock.MagicMock()
        mock_self.get_repository(
            "attribute"
        ).search_attribute_details_by_name.return_value = [search_result]

        res = Queries.search_attribute_details_by_name(
            mock_self, search_string="search", attribute_type="file"
        )
        assert res == [search_result]

    def test_get_email_template_details_for_edit(self):
        mock_self = mock.MagicMock()
        mock_self.get_repository(
            "email_template"
        ).get_email_template_details_by_uuid.return_value = (
            "email_template_details"
        )

        res = Queries.get_email_template_details_for_edit(
            mock_self, uuid=str(uuid4())
        )
        assert res == "email_template_details"

    def test_get_document_template_details_for_edit(self):
        mock_self = mock.MagicMock()
        mock_self.get_repository(
            "document_template"
        ).get_document_template_details_by_uuid.return_value = (
            "document_template_details"
        )

        res = Queries.get_document_template_details_for_edit(
            mock_self, uuid=str(uuid4())
        )
        assert res == "document_template_details"
