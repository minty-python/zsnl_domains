# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import pytz
from collections import namedtuple
from dataclasses import dataclass
from datetime import datetime, timezone
from hashlib import md5
from minty.cqrs import EventService
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, NotFound, ValidationError
from minty.repository import RepositoryFactory
from sqlalchemy.orm.exc import NoResultFound
from typing import Optional
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import document
from zsnl_domains.document import (
    DocumentLabelRepository,
    DocumentRepository,
    FileRepository,
    MessageRepository,
)
from zsnl_domains.document.commands import Commands
from zsnl_domains.document.entities.message_external import (
    Contact,
    MessageAttachment,
)


@dataclass
class CreatedByRow:
    id: int
    subject_type: str


@dataclass
class ContactRow:
    """
    Dataclass to represent a Contact query SQL result
    """

    type: str
    name: str


@dataclass
class FileRow:
    """
    Dataclass to represent a File SQL result
    """

    uuid: UUID
    filename: str
    mimetype: str
    size: int
    storage_location: list
    md5: str
    is_archivable: bool
    virus_scan_status: str


@dataclass
class MessageRow:
    """
    Dataclass to represent a Message SQL query result.
    """

    id: int
    uuid: UUID
    message_date: datetime
    created: datetime
    external_message_subject: str
    participants: list
    type: str
    external_message_source_file_uuid: Optional[UUID]
    external_message_content: str
    external_message_type: str
    external_message_id: Optional[int]
    attachments: list
    case_uuid: UUID
    case_id: int
    created_by_displayname: str
    created_by_uuid: UUID
    last_modified: datetime
    attachment_count: int


@dataclass
class DocumentRow:
    """
    Dataclass to represent a Document SQL query result
    """

    id: int
    document_uuid: UUID
    filename: str
    extension: str
    store_uuid: UUID
    directory_uuid: UUID
    case_uuid: UUID
    case_display_number: int
    mimetype: str
    size: int
    storage_location: str
    md5: str
    is_archivable: bool
    virus_scan_status: str
    accepted: bool
    date_modified: datetime
    thumbnail: str
    creator_uuid: UUID
    creator_displayname: str
    preview_uuid: UUID
    preview_storage_location: str
    preview_mimetype: str
    description: str
    origin: str
    origin_date: datetime
    thumbnail_uuid: UUID
    thumbnail_storage_location: list
    thumbnail_mimetype: str
    intake_owner_uuid: Optional[UUID]
    intake_group_uuid: Optional[UUID]
    intake_role_uuid: Optional[UUID]


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context, event_service):
        mock_repo = self.repositories[name]
        return mock_repo


class TestDocumentDomainCommands:
    @mock.patch(
        "zsnl_domains.document.repositories.document_label.DocumentLabelRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.document.repositories.message.MessageRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.document.repositories.file.FileRepository", autospec=True
    )
    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository",
        autospec=True,
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository",
        autospec=True,
    )
    def setup(
        self,
        repo_mock_case,
        repo_mock_document,
        repo_mock_file,
        repo_mock_message,
        repo_mock_document_label,
    ):
        self.repo_factory = MockRepositoryFactory(infra_factory={})
        self.repo_factory.repositories["case_management"] = repo_mock_case
        self.repo_factory.repositories["document"] = repo_mock_document
        self.repo_factory.repositories["file"] = repo_mock_file
        self.repo_factory.repositories["message"] = repo_mock_message
        self.repo_factory.repositories[
            "document_label"
        ] = repo_mock_document_label
        self.commandmock = Commands(
            repository_factory=self.repo_factory,
            context=None,
            user_uuid="8ee6f9de-d93b-453d-9246-536c83a27318",
            event_service=None,
        )
        self.commandmock.user_info = mock.MagicMock()

    def test_create_document(self):
        valid_document_uuid = str(uuid4())
        valid_store_uuid = str(uuid4())
        valid_case_uuid = str(uuid4())
        valid_directory_uuid = str(uuid4())
        user_info = mock.MagicMock()
        user_info.user_uuid = "8ee6f9de-d93b-453d-9246-536c83a27318"
        filename = "fake_basename.fake_extension"
        mimetype = "fake/mimetype"
        size = 3
        storage_location = "fake_storage_location"
        md5 = "fake_md5"
        magic_strings = ["fake_magic_string"]

        base_arguments = {
            "document_uuid": valid_document_uuid,
            "filename": filename,
            "store_uuid": valid_store_uuid,
            "directory_uuid": valid_directory_uuid,
            "case_uuid": valid_case_uuid,
            "mimetype": mimetype,
            "size": 3,
            "storage_location": storage_location,
            "md5": md5,
            "magic_strings": magic_strings,
        }

        test_cases = [
            {"additional_arguments": {}, "error_message": ""},
            {
                "additional_arguments": {"document_uuid": 1},
                "error_message": "[{'context': 'type', 'message': \"1 is not of type 'string'\", 'cause': None, 'property': '/document_uuid'}, {'context': 'format', 'message': \"1 is not a 'uuid'\", 'cause': 'badly formed hexadecimal UUID string', 'property': '/document_uuid'}]",
            },
            {
                "additional_arguments": {"store_uuid": 2},
                "error_message": "[{'context': 'type', 'message': \"2 is not of type 'string'\", 'cause': None, 'property': '/store_uuid'}, {'context': 'format', 'message': \"2 is not a 'uuid'\", 'cause': 'badly formed hexadecimal UUID string', 'property': '/store_uuid'}]",
            },
            {
                "additional_arguments": {"directory_uuid": 3},
                "error_message": "[{'context': 'oneOf', 'message': '3 is not valid under any of the given schemas', 'cause': None, 'property': '/directory_uuid'}]",
            },
            {
                "additional_arguments": {"mimetype": 4},
                "error_message": "[{'context': 'type', 'message': \"4 is not of type 'string'\", 'cause': None, 'property': '/mimetype'}]",
            },
            {
                "additional_arguments": {"size": "5"},
                "error_message": "[{'context': 'type', 'message': \"'5' is not of type 'integer'\", 'cause': None, 'property': '/size'}]",
            },
            {
                "additional_arguments": {"case_uuid": 6},
                "error_message": "[{'context': 'type', 'message': \"6 is not of type 'string'\", 'cause': None, 'property': '/case_uuid'}, {'context': 'format', 'message': \"6 is not a 'uuid'\", 'cause': 'badly formed hexadecimal UUID string', 'property': '/case_uuid'}]",
            },
            {
                "additional_arguments": {"md5": 7},
                "error_message": "[{'context': 'type', 'message': \"7 is not of type 'string'\", 'cause': None, 'property': '/md5'}]",
            },
        ]

        self.commandmock.user_info.permissions = {"pip_user": False}
        repo = self.commandmock.repository_factory.repositories["document"]
        mock_document = mock.MagicMock()
        repo.create_document.return_value = mock_document

        for test_case in test_cases:
            create_arguments = dict(base_arguments)
            for k, v in test_case["additional_arguments"].items():
                create_arguments[k] = v

            if test_case["error_message"] == "":
                self.commandmock.create_document(**create_arguments)
            else:
                with pytest.raises(ValidationError):
                    self.commandmock.create_document(**create_arguments)

        repo.create_document.assert_called_once_with(
            document_uuid=valid_document_uuid,
            filename=filename,
            store_uuid=valid_store_uuid,
            directory_uuid=valid_directory_uuid,
            case_uuid=valid_case_uuid,
            mimetype=mimetype,
            size=size,
            storage_location=storage_location,
            md5=md5,
            skip_intake=False,
            auto_accept=False,
            editor_update=False,
        )
        repo.save.assert_called_once()

    def test_create_file(self):
        file_repo = mock.MagicMock()
        file_uuid = str(uuid4())

        self.repo_factory.repositories["file"] = file_repo
        self.commandmock.create_file(
            file_uuid=file_uuid,
            filename="filename here.odt",
            mimetype="mime/type",
            size=1234,
            storage_location="Somewhere",
            md5="d41d8cd98f00b204e9800998ecf8427e",
        )

        file_repo.create_file.assert_called_once_with(
            file_uuid,
            "filename here.odt",
            "mime/type",
            1234,
            "Somewhere",
            "d41d8cd98f00b204e9800998ecf8427e",
        )

    def test_create_document_from_attachment(self):
        document_repository = mock.MagicMock()
        attachment_uuid = str(uuid4())

        self.repo_factory.repositories["document"] = document_repository
        self.commandmock.create_document_from_attachment(
            attachment_uuid=attachment_uuid
        )

        document_repository.create_document_from_attachment.assert_called_once_with(
            attachment_uuid
        )


class TestCreateDocumentFromFile(TestBase):
    def setup(self):

        self.mock_infrastructures = {
            "database": mock.MagicMock(),
            "s3": mock.MagicMock(),
            "converter": mock.MagicMock(),
        }

        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(uuid4())
        self.user_info.permissions = {"pip_user": False}

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        self.event_service = EventService(
            correlation_id=str(uuid4()),
            domain="domain",
            context="context",
            user_uuid=self.user_info.user_uuid,
        )

        self.file_uuid = str(uuid4())
        self.case_uuid = str(uuid4())
        self.directory_uuid = str(uuid4())
        self.document_uuid = str(uuid4())
        self.magic_strings = []

        repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        repo_factory.register_repository("document", DocumentRepository)
        repo_factory.register_repository("file", FileRepository)
        repo_factory.register_repository(
            "document_label", DocumentLabelRepository
        )

        self.command_instance = Commands(
            repository_factory=repo_factory,
            context="testcontext",
            user_uuid=self.user_info.user_uuid,
            event_service=self.event_service,
        )
        self.command_instance.user_info = self.user_info

        self.mock_get_file = mock.MagicMock(name="mock_get_file")
        self.mock_get_file.fetchone.return_value = namedtuple(
            "ResultProxy",
            [
                "uuid",
                "filename",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
            ],
        )(
            uuid=uuid4(),
            filename="test_file.pdf",
            mimetype="application/pdf",
            size=12,
            storage_location=["{minio}"],
            md5="test_md5",
            is_archivable=True,
            virus_scan_status=True,
        )

        self.mock_is_user_admin = mock.MagicMock(name="mock_is_user_admin")
        self.mock_is_user_admin.fetchall.return_value = []

        self.mock_get_case = mock.MagicMock(name="mock_get_case")
        self.mock_get_case.fetchone.return_value = namedtuple(
            "ResultProxy", ["id", "status", "behandelaar_gm_id"]
        )(id=1, status="open", behandelaar_gm_id=3)

        self.mock_get_directory = mock.MagicMock(name="mock_get_directory")
        self.mock_get_directory.fetchone.return_value = namedtuple(
            "ResultProxy", ["id"]
        )(id=6)

        self.mock_get_root_file = mock.MagicMock(name="mock_get_root_file")
        self.mock_get_root_file.fetchone.return_value = None

        self.mock_get_file_store = mock.MagicMock(name="mock_get_file_Store")
        self.mock_get_file_store.fetchone.return_value = namedtuple(
            "ResultProxy",
            [
                "id",
                "uuid",
                "filename",
                "file_id",
                "filestore_id",
                "file_uuid",
                "case_id",
                "basename",
                "accepted",
                "created_by",
            ],
        )(
            id=2,
            uuid=self.file_uuid,
            filename="test_file.pdf",
            file_id=3,
            filestore_id=3,
            file_uuid=str(uuid4()),
            case_id=None,
            basename="pdf",
            accepted=True,
            created_by="betrokkene_medewerker_2",
        )

        self.mock_get_created_by = mock.MagicMock(name="mock_get_created_by")
        self.mock_get_created_by.fetchone.return_value = namedtuple(
            "ResultProxy", ["id", "subject_type"]
        )(id=2, subject_type="medewerker")

        self.mock_file_insert = mock.MagicMock(name="mock_file_insert")

        self.mock_get_user_display_name_and_id = mock.MagicMock(
            name="mock_get_user_display_name_and_id"
        )
        self.mock_get_user_display_name_and_id.fetchone.return_value = (
            namedtuple("ResultProxy", ["id", "displayname"])(
                id=2, displayname="Admin"
            )
        )

        self.mock_get_labels = mock.Mock(name="mock_get_labels")
        self.mock_get_labels.fetchall.return_value = [
            mock.Mock(
                uuid=uuid4(),
                label="SomeLabel",
                magic_string="abracadabra",
                naam_public="Public",
            )
        ]
        self.mock_get_documents = mock.Mock(name="mock_get_documents")
        mock_document = mock.Mock()
        mock_document.configure_mock(
            document_label_uuid=uuid4(),
            uuid=uuid4(),
            name="filename",
            extension=".pdf",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            size=0,
            mimetype="text/plain",
            is_archivable=True,
            original_name="nope.pdf",
        )

        self.mock_get_documents.fetchall.return_value = [mock_document]

        self.mock_insert_logging = mock.MagicMock(name="mock_insert_logging")
        self.mock_insert_message = mock.MagicMock(name="mock_insert_message")
        self.mock_update_case_property_case_documents = mock.MagicMock(
            name="mock_update_case_property_case_documents"
        )
        self.mock_get_unaccepted_count = mock.MagicMock(
            name="mock_get_unaccepted_count"
        )
        self.mock_case_property_upsert = mock.MagicMock(
            name="mock_case_property_upsert"
        )
        self.mock_update_object_data = mock.MagicMock(
            name="mock_update_object_data"
        )
        self.mock_get_file_metadata = mock.MagicMock(
            name="mock_get_file_metadata"
        )
        self.mock_get_publishing_setting = mock.MagicMock(
            name="mock_get_publishing_setting"
        )

    def test_create_document_from_file_filestore_already_linked_to_case(self):
        self.mock_get_file_store.fetchone.return_value = namedtuple(
            "ResultProxy",
            [
                "id",
                "uuid",
                "filename",
                "file_id",
                "filestore_id",
                "file_uuid",
                "case_id",
                "basename",
                "accepted",
                "created_by",
            ],
        )(
            id=2,
            uuid=self.file_uuid,
            filename="test_file.pdf",
            file_id=3,
            filestore_id=3,
            file_uuid=str(uuid4()),
            case_id=1,
            basename="pdf",
            accepted=True,
            created_by="betrokkene_medewerker_2",
        )

        self.mock_get_file_metadata.fetchone.return_value = namedtuple(
            "ResultProxy",
            [
                "id",
                "description",
                "trust_level",
                "document_category",
                "pronom_format",
                "origin_date",
                "appearance",
                "structure",
                "origin",
            ],
        )(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        self.mock_get_publishing_setting.fetchone.return_value = namedtuple(
            "ResultProxy",
            ["publish_pip", "publish_website"],
        )(
            publish_pip="t",
            publish_website="t",
        )

        mock_rows = [
            self.mock_get_file,
            self.mock_is_user_admin,
            self.mock_get_case,
            self.mock_get_root_file,
            self.mock_get_file_store,
            self.mock_get_file_metadata,
            self.mock_get_publishing_setting,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.command_instance.create_document_from_file(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                directory_uuid=self.directory_uuid,
                file_uuid=self.file_uuid,
            )

        assert excinfo.value.args == (
            "Filestore is already linked to case number 1",
            "document/filestore/not_allowed",
        )

    def test_create_document_from_file_no_directory_found(self):
        self.mock_get_directory.fetchone.return_value = None

        mock_rows = [
            self.mock_get_file,
            self.mock_is_user_admin,
            self.mock_get_case,
            self.mock_get_root_file,
            self.mock_get_file_store,
            self.mock_get_directory,
            self.mock_get_file_metadata,
            self.mock_get_publishing_setting,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.command_instance.create_document_from_file(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                directory_uuid=self.directory_uuid,
                file_uuid=self.file_uuid,
            )

        assert excinfo.value.args == (
            f"Directory with uuid: '{self.directory_uuid}' not found.",
            "directory/not_found",
        )

    def test_create_document_from_file_no_case_found(self):
        self.mock_get_case.fetchone.return_value = None

        mock_rows = [
            self.mock_get_file,
            self.mock_is_user_admin,
            self.mock_get_case,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.command_instance.create_document_from_file(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                directory_uuid=self.directory_uuid,
                file_uuid=self.file_uuid,
            )

        assert excinfo.value.args == (
            f"Case with uuid: '{self.case_uuid}' not found.",
            "case/not_found",
        )

    def test_create_document_from_file_no_file_found(self):
        self.mock_get_file.fetchone.return_value = None

        mock_rows = [self.mock_get_file]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.command_instance.create_document_from_file(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                directory_uuid=self.directory_uuid,
                file_uuid=self.file_uuid,
            )

        assert excinfo.value.args == (
            f"No file found with uuid={self.file_uuid}",
            "file/not_found",
        )


class TestCreateDocumentFromOriginal(TestBase):
    def setup(self):
        self.valid_user_uuid = str(uuid4())

        self.mock_infrastructures = {
            "database": mock.MagicMock(),
            "converter": mock.MagicMock(),
            "s3": mock.MagicMock(),
        }

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        self.message_uuid = uuid4()
        self.message_type = "external"
        self.contact_uuid = uuid4()
        self.case_uuid = uuid4()
        self.now = datetime.now(timezone.utc)
        self.contact_uuid = uuid4()
        self.contact_id = 1
        self.contact_type = "employee"
        self.contact_name = "Ad Min"

        self.contact_row = ContactRow(
            type=self.contact_type, name=self.contact_name
        )

        self.file_uuid = uuid4()
        self.file_filename = "myfilename.pdf"
        self.file_mimetype = "application/pdf"
        self.file_size = 1
        self.file_storage_location = ["minio"]
        self.file_md5 = md5()

        self.file_row = FileRow(
            uuid=self.file_uuid,
            filename=self.file_filename,
            mimetype=self.file_mimetype,
            size=self.file_size,
            storage_location=self.file_storage_location,
            md5=self.file_md5,
            is_archivable=True,
            virus_scan_status="pending",
        )
        self.display_date = self.now.replace(tzinfo=timezone.utc).astimezone(
            tz=pytz.timezone("Europe/Amsterdam")
        )
        self.display_date = self.display_date.strftime("%d-%m-%Y om %H:%M:%S")

        self.message_row = MessageRow(
            id=1,
            message_date=self.now,
            created=self.now,
            external_message_subject="subject",
            type=self.message_type,
            participants=[
                {
                    "role": "from",
                    "address": "from@address.com",
                    "display_name": "FromDisplayName",
                },
                {
                    "role": "to",
                    "address": "to@address.com",
                    "display_name": "ToDisplayName",
                },
            ],
            external_message_source_file_uuid=None,
            external_message_content="content",
            external_message_type="email",
            external_message_id=1,
            attachments=[{"filename": "testfile.txt"}],
            uuid=self.message_uuid,
            case_uuid=self.case_uuid,
            case_id=1,
            created_by_displayname="Ad Min",
            created_by_uuid=self.contact_uuid,
            last_modified=self.now,
            attachment_count=0,
        )

        self.message_repo = MessageRepository(
            infrastructure_factory=self.mock_infra,
            context=None,
            event_service=EventService(
                correlation_id=uuid4(),
                domain="domain",
                context="context",
                user_uuid=self.valid_user_uuid,
            ),
        )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    def test_create_document_from_message_as_original_without_original_source_file(
        self, mock_create_document
    ):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.external_message_source_file_uuid = None
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            None,
            self.contact_row,
        ]

        with pytest.raises(NotFound):
            self.cmd.create_document_from_message(
                message_uuid=str(uuid4()), output_format="original"
            )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    def test_create_document_from_message_as_original_with_original_source_file(
        self, mock_create_document
    ):
        self.load_command_instance(document, self.mock_infrastructures)

        self.message_row.external_message_source_file_uuid = uuid4()

        self.file_row.filename = "original_message.msg"

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        self.cmd.create_document_from_message(
            message_uuid=str(uuid4()), output_format="original"
        )

        func_name, args, kwargs = mock_create_document.mock_calls[0]
        assert (
            kwargs["filename"]
            == f"{self.message_row.external_message_subject}.msg"
        )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    def test_create_document_from_message_as_original_with_no_case_id_raises_conflict(
        self, mock_create_document
    ):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.case_id = None
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            None,
        ]

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="original"
            )

        assert exception_info.value.args == (
            f"Email message with uuid '{self.message_uuid}' cannot be linked.",
            "document/filestore/not_allowed",
        )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_original_no_source_file_found_raises_not_found(
        self, mock_tempfile, mock_create_document
    ):

        self.message_repo.session.execute().fetchone.side_effect = [
            None,
            self.contact_row,
            None,
        ]
        self.load_command_instance(document, self.mock_infrastructures)

        with pytest.raises(NotFound):
            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="original"
            )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_original_also_retrieves_attachments(
        self, mock_tempfile, mock_create_document
    ):

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        self.mock_infrastructures["database"].reset_mock()

        self.load_command_instance(document, self.mock_infrastructures)
        self.cmd.create_document_from_message(
            message_uuid=str(self.message_uuid), output_format="original"
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list

        select_query = str(db_execute_calls[0][0][0]).replace("\n", "")

        # To keep the assertion readable only compare the bit that selects
        # the subquery AS attachments field in the result.
        assert "))) AS attachments" in select_query

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_original_without_case_id(
        self, mock_tempfile, mock_create_document
    ):

        self.message_row.case_id = None
        self.message_row.external_message_type = "email"

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            self.file_row,
        ]
        self.load_command_instance(document, self.mock_infrastructures)

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="original"
            )

        assert exception_info.value.args == (
            f"Email message with uuid '{self.message_uuid}' cannot be linked.",
            "document/filestore/not_allowed",
        )


class TestCreateDocumentFromEmailAsPDF(TestBase):
    def setup(self):

        self.mock_infrastructures = {
            "database": mock.MagicMock(),
            "s3": mock.MagicMock(),
            "converter": mock.MagicMock(),
        }

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        repo_factory = RepositoryFactory(
            infra_factory=self.mock_infrastructures
        )
        repo_factory.register_repository("document", DocumentRepository)
        repo_factory.register_repository("file", FileRepository)
        repo_factory.register_repository("message", MessageRepository)

        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = uuid4()
        self.user_info.permissions = {"pip_user": False}

        self.event_service = EventService(
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=self.user_info.user_uuid,
        )

        self.message_repo = MessageRepository(
            infrastructure_factory=self.mock_infra,
            context=None,
            event_service=EventService(
                correlation_id=uuid4(),
                domain="domain",
                context="context",
                user_uuid=uuid4(),
            ),
        )

        self.file_uuid = uuid4()
        self.file_filename = "myfilename.pdf"
        self.file_mimetype = "application/pdf"
        self.file_size = 1
        self.file_storage_location = ["minio"]
        self.file_md5 = md5()

        self.file_row = FileRow(
            uuid=self.file_uuid,
            filename=self.file_filename,
            mimetype=self.file_mimetype,
            size=self.file_size,
            storage_location=self.file_storage_location,
            md5=self.file_md5,
            is_archivable=True,
            virus_scan_status="pending",
        )

        self.message_uuid = uuid4()
        self.message_type = "external"
        self.contact_uuid = uuid4()
        self.case_uuid = uuid4()
        self.now = datetime.now(timezone.utc)
        self.contact_uuid = uuid4()
        self.contact_id = 1
        self.contact_type = "employee"
        self.contact_name = "Ad Min"
        self.message_subject = "subject"

        self.display_date = self.now.replace(tzinfo=timezone.utc).astimezone(
            tz=pytz.timezone("Europe/Amsterdam")
        )
        self.display_date = self.display_date.strftime("%d-%m-%Y om %H:%M:%S")

        self.contact_row = ContactRow(
            type=self.contact_type, name=self.contact_name
        )

        self.message_row = MessageRow(
            id=1,
            message_date=self.now,
            created=self.now,
            external_message_subject=self.message_subject,
            type=self.message_type,
            participants=[
                {
                    "role": "from",
                    "address": "from@address.com",
                    "display_name": "FromDisplayName",
                },
                {
                    "role": "to",
                    "address": "to@address.com",
                    "display_name": "ToDisplayName",
                },
            ],
            external_message_source_file_uuid=None,
            external_message_content="content",
            external_message_type="email",
            external_message_id=1,
            attachments=[MessageAttachment(filename="testfile.txt")],
            uuid=self.message_uuid,
            case_uuid=self.case_uuid,
            case_id=1,
            created_by_displayname="Ad Min",
            created_by_uuid=self.contact_uuid,
            last_modified=self.now,
            attachment_count=0,
        )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf(self, mock_tempfile):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.attachments = [
            MessageAttachment(filename="example.txt")
        ]
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ) as mock_create_document:

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            func_name, args, kwargs = mock_create_document.mock_calls[0]

            assert kwargs["case_uuid"] == self.case_uuid
            assert (
                kwargs["filename"]
                == f"{self.message_subject} op {self.display_date}.pdf"
            )
            assert kwargs["mimetype"] == "application/pdf"

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_no_external_case_id_raises_conflict(
        self, mock_create_document, mock_tempfile
    ):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.case_id = None
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            self.file_row,
        ]

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

        assert exception_info.value.args == (
            f"Email message with uuid '{self.message_uuid}' cannot be linked.",
            "document/filestore/not_allowed",
        )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_pip_message_as_pdf(self, mock_tempfile):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.attachments = [
            MessageAttachment(filename="example.txt")
        ]
        self.message_row.external_message_type = "pip"

        # When doing a message to PDF query the SQL result does not
        # include a external_message_source_file_uuid field. So remove
        # it from the dataclass to handle cases without this result
        del self.message_row.external_message_source_file_uuid

        self.message_row.created_by_uuid = uuid4()
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            self.file_row,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ) as mock_create_document:

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            func_name, args, kwargs = mock_create_document.mock_calls[0]

            assert kwargs["case_uuid"] == self.case_uuid
            assert (
                kwargs["filename"]
                == f"{self.message_subject} op {self.display_date}.pdf"
            )
            assert kwargs["mimetype"] == "application/pdf"

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_pip_message_as_pdf_no_created_by_uuid(
        self, mock_tempfile
    ):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.attachments = [
            MessageAttachment(filename="example.txt")
        ]
        self.message_row.created_by_uuid = None
        self.message_row.external_message_type = "pip"

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ):

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            mock_converter_infra = self.mock_infra.get_infrastructure(
                "context", "converter"
            )
            func_name, args, kwargs = mock_converter_infra.mock_calls[0]

            assert kwargs["content"] == bytes(
                f"PIP bericht Zaak 1\n--------------------------------------------------------------------------------\n\nVan:   Ad Min (Aanvrager)\n\nAan:   Behandelaar\n\nDatum: {self.display_date}\n\nOnderwerp: subject op {self.display_date}\n\ncontent\n\nBijlagen: geen",
                "utf-8",
            )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_pip_message_as_pdf_contact_type_not_employee(
        self, mock_tempfile
    ):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.attachments = [
            MessageAttachment(filename="example.txt")
        ]
        self.message_row.external_message_type = "pip"
        self.message_row.created_by_uuid = uuid4()

        self.contact_row.type = "person"

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            self.file_row,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ):

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            mock_converter_infra = self.mock_infra.get_infrastructure(
                "context", "converter"
            )
            func_name, args, kwargs = mock_converter_infra.mock_calls[0]

            assert kwargs["content"] == bytes(
                f"PIP bericht Zaak 1\n--------------------------------------------------------------------------------\n\nVan:   Ad Min (Aanvrager)\n\nAan:   Behandelaar\n\nDatum: {self.display_date}\n\nOnderwerp: subject op {self.display_date}\n\ncontent\n\nBijlagen: geen",
                "utf-8",
            )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_no_message_date_uses_created_date(
        self, mock_create_tempfile
    ):

        self.message_row.message_date = None
        self.load_command_instance(document, self.mock_infrastructures)
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ) as mock_create_document:

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            func_name, args, kwargs = mock_create_document.mock_calls[0]

            assert kwargs["case_uuid"] == self.case_uuid
            assert (
                kwargs["filename"]
                == f"{self.message_subject} op {self.display_date}.pdf"
            )
            assert kwargs["mimetype"] == "application/pdf"

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_no_contact_by_uuid_raises_not_found(
        self, mock_create_document, mock_create_tempfile
    ):

        self.message_row.created_by_uuid = uuid4()
        self.message_row.external_message_type = "pip"

        self.load_command_instance(document, self.mock_infrastructures)

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            None,
            self.file_row,
        ]

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(uuid4()), output_format="pdf"
            )

        assert exception_info.value.args == (
            f"No Contact found with UUID '{self.message_row.created_by_uuid}'",
            "document/message/no_contact_found",
        )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    def test_create_document_from_message_as_pdf_invalid_external_message_type_raises_conflict(
        self, mock_create_document
    ):

        self.message_row.external_message_type = "invalid"
        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.created_by_uuid = uuid4()
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            self.file_row,
        ]

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(uuid4()), output_format="pdf"
            )

        assert exception_info.value.args == (
            "Only messages of type email, pip can be saved as PDF",
            "domains/document/message/not_allowed",
        )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_no_from_display_name(
        self, mock_tempfile
    ):

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.attachments = [
            MessageAttachment(filename="example.txt")
        ]

        self.message_row.participants = list()
        self.message_row.participants.append(
            {"role": "from", "address": "from@address.com", "display_name": ""}
        )
        self.message_row.participants.append(
            {
                "role": "to",
                "address": "to@address.com",
                "display_name": "ToName",
            }
        )

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ):

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            mock_converter_infra = self.mock_infra.get_infrastructure(
                "context", "converter"
            )
            func_name, args, kwargs = mock_converter_infra.mock_calls[0]

            assert kwargs["content"] == bytes(
                f"E-Mailbericht Zaak 1\n--------------------------------------------------------------------------------\n\nVan:   from@address.com\n\nAan:   to@address.com (ToName)\n\nDatum: {self.display_date}\n\nOnderwerp: subject op {self.display_date}\n\ncontent\n\nBijlagen: geen",
                "utf-8",
            )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_multiple_recipients_and_cc_bcc(
        self, mock_tempfile
    ):
        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.attachments = [
            MessageAttachment(filename="example.txt")
        ]

        self.message_row.participants = list()
        self.message_row.participants.append(
            {
                "role": "from",
                "address": "from@address.com",
                "display_name": "FromName",
            }
        )
        self.message_row.participants.append(
            {
                "role": "to",
                "address": "to@address.com",
                "display_name": "ToName",
            }
        )
        self.message_row.participants.append(
            {
                "role": "to",
                "address": "to_second@address.com",
                "display_name": "ToNameSecond",
            }
        )
        self.message_row.participants.append(
            {
                "role": "cc",
                "address": "cc@address.com",
                "display_name": "ToCCName",
            }
        )
        self.message_row.participants.append(
            {
                "role": "bcc",
                "address": "bcc@address.com",
                "display_name": "ToBCCName",
            }
        )

        self.load_command_instance(document, self.mock_infrastructures)
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ) as mock_create_document:

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            mock_converter_infra = self.mock_infra.get_infrastructure(
                "context", "converter"
            )
            func_name, args, kwargs = mock_converter_infra.mock_calls[0]

            assert kwargs["content"] == bytes(
                f"E-Mailbericht Zaak 1\n--------------------------------------------------------------------------------\n\nVan:   from@address.com (FromName)\n\nAan:   to@address.com (ToName), to_second@address.com (ToNameSecond)\n\nCC:    cc@address.com (ToCCName)\n\nBCC:   bcc@address.com (ToBCCName)\n\nDatum: {self.display_date}\n\nOnderwerp: subject op {self.display_date}\n\ncontent\n\nBijlagen: geen",
                "utf-8",
            )

            func_name, args, kwargs = mock_create_document.mock_calls[0]

            assert kwargs["case_uuid"] == self.case_uuid
            assert (
                kwargs["filename"]
                == f"{self.message_subject} op {self.display_date}.pdf"
            )
            assert kwargs["mimetype"] == "application/pdf"

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_no_to_display_name(
        self, mock_tempfile
    ):

        self.message_row.participants = list()
        self.message_row.participants.append(
            {
                "role": "from",
                "address": "from@address.com",
                "display_name": "FromDisplayName",
            }
        )
        self.message_row.participants.append(
            {"role": "to", "address": "to@address.com", "display_name": ""}
        )

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]
        self.load_command_instance(document, self.mock_infrastructures)

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ):

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            mock_converter_infra = self.mock_infra.get_infrastructure(
                "context", "converter"
            )
            func_name, args, kwargs = mock_converter_infra.mock_calls[0]

            assert kwargs["content"] == bytes(
                f"E-Mailbericht Zaak 1\n--------------------------------------------------------------------------------\n\nVan:   from@address.com (FromDisplayName)\n\nAan:   to@address.com\n\nDatum: {self.display_date}\n\nOnderwerp: subject op {self.display_date}\n\ncontent\n\nBijlagen: geen",
                "utf-8",
            )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_no_attachments(
        self, mock_tempfile
    ):
        self.message_row.attachments = None
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]
        self.load_command_instance(document, self.mock_infrastructures)

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ) as mock_create_document:

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            func_name, args, kwargs = mock_create_document.mock_calls[0]
            mock_converter_infra = self.mock_infra.get_infrastructure(
                "context", "converter"
            )
            func_name, args, kwargs = mock_converter_infra.mock_calls[0]

            assert kwargs["content"] == bytes(
                f"E-Mailbericht Zaak 1\n--------------------------------------------------------------------------------\n\nVan:   from@address.com (FromDisplayName)\n\nAan:   to@address.com (ToDisplayName)\n\nDatum: {self.display_date}\n\nOnderwerp: subject op {self.display_date}\n\ncontent\n\nBijlagen: geen",
                "utf-8",
            )

    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_with_attachments(
        self, mock_tempfile
    ):
        self.message_row.attachment_count = 1
        self.message_row.attachments = [
            MessageAttachment(filename="testfile.txt")
        ]
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]
        self.load_command_instance(document, self.mock_infrastructures)

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
        ) as mock_create_document:

            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

            func_name, args, kwargs = mock_create_document.mock_calls[0]
            mock_converter_infra = self.mock_infra.get_infrastructure(
                "context", "converter"
            )
            func_name, args, kwargs = mock_converter_infra.mock_calls[0]

            assert kwargs["content"] == bytes(
                f"E-Mailbericht Zaak 1\n--------------------------------------------------------------------------------\n\nVan:   from@address.com (FromDisplayName)\n\nAan:   to@address.com (ToDisplayName)\n\nDatum: {self.display_date}\n\nOnderwerp: subject op {self.display_date}\n\ncontent\n\nBijlagen:\ntestfile.txt\n",
                "utf-8",
            )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_without_case_id_raises_conflict(
        self, mock_tempfile, mock_create_document
    ):

        self.message_row.case_id = None
        self.message_row.external_message_type = "email"

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            self.file_row,
        ]
        self.load_command_instance(document, self.mock_infrastructures)

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="original"
            )

        assert exception_info.value.args == (
            f"Email message with uuid '{self.message_uuid}' cannot be linked.",
            "document/filestore/not_allowed",
        )

    @mock.patch(
        "zsnl_domains.document.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.document.repositories.file.FileRepository.get_file_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository._create_document"
    )
    @mock.patch("zsnl_domains.document.repositories.message.tempfile")
    def test_create_document_from_message_as_pdf_from_not_external_message_raises_conflict(
        self,
        mock_tempfile,
        mock_create_document,
        mock_get_file_by_uuid,
        mock_get_message,
    ):

        self.message_row.case_id = None
        self.message_row.external_message_id = None
        self.message_row.external_message_type = "email"

        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.contact_row,
            self.file_row,
        ]
        self.load_command_instance(document, self.mock_infrastructures)

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(self.message_uuid), output_format="pdf"
            )

        assert exception_info.value.args == (
            "Can only save by original source file for external email messages",
            "document/message/no_external_message",
        )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.create_document"
    )
    def test_create_document_from_message_as_pdf_invalid_message_type(
        self, mock_create_document
    ):

        self.message_row.external_message_type = "invalid"
        self.load_command_instance(document, self.mock_infrastructures)
        self.message_row.created_by_uuid = uuid4()
        self.message_repo.session.execute().fetchone.side_effect = [
            self.message_row,
            self.file_row,
            self.contact_row,
        ]

        with pytest.raises(Conflict) as exception_info:
            self.cmd.create_document_from_message(
                message_uuid=str(uuid4()), output_format="original"
            )

        assert exception_info.value.args == (
            "Only messages of type email can be saved in original format",
            "domains/document/message/not_allowed",
        )


class TestCreateDocumentFromMessageArchive(TestBase):
    def setup(self):
        self.mock_infrastructures = {"database": mock.MagicMock()}

        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(uuid4())
        self.user_info.permissions = {"pip_user": True}

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )
        self.event_service = EventService(
            correlation_id=str(uuid4()),
            domain="domain",
            context="context",
            user_uuid=self.user_info.user_uuid,
        )

        self.message_uuid = str(uuid4())
        self.file_uuid = str(uuid4())
        self.case_uuid = str(uuid4())
        self.now = datetime.now(timezone.utc)

        repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        repo_factory.register_repository("document", DocumentRepository)
        repo_factory.register_repository("file", FileRepository)
        repo_factory.register_repository("message", MessageRepository)

        self.command_instance = Commands(
            repository_factory=repo_factory,
            context="testcontext",
            user_uuid=self.user_info.user_uuid,
            event_service=self.event_service,
        )
        self.command_instance.user_info = self.user_info

        now = datetime.now(timezone.utc)

        attachment = MessageAttachment(filename="testfile.txt")

        self.mock_get_message = mock.MagicMock()
        self.mock_get_message.fetchone.return_value = namedtuple(
            "ResultProxy",
            [
                "id",
                "uuid",
                "case_uuid",
                "case_id",
                "message_date",
                "created",
                "last_modified",
                "participants",
                "type",
                "created_by_displayname",
                "created_by_uuid",
                "external_message_id",
                "external_message_subject",
                "external_message_content",
                "external_message_source_file_uuid",
                "external_message_type",
                "attachments",
                "attachment_count",
            ],
        )(
            id=1,
            uuid=uuid4(),
            case_uuid=self.case_uuid,
            case_id=1234,
            message_date=now,
            last_modified=now,
            created=now,
            participants=[
                {
                    "role": "from",
                    "address": "from@address.com",
                    "display_name": "FromName",
                },
                {
                    "role": "to",
                    "address": "to@address.com",
                    "display_name": "ToName",
                },
            ],
            created_by_uuid=uuid4(),
            external_message_subject="subject",
            external_message_content="content",
            external_message_type="email",
            external_message_source_file_uuid=None,
            created_by_displayname="Jan Bakker",
            attachments=[attachment],
            type="external",
            external_message_id=1,
            attachment_count=0,
        )

        self.mock_get_file = mock.MagicMock()
        self.mock_get_file.fetchone.return_value = namedtuple(
            "ResultProxy",
            [
                "id",
                "uuid",
                "filename",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
            ],
        )(
            id=1,
            uuid=uuid4(),
            filename="test_file.pdf",
            mimetype="application/pdf",
            size=12,
            storage_location=["{minio}"],
            md5="test_md5",
            is_archivable=True,
            virus_scan_status=True,
        )

        self.mock_is_user_admin = mock.MagicMock()
        self.mock_is_user_admin.fetchall.return_value = []

        self.mock_get_case = mock.MagicMock()
        self.mock_get_case.fetchone.return_value = namedtuple(
            "ResultProxy", ["id", "status", "behandelaar_gm_id"]
        )(id=1, status="open", behandelaar_gm_id=3)

        self.mock_get_root_file = mock.MagicMock()
        self.mock_get_root_file.fetchone.return_value = None

        self.mock_get_file_store = mock.MagicMock()
        self.mock_get_file_store.fetchone.return_value = namedtuple(
            "ResultProxy",
            [
                "id",
                "uuid",
                "filename",
                "file_id",
                "filestore_id",
                "file_uuid",
                "case_id",
                "basename",
                "accepted",
                "created_by",
                "created_by_uuid",
            ],
        )(
            id=2,
            uuid=self.file_uuid,
            filename="test_file.pdf",
            file_id=3,
            filestore_id=3,
            file_uuid=str(uuid4()),
            case_id=None,
            basename="pdf",
            accepted=True,
            created_by_uuid=uuid4(),
            created_by="betrokkene_medewerker_2",
        )

        self.mock_get_created_by = mock.MagicMock()
        self.mock_get_created_by.fetchone.return_value = namedtuple(
            "ResultProxy", ["id", "subject_type"]
        )(id=2, subject_type="medewerker")

        self.mock_file_insert = mock.MagicMock()

        self.mock_get_user_display_name_and_id = mock.MagicMock()
        self.mock_get_user_display_name_and_id.fetchone.return_value = (
            namedtuple("ResultProxy", ["id", "displayname"])(
                id=2, displayname="Admin"
            )
        )

        self.mock_insert_logging = mock.MagicMock()
        self.mock_insert_message = mock.MagicMock()
        self.mock_update_case_property_case_documents = mock.MagicMock()
        self.mock_get_unaccepted_count = mock.MagicMock()
        self.mock_case_property_upsert = mock.MagicMock()
        self.mock_update_object_data = mock.MagicMock()
        self.mock_get_file_metadata = mock.MagicMock()
        self.mock_get_publishing_setting = mock.MagicMock()

    @mock.patch(
        "zsnl_domains.document.repositories.message.MessageRepository._get_contact_by_uuid"
    )
    def test_create_document_from_message_archive(self, mock_get_contact):

        mock_rows = [
            self.mock_get_message,
            self.mock_get_file,
            self.mock_is_user_admin,
            self.mock_get_case,
            self.mock_get_root_file,
            self.mock_get_file_store,
            self.mock_is_user_admin,
            self.mock_get_case,
            self.mock_get_root_file,
            self.mock_get_created_by,
            self.mock_get_file_metadata,
            self.mock_get_publishing_setting,
            self.mock_file_insert,
            self.mock_get_user_display_name_and_id,
            self.mock_insert_logging,
            self.mock_get_user_display_name_and_id,
            self.mock_insert_message,
            self.mock_update_case_property_case_documents,
            self.mock_get_unaccepted_count,
            self.mock_case_property_upsert,
            self.mock_update_object_data,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        mock_get_contact.return_value = Contact(
            uuid=uuid4(), name="Name", contact_type="employee"
        )

        self.command_instance.create_document_from_message_archive(
            message_uuid=self.message_uuid, file_uuid=self.file_uuid
        )

    @mock.patch(
        "zsnl_domains.document.repositories.message.MessageRepository._get_contact_by_uuid"
    )
    def test_create_document_from_message_archive_no_case_found(
        self, mock_get_contact
    ):

        mock_get_contact.return_value = Contact(
            uuid=uuid4(), name="Name", contact_type="employee"
        )

        self.mock_get_case.fetchone.return_value = None

        mock_rows = [
            self.mock_get_message,
            self.mock_get_file,
            self.mock_is_user_admin,
            self.mock_get_case,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.command_instance.create_document_from_message_archive(
                message_uuid=self.message_uuid, file_uuid=self.file_uuid
            )

        assert excinfo.value.args == (
            f"Case with uuid: '{self.case_uuid}' not found.",
            "case/not_found",
        )

    @mock.patch(
        "zsnl_domains.document.repositories.message.MessageRepository._get_contact_by_uuid"
    )
    def test_create_document_from_message_archive_no_file_found(
        self, mock_get_contact
    ):

        self.mock_get_file.fetchone.return_value = None

        mock_get_contact.return_value = Contact(
            uuid=uuid4(), name="Name", contact_type="employee"
        )
        mock_rows = [self.mock_get_message, self.mock_get_file]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.command_instance.create_document_from_message_archive(
                message_uuid=self.message_uuid, file_uuid=self.file_uuid
            )

        assert excinfo.value.args == (
            f"No file found with uuid={self.file_uuid}",
            "file/not_found",
        )

    def test_create_document_from_message_archive_no_message_found(self):
        self.mock_get_message.fetchone.return_value = None

        mock_rows = [self.mock_get_message, self.mock_get_file]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.mock_infrastructures["database"].execute = mock_execute
        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.command_instance.create_document_from_message_archive(
                message_uuid=self.message_uuid, file_uuid=self.file_uuid
            )

        assert excinfo.value.args == (
            f"Message with uuid '{self.message_uuid}' not found.",
            "document/message/not_found",
        )


@dataclass
class FileSQLResult:

    object_type: str
    searchable_id: int
    id: int
    filestore_id: int
    name: str
    extension: str
    root_file_id: int
    version: int
    case_id: int
    metadata_id: int
    subject_id: int
    directory_id: int
    creation_reason: str
    accepted: bool
    rejection_reason: str
    reject_to_queue: bool
    is_duplicate_name: bool
    publish_pip: bool
    publish_website: bool
    date_created: datetime
    created_by: str
    date_modified: datetime
    modified_by: str
    date_deleted: datetime
    deleted_by: str
    destroyed: bool
    scheduled_jobs_id: int
    intake_owner: str
    active_version: bool
    is_duplicate_of: int
    queue: bool
    document_status: str
    generator: str
    lock_timestamp: datetime
    lock_subject_id: int
    lock_subject_name: str
    uuid: UUID
    confidential: bool
    thumbnail_uuid: UUID
    thumbnail_storage_location: list
    thumbnail_mimetype: str
    intake_owner_uuid: Optional[UUID]
    intake_group_uuid: Optional[UUID]
    intake_role_uuid: Optional[UUID]


class TestDeleteDocument(TestBase):
    def setup(self):

        # Setup user_info mock
        self.user_uuid = uuid4()
        self.is_pip_user = False
        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(self.user_uuid)
        self.user_info.permissions = {"pip_user": self.is_pip_user}

        # Setup the mock infrastructures
        self.mock_infrastructures = {"database": mock.MagicMock()}

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        # Setup and register the mock Document repository
        repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        repo_factory.register_repository("document", DocumentRepository)

        # Create an EventService
        self.event_service = EventService(
            correlation_id=str(uuid4()),
            domain="test_domain",
            context="test_context",
            user_uuid=self.user_uuid,
        )

        self.document_repo = DocumentRepository(
            infrastructure_factory=self.mock_infra,
            context="test_context",
            event_service=self.event_service,
        )

        # sets self.cmd to a Command instance based on the mock infra
        self.load_command_instance(document, self.mock_infrastructures)

        # Setup other variables needed for testing.
        self.document_uuid = uuid4()
        self.case_uuid = uuid4()
        self.now = datetime.now()

        self.file_sql_result = FileSQLResult(
            object_type="",
            searchable_id="",
            id=1,
            filestore_id=1,
            name="testbestand",
            extension=".pdf",
            root_file_id=1,
            version=1,
            case_id=None,
            metadata_id=None,
            subject_id=1,
            directory_id=None,
            creation_reason="Toegevoegd",
            accepted=False,
            rejection_reason="",
            reject_to_queue=False,
            is_duplicate_name=False,
            publish_pip=False,
            publish_website=False,
            date_created=self.now,
            created_by="betrokkene-medewerker-1",
            date_modified=None,
            modified_by=None,
            date_deleted=None,
            deleted_by=None,
            destroyed=False,
            scheduled_jobs_id=None,
            intake_owner=None,
            active_version=True,
            is_duplicate_of=None,
            queue=False,
            document_status="original",
            generator=None,
            lock_timestamp=None,
            lock_subject_id=None,
            lock_subject_name=None,
            uuid=self.document_uuid,
            confidential=False,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        self.document_sql_result = DocumentRow(
            id=1,
            document_uuid=self.document_uuid,
            filename="basename.pdf",
            extension=".pdf",
            store_uuid=None,
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            mimetype="application/pdf",
            size=1000,
            storage_location="minio",
            md5=md5(),
            is_archivable=True,
            virus_scan_status="pending",
            accepted=False,
            date_modified=self.now,
            thumbnail=False,
            creator_uuid=uuid4(),
            creator_displayname="",
            preview_uuid=None,
            preview_storage_location=None,
            preview_mimetype="application/pdf",
            description="",
            origin=None,
            origin_date=self.now,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        self.medewerker_sql_result = CreatedByRow(
            id=1, subject_type="medewerker"
        )

    def test_delete_document_document_uuid_not_found_raises_conflict(self):

        self.document_repo.session.execute().fetchone.side_effect = [None]

        with pytest.raises(NotFound) as exception_info:
            self.cmd.delete_document(
                user_info=self.user_info, document_uuid=str(self.document_uuid)
            )

        assert exception_info.value.args == (
            f"No document found with uuid={self.document_uuid}",
            "document/not_found",
        )

    def test_delete_document_assigned_to_case_raises_conflict(self):

        self.document_sql_result.case_uuid = self.case_uuid

        self.document_repo.session.execute().fetchone.side_effect = [
            self.document_sql_result
        ]

        with pytest.raises(Conflict) as exception_info:
            self.cmd.delete_document(
                user_info=self.user_info, document_uuid=str(self.document_uuid)
            )

        assert exception_info.value.args == (
            f"Document '{self.document_uuid}' is assign to case '{self.case_uuid}'",
            "document/delete_document/assigned_to_case",
        )

    def test_delete_document_calls_delete_with_correct_arguments(self):

        self.document_repo.session.execute().fetchone.side_effect = [
            self.document_sql_result,
            self.medewerker_sql_result,
        ]

        with mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository._delete_document"
        ) as delete_mock:
            self.cmd.delete_document(
                user_info=self.user_info,
                document_uuid=str(self.document_uuid),
                reason="Just for fun...",
            )

            func_name, args, kwargs = delete_mock.mock_calls[0]
            assert args[0].entity_id == self.document_uuid
            assert args[0].entity_data == {"destroy_reason": "Just for fun..."}

    def test_delete_document_deletes_document(self):

        self.document_repo.session.execute().fetchone.side_effect = [
            self.document_sql_result,
            self.medewerker_sql_result,
        ]

        self.document_repo.session.query().filter().with_for_update().order_by().one.side_effect = [
            self.file_sql_result,
        ]

        self.cmd.delete_document(
            user_info=self.user_info,
            document_uuid=str(self.document_uuid),
            reason="Just for fun...",
        )

    def test_delete_document_no_deletable_document_raises_conflict(self):

        self.document_repo.session.execute().fetchone.side_effect = [
            self.document_sql_result,
            self.medewerker_sql_result,
        ]

        self.file_sql_result.active_version = False

        self.document_repo.session.query().filter().with_for_update().order_by().one.side_effect = [
            NoResultFound,
        ]
        with pytest.raises(Conflict) as exception_info:
            self.cmd.delete_document(
                user_info=self.user_info,
                document_uuid=str(self.document_uuid),
                reason="Just for fun...",
            )

        assert exception_info.value.args == (
            f"Deletable document with UUID '{self.document_uuid}' not found",
            "document/delete_document/not_found",
        )

    def test_delete_document_directly_assigned_to_case_raises_conflict(self):

        self.document_repo.session.execute().fetchone.side_effect = [
            self.document_sql_result,
            self.medewerker_sql_result,
        ]

        self.file_sql_result.active_version = False

        self.document_repo.session.query().filter().with_for_update().order_by().one.side_effect = [
            self.file_sql_result,
        ]

        self.file_sql_result.case_id = 83

        with pytest.raises(Conflict) as exception_info:
            self.cmd.delete_document(
                user_info=self.user_info,
                document_uuid=str(self.document_uuid),
                reason="Just for fun...",
            )

        assert exception_info.value.args == (
            f"Document '{self.document_uuid}' is assigned to case '83'",
            "document/delete_document/assigned_to_case",
        )
