# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .commands import GEO_COMMANDS
from .queries import GEO_QUERIES
from .repositories import (
    CaseRepository,
    ContactRepository,
    CustomObjectRepository,
    GeoFeatureRelationshipRepository,
    GeoFeatureRepository,
)

REQUIRED_REPOSITORIES = {
    "case": CaseRepository,
    "contact": ContactRepository,
    "custom_object": CustomObjectRepository,
    "geo_feature": GeoFeatureRepository,
    "geo_feature_relationship": GeoFeatureRelationshipRepository,
}


def get_query_instance(repository_factory, context, user_uuid):
    return minty.cqrs.DomainQueryContainer(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        query_lookup_table=GEO_QUERIES,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):

    return minty.cqrs.DomainCommandContainer(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
        command_lookup_table=GEO_COMMANDS,
    )
