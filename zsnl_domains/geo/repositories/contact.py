# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import sqlalchemy.types as sqltypes
from ... import DatabaseRepositoryBase
from ..entities import Contact
from minty.exceptions import NotFound
from minty.repository import Repository
from sqlalchemy import sql
from typing import Dict
from uuid import UUID
from zsnl_domains.database import schema

organization_query = sql.select(
    [
        schema.Bedrijf.uuid,
        schema.Bedrijf.handelsnaam.label("name"),
        sql.case(
            [
                # Company with a "vestigingsadres" in NL
                (
                    schema.Bedrijf.vestiging_straatnaam.isnot(None),
                    sql.func.concat(
                        schema.Bedrijf.vestiging_straatnaam,
                        " "
                        + sql.cast(
                            schema.Bedrijf.vestiging_huisnummer,
                            sqltypes.String,
                        ),
                        schema.Bedrijf.vestiging_huisletter,
                        "-"
                        + sql.func.nullif(
                            schema.Bedrijf.vestiging_huisnummertoevoeging,
                            "",
                        ),
                        ",",
                        " " + schema.Bedrijf.vestiging_postcode,
                        " " + schema.Bedrijf.vestiging_woonplaats,
                    ),
                ),
                # Company with a "vestigingsadres" outside NL
                (
                    schema.Bedrijf.vestiging_adres_buitenland1.isnot(None),
                    sql.func.concat(
                        schema.Bedrijf.vestiging_adres_buitenland1,
                        ", " + schema.Bedrijf.vestiging_adres_buitenland2,
                        ", " + schema.Bedrijf.vestiging_adres_buitenland3,
                    ),
                ),
                # Company with a "correspondentieadres" in NL
                (
                    schema.Bedrijf.correspondentie_straatnaam.isnot(None),
                    sql.func.concat(
                        schema.Bedrijf.correspondentie_straatnaam,
                        " "
                        + sql.cast(
                            schema.Bedrijf.correspondentie_huisnummer,
                            sqltypes.String,
                        ),
                        schema.Bedrijf.correspondentie_huisletter,
                        "-"
                        + sql.func.nullif(
                            schema.Bedrijf.correspondentie_huisnummertoevoeging,
                            "",
                        ),
                        ",",
                        " " + schema.Bedrijf.correspondentie_postcode,
                        " " + schema.Bedrijf.correspondentie_woonplaats,
                    ),
                ),
                # Company with a "correspondentieadres" outside NL
                (
                    schema.Bedrijf.correspondentie_adres_buitenland1.isnot(
                        None
                    ),
                    sql.func.concat(
                        schema.Bedrijf.correspondentie_adres_buitenland1,
                        ", "
                        + schema.Bedrijf.correspondentie_adres_buitenland2,
                        ", "
                        + schema.Bedrijf.correspondentie_adres_buitenland3,
                    ),
                ),
            ],
            else_="Adres onbekend",
        ).label("address"),
        sql.literal("organization").label("type"),
        sql.case(
            [
                (
                    sql.and_(
                        schema.Bedrijf.date_ceased.is_(None),
                        schema.Bedrijf.date_founded.is_(None),
                    ),
                    None,
                ),
                (
                    schema.Bedrijf.date_ceased <= sql.func.current_date(),
                    "inactive",
                ),
            ],
            else_="active",
        ).label("status"),
    ]
).where(schema.Bedrijf.deleted_on.is_(None))

person_query = (
    sql.select(
        [
            schema.NatuurlijkPersoon.uuid,
            sql.func.concat(
                schema.NatuurlijkPersoon.adellijke_titel + " ",
                schema.NatuurlijkPersoon.voorletters + " ",
                schema.NatuurlijkPersoon.naamgebruik,
            ).label("name"),
            sql.func.coalesce(
                sql.func.concat(
                    schema.Adres.straatnaam
                    + " "
                    + sql.cast(schema.Adres.huisnummer, sqltypes.String),
                    schema.Adres.huisletter,
                    "-" + schema.Adres.huisnummertoevoeging,
                    ", ",
                    schema.Adres.postcode + " ",
                    schema.Adres.woonplaats,
                ),
                "Adres onbekend",
            ).label("address"),
            sql.literal("person").label("type"),
            sql.case(
                [
                    (
                        schema.NatuurlijkPersoon.datum_overlijden.isnot(None),
                        "inactive",
                    )
                ],
                else_="active",
            ).label("status"),
        ]
    )
    .select_from(
        sql.join(
            schema.NatuurlijkPersoon,
            schema.Adres,
            schema.NatuurlijkPersoon.adres_id == schema.Adres.id,
            isouter=True,
        )
    )
    .where(
        sql.and_(
            schema.NatuurlijkPersoon.active.is_(True),
            schema.NatuurlijkPersoon.deleted_on.is_(None),
        )
    )
)


class ContactRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "Contact"
    _events_to_calls: Dict[str, str] = {}

    def get_by_uuid(self, uuid: UUID):
        row = self.session.execute(
            sql.union_all(
                organization_query.where(sql.column("uuid") == uuid),
                person_query.where(sql.column("uuid") == uuid),
            )
        ).fetchone()

        if row is None:
            raise NotFound(
                f"Contact with uuid={uuid} not found", "geo/case/not_found"
            )

        return self.__entity_from_row(row=row)

    def __entity_from_row(self, row) -> Contact:
        return Contact(
            uuid=row.uuid,
            entity_id=row.uuid,
            name=row.name,
            address=row.address,
            type=row.type,
            status=row.status,
        )
