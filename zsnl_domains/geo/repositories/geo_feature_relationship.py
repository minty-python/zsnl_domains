# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities import GeoFeatureRelationship
from minty.cqrs import Event, UserInfo
from minty.repository import Repository
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from typing import List
from uuid import UUID
from zsnl_domains.database import schema

geo_feature_relation_query = sql.select(
    [
        schema.ServiceGeoJSONRelationship.uuid.label("entity_id"),
        # GeoFeatureRelationships are retrieved from the perspective of a
        # "source" object with a "relationship" field in it; this is
        # stored the other way around in the service_geojson tables: the
        # "source" object is in the "related_uuid" field of the
        # service_geojson_relationship table.
        schema.ServiceGeoJSON.uuid.label("related_uuid"),
        schema.ServiceGeoJSONRelationship.related_uuid.label("origin_uuid"),
    ]
).select_from(
    sql.join(
        schema.ServiceGeoJSONRelationship,
        schema.ServiceGeoJSON,
        schema.ServiceGeoJSON.id
        == schema.ServiceGeoJSONRelationship.service_geojson_id,
    )
)


class GeoFeatureRelationshipRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "GeoFeatureRelationship"
    _events_to_calls = {
        "GeoFeatureRelationshipCreated": "_create_geo_feature_relationship",
        "GeoFeatureRelationshipDeleted": "_delete_geo_feature_relationship",
    }

    def create_geo_feature_relationship(
        self, relationship_uuid: UUID, origin_uuid: UUID, related_uuid: UUID
    ) -> GeoFeatureRelationship:
        return GeoFeatureRelationship.create(
            uuid=relationship_uuid,
            origin_uuid=origin_uuid,
            related_uuid=related_uuid,
            # Internal values
            entity_id=relationship_uuid,
            # Services
            _event_service=self.event_service,
        )

    def get_relationships(
        self, origin_uuid: UUID
    ) -> List[GeoFeatureRelationship]:
        "Retrieve all existing geo feature relations an object has"

        relation_rows = self.session.execute(
            geo_feature_relation_query.where(
                schema.ServiceGeoJSONRelationship.related_uuid == origin_uuid,
            )
        ).fetchall()

        return [
            self._entity_from_row(row=relation_row)
            for relation_row in relation_rows
        ]

    def _entity_from_row(self, row) -> GeoFeatureRelationship:
        return GeoFeatureRelationship(
            uuid=row.entity_id,
            origin_uuid=row.origin_uuid,
            related_uuid=row.related_uuid,
            # Internals
            entity_id=row.entity_id,
            # Services
            _event_service=self.event_service,
        )

    # Event handlers
    def _create_geo_feature_relationship(
        self, event: Event, user_info: UserInfo = None, dry_run: bool = False
    ):
        changes = event.format_changes()

        self.session.execute(
            sql_pg.insert(schema.ServiceGeoJSONRelationship)
            .from_select(
                ["uuid", "service_geojson_id", "related_uuid"],
                sql.select(
                    [
                        sql.literal(event.entity_id),
                        schema.ServiceGeoJSON.id,
                        sql.literal(str(changes["origin_uuid"])),
                    ]
                ).where(schema.ServiceGeoJSON.uuid == changes["related_uuid"]),
            )
            .on_conflict_do_nothing()
        )

    def _delete_geo_feature_relationship(
        self, event: Event, user_info: UserInfo = None, dry_run: bool = False
    ):
        self.session.execute(
            sql.delete(schema.ServiceGeoJSONRelationship)
            .where(schema.ServiceGeoJSONRelationship.uuid == event.entity_id)
            .execution_options(synchronize_session=False)
        )
