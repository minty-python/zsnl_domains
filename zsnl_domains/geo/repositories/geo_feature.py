# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities import GeoFeature
from minty.cqrs import Event, UserInfo
from minty.exceptions import NotFound
from minty.repository import Repository
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from typing import Iterable, List
from uuid import UUID
from zsnl_domains.database import schema

base_geo_query = sql.select(
    [
        schema.ServiceGeoJSON.uuid,
        schema.ServiceGeoJSON.geo_json,
        schema.ServiceGeoJSON.uuid.label("related_uuids"),
    ]
)

related_geo_query = sql.select(
    [
        schema.ServiceGeoJSON.uuid,
        schema.ServiceGeoJSON.geo_json,
        schema.ServiceGeoJSONRelationship.related_uuid.label("related_uuids"),
    ]
).select_from(
    sql.join(
        schema.ServiceGeoJSONRelationship,
        schema.ServiceGeoJSON,
        schema.ServiceGeoJSON.id
        == schema.ServiceGeoJSONRelationship.service_geojson_id,
    )
)


class GeoFeatureRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "GeoFeature"
    _events_to_calls = {
        "GeoFeatureCreated": "_create_geo_feature",
        "GeoFeatureDeleted": "_delete_geo_feature",
    }

    def get_geo_feature(self, uuid: UUID) -> GeoFeature:
        """
        Find the main geo feature for a specific UUID
        """
        geo_feature_row = self.session.execute(
            base_geo_query.where(schema.ServiceGeoJSON.uuid == uuid)
        ).fetchone()

        if geo_feature_row is None:
            raise NotFound(f"No geo feature found with {uuid=}")

        return self._entity_from_row(geo_feature_row)

    def get_geo_features(self, uuids: Iterable[UUID]) -> List[GeoFeature]:
        """
        Find all GeoFeature entities for a specific UUID

        :param uuid UUID of the object/case/etc. to retrieve geo feature info for
        :return List of GeoFeature entities
        """

        geo_union = sql.union(
            base_geo_query.where(schema.ServiceGeoJSON.uuid.in_(uuids)),
            related_geo_query.where(
                schema.ServiceGeoJSONRelationship.related_uuid.in_(uuids)
            ),
        ).subquery()

        full_query = (
            sql.select(
                [
                    geo_union.c.uuid,
                    geo_union.c.geo_json,
                    sql.func.array_agg(geo_union.c.related_uuids).label(
                        "related_uuids"
                    ),
                ]
            )
            .select_from(geo_union)
            .group_by(geo_union.c.uuid, geo_union.c.geo_json)
        )

        result = self.session.execute(full_query).fetchall()

        features = []
        for row in result:
            feature = self._entity_from_row(row)
            features.append(feature)

        return features

    def _entity_from_row(self, row):
        if isinstance(row.related_uuids, list):
            related_uuids = row.related_uuids
        else:
            related_uuids = [row.related_uuids]

        return GeoFeature(
            uuid=row.uuid,
            entity_id=row.uuid,
            geo_json=row.geo_json,
            origin=related_uuids,
            _event_service=self.event_service,
        )

    def create_geo_feature(self, uuid: UUID, geo_json: dict) -> GeoFeature:
        """
        Create a new GeoJSON feature

        :param uuid Unique User IDentifier v4
        :type uuid
        :param geojson GeoJSON object formatted as a dict
        :type dict
        """

        return GeoFeature.create(
            # Basic attributes
            uuid=uuid,
            geo_json=geo_json,
            origin=[],
            # Services
            _event_service=self.event_service,
        )

    def _create_geo_feature(
        self, event: Event, user_info: UserInfo = None, dry_run: bool = False
    ) -> None:
        changes = event.format_changes()

        query = sql_pg.insert(
            schema.ServiceGeoJSON,
            {"uuid": changes["uuid"], "geo_json": changes["geo_json"]},
        ).on_conflict_do_update(
            index_elements=[schema.ServiceGeoJSON.uuid],
            set_={"geo_json": sql.literal_column("excluded.geo_json")},
        )
        self.session.execute(query)

    def _delete_geo_feature(
        self, event: Event, user_info=UserInfo, dry_run: bool = False
    ) -> None:
        self.session.execute(
            sql.delete(schema.ServiceGeoJSON)
            .where(schema.ServiceGeoJSON.uuid == event.entity_id)
            .execution_options(synchronize_session=False)
        )
