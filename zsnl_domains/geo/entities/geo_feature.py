# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, Field
from typing import List
from uuid import UUID


class GeoFeature(Entity):
    "Represents the geographic features in a custom object"

    uuid: UUID = Field(..., title="Unique Identifier for a GeoFeature")
    geo_json: dict = Field(..., title="GeoJSON object detailing the feature")
    origin: List[UUID] = Field(
        ...,
        title="List of UUIDs indicating 'why' this result is being returned",
        description="""
        Geo features are retrieved based on the UUID of an entity in the system.

        An entity can be related to other entities, and get "related geo" in that
        way.

        This list indicates which UUID(s) in the query led to this geo feature
        entity being returned.
        """,
    )

    entity_type = "geo_feature"
    entity_id__fields = ["uuid"]

    @classmethod
    @Entity.event("GeoFeatureCreated")
    def create(cls, **kwargs):
        geo_feature = cls(**kwargs)
        return geo_feature

    @Entity.event("GeoFeatureDeleted")
    def delete(self):
        pass
