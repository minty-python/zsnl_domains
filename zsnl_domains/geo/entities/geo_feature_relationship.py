# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, Field
from uuid import UUID


class GeoFeatureRelationship(Entity):
    """Represents a relation between two objects, for the purposes of linking
    their geo information together."""

    uuid: UUID = Field(..., title="Identifier for this entity")

    origin_uuid: UUID = Field(
        ..., title="UUID of the 'original' object or case"
    )
    related_uuid: UUID = Field(..., title="UUID of related object")

    entity_type = "geo_feature_relationship"
    entity_id__fields = ["id"]

    @classmethod
    @Entity.event("GeoFeatureRelationshipCreated")
    def create(cls, **kwargs):
        geo_feature_rel = cls(**kwargs)
        return geo_feature_rel

    @Entity.event("GeoFeatureRelationshipDeleted")
    def delete(self):
        pass
