# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._common import MapMagicString
from minty.entity import Entity, Field
from typing import List, Optional
from uuid import UUID


class CustomObject(Entity):
    """
    Entity representing a custom object as seen by the geo domain.

    It is very minimal on purpose, as the "actual" custom object is part of the
    case-management domain; this only has the version-independent UUID and the
    fields that should be represented on a map.
    """

    entity_type = "custom_object"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(
        ..., title="Unique Identifier for a version of a custom object type"
    )
    map_magic_strings: List[MapMagicString] = Field(
        ...,
        title="List of magic strings of custom fields that should be shown on the map",
    )
    title: Optional[str] = Field(..., title="Title of a custom object")
    subtitle: Optional[str] = Field(..., title="Subtitle of a custom object")
    status: Optional[str] = Field(..., title="Status of a custom object")
    family: Optional[str] = Field(
        ..., title="Family of a custom object is Object"
    )
    type: Optional[str] = Field(..., title="Type of a custom object")
