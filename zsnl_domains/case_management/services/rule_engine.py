# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from ..entities import Case
from ..entities.case_type_version import (
    CasetypeRule,
    RuleAction,
    RuleCondition,
)
from minty.exceptions import NotFound
from typing import List


class ChangeConfidentialty:
    """Class to change confidentiality of case"""

    def run(case: Case, value: str):
        case.confidentiality = value


class RuleEngine:

    action_to_class_mapping = {
        "change_confidentiality": "ChangeConfidentialty"
    }
    allowed_rule_conidtions = ["contactchannel"]

    def _execute_rule_action(self, case: Case, actions: List[RuleAction]):
        for action in actions:
            if self.action_to_class_mapping.get(action.action, None):
                # class object out of action_to_class_mapping
                eval(self.action_to_class_mapping[action.action]).run(
                    case, action.value
                )
            else:
                raise NotImplementedError(
                    f"Action '{action.action}' is not implemented in v2 rule_engine",
                    "rule_engine/action_not_implemeneted",
                )

    def _check_condition(self, case, condition: RuleCondition):
        if condition.kenmerk in self.allowed_rule_conidtions:
            return case.contact_channel == condition.value
        else:
            raise NotImplementedError(
                f"Condition '{condition.kenmerk}' is not implemented in v2 rule_engine",
                "rule_engine/condition_not_implemeneted",
            )

    def execute_rule(self, rules: List[CasetypeRule], case: Case):
        for rule in rules:
            if rule.condition_type == "or":
                rule_condition_matcher = any
            elif rule.condition_type == "and":
                rule_condition_matcher = all
            else:
                raise NotFound(
                    f"Condition type {rule.condition_type } condtion is not allowed in v2 rule_engine",
                    "rule_engine/condition_type_not_allowed",
                )

            if rule_condition_matcher(
                [
                    self._check_condition(case, condition)
                    for condition in rule.conditions
                ]
            ):
                self._execute_rule_action(case, rule.match_actions)
            else:
                self._execute_rule_action(case, rule.nomatch_actions)
