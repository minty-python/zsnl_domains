# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ...shared.types import ComparisonFilterConditionStr
from ...shared.validators import split_comma_separated_field
from ..entities.case_search_result import CaseSearchResult
from ..repositories import CaseSearchResultRepository
from minty.entity import EntityCollection
from pydantic import BaseModel, Field, validate_arguments, validator
from typing import List, Optional, Set, cast
from uuid import UUID
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidContactChannel,
)


class CaseSearchResultFilter(BaseModel):

    filter_status: Optional[Set[ValidCaseStatus]] = Field(
        None, alias="attributes.status"
    )

    case_type_uuids: Optional[Set[UUID]] = Field(
        None, alias="relationship.case_type.id"
    )

    assignee_uuids: Optional[Set[UUID]] = Field(
        None, alias="relationship.assignee.id"
    )
    coordinator_uuids: Optional[Set[UUID]] = Field(
        None, alias="relationship.coordinator.id"
    )
    requestor_uuids: Optional[Set[UUID]] = Field(
        None, alias="relationship.requestor.id"
    )

    filter_registration_date: Optional[
        List[ComparisonFilterConditionStr]
    ] = Field(None, alias="attributes.registration_date")

    filter_completion_date: Optional[
        List[ComparisonFilterConditionStr]
    ] = Field(None, alias="attributes.completion_date")

    filter_payment_status: Optional[Set[ValidCasePaymentStatus]] = Field(
        None, alias="attributes.payment_status"
    )
    filter_channel_of_contact: Optional[Set[ValidContactChannel]] = Field(
        None, alias="attributes.channel_of_contact"
    )
    filter_confidentiality: Optional[Set[ValidCaseConfidentiality]] = Field(
        None, alias="attributes.confidentiality"
    )
    filter_archival_state: Optional[Set[ValidCaseArchivalState]] = Field(
        None, alias="attributes.archival_state"
    )
    filter_retention_period_source_date: Optional[
        Set[ValidCaseRetentionPeriodSourceDate]
    ] = Field(None, alias="attributes.retention_period_source_date")

    filter_result: Optional[Set[ValidCaseResult]] = Field(
        None, alias="attributes.result"
    )
    filter_case_location: Optional[Set[str]] = Field(
        None, alias="attributes.case_location"
    )
    filter_num_unread_messages: Optional[
        List[ComparisonFilterConditionStr]
    ] = Field(None, alias="attributes.num_unread_messages")

    filter_num_unaccepted_files: Optional[
        List[ComparisonFilterConditionStr]
    ] = Field(None, alias="attributes.num_unaccepted_files")

    filter_num_unaccepted_updates: Optional[
        List[ComparisonFilterConditionStr]
    ] = Field(None, alias="attributes.num_unaccepted_updates")

    filter_keyword: Optional[str] = Field(None, alias="keyword")

    class Config:
        validate_all = True

    _split_comma_separated_fields = validator(
        "filter_status",
        "case_type_uuids",
        "requestor_uuids",
        "assignee_uuids",
        "coordinator_uuids",
        "filter_registration_date",
        "filter_completion_date",
        "filter_payment_status",
        "filter_channel_of_contact",
        "filter_confidentiality",
        "filter_archival_state",
        "filter_retention_period_source_date",
        "filter_result",
        "filter_case_location",
        "filter_num_unread_messages",
        "filter_num_unaccepted_files",
        "filter_num_unaccepted_updates",
        allow_reuse=True,
        pre=True,
    )(split_comma_separated_field)


class CaseSearch(minty.cqrs.SplitQueryBase):
    name = "search_case"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        filters: Optional[CaseSearchResultFilter] = None,
    ) -> EntityCollection[CaseSearchResult]:
        """
        Search for Cases based on the filters
        """
        if not filters:
            filters = CaseSearchResultFilter()
        repo = cast(
            CaseSearchResultRepository,
            self.get_repository("case_search_result"),
        )
        cases = repo.search(
            user_info=self.qry.user_info,
            permission="search",
            page=page,
            page_size=page_size,
            filters=filters.dict(),
        )

        return cases
