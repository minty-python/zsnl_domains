# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities import _shared
from ..repositories import (
    CaseRelationRepository,
    ContactRelatedCaseRepository,
    SubjectRelationRepository,
)
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_arguments
from typing import Optional, cast
from uuid import UUID


class GetSubjectRelations(minty.cqrs.SplitQueryBase):
    name = "get_subject_relations_for_case"

    def __call__(self, case_uuid: str):
        """
        Get related subjects for a case.

        :param case_uuid: UUID of the case
        :return: List of related subjects for a case
        """
        repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )

        subject_relations = repo.find_subject_relations_for_case(
            case_uuid=UUID(case_uuid), user_info=self.qry.user_info
        )
        return {"result": subject_relations}


class GetCaseRelations(minty.cqrs.SplitQueryBase):
    name = "get_case_relations"

    @validate_arguments
    def __call__(
        self,
        case_uuid: UUID,
        authorization: _shared.CaseAuthorizationLevel = _shared.CaseAuthorizationLevel.search,
    ):
        repo = cast(
            CaseRelationRepository, self.get_repository("case_relation")
        )

        case_relations = repo.get_case_relations(
            case_uuid=case_uuid,
            authorization=authorization,
            user_info=self.qry.user_info,
        )

        return {"result": case_relations}


class GetContactRelatedCases(minty.cqrs.SplitQueryBase):
    name = "get_contact_related_cases"

    @validate_with(
        get_data(__name__, "validation/get_contact_related_cases.json")
    )
    def __call__(
        self,
        contact_uuid: str,
        page: str,
        page_size: str,
        is_authorized: Optional[str] = None,
        include_cases_on_location: Optional[str] = None,
        status: Optional[str] = None,
        sort: Optional[str] = None,
        roles_to_include: Optional[str] = None,
        roles_to_exclude: Optional[str] = None,
        keyword: Optional[str] = None,
    ):
        "Get list of cases for a contact"

        repo = cast(
            ContactRelatedCaseRepository,
            self.get_repository("contact_related_case"),
        )

        get_contact_list_params = {
            "user_info": self.qry.user_info,
            "include_cases_on_location": (
                True if include_cases_on_location == "true" else False
            ),
        }

        if status:
            status_list = {status_item for status_item in status.split(",")}
            get_contact_list_params["status_list"] = status_list

        if roles_to_include:
            roles_list = {role for role in roles_to_include.split(",")}
            get_contact_list_params["roles_to_include"] = roles_list

        if roles_to_exclude:
            roles_list = {role for role in roles_to_exclude.split(",")}
            get_contact_list_params["roles_to_exclude"] = roles_list

        if is_authorized:
            get_contact_list_params["is_authorized"] = (
                True if is_authorized == "true" else False
            )

        if keyword:
            get_contact_list_params["keyword"] = keyword

        if sort:
            get_contact_list_params["sort"] = sort

        get_contact_list_params["contact_uuid"] = contact_uuid
        get_contact_list_params["page"] = int(page)
        get_contact_list_params["page_size"] = int(page_size)

        return repo.get_contact_related_cases_list(**get_contact_list_params)
