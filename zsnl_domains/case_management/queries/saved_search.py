# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
from .. import repositories
from ..entities import saved_search as entity
from pydantic import BaseModel, Field, validate_arguments
from typing import Optional, cast
from uuid import UUID


class ListSavedSearchFilter(BaseModel):
    kind: Optional[entity.SavedSearchKind] = Field(
        None, title="Kind of saved search to retrieve a list of"
    )


class GetSavedSearch(minty.cqrs.SplitQueryBase):
    name = "get_saved_search"

    @validate_arguments
    def __call__(self, uuid: UUID) -> entity.SavedSearch:
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        return repo.get(
            uuid=uuid,
            user_info=self.qry.user_info,
        )


class ListSavedSearch(minty.cqrs.SplitQueryBase):
    name = "list_saved_search"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        filter: Optional[ListSavedSearchFilter] = None,
    ) -> minty.entity.EntityCollection[entity.SavedSearch]:
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        result = repo.get_multiple(
            kind=filter.kind if filter else None,
            page=page,
            page_size=page_size,
            user_info=self.qry.user_info,
        )

        return result
