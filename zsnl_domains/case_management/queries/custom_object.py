# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities import AuthorizationLevel, RelatedObject, RelatedSubject
from ..repositories import (
    CustomObjectRepository,
    RelatedCaseRepository,
    RelatedObjectRepository,
    RelatedSubjectRepository,
)
from minty.exceptions import NotFound
from typing import List, cast
from uuid import UUID


class GetCustomObject(minty.cqrs.SplitQueryBase):
    name = "get_custom_object_by_uuid"

    def __call__(self, uuid: str):
        """
        Get a custom object by its uuid.

        :param uuid: identifier of the custom object
        :return: CustomObject
        """

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_object = repo.find_by_uuid(
            uuid=UUID(uuid),
            user_info=self.qry.user_info,
            authorization=AuthorizationLevel.read,
        )

        if not custom_object:
            raise NotFound(
                "Could not find or rights are insufficient for 'custom_object' with uuid {}".format(
                    uuid
                ),
                "case_management/custom_object/object/not_found",
            )

        return custom_object


class GetCustomObjects(minty.cqrs.SplitQueryBase):
    name = "get_custom_objects"

    def __call__(self, filter_params={}):
        """
        Get a list of custom objects

        Returns a list of all custom objects

        :param filter_params: filter of custom objects
        :return: CustomObject
        """

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_objects = repo.filter(
            user_info=self.qry.user_info, params=filter_params
        )

        return custom_objects


class GetRelatedCases(minty.cqrs.SplitQueryBase):
    name = "get_related_cases_for_custom_object"

    def __call__(self, object_uuid: str):
        """
        Get a list of cases related to the custom_object

        Returns a list of all cases related to object

        :param object_uuid: identifier of the custom_object_type
        :return: RelatedCases
        """

        repo = cast(RelatedCaseRepository, self.get_repository("related_case"))
        related_cases = repo.find_for_custom_object(
            object_uuid=UUID(object_uuid), user_uuid=self.user_uuid
        )
        return related_cases


class GetRelatedObjects(minty.cqrs.SplitQueryBase):
    name = "get_related_objects_for_custom_object"

    def __call__(self, object_uuid: str):
        """
        Get a list of objects related to the custom_object

        Returns a list of all custom_objects related to object

        :param object_uuid: identifier of the custom_object
        :type object_uuid: UUID
        :return: custom_objects related to custom_object
        :rtype: List[RelatedObject]
        """

        repo = cast(
            RelatedObjectRepository, self.get_repository("related_object")
        )
        related_objects = repo.find_for_custom_object(
            object_uuid=UUID(object_uuid)
        )
        return related_objects


class GetRelatedCustomObjectsForSubject(minty.cqrs.SplitQueryBase):
    name = "get_related_custom_objects_for_subject"

    def __call__(self, subject_uuid: str) -> List[RelatedObject]:
        """
        Get a list of custom objects related to a subject

        :param subject_uuid: identifier of the subject to search in
        :return: related custom objects
        """

        repo = cast(
            RelatedObjectRepository, self.get_repository("related_object")
        )
        related_objects = repo.find_for_subject(
            subject_uuid=UUID(subject_uuid)
        )
        return related_objects


class GetRelatedSubjects(minty.cqrs.SplitQueryBase):
    name = "get_related_subjects_for_custom_object"

    def __call__(self, object_uuid: str) -> List[RelatedSubject]:
        """
        Get a list of subjects related to the custom_object

        Returns a list of all subjects related to object

        :param object_uuid: identifier of the custom_object
        :return: subjects related to custom_object
        """

        repo = cast(
            RelatedSubjectRepository, self.get_repository("related_subject")
        )
        related_contacts = repo.find_for_custom_object(
            object_uuid=UUID(object_uuid)
        )
        return related_contacts


class GetRelatedSubjectsForSubject(minty.cqrs.SplitQueryBase):
    name = "get_related_subjects_for_subject"

    def __call__(self, subject_uuid: str) -> List[RelatedSubject]:
        """
        Get a list of subjects related to another subject

        :param subject_uuid: UUID of the subject to retrieve relationships of
        :return: subjects related to subject with specified UUID
        """

        repo = cast(
            RelatedSubjectRepository, self.get_repository("related_subject")
        )
        related_contacts = repo.find_for_subject(
            subject_uuid=UUID(subject_uuid)
        )
        return related_contacts
