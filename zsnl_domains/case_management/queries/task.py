# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .. import entities
from ..repositories import TaskRepository
from minty.validation import validate_with
from pkgutil import get_data
from typing import Any, Dict, List, Optional, cast
from uuid import UUID


def _unpack_filters(
    assignee_uuids: Optional[List[str]] = None,
    case_uuids: Optional[List[str]] = None,
    case_ids: Optional[List[int]] = None,
    case_type_uuids: Optional[List[str]] = None,
    department_uuids: Optional[List[str]] = None,
):
    filters = {}
    if case_uuids:
        filters["case_uuids"] = [UUID(case_uuid) for case_uuid in case_uuids]
    if case_ids:
        filters["case_ids"] = case_ids
    if case_type_uuids:
        filters["case_type_uuids"] = [
            UUID(case_type_uuid) for case_type_uuid in case_type_uuids
        ]
    if assignee_uuids:
        filters["assignee_uuids"] = [
            UUID(assignee_uuid) for assignee_uuid in assignee_uuids
        ]
    if department_uuids:
        filters["department_uuids"] = [
            UUID(department_uuid) for department_uuid in department_uuids
        ]

    return filters


class GetTaskList(minty.cqrs.SplitQueryBase):
    name = "get_task_list"

    @validate_with(get_data(__name__, "validation/tasks/get_task_list.json"))
    def __call__(
        self,
        page: str,
        page_size: str,
        assignee_uuids: Optional[List[str]] = None,
        case_uuids: Optional[List[str]] = None,
        case_ids: Optional[List[int]] = None,
        case_type_uuids: Optional[List[str]] = None,
        department_uuids: Optional[List[str]] = None,
        title_words: Optional[List[str]] = None,
        keyword: Optional[str] = None,
        phase: Optional[int] = None,
        completed: Optional[bool] = None,
        sort: Optional[str] = None,
    ) -> List[entities.Task]:
        """
        Get list of tasks by using filter case and phase(milestone) and assignee.

        :param assignee_uuids: UUIDs of the assignees to filter for
        :param case_uuids: return only tasks with one of these case UUIDs
        :param case_ids: return only tasks with one of these case IDs.
        :param case_type_uuid: return only tasks with one of these case type
            UUIDs
        :param department_uuid: UUIDs of the departments to filter for
        :param title_words: List of words to search for in the title. All
            options will be ORed together.
        :param keyword: Keyword to search for. ANDed with title_words.
        :param phase: Phase which to retrieve tasks for.
        :param completed: State of the task
        :param sort: Sort order
        :return: List of tasks
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))

        get_task_list_params: Dict[str, Any] = {
            "user_uuid": self.user_uuid,
            **_unpack_filters(
                case_uuids=case_uuids,
                case_ids=case_ids,
                assignee_uuids=assignee_uuids,
                case_type_uuids=case_type_uuids,
                department_uuids=department_uuids,
            ),
        }

        if title_words:
            get_task_list_params["title_words"] = title_words

        if keyword:
            get_task_list_params["keyword"] = keyword

        if phase is not None:
            get_task_list_params["phase"] = phase

        if completed is not None:
            get_task_list_params["completed"] = completed

        if sort:
            get_task_list_params["sort"] = sort

        get_task_list_params["page"] = int(page)
        get_task_list_params["page_size"] = int(page_size)

        task_list = task_repo.get_task_list(**get_task_list_params)
        return task_list
