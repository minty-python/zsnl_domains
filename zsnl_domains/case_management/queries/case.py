# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import CaseRepository, CaseSummaryRepository
from minty.validation import validate_with
from pkgutil import get_data
from typing import Iterable, cast
from uuid import UUID


class Get(minty.cqrs.SplitQueryBase):
    name = "get_case_by_uuid"

    def __call__(self, case_uuid: str) -> dict:
        """
        Get case entity as python dictionary by given `case id`.

        :param case_uuid: identifier of case
        :return: case dictionary
        """

        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.qry.user_info,
            permission="read",
        )
        return case


class GetSummaries(minty.cqrs.SplitQueryBase):
    name = "get_case_summaries_by_uuid"

    @validate_with(get_data(__name__, "validation/get_case_summaries.json"))
    def __call__(self, case_uuids: Iterable[str]):
        """
        Get list of case_summaries by uuid.

        :param case_uuids: List of UUIDs
        :return: List of case_summary entities
        """

        case_summary_repo = cast(
            CaseSummaryRepository, self.get_repository("case_summary")
        )
        case_summaries = case_summary_repo.get_case_summaries_by_uuid(
            uuids=map(lambda u: UUID(u), case_uuids), user_uuid=self.user_uuid
        )
        return {"result": case_summaries}
