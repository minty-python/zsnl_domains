# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .. import entities
from ..repositories import ExportFileRepository
from typing import Any, Dict, List, cast


class GetExportFileList(minty.cqrs.SplitQueryBase):
    name = "get_export_file_list"

    def __call__(
        self,
        page: str,
        page_size: str,
    ) -> List[entities.ExportFile]:
        """
        Get list of export files by using query user.

        :return: List of Export Files
        """
        repo = cast(ExportFileRepository, self.get_repository("export_file"))

        get_export_file_list_params: Dict[str, Any] = {
            "user_uuid": self.qry.user_info.user_uuid,
            "page": int(page),
            "page_size": int(page_size),
        }

        export_file_list = repo.get_export_file_list(
            **get_export_file_list_params
        )
        return export_file_list
