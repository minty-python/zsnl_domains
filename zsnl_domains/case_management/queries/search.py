# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ...shared.repositories.search_result import SearchResultRepository
from minty.validation import validate_with
from pkgutil import get_data
from typing import Any, Mapping, Optional, cast


class Search(minty.cqrs.SplitQueryBase):
    name = "search"

    @validate_with(get_data(__name__, "validation/search.json"))
    def __call__(
        self,
        type: str,
        keyword: str,
        max_results: int = 100,
        max_results_per_type: Optional[int] = None,
        filter_params: Optional[Mapping[str, Any]] = None,
    ):
        "Search by (list of) entity type and keyword"
        if max_results > 100:
            max_results = 100

        types = sorted(set(type.split(",")))

        search_repo = cast(
            SearchResultRepository, self.get_repository("search_result")
        )
        return search_repo.search(
            user_info=self.qry.user_info,
            types=types,
            keyword=keyword,
            max_results=max_results,
            max_results_per_type=max_results_per_type,
            filter_params=filter_params,
        )
