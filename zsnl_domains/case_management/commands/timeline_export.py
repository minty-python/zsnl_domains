# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import dateutil.parser
import minty.cqrs
from ..repositories import TimelineEntryRepository, TimelineExportRepository
from minty.cqrs import UserInfo
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_arguments
from typing import Optional, cast
from uuid import UUID


class RequestExport(minty.cqrs.SplitCommandBase):
    name = "request_export"

    @validate_with(
        get_data(
            __name__,
            "validation/timeline_export/request_timeline_export.json",
        )
    )
    def __call__(
        self,
        type: str,
        uuid: str,
        period_start: Optional[str] = None,
        period_end: Optional[str] = None,
    ):
        timeline_export_repo = cast(
            TimelineExportRepository, self.get_repository("timeline_export")
        )
        timeline_export_repo.request_export(
            uuid=UUID(uuid),
            type=type,
            output_user=self.cmd.user_info.__dict__,
            period_start=period_start,
            period_end=period_end,
        )


class RequestCaseTimelineExport(minty.cqrs.SplitCommandBase):
    name = "request_case_timeline_export"

    @validate_arguments
    def __call__(
        self,
        case_uuid: str,
        period_start: Optional[str] = None,
        period_end: Optional[str] = None,
    ):
        timeline_export_repo = cast(
            TimelineExportRepository, self.get_repository("timeline_export")
        )
        timeline_export_repo.request_export(
            uuid=UUID(case_uuid),
            type="case",
            output_user=self.cmd.user_info.__dict__,
            period_start=period_start,
            period_end=period_end,
        )


class RunExport(minty.cqrs.SplitCommandBase):
    name = "run_export"

    @validate_with(
        get_data(
            __name__,
            "validation/timeline_export/run_timeline_export.json",
        )
    )
    def __call__(
        self,
        type: str,
        uuid: str,
        output_user: dict,
        period_start: Optional[str] = None,
        period_end: Optional[str] = None,
    ):
        timeline_entry_repo = cast(
            TimelineEntryRepository, self.get_repository("timeline_entry")
        )
        timeline_entries = None

        start = None
        if period_start:
            start = dateutil.parser.isoparse(period_start)

        end = None
        if period_end:
            end = dateutil.parser.isoparse(period_end)

        if type in ["person", "organization", "employee"]:
            # get_contact_event_logs returns empty list([]) if there
            # are no events related to a subject

            timeline_entries = (
                timeline_entry_repo.get_contact_event_logs_generator(
                    contact_type=type,
                    contact_uuid=uuid,
                    user_info=UserInfo(**output_user),
                    period_start=start,
                    period_end=end,
                )
            )

        elif type == "case":
            # get_case_event_logs returns empty list([]) if there
            # are no events related to a case

            timeline_entries = (
                timeline_entry_repo.get_case_event_logs_generator(
                    case_uuid=UUID(uuid),
                    user_info=UserInfo(**output_user),
                    period_start=start,
                    period_end=end,
                )
            )

        if timeline_entries is not None:
            timeline_export_repo = cast(
                TimelineExportRepository,
                self.get_repository("timeline_export"),
            )
            timeline_export_repo.create_export(
                type=type,
                uuid=UUID(uuid),
                output_user=output_user,
                entries=timeline_entries,
            )
            timeline_export_repo.save()
