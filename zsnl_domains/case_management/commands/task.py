# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import minty.cqrs
from ..repositories import CaseRepository, EmployeeRepository, TaskRepository
from minty.exceptions import Conflict, Forbidden, NotFound
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast
from uuid import UUID


class CreateTask(minty.cqrs.SplitCommandBase):
    name = "create_task"

    @validate_with(get_data(__name__, "validation/tasks/create_task.json"))
    def __call__(self, case_uuid: str, task_uuid: str, title: str, phase: int):
        """
        Create task related to case.

        :param case_uuid: uuid of case
        :param task_uuid: uuid of task
        :param title: task title
        :param phase: phase of which task falls under
        :raises Forbidden: No permission to edit case
        :raises Conflict: Task is not editable
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))
        case_repo = cast(CaseRepository, self.get_repository("case"))

        try:
            case = case_repo.find_case_by_uuid(
                case_uuid=UUID(case_uuid),
                user_info=self.cmd.user_info,
                permission="write",
            )
        except NotFound:
            raise Forbidden(
                "Not allowed to perform action on task for this case.",
                "case/task/not_allowed",
            )
        task = task_repo.create_task(
            uuid=UUID(task_uuid), case=case, title=title, phase=phase
        )
        if task.is_editable and not task.completed:
            task_repo.save()
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )


class DeleteTask(minty.cqrs.SplitCommandBase):
    name = "delete_task"

    @validate_with(get_data(__name__, "validation/tasks/delete_task.json"))
    def __call__(self, task_uuid: str):
        """
        Delete task.

        :param task_uuid: task uuid
        :raises Conflict: when not allowed to edit / delete.
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))

        task = task_repo.get_task(
            task_uuid=UUID(task_uuid),
            permission="write",
            user_uuid=self.user_uuid,
        )
        if task.is_editable and not task.completed:
            task.delete()
            task_repo.save()
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )


class SetCompletionOnTask(minty.cqrs.SplitCommandBase):
    name = "set_completion_on_task"

    @validate_with(
        get_data(__name__, "validation/tasks/set_completion_on_task.json")
    )
    def __call__(self, task_uuid: str, completed: bool):
        """Set completion of task to True/False.

        :param task_uuid: uuid of task
        :param completed: completion of task
        :raises Conflict: when not allowed to edit
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))

        task = task_repo.get_task(
            task_uuid=UUID(task_uuid),
            permission="write",
            user_uuid=self.user_uuid,
        )
        if task.can_set_completion:
            task.set_completion(completed=completed)
            task_repo.save()
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )
        task_repo.create_task_status_notification_for_case_assignee(
            task=task, user_uuid=self.user_uuid
        )


class UpdateTask(minty.cqrs.SplitCommandBase):
    name = "update_task"

    @validate_with(get_data(__name__, "validation/tasks/update_task.json"))
    def __call__(
        self,
        task_uuid: str,
        title: str,
        description: str,
        due_date: str,
        assignee,
    ):
        """
        Update/edit a task.

        :param task_uuid: task uuid
        :param title: title of task
        :param description: description of task
        :param due_date: due date of task
        :param assignee: assignee uuid
        :raises Conflict: when not allowed to edit
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))
        employee_repo = cast(
            EmployeeRepository, self.get_repository("employee")
        )

        # yyyy-mm-dd format is checked by @validate_with
        date = due_date
        if due_date is not None:
            date = datetime.date.fromisoformat(due_date)

        task = task_repo.get_task(
            task_uuid=UUID(task_uuid),
            permission="write",
            user_uuid=self.user_uuid,
        )
        if assignee:
            assignee = employee_repo.find_employee_by_uuid(uuid=assignee)
            if not task.assignee or str(assignee.uuid) != str(
                task.assignee["uuid"]
            ):
                task_repo.create_assigned_notification_for_task_assignee(
                    task=task, user_uuid=self.user_uuid, assignee=assignee
                )

        if task.is_editable and not task.completed:
            task.update(
                title=title,
                description=description,
                due_date=date,
                assignee=assignee,
            )
            task_repo.save()
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )
