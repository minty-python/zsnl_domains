# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import minty.cqrs
from .._shared import get_subject
from ..entities import (
    CaseContact,
    CaseContactEmployee,
    CaseTypeVersionEntity,
    Department,
    Role,
)
from ..repositories import (
    CaseRepository,
    CaseTypeRepository,
    CaseTypeResultRepository,
    CustomObjectRepository,
    DepartmentRepository,
    ObjectRelationRepository,
    RoleRepository,
    SubjectRelationRepository,
)
from ..services import CaseTemplateService, RuleEngine
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_arguments
from typing import List, Optional, cast
from uuid import UUID


class SetRegistrationDate(minty.cqrs.SplitCommandBase):
    name = "set_case_registration_date"

    @validate_with(get_data(__name__, "validation/change_case_date.json"))
    def __call__(
        self,
        case_uuid: str,
        target_date: str,
    ):
        """
        Set case registration date for given case id and date.

        :param case_uuid: case uuid
        :param target_date: date to change registration date to
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )

        case.set_registration_date(
            registration_date=target_date,
            template_service=CaseTemplateService(),
        )
        repo.save()


class SetTargetCompletionDate(minty.cqrs.SplitCommandBase):
    name = "set_case_target_completion_date"

    @validate_with(get_data(__name__, "validation/change_case_date.json"))
    def __call__(self, case_uuid: str, target_date: str):
        """
        Set case target completion date for given case id and target date.

        :param case_uuid: case uuid
        :param target_date: new target completion date
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.set_target_completion_date(
            target_completion_date=target_date,
            template_service=CaseTemplateService(),
        )
        repo.save()


class SetCompletionDate(minty.cqrs.SplitCommandBase):
    name = "set_case_completion_date"

    @validate_with(get_data(__name__, "validation/change_case_date.json"))
    def __call__(self, case_uuid: str, target_date: str):
        """
        Set completion date of completed case.

        :param case_uuid: case uuid
        :param target_date: new completion date
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.set_completion_date(
            completion_date=target_date, template_service=CaseTemplateService()
        )
        repo.save()


class Pause(minty.cqrs.SplitCommandBase):
    name = "pause_case"

    @validate_with(get_data(__name__, "validation/pause_case.json"))
    def __call__(
        self,
        case_uuid: str,
        suspension_reason: str,
        suspension_term_type: str,
        suspension_term_value=None,
    ):
        """Pause case until given date and set suspension reason.

        :param case_uuid: case uuid
        :param suspension_reason: reason for suspension
        :param suspension_term_value: int or string or None the type of the suspension
        :param suspension_term_type: String or None the type of the suspension
        """
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.pause(
            suspension_reason=suspension_reason,
            suspension_term_value=suspension_term_value,
            suspension_term_type=suspension_term_type,
            template_service=CaseTemplateService(),
        )
        repo.save()


class Resume(minty.cqrs.SplitCommandBase):
    name = "resume_case"

    @validate_with(get_data(__name__, "validation/resume_case.json"))
    def __call__(
        self,
        case_uuid: str,
        resume_reason: str,
        stalled_until_date: str,
        stalled_since_date: str,
    ):
        """
        Resume stalled case and calculate new target completion date.

        :param case_uuid: case uuid
        :param resume_reason: Reason for the resume.
        :param stalled_until_date: stalled until date
        :param stalled_since_date: stalled since date
        """
        repo = cast(CaseRepository, self.get_repository("case"))

        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case.resume(
            stalled_since_date=stalled_since_date,
            stalled_until_date=stalled_until_date,
            resume_reason=resume_reason,
            template_service=CaseTemplateService(),
        )
        repo.save()


class AssignToDepartment(minty.cqrs.SplitCommandBase):
    name = "assign_case_to_department"

    @validate_with(
        get_data(__name__, "validation/assign_case_to_department.json")
    )
    def __call__(
        self,
        case_uuid: str,
        department_uuid: str,
        role_uuid: str,
    ):
        """
        Assign case to given department and role.

        :param case_uuid: str of the case
        :param department_uuid: str of the department
        :param role_uuid: str of the role
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        department_repo = cast(
            DepartmentRepository, self.get_repository("department")
        )
        department_entity = department_repo.find(UUID(department_uuid))

        role_repo = cast(RoleRepository, self.get_repository("role"))
        role_entity = role_repo.find(UUID(role_uuid))

        if role_entity or department_entity:
            case.clear_assignee(template_service=CaseTemplateService())
            case.set_allocation(
                department=department_entity,
                role=role_entity,
                template_service=CaseTemplateService(),
            )

        case_repo.save()


class AssignToUser(minty.cqrs.SplitCommandBase):
    name = "assign_case_to_user"

    @validate_with(get_data(__name__, "validation/assign_case_to_user.json"))
    def __call__(
        self,
        case_uuid: str,
        user_uuid: str,
    ):
        """
        Assign case to given user.

        :param case_uuid: str of the user
        :param user_uuid: str of the user
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))

        subject = case_repo.get_contact_employee(uuid=UUID(user_uuid))

        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )

        case.set_assignee(assignee=subject)

        if not case.coordinator:
            case.set_coordinator(coordinator=subject)

        case.set_status(status="open", template_service=CaseTemplateService())

        self.cmd.reassign_case_messages(
            case_uuid=case_uuid,
            subject_uuid=user_uuid,
        )

        case_repo.save()


class AssignToSelf(minty.cqrs.SplitCommandBase):
    name = "assign_case_to_self"

    @validate_with(get_data(__name__, "validation/assign_case_to_self.json"))
    def __call__(self, case_uuid: str):
        """
        Assign case to self. Special case of "Assign to user".

        :param case_uuid: case uuid
        """

        self.cmd.assign_case_to_user(
            case_uuid=case_uuid, user_uuid=self.user_uuid
        )


class ChangeCoordinator(minty.cqrs.SplitCommandBase):
    name = "change_case_coordinator"

    @validate_with(
        get_data(__name__, "validation/change_case_coordinator.json")
    )
    def __call__(self, case_uuid: str, coordinator_uuid: str):
        """
        Changes a coordinator of a case.

        :param case_uuid: UUID of the chase
        :param coordinator_uuid: UUID of the new coordinator (must be employee)
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))
        coordinator_entity = case_repo.get_contact_employee(
            uuid=UUID(coordinator_uuid)
        )

        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )

        case.set_coordinator(
            coordinator=coordinator_entity,
            template_service=CaseTemplateService(),
        )

        case_repo.save()


class Create(minty.cqrs.SplitCommandBase):
    name = "create_case"

    def _get_assignee_entity(
        self, case_type: CaseTypeVersionEntity, assignee_uuid, type
    ) -> Optional[CaseContactEmployee]:
        """
        Get assignee entity for case creation.

        :param case_type: case type for the case
        :param assignee_uuid: assignee for the case
        :return: assignee for the case as subject entity
        """
        if not assignee_uuid and case_type.preset_assignee:
            assignee_uuid = case_type.preset_assignee["id"]

        assignee_entity = None
        if assignee_uuid:
            case_repo = cast(CaseRepository, self.get_repository("case"))
            if type == "employee":
                assignee_entity = case_repo.get_contact_employee(
                    uuid=assignee_uuid
                )
            elif type == "person":
                assignee_entity = case_repo.get_contact_person(
                    uuid=assignee_uuid
                )
            elif type == "organization":
                assignee_entity = case_repo.get_contact_organization(
                    uuid=assignee_uuid
                )

        return assignee_entity

    def _get_department_entity(
        self,
        case_type: CaseTypeVersionEntity,
        department_uuid: Optional[UUID],
        use_assignee_department=False,
        assignee_entity: Optional[CaseContact] = None,
    ) -> Optional[Department]:
        """
        Get department entity for case creation.

        :param case_type: case type for the case
        :param department_uuid: uuid of department
        :return: department for the case as Department entity
        """
        if use_assignee_department and assignee_entity:
            return assignee_entity.department

        if not department_uuid and case_type.phases[0].allocation:
            department_uuid = case_type.phases[0].allocation.department.uuid

        if department_uuid:
            department_repo = cast(
                DepartmentRepository, self.get_repository("department")
            )
            department_entity = department_repo.find(department_uuid)
            return department_entity

        return None

    def _get_role_entity(
        self, case_type: CaseTypeVersionEntity, role_uuid: Optional[UUID]
    ) -> Optional[Role]:
        """Get role entity for case.

        :param case_type: case type of case
        :param role_uuid: uuid of the role
        :return: role for case as Role entity
        """
        if not role_uuid and case_type.phases[0].allocation:
            role_uuid = case_type.phases[0].allocation.role.uuid

        if role_uuid:
            role_repo = cast(RoleRepository, self.get_repository("role"))
            role_entity = role_repo.find(role_uuid)
            return role_entity

        return None

    def _get_requestor_entity(
        self, requestor_uuid: Optional[UUID], requstor_type: str
    ) -> Optional[CaseContact]:
        """Get requestor entity for case."""
        requestor_entity = None
        if requestor_uuid:
            case_repo = cast(CaseRepository, self.get_repository("case"))
            if requstor_type == "employee":
                requestor_entity = case_repo.get_contact_employee(
                    uuid=requestor_uuid
                )
            elif requstor_type == "person":
                requestor_entity = case_repo.get_contact_person(
                    uuid=requestor_uuid
                )
            elif requstor_type == "organization":
                requestor_entity = case_repo.get_contact_organization(
                    uuid=requestor_uuid
                )

        return requestor_entity

    @validate_with(get_data(__name__, "validation/create_case.json"))
    def __call__(
        self,
        case_uuid: str,
        case_type_version_uuid: str,
        contact_channel: str,
        requestor: dict,
        custom_fields: Optional[dict] = {},
        confidentiality: Optional[str] = None,
        assignment: Optional[dict] = None,
        contact_information: Optional[dict] = {},
        options: Optional[dict] = {},
    ):
        """
        Create a case.

        :param user_info: user_info
        :param case_uuid: UUID of the case(UUID is an optional field).
        :param case_type_uuid: UUID of the case type the case descended from.
        :param contactchannel: In which way the case ended in creation.
        :param requestor: requestor of the case.
        :param custom_fields: custom_fields of the case
        :param confidentiality: confidentiality of the case
        :param assignee: assignee of the case
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case_type = case_repo.get_case_type_version(
            case_type_version_uuid=UUID(case_type_version_uuid)
        )

        assignment = assignment or {}
        use_assignee_department = False
        send_email_to_assignee = False
        assignee_uuid = department_uuid = role_uuid = None
        assignee_entity = None

        if assignment.get("employee"):
            use_assignee_department = assignment["employee"].get(
                "use_employee_department"
            )
            send_email_to_assignee = assignment["employee"].get(
                "send_email_notification"
            )
            assignee_uuid = assignment["employee"]["id"]
            assignee_entity = self._get_assignee_entity(
                case_type, assignee_uuid, assignment["employee"].get("type")
            )

        if assignment.get("department"):
            department_uuid = UUID(assignment["department"]["id"])

        if assignment.get("role"):
            role_uuid = UUID(assignment["role"]["id"])

        requestor_entity = self._get_requestor_entity(
            requestor["id"], requestor["type"]
        )
        department_entity = self._get_department_entity(
            case_type,
            department_uuid,
            use_assignee_department,
            assignee_entity,
        )
        role_entity = self._get_role_entity(case_type, role_uuid)

        case = case_repo.create_new_case(
            user_info=self.cmd.user_info,
            uuid=UUID(case_uuid),
            case_type=case_type,
            contact_channel=contact_channel,
            requestor=requestor_entity,
            custom_fields=custom_fields,
            confidentiality=confidentiality,
            contact_information=contact_information,
            options=options,
            rule_engine=RuleEngine(),
            template_service=CaseTemplateService(),
        )

        if assignee_entity:
            case.set_assignee(
                assignee=assignee_entity,
                send_email_to_assignee=send_email_to_assignee,
                template_service=CaseTemplateService(),
            )
            if str(assignee_entity.uuid) == self.user_uuid:
                case.set_coordinator(coordinator=assignee_entity)
                case.set_status(
                    status="open", template_service=CaseTemplateService()
                )

        if role_entity or department_entity:
            case.set_allocation(
                department=department_entity,
                role=role_entity,
                template_service=CaseTemplateService(),
            )

        case_repo.save()


class EnqueueAssigneeMail(minty.cqrs.SplitCommandBase):
    name = "enqueue_case_assignee_email"

    @validate_with(
        get_data(__name__, "validation/enqueue_case_assignee_email.json")
    )
    def __call__(self, case_uuid: UUID):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="read",
        )
        case.enqueue_assignee_email()
        repo.save()


class SendAssigneeMail(minty.cqrs.SplitCommandBase):
    name = "send_case_assignee_email"

    @validate_with(
        get_data(__name__, "validation/send_case_assignee_email.json")
    )
    def __call__(self, case_uuid: str, queue_id: str):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="read",
        )
        case.send_assignee_email(queue_id=queue_id)
        repo.save()


class SynchronizeRelations(minty.cqrs.SplitCommandBase):
    name = "synchronize_relations_for_case"

    def _sync_subject_relation(
        self,
        repository,
        case_uuid,
        existing_subject_relations,
        custom_field_definition,
        custom_field_value,
    ):
        # This is not the prettiest way to handle this, but the legacy code
        # sends json-in-a-string
        custom_field_value = [
            json.loads(value) for value in custom_field_value
        ]

        if len(custom_field_value) == 0:
            # The user emptied the custom field. Remove relation.
            for relation in existing_subject_relations:
                self.logger.debug(f"Removing subject relation {relation.uuid}")
                relation.delete()
            return

        try:
            subject_uuid = custom_field_value[0]["value"]
            subject_type = custom_field_value[0]["specifics"][
                "relationship_type"
            ]
        except (KeyError, TypeError):
            self.logger.warning(
                "Exception caught while reading custom field value",
                exc_info=True,
            )
            return

        # The custom field has a value.
        found_existing = False
        for relation in existing_subject_relations:
            if str(relation.subject.id) != subject_uuid:
                relation.delete()
            else:
                found_existing = True

        if found_existing:
            self.logger.debug(
                "Found existing relation. Not creating a new one."
            )
            return
        subject = get_subject(self.get_repository, subject_type, subject_uuid)

        case_repo = cast(CaseRepository, self.get_repository("case"))
        user_info = case_repo.get_user_information(user_uuid=self.user_uuid)
        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=user_info,
            permission="write",
        )

        repository.create_subject_relation(
            case=case,
            subject=subject,
            role=custom_field_definition.relationship_subject_role,
            magic_string_prefix=custom_field_definition.field_magic_string,
            authorized=False,
            permission=None,
            send_confirmation_email=False,
            source_custom_field_type_id=custom_field_definition.uuid,
        )

    def _get_values_from_custom_field(self, custom_field_value) -> List:
        relations = []
        for object in custom_field_value:
            object_value = object.get("value")
            if isinstance(object_value, str):
                # single value relation
                relations.append(object)
            elif isinstance(object_value, list):
                # multi value relation
                relations.extend(object_value)
            else:
                raise ValueError(f"Invalid relation value {object}")
        return relations

    def _sync_object_relation(
        self,
        case_uuid: UUID,
        existing_object_relations,
        custom_field_definition,
        custom_field_value,
    ):
        custom_field_value = [
            json.loads(value) for value in custom_field_value
        ]

        old_objects = set(
            [
                o.custom_object_uuid
                for o in existing_object_relations
                if o.source_custom_field_type_id
                == custom_field_definition.uuid
            ]
        )

        relations = self._get_values_from_custom_field(custom_field_value)

        new_objects = set()
        for rel in relations:
            new_objects.add(UUID(rel.get("value")))

        removed_objects = old_objects - new_objects
        added_objects = new_objects - old_objects

        custom_object_repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )

        # remove custom_object relation(s)
        if removed_objects:
            for to_remove in removed_objects:
                custom_object = custom_object_repo.find_by_uuid(uuid=to_remove)
                if not custom_object:
                    self.logger.warning(
                        f"Custom object with uuid={to_remove} not found"
                    )
                    continue

                custom_object.unrelate_from(
                    relationship_type="case",
                    related_uuids=[case_uuid],
                    source_custom_field_uuid=custom_field_definition.uuid,
                )

        # add new custom_object relation(s)
        if added_objects:
            for to_add in added_objects:
                custom_object = custom_object_repo.find_by_uuid(uuid=to_add)
                if not custom_object:
                    self.logger.warning(
                        f"Custom object with uuid={to_add} not found"
                    )
                    continue

                custom_object.relate_to(
                    relationship_type="case",
                    related_uuids=[case_uuid],
                    source_custom_field_uuid=custom_field_definition.uuid,
                )

        custom_object_repo.save()

    @validate_with(
        get_data(__name__, "validation/synchronize_relations_for_case.json")
    )
    def __call__(self, case_uuid, custom_fields):
        case_type_repo = cast(
            CaseTypeRepository, self.get_repository("case_type")
        )
        case_type_version = case_type_repo.find_case_type_version_for_case(
            case_uuid=case_uuid
        )

        relation_fields = case_type_version.get_custom_fields_of_type(
            "relationship"
        )

        subject_relation_repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )
        case_repo = cast(CaseRepository, self.get_repository("case"))
        user_info = case_repo.get_user_information(user_uuid=self.user_uuid)
        existing_subject_relations = (
            subject_relation_repo.find_subject_relations_for_case(
                case_uuid=case_uuid, user_info=user_info
            )
        )

        object_relation_repo = cast(
            ObjectRelationRepository, self.get_repository("object_relation")
        )
        existing_object_relations = (
            object_relation_repo.find_object_relations_for_case(
                case_uuid=case_uuid
            )
        )
        for relation_field in relation_fields:
            if relation_field not in custom_fields:
                continue

            custom_field_definition = relation_fields[relation_field]

            if custom_field_definition.relationship_type == "subject":

                existing_relations = [
                    rel
                    for rel in existing_subject_relations
                    if str(rel.source_custom_field_type_id)
                    == str(custom_field_definition.uuid)
                ]

                self._sync_subject_relation(
                    repository=subject_relation_repo,
                    case_uuid=case_uuid,
                    existing_subject_relations=existing_relations,
                    custom_field_definition=custom_field_definition,
                    custom_field_value=custom_fields[relation_field],
                )
            elif custom_field_definition.relationship_type == "custom_object":

                existing_relations = [
                    rel
                    for rel in existing_object_relations
                    if rel.source_custom_field_type_id
                    == custom_field_definition.uuid
                ]

                self._sync_object_relation(
                    case_uuid=case_uuid,
                    existing_object_relations=existing_relations,
                    custom_field_definition=custom_field_definition,
                    custom_field_value=custom_fields[relation_field],
                )
            else:
                self.logger.debug(
                    f"Relation type ignored: {custom_field_definition.relationship_type}"
                )

        subject_relation_repo.save()


class SetCaseParent(minty.cqrs.SplitCommandBase):
    name = "set_case_parent"

    @validate_arguments
    def __call__(self, case_uuid: str, parent_uuid: str):
        case_repo = cast(CaseRepository, self.get_repository("case"))
        main_case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        main_case.set_case_parent(parent_uuid=parent_uuid)
        case_repo.save(
            user_info=self.cmd.user_info,
        )


class SetCaseResult(minty.cqrs.SplitCommandBase):
    name = "set_case_result"

    @validate_arguments
    def __call__(self, case_uuid: str, case_type_result_uuid: str):
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case_type_result_repo = cast(
            CaseTypeResultRepository, self.get_repository("case_type_result")
        )
        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="write",
        )
        case_repo.cache[case_uuid] = case

        # check if the case_type_result is available for this case
        matching_result = case_type_result_repo.get(
            case_uuid=UUID(case_uuid),
            case_type_result_uuid=UUID(case_type_result_uuid),
            user_info=self.cmd.user_info,
            permission="read",
        )

        case.set_case_result(
            matching_result,
            case_type_result_repo.get_case_type_result_id(
                case_type_result_uuid=UUID(case_type_result_uuid)
            ),
        )

        case_repo.save(
            user_info=self.cmd.user_info,
        )
