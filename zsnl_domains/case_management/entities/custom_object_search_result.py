# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from .custom_object import ValidArchiveStatus, ValidObjectStatus
from datetime import datetime
from minty.entity import Entity
from pydantic import Field
from typing import Dict, Optional
from uuid import UUID


class CustomObjectSearchOrder(str, enum.Enum):
    title_asc = "title"
    subtitle_asc = "subtitle"
    external_reference_asc = "external_reference"
    date_created_asc = "date_created"
    last_modified_asc = "last_modified"

    title_desc = "-title"
    subtitle_desc = "-subtitle"
    external_reference_desc = "-external_reference"
    date_created_desc = "-date_created"
    last_modified_desc = "-last_modified"


class RelatedCustomObjectType(Entity):
    entity_type = "custom_object_type"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of this entity")


class CustomObjectSearchResult(Entity):
    """Content of a user defined Custom Object"""

    entity_type = "custom_object"
    entity_id__fields = ["uuid"]

    entity_relationships = [
        "custom_object_type",
    ]

    name: str = Field(..., title="Name of the related object type")
    title: Optional[str] = Field(..., title="Title of this specific object")
    subtitle: Optional[str] = Field(
        ..., title="Subtitle of this specific object"
    )
    uuid: UUID = Field(
        ..., title="Internal identifier of this specific custom object version"
    )

    external_reference: Optional[str] = Field(
        None, title="External reference this specific object"
    )
    date_created: datetime = Field(
        ..., title="Date this custom object got created"
    )
    last_modified: datetime = Field(
        ..., title="Last modified date for this custom object"
    )
    date_deleted: datetime = Field(
        None, title="Deleted date for this custom object"
    )

    version_independent_uuid: UUID = Field(
        None, title="The version independent UUID for this object"
    )

    status: ValidObjectStatus = Field(..., title="Status of the custom object")
    archive_status: ValidArchiveStatus = Field(
        ..., title="Archive status of the custom object"
    )

    custom_fields: Dict = Field(
        default_factory=dict, title="Key-value pair of custom fields"
    )

    custom_object_type: RelatedCustomObjectType = Field(
        ..., title="Object type of this custom object"
    )
