# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..constants import PERMISSIONS_FOR_SUBJECT_RELATION
from minty.entity import Entity, Field, ValueObject
from minty.exceptions import Conflict
from typing import Optional
from uuid import UUID


class RelatedSubject(ValueObject):
    id: Optional[UUID] = Field(
        None, title="Internal identifier of related subject"
    )
    type: Optional[str] = Field(None, title="Type of related subject")
    name: Optional[str] = Field(None, title="Name of related subject")


class RelatedCase(ValueObject):
    id: Optional[UUID] = Field(
        None, title="Internal identifier of related Case"
    )
    type: Optional[str] = Field(None, title="Type is case")


class EnqueuedEmailData(ValueObject):
    case_uuid: Optional[UUID] = Field(
        None, title="Internal identifier of case"
    )
    subject_type: Optional[str] = Field(None, title="Type of related subject")
    subject_uuid: Optional[UUID] = Field(
        None, title="Internal identifier of subject"
    )
    loop_protection_counter: Optional[int] = Field(
        None, title="Counter value for loop protection"
    )


class SubjectRelation(Entity):
    entity_type = "subject_relation"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of subject relation")
    role: Optional[str] = Field(None, title="Role of subject to be related")

    magic_string_prefix: Optional[str] = Field(
        None, title="Magic string prefix for subject"
    )
    authorized: Optional[bool] = Field(
        None, title="Authorization value for the given subject"
    )
    subject: Optional[RelatedSubject] = Field(
        None, title="Details of related subject"
    )
    case: Optional[RelatedCase] = Field(None, title="Details of related case")
    send_confirmation_email: Optional[bool] = Field(
        None, title="Boolean indicating about send email or not"
    )
    enqueued_email_data: Optional[EnqueuedEmailData] = Field(
        None, title="Details for enqueued email"
    )
    permission: Optional[str] = Field(None, title="Permission for the subject")
    is_preset_client: Optional[bool] = Field(
        None, title="Boolean determining present client"
    )
    source_custom_field_type_id: Optional[UUID] = Field(
        None, title="Id of source custom field type"
    )
    type: str = Field(None, title="Type is subject_relation")

    @Entity.event("SubjectRelationCreated", extra_fields=["case", "subject"])
    def create(
        self,
        role: str,
        magic_string_prefix: str,
        subject: dict,
        case: dict,
        authorized: Optional[bool] = False,
        send_confirmation_email: Optional[bool] = False,
        permission: Optional[str] = None,
        source_custom_field_type_id: Optional[UUID] = None,
    ):
        """Create subject relations for a case."""
        self.role = role
        self.magic_string_prefix = magic_string_prefix
        self.subject = subject
        self.case = case
        self.source_custom_field_type_id = source_custom_field_type_id

        if subject["type"] == "employee":
            permission = permission or "none"
            if permission not in PERMISSIONS_FOR_SUBJECT_RELATION:
                raise Conflict(
                    f"'{permission}' is not a valid permission",
                    "subject_relation/invalid_permission",
                )
            self.permission = permission
            # Set pip_authorization to False and always send confirmation email
            # for employee
            self.authorized = False
            self.send_confirmation_email = True
        else:
            self.authorized = authorized
            self.send_confirmation_email = send_confirmation_email

    @Entity.event("SubjectRelationEmailEnqueued", extra_fields=["case"])
    def enqueue_email(self):
        self.enqueued_email_data = EnqueuedEmailData(
            case_uuid=self.case.id,
            subject_type=self.subject.type,
            subject_uuid=self.subject.id,
            loop_protection_counter=1,
        )

    @Entity.event("SubjectRelationEmailSend")
    def send_email(self):
        pass

    @Entity.event("SubjectRelationUpdated", extra_fields=["case", "subject"])
    def update(
        self,
        role: str,
        magic_string_prefix: str,
        authorized: Optional[bool] = None,
        permission: Optional[str] = None,
    ):
        """Update subject relation."""
        if self.role == "Aanvrager":
            raise Conflict(
                "Subject relation with role 'aanvrager' cannot be updated",
                "subject_relation/cannot_update_aanvrager",
            )

        self.role = role
        self.magic_string_prefix = magic_string_prefix
        if self.subject.type == "employee":
            if (
                permission
                and permission not in PERMISSIONS_FOR_SUBJECT_RELATION
            ):
                raise Conflict(
                    f"'{permission}' is not a valid permission",
                    "subject_relation/invalid_permission",
                )
            self.permission = permission
        elif authorized is not None:
            self.authorized = authorized

    @Entity.event(
        "SubjectRelationDeleted", extra_fields=["case", "subject", "role"]
    )
    def delete(self):
        """Delete subject relation."""
        if self.role == "Aanvrager":
            raise Conflict(
                "You cannot delete a subject_relation with 'Aanvrager' role",
                "subject_relation/cannot_delete_aanvrager",
            )
        else:
            pass
