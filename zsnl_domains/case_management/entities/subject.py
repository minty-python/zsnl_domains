# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .department import Department
from datetime import date
from minty.entity import Entity, Field
from typing import List, Optional
from uuid import UUID


class Subject(Entity):
    entity_type = "subject"
    entity_id__fields = ["uuid"]

    id: int = Field(..., title="Id of subject")
    uuid: UUID = Field(..., title="Internal identifier of this entity")
    subject_type: str = Field(..., title="type of subject")
    properties: Optional[dict] = Field(None, title="properties of subject")
    settings: Optional[dict] = Field(None, title="settingsof subject")
    username: str = Field(..., title="user of subject")
    last_modified: Optional[date] = Field(
        ..., title="last modified date of subject"
    )
    role_ids: List[int] = Field(..., title="role ids of subject")
    group_ids: List[int] = Field(..., title="group ids of subject")
    nobody: bool = Field(..., title="nobody of subject")
    system: bool = Field(..., title="system of subject")
    department: Optional[Department] = Field(None, title="system of subject")
    related_custom_object_uuid: Optional[UUID] = Field(
        None, title="system of subject"
    )
