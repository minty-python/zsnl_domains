# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class CaseBasic(Entity):
    entity_type = "CaseBasic"
    entity_id__fields = ["uuid"]

    number: int = Field(..., title="Id of Case")
    uuid: UUID = Field(..., title="Internal identifier of this entity")
    custom_fields: dict = Field(..., title="custom fields for the case")

    case_type_uuid: UUID = Field(..., title="uuid of case type")
    case_type_version_uuid: UUID = Field(..., title="case type version uuid")
