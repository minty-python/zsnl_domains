# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from enum import Enum
from minty.entity import Entity, Field, ValueObject
from typing import List, Optional
from uuid import UUID
from zsnl_domains.shared.entities.case import ValidCaseStatus


class DaysLeft(ValueObject):
    """Days left for the case"""

    amount: int = Field(..., title="Number of days left")
    unit: str = Field(..., title="Unit of measure")


class RelatedCaseTypeVersion(Entity):
    """Type of given case"""

    entity_type = "case_type_version"
    entity_id__fields = ["uuid"]

    uuid: Optional[UUID] = Field(
        None, title="Internal identifier of case type"
    )


class ValidArchivalState(str, Enum):
    """Valid archival state for the case"""

    overdragen = "overdragen"
    vernietigen = "vernietigen"


class ContactRelatedCase(Entity):
    """Content of related cases for given contact"""

    entity_type = "contact_related_case"
    entity_relationships = ["case_type"]
    entity_id__fields = ["uuid"]

    # Properties
    uuid: Optional[UUID] = Field(None, title="Internal identifier of the case")
    number: int = Field(..., title="Case ID")
    summary: Optional[str] = Field(None, title="Summary of the case")
    case_type: RelatedCaseTypeVersion = Field(
        ..., title="Case type of the case"
    )
    status: ValidCaseStatus = Field(..., title="Status of the case")
    progress: float = Field(..., title="Progress status of the case")
    roles: List[str] = Field(
        ..., title="Roles that contact has for the case (can be an empty list)"
    )
    days_left: Optional[DaysLeft] = Field(
        None, title="Days left for given case"
    )
    is_authorized: Optional[bool] = Field(
        None, title="Authorization status for given case"
    )
    archival_state: Optional[ValidArchivalState] = Field(
        ..., title="Archival state of case"
    )
    destruction_date: Optional[datetime] = Field(
        ..., title="Destruction date of case"
    )
