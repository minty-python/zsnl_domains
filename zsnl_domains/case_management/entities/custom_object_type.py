# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from enum import Enum
from minty.entity import Entity, Field, ValueObject
from pydantic import validator
from typing import Any, List, Optional, Set
from uuid import UUID
from zsnl_domains.shared import custom_field


class CustomObjectTypeRole(ValueObject):
    """A role in the organization"""

    uuid: UUID = Field(..., title="Unique identifier for this role")
    name: str = Field(..., title="Name of the role")


class CustomObjectTypeDepartment(ValueObject):
    """A department within an organization"""

    uuid: UUID = Field(..., title="Unique identifier for this department")
    name: str = Field(..., title="Name of the department")


class Authorization(str, Enum):
    read = "read"
    readwrite = "readwrite"
    admin = "admin"


class CustomObjectTypeAuthorization(ValueObject):
    """Authorization settings for this object type"""

    authorization: Authorization = Field(..., title="Set of permissions")

    role: CustomObjectTypeRole = Field(..., title="Role name")
    department: CustomObjectTypeDepartment = Field(..., title="Group name")


class CustomObjectTypeAuthorizationDefinition(ValueObject):
    """Authorization settings for this object type"""

    authorizations: Optional[List[CustomObjectTypeAuthorization]] = Field(
        None, title="Optional list of authorizations for this object"
    )


class CustomObjectTypeRelationship(ValueObject):
    """Authorization settings for this object type"""

    custom_object_type_uuid: Optional[UUID] = Field(
        None, title="UUID of related Custom Object Type"
    )

    name: str = Field(..., title="Name of this relationship")

    case_type_uuid: Optional[UUID] = Field(
        None, title="UUID of related Case Type"
    )

    description: Optional[str] = Field(
        None, title="Description of this field for internal purposes"
    )
    external_description: Optional[str] = Field(
        None, title="Description of this field for public purposes"
    )

    is_required: bool = Field(
        False,
        title="Indicate that this relationship is required on creation of this object",
    )

    # TODO: implement stringified ENUM of types
    # relationship_type: str = Field(..., title="Type of this relationship")

    @validator("case_type_uuid")
    def only_one_relationship(cls, v, values):
        """Allow only one uuid to be set per relationship definition"""

        if (
            "custom_object_type_uuid" in values
            and values["custom_object_type_uuid"]
            and v
        ):
            raise ValueError(
                "Cannot set a case_type_uuid when a custom_object_type_uuid is already set"
            )
        return v


class CustomObjectTypeRelationshipDefinition(ValueObject):
    """Relationship settings for this object type"""

    relationships: Optional[List[CustomObjectTypeRelationship]] = Field(
        None, title="Optional list of relationships for this object"
    )


class ValidAuditLogComponents(str, Enum):
    attributes = "attributes"
    authorizations = "authorizations"
    custom_fields = "custom_fields"
    relationships = "relationships"


class CustomObjectTypeAuditLog(ValueObject):
    """Auditlog for this Custom Object Type"""

    updated_components: List[ValidAuditLogComponents] = Field(
        ..., title="Components affected by this change"
    )
    description: str = Field(..., title="Description of this change")


class ValidObjectTypeStatus(str, Enum):
    active = "active"
    inactive = "inactive"


class CustomObjectType(Entity):
    """Definition of an object"""

    entity_type = "custom_object_type"

    # Properties
    name: str = Field(..., title="Name of this object type")
    uuid: UUID = Field(
        ..., title="Internal identifier of this specific object type version"
    )
    title: Optional[str] = Field(
        None, title="Title for the created object type"
    )
    subtitle: Optional[str] = Field(
        None, title="Subtitle for the created object type"
    )
    status: ValidObjectTypeStatus = Field(
        ValidObjectTypeStatus.active,
        title="The current status of this object type",
    )
    external_reference: Optional[str] = Field(
        None, title="An external reference for the object"
    )

    version: int = Field(..., title="The current version of this object type'")

    date_created: datetime = Field(..., title="Date this object got created")
    last_modified: datetime = Field(
        ..., title="Last modified date for this object"
    )
    date_deleted: Optional[datetime] = Field(
        None, title="Deleted date for this object"
    )

    version_independent_uuid: UUID = Field(
        None, title="The version independent UUID for this object"
    )
    is_active_version: bool = Field(
        ..., title="Indicates whether this entity is the latest version"
    )

    custom_field_definition: Optional[
        custom_field.CustomFieldDefinition
    ] = Field(None, title="Definition of the related custom fields")
    authorization_definition: Optional[
        CustomObjectTypeAuthorizationDefinition
    ] = Field(None, title="Authorization settings for this custom object type")
    relationship_definition: Optional[
        CustomObjectTypeRelationshipDefinition
    ] = Field(None, title="Relationship settings for this custom object type")

    audit_log: CustomObjectTypeAuditLog = Field(
        ..., title="Auditlog for this Custom Object Type Version"
    )
    catalog_folder_id: Optional[int] = Field(
        None, title="Integer of folder in our elements catalog"
    )

    entity_relationships = ["latest_version"]
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary", "entity_meta_authorizations"]

    entity_meta_authorizations: Optional[Set[Authorization]] = Field(
        None,
        title="The set of rights/authorizations the current user has for objects of the custom object type",
    )

    # Relationships
    latest_version: Any = Field(None, title="Name of this class")

    # function is added to seperate complexity in 1 function due to flake8
    def _update_entity_fields(
        self,
        uuid: UUID,
        name: str,
        title: str = None,
        subtitle: str = None,
        status: str = "active",
    ):
        if uuid is not None:
            self.uuid = uuid

        if name is not None:
            self.name = name

        if title is not None:
            self.title = title

        if subtitle is not None:
            self.subtitle = subtitle

        if status is not None:
            self.status = status

    # Events
    @classmethod
    @Entity.event("CustomObjectTypeCreated")
    def create(cls, **kwargs):
        return cls(**kwargs)

    @Entity.event("CustomObjectTypeUpdated")
    def update(
        self,
        uuid: UUID,
        name: str,
        audit_log: CustomObjectTypeAuditLog,
        external_reference: str = None,
        custom_field_definition: dict = None,
        authorization_definition: dict = None,
        relationship_definition: dict = None,
        title: str = None,
        subtitle: str = None,
        status: str = "active",
        catalog_folder_id: UUID = None,
    ):
        self._update_entity_fields(
            uuid=uuid,
            name=name,
            title=title,
            subtitle=subtitle,
            status=status,
        )
        self.audit_log = audit_log
        self.external_reference = external_reference

        if custom_field_definition is not None:
            self.custom_field_definition = (
                custom_field.CustomFieldDefinition.parse_obj(
                    custom_field_definition
                )
            )

        if authorization_definition is not None:
            self.authorization_definition = (
                CustomObjectTypeAuthorizationDefinition.parse_obj(
                    authorization_definition
                )
            )

        if relationship_definition is not None:
            self.relationship_definition = (
                CustomObjectTypeRelationshipDefinition.parse_obj(
                    relationship_definition
                )
            )

        if catalog_folder_id is not None:
            if catalog_folder_id == 0:
                self.catalog_folder_id = None
            else:
                self.catalog_folder_id = catalog_folder_id

    @Entity.event("CustomObjectTypeDeleted")
    def delete(self):
        pass
