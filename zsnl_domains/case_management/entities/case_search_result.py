# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import date
from minty.entity import Entity, ValueObject
from pydantic import Field
from typing import Optional
from uuid import UUID


class Assignee(ValueObject):
    uuid: UUID = Field(None, title="UUID of the assignee of the case")
    name: str = Field(None, title="Name of the assignee of the case")


class Requestor(ValueObject):
    uuid: UUID = Field(..., title="UUID of the requestor of the case")
    type: str = Field(..., title="Type of the requestor of the case")
    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchResult(Entity):
    entity_type = "case_search_result"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Uuid of the case")

    number: int = Field(..., title="Id of case")
    status: str = Field(..., title="Status of case")
    destruction_date: Optional[date] = Field(
        None, title="Destruction date of the case"
    )
    archival_state: Optional[str] = Field(
        None, title="Archival state of the case"
    )
    subject: Optional[str] = Field(None, title="Subject of te case")

    case_type_title: Optional[str] = Field(
        None, title="Title of the case_type"
    )

    requestor: Requestor = Field(..., title="Requestor of the case")
    assignee: Optional[Assignee] = Field(..., title="Assignee of the case")

    percentage_days_left: Optional[int] = Field(
        None,
        title="Percentage of remaining days until completion.",
        description="Indicates with a percentage how much time is left to "
        + "complete the case. The percentage will be above 100 if the "
        + "remaining days are exeeded. When a case has the status stalled, "
        + "then the value is null",
    )

    progress: int = Field(..., title="Progress status of the case")

    unread_message_count: int = Field(..., title="Number of unread messages")
    unaccepted_files_count: int = Field(..., title="Count of unaccepted files")
    unaccepted_attribute_update_count: int = Field(
        ..., title="Count of unaccepted updates"
    )
