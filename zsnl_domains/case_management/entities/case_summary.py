# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.entity
from pydantic import Field
from typing import Optional
from uuid import UUID


class AssigneeInfo(minty.entity.ValueObject):
    uuid: UUID = Field(..., title="UUID of the assignee")
    name: str = Field(..., title="Name of the assignee")


class CaseSummary(minty.entity.Entity):
    entity_type = "case_summary"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Unique identifier of the case")
    number: int = Field(..., title="Case number")
    progress_status: float = Field(
        ...,
        title="Progress of the case",
        description="""
        This value is expressed as a ratio of phases finished, with 0 meaning
        'not started' and 1 meaning 'completely done'.
        """,
    )
    summary: Optional[str] = Field(..., title="Summary/subject of the case")
    casetype_title: str = Field(..., title="Title of the case's case type")
    result: Optional[str] = Field(
        ..., title="Short name of the result of the case"
    )
    assignee: Optional[AssigneeInfo] = Field(..., title="Case assignee")
