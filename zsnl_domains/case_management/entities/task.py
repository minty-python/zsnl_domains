# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from .. import entities
from minty import cqrs
from minty.entity import EntityBase
from typing import Optional, TypedDict
from uuid import UUID


class TaskAssigneeData(TypedDict):
    "Data structure for task assignee data"
    type: str
    uuid: UUID
    display_name: str


class Task(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        title: Optional[str],
        description: Optional[str],
        due_date: Optional[datetime.date],
        completed: Optional[bool],
        user_defined: Optional[bool],
        assignee: Optional[TaskAssigneeData],
        case: Optional[dict],
        case_type: Optional[dict],
        phase: Optional[int],
        department: Optional[dict],
    ):
        self.uuid = uuid
        self.title = title
        self.description = description
        self.due_date = due_date
        self.completed = completed
        self.user_defined = user_defined
        self.case = case
        self.case_type = case_type
        self.assignee = assignee
        self.phase = phase
        self.department = department

    @cqrs.event("TaskCreated", extra_fields=["case", "title"])
    def create(self, title: str, case: entities.Case, phase: int):
        case_dict = {
            "id": case.id,
            "uuid": case.uuid,
            "status": case.status,
            "milestone": case.milestone,
        }
        self.case = case_dict

        self.title = title
        self.phase = phase
        self.completed = False
        self.user_defined = True
        self.description = ""

    @property
    def is_editable(self) -> bool:
        """Check to see if task is editable.

        Will return 'true' if the case is resolved, if the current milestone of
        the case is higher than the task's phase or if the task is NOT
        user-defined.

        :return: True/False
        :rtype: bool
        """
        if self.case["status"] == "resolved":
            return False
        elif self.case["milestone"] > self.phase:
            return False
        else:
            return True

    @property
    def can_set_completion(self) -> bool:
        return self.is_editable

    @cqrs.event("TaskDeleted", extra_fields=["case", "title"])
    def delete(self):
        pass

    @cqrs.event("TaskUpdated", extra_fields=["case", "title"])
    def update(
        self,
        title: str,
        description: str,
        due_date: datetime.date,
        assignee: entities.Employee,
    ):
        self.title = title
        self.description = description
        self.due_date = due_date

        if assignee:
            self.assignee = TaskAssigneeData(
                type="employee", uuid=assignee.uuid, display_name=""
            )
        else:
            self.assignee = None

    @cqrs.event("TaskCompletionSet")
    def set_completion(self, completed: bool):
        self.completed = completed
