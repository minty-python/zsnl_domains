# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from ._shared import AuthorizationLevel
from .custom_object_type import CustomObjectType
from datetime import datetime
from minty import exceptions
from minty.entity import Entity, ValueObject
from pydantic import Field
from typing import TYPE_CHECKING, Dict, List, Optional, Set
from uuid import UUID

# Prevent import loops by only importing when checking type information:
if TYPE_CHECKING:
    from ..services import CustomObjectTemplateService


class ValidArchiveStatus(str, enum.Enum):
    archive = "archived"
    to_destroy = "to destroy"
    to_preserve = "to preserve"


class ValidObjectStatus(str, enum.Enum):
    active = "active"
    inactive = "inactive"
    draft = "draft"


class CustomObjectArchiveMetadata(ValueObject):
    status: Optional[ValidArchiveStatus] = Field(
        None, title="Archival status for this custom object"
    )
    ground: Optional[str] = Field(
        None, title="Archival ground for this custom object"
    )
    retention: Optional[int] = Field(
        None, title="Archival retention for this custom object"
    )


class CustomObjectRelatedCase(Entity):
    entity_type = "case"
    entity_id__fields = ["uuid"]

    entity_meta__fields = ["entity_meta_summary", "source_custom_field_uuid"]

    uuid: UUID = Field(..., title="Internal identifier of this entity")
    source_custom_field_uuid: Optional[UUID] = Field(
        None,
        title="UUID of the library attribute this relation originates from.",
        description="""
        When relating objects to cases through case custom fields or objects to
        objects through object custom fields, the UUID of the library attribute
        the relation originates from is stored here, so when an object is
        linked from multiple custom fields in a single case/object, we can tell
        them apart.
        """,
    )


class CustomObjectRelatedCustomObject(Entity):
    entity_type = "custom_object"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of this entity")


class CustomObjectRelatedDocument(Entity):
    entity_type = "document"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of this entity")


class CustomObjectRelatedSubject(Entity):
    entity_type = "subject"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of this entity")


class CustomObject(Entity):
    """Content of a user defined Custom Object"""

    entity_type = "custom_object"

    _related_entity_map = {
        "cases": CustomObjectRelatedCase,
        "custom_objects": CustomObjectRelatedCustomObject,
        "subjects": CustomObjectRelatedSubject,
    }

    # Properties
    name: str = Field(..., title="Name of the related object type")
    title: Optional[str] = Field(..., title="Title of this specific object")
    subtitle: Optional[str] = Field(
        ..., title="Subtitle of this specific object"
    )
    uuid: UUID = Field(
        ..., title="Internal identifier of this specific custom object version"
    )

    external_reference: Optional[str] = Field(
        None, title="External reference this specific object"
    )
    status: ValidObjectStatus = Field(
        ValidObjectStatus.active,
        title="The current status of this custom object",
    )

    version: int = Field(
        ..., title="The current version of this custom object"
    )
    date_created: datetime = Field(
        ..., title="Date this custom object got created"
    )
    last_modified: datetime = Field(
        ..., title="Last modified date for this custom object"
    )
    date_deleted: Optional[datetime] = Field(
        None, title="Deleted date for this custom object"
    )

    version_independent_uuid: UUID = Field(
        None, title="The version independent UUID for this object"
    )

    is_active_version: bool = Field(
        ..., title="Indicates whether this entity is the latest version"
    )

    custom_fields: Dict = Field(
        default_factory=dict, title="Key-value pair of custom fields"
    )
    archive_metadata: CustomObjectArchiveMetadata = Field(
        ..., title="Archiving metadata for this object"
    )

    cases: List[CustomObjectRelatedCase] = Field(
        default_factory=list, title="List of related cases"
    )

    documents: List[CustomObjectRelatedDocument] = Field(
        default_factory=list, title="List of related documents"
    )

    subjects: List[CustomObjectRelatedSubject] = Field(
        default_factory=list, title="List of related subjects"
    )

    custom_objects: List[CustomObjectRelatedCustomObject] = Field(
        default_factory=list, title="List of related custom objects"
    )

    entity_relationships = [
        "custom_object_type",
        "cases",
        "documents",
        "custom_objects",
        "subjects",
    ]
    entity_id__fields = ["uuid"]

    entity_meta_authorizations: Optional[Set[AuthorizationLevel]] = Field(
        None,
        title="The set of rights/authorizations the current user has for the custom object",
    )

    entity_meta__fields = ["entity_meta_summary", "entity_meta_authorizations"]

    custom_object_type: CustomObjectType = Field(
        ..., title="Object type of this custom object"
    )

    # Events
    @classmethod
    @Entity.event("CustomObjectCreated")
    def create(cls, **kwargs):
        template_service = kwargs.pop("_template_service")

        custom_object = cls(**kwargs)
        custom_object._sync_title(template_service)
        custom_object._sync_subtitle(template_service)
        custom_object._sync_external_reference(template_service)

        return custom_object

    @Entity.event("CustomObjectRelatedTo")
    def relate_to(
        self,
        relationship_type: str,
        related_uuids: List[UUID],
        source_custom_field_uuid: Optional[UUID] = None,
    ):
        full_relationship_name = f"{relationship_type}s"

        try:
            relation_type = self._related_entity_map[full_relationship_name]
        except KeyError as e:
            raise KeyError(
                f"Cannot relate given type {relationship_type}"
            ) from e

        related_entities = getattr(self, full_relationship_name).copy()

        for related_uuid in related_uuids:
            relation_to_add = relation_type.parse_obj(
                {
                    "uuid": related_uuid,
                    "entity_id": related_uuid,
                    # Only used by CustomObjectRelatedCase entity:
                    "source_custom_field_uuid": source_custom_field_uuid,
                }
            )

            if relation_to_add in related_entities:
                raise exceptions.Conflict(
                    f"Object is already related to {relationship_type} "
                    f"{related_uuid} "
                    f"(source custom field: {source_custom_field_uuid})",
                    "custom_object/duplicate_relation",
                )

            related_entities.append(relation_to_add)

        setattr(self, full_relationship_name, related_entities)

    @Entity.event("CustomObjectUnrelatedFrom")
    def unrelate_from(
        self,
        relationship_type: str,
        related_uuids: List[UUID],
        source_custom_field_uuid: UUID = None,
    ):
        full_relationship_name = f"{relationship_type}s"

        try:
            relation_type = self._related_entity_map[full_relationship_name]
        except KeyError as e:
            raise KeyError(
                f"Cannot relate given type {relationship_type}"
            ) from e

        related_entities = getattr(self, full_relationship_name).copy()

        for related_uuid in related_uuids:
            relation_to_remove = relation_type.parse_obj(
                {
                    "uuid": related_uuid,
                    "entity_id": related_uuid,
                    # Only used by CustomObjectRelatedCase entity:
                    "source_custom_field_uuid": source_custom_field_uuid,
                }
            )

            try:
                related_entities.remove(relation_to_remove)
            except ValueError as e:
                raise exceptions.Conflict(
                    f"Object is not related to {relationship_type} "
                    f"{related_uuid} "
                    f"(source custom field: {source_custom_field_uuid})",
                    "custom_object/unknown_relation",
                ) from e

        setattr(self, full_relationship_name, related_entities)

    @Entity.event("CustomObjectUpdated")
    def update(
        self,
        template_service: "CustomObjectTemplateService",
        uuid: UUID,
        custom_fields: Dict,
        archive_metadata: Optional[CustomObjectArchiveMetadata] = None,
        cases: List = None,
        status: str = "active",
    ):
        if uuid is not None:
            self.uuid = uuid

        # TODO: Because we record changes to an attribute, this
        # will not work on a reference... We have to re-assign the object
        # afterwards.
        if custom_fields is not None:
            new_custom_fields = self.custom_fields.copy()
            for key, value in custom_fields.items():
                new_custom_fields[key] = value

            self.custom_fields = new_custom_fields

        # TODO: this sucks. Because we record changes to an attribute, this
        # will not work on a reference... We have to re-assign the object
        # afterwards.
        if archive_metadata is not None:
            current_archive_metadata = self.archive_metadata.dict()
            for key, value in archive_metadata.items():
                current_archive_metadata[key] = value

            self.archive_metadata = CustomObjectArchiveMetadata.parse_obj(
                current_archive_metadata
            )

        if cases is not None:
            newcases = []
            for case in cases:
                newcases.append(CustomObjectRelatedCase.parse_obj(case))

            self.cases = newcases

        if status is not None:
            self.status = ValidObjectStatus[status]

        self._sync_title(template_service)
        self._sync_subtitle(template_service)
        self._sync_external_reference(template_service)

    def _sync_title(self, template_service: "CustomObjectTemplateService"):
        self.title = template_service.render_value(
            custom_object=self, template="title"
        )

    def _sync_subtitle(self, template_service: "CustomObjectTemplateService"):
        self.subtitle = template_service.render_value(
            custom_object=self, template="subtitle"
        )

    def _sync_external_reference(
        self, template_service: "CustomObjectTemplateService"
    ):
        self.external_reference = template_service.render_value(
            custom_object=self, template="external_reference"
        )

    @Entity.event("CustomObjectDeleted")
    def delete(self):
        self.title = None
        self.subtitle = None
        self.version_independent_uuid = None
