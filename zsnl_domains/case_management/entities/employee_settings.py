# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, Field, ValueObject
from typing import Optional
from uuid import UUID


class NotificationSettings(ValueObject):
    """Notification settings for an employee"""

    new_document: bool = Field(
        ..., title="Notify outside zaaksytem in case of new document events."
    )
    new_assigned_case: bool = Field(
        ...,
        title="Send notification outside zaaksytem when new case is assigned to the employee",
    )
    case_term_exceeded: bool = Field(
        ...,
        title="Send notification outside zaaksytem when the processing time of case has expired",
    )

    new_ext_pip_message: bool = Field(
        ...,
        title="Send notification outside zaaksyteem when employee receives new pip message",
    )
    new_attribute_proposal: bool = Field(
        ...,
        title="Send notification outside zaaksytem when attribute has changed",
    )
    case_suspension_term_exceeded: bool = Field(
        ...,
        title="Send notification when the suspension period of case has expired",
    )


class Signature(Entity):
    """Signature filestore of an employee"""

    entity_type = "file"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the signature filestore")


class RelatedEmployee(Entity):
    entity_type = "employee"
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class EmployeeSettings(Entity):
    """Settings for an employee"""

    entity_type = "employee_settings"

    notification_settings: Optional[NotificationSettings] = Field(
        None, title="Notification settings of employee"
    )
    entity_relationships = ["signature", "employee"]
    signature: Optional[Signature] = Field(None, title="Signature of employee")

    phone_extension: Optional[str] = Field(
        None,
        title="The phone extension is used by the phone/PBX integration "
        "to show a pop-up when there's a call on the specified extension.",
    )

    employee: RelatedEmployee = Field(
        ..., title="Employee to which this settings are related"
    )

    # Events
    @Entity.event("NotificationSettingsSet", extra_fields=["employee"])
    def set_notification_settings(self, notification_settings: dict):
        self.notification_settings = NotificationSettings(
            **notification_settings
        )

    @Entity.event("SignatureSaved", extra_fields=["employee"])
    def save_signature(self, file_uuid: UUID):
        self.signature = Signature(
            **{"uuid": file_uuid, "entity_id": file_uuid}
        )

    @Entity.event("SignatureDeleted", extra_fields=["employee"])
    def delete_signature(self):
        self.signature = None

    @Entity.event("PhoneExtensionChanged", extra_fields=["employee"])
    def set_phone_extension(self, extension: Optional[str]):
        self.phone_extension = extension
