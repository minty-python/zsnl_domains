# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
import minty.cqrs
from ._shared import AuthorizationLevel
from minty import entity
from pydantic import Field
from typing import Generic, List, Optional, Set, TypeVar, Union
from uuid import UUID

SimpleFilterType = TypeVar("SimpleFilterType")


class SimpleFilterObject(entity.ValueObject, Generic[SimpleFilterType]):
    label: str = Field(
        ..., title="Human-readable preview value of the filter value"
    )
    value: SimpleFilterType = Field(..., title="Value to filter for")


class FilterDefinitionBase(entity.ValueObject):
    keyword: Optional[str]

    class Config:
        allow_population_by_field_name = True


class CustomObjectSearchFilterDefinition(FilterDefinitionBase):
    "Filter definition for saved searches for custom objects"

    custom_object_type: SimpleFilterObject[UUID] = Field(
        ..., alias="relationship.custom_object_type"
    )


class CaseSearchFilterDefinition(FilterDefinitionBase):
    "Filter definition for saved searches for cases"


class SavedSearchPermission(str, enum.Enum):
    """Permissions a user can give to a group/role on a saved search."""

    read = "read"
    write = "write"


class SortOrder(str, enum.Enum):
    asc = "asc"
    desc = "desc"


class SavedSearchKind(str, enum.Enum):
    case = "case"
    custom_object = "custom_object"


class SavedSearchPermissionDefinition(entity.ValueObject):
    group_id: UUID = Field(
        ..., title="Identifier of the group this permission applies to"
    )
    role_id: UUID = Field(
        ..., title="Identifier of the role this permission applies to"
    )
    permission: Set[SavedSearchPermission] = Field(
        ...,
        title="Permissions the specified group + role has for this saved search",
    )


class SavedSearchColumnDefinition(entity.ValueObject):
    source: List[str] = Field(
        ...,
        title="Path to retrieve the field from the case or custom_object entity",
    )
    label: str = Field(..., title="Human-readable name for this column")
    type: str = Field(
        ...,
        title="Type of the field; can be used to determine how to show the 'raw' value",
    )


class SavedSearchOwner(entity.Entity):
    entity_type = "employee"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class SavedSearch(entity.Entity):
    """
    Represents a saved state of the "advanced search" screen.

    Users can create (and save) multiple search queries, for quick access
    while working with the system.
    """

    entity_type = "saved_search"
    entity_id__fields = ["uuid"]
    entity_relationships = ["owner"]
    entity_meta__fields = ["entity_meta_summary", "entity_meta_authorizations"]

    entity_is_deleted = False

    entity_meta_authorizations: Optional[Set[AuthorizationLevel]] = Field(
        None,
        title="The set of rights/authorizations the current user has for the saved search",
    )

    uuid: UUID = Field(..., title="Identifier for this saved search")
    name: str = Field(..., title="Unique name of this search")

    owner: SavedSearchOwner = Field(..., title="Owner of this saved search")

    kind: SavedSearchKind = Field(
        ..., title="The kind of entity this saved search searches for"
    )

    filters: Union[
        CustomObjectSearchFilterDefinition, CaseSearchFilterDefinition
    ] = Field(..., title="Filters to apply when using this search")
    permissions: List[SavedSearchPermissionDefinition] = Field(
        ..., title="Group/role/permission"
    )

    columns: List[SavedSearchColumnDefinition] = Field(
        ..., title="Columns visible when using this search"
    )
    sort_column: str = Field(
        ..., title="Column to sort the result on, when using this search"
    )
    sort_order: SortOrder = Field(
        ..., title="Order for sorting results (ascending or descending)"
    )

    @classmethod
    @entity.Entity.event("SavedSearchCreated")
    def create(
        cls,
        uuid: UUID,
        name: str,
        kind: SavedSearchKind,
        owner: SavedSearchOwner,
        filters: Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        permissions: List[SavedSearchPermissionDefinition],
        columns: List[SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: SortOrder,
        event_service: minty.cqrs.EventService,
    ):
        return cls.parse_obj(
            {
                "entity_id": uuid,
                "uuid": uuid,
                "name": name,
                "kind": kind,
                "owner": owner,
                "filters": filters,
                "permissions": permissions,
                "columns": columns,
                "sort_column": sort_column,
                "sort_order": sort_order,
                "_event_service": event_service,
            }
        )

    @entity.Entity.event("SavedSearchDeleted")
    def delete(self):
        self.entity_is_deleted = True

    @entity.Entity.event("SavedSearchUpdated")
    def update(
        self,
        name: str,
        filters: Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        columns: List[SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: SortOrder,
    ):
        self.name = name
        self.filters = filters
        self.columns = columns
        self.sort_column = sort_column
        self.sort_order = sort_order

    @entity.Entity.event("SavedSearchPermissionsUpdated")
    def update_permissions(
        self, permissions: List[SavedSearchPermissionDefinition]
    ):
        self.permissions = permissions
