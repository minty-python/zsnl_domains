# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..shared.repositories import (
    ConfigurationRepository,
    SearchResultRepository,
)
from .commands import COMMANDS
from .queries import QUERIES
from .repositories import (
    CaseBasicRepository,
    CaseMessageListRepository,
    CaseRelationRepository,
    CaseRepository,
    CaseSearchResultRepository,
    CaseSummaryRepository,
    CaseTypeRepository,
    CaseTypeResultRepository,
    ContactRelatedCaseRepository,
    CountryRepository,
    CustomObjectRepository,
    CustomObjectSearchResultRepository,
    CustomObjectTypeRepository,
    DepartmentRepository,
    EmployeeRepository,
    EmployeeSettingsRepository,
    ExportFileRepository,
    ObjectRelationRepository,
    OrganizationRepository,
    PersonRepository,
    PersonSensitiveDataRepository,
    RelatedCaseRepository,
    RelatedObjectRepository,
    RelatedSubjectRepository,
    RoleRepository,
    SavedSearchRepository,
    SubjectRelationRepository,
    SubjectRepository,
    TaskRepository,
    TimelineEntryRepository,
    TimelineExportRepository,
)

REQUIRED_REPOSITORIES = {
    "case_basic": CaseBasicRepository,
    "case_message_list": CaseMessageListRepository,
    "case_relation": CaseRelationRepository,
    "case_search_result": CaseSearchResultRepository,
    "case_summary": CaseSummaryRepository,
    "case_type": CaseTypeRepository,
    "case_type_result": CaseTypeResultRepository,
    "case": CaseRepository,
    "config": ConfigurationRepository,
    "contact_related_case": ContactRelatedCaseRepository,
    "country": CountryRepository,
    "custom_object_search": CustomObjectSearchResultRepository,
    "custom_object_type": CustomObjectTypeRepository,
    "custom_object": CustomObjectRepository,
    "department": DepartmentRepository,
    "employee_settings": EmployeeSettingsRepository,
    "employee": EmployeeRepository,
    "export_file": ExportFileRepository,
    "object_relation": ObjectRelationRepository,
    "organization": OrganizationRepository,
    "person_sensitive_data": PersonSensitiveDataRepository,
    "person": PersonRepository,
    "related_case": RelatedCaseRepository,
    "related_object": RelatedObjectRepository,
    "related_subject": RelatedSubjectRepository,
    "role": RoleRepository,
    "saved_search": SavedSearchRepository,
    "search_result": SearchResultRepository,
    "subject_relation": SubjectRelationRepository,
    "subject": SubjectRepository,
    "task": TaskRepository,
    "timeline_entry": TimelineEntryRepository,
    "timeline_export": TimelineExportRepository,
}


def get_query_instance(repository_factory, context, user_uuid):
    return minty.cqrs.DomainQueryContainer(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        query_lookup_table=QUERIES,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return minty.cqrs.DomainCommandContainer(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
        command_lookup_table=COMMANDS,
    )
