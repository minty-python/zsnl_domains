# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities import Department, DepartmentSummary
from minty.entity import EntityCollection
from minty.exceptions import NotFound
from sqlalchemy import sql
from zsnl_domains.database import schema

department_parent_alias = sql.alias(schema.Group)
department_query = sql.select(
    [
        schema.Group.uuid,
        schema.Group.name,
        schema.Group.description,
        department_parent_alias.c.uuid.label("parent_uuid"),
        department_parent_alias.c.name.label("parent_name"),
    ]
).select_from(
    sql.join(
        schema.Group,
        department_parent_alias,
        sql.and_(
            sql.func.array_length(schema.Group.path, 1) > 1,
            schema.Group.path[sql.func.array_upper(schema.Group.path, 1) - 1]
            == department_parent_alias.c.id,
        ),
        isouter=True,
    )
)


class DepartmentRepository(DatabaseRepositoryBase):
    def find(self, uuid):
        """Find department by uuid."""

        qry_stmt = department_query.where(schema.Group.uuid == uuid)

        department_row = self.session.execute(qry_stmt).fetchone()

        if not department_row:
            raise NotFound(
                f"Department with uuid '{uuid}' not found.",
                "department/not_found",
            )
        return self._entity_from_row(row=department_row)

    def find_all(self) -> EntityCollection[Department]:
        """Get all departments."""

        department_rows = self.session.execute(department_query).fetchall()

        return EntityCollection[Department](
            [self._entity_from_row(row=r) for r in department_rows]
        )

    def find_by_id(self, id) -> Department:
        """Find department by id."""

        qry_stmt = department_query.where(schema.Group.id == id)

        department_row = self.session.execute(qry_stmt).fetchone()
        if not department_row:
            raise NotFound(
                f"Department with id '{id}' not found.",
                "department/not_found",
            )

        return self._entity_from_row(row=department_row)

    def _entity_from_row(self, row) -> Department:
        """Initialize Department Entity from a database row"""
        if row.parent_uuid is not None:
            parent = DepartmentSummary(
                entity_id=row.parent_uuid,
                entity_meta_summary=row.parent_name,
                uuid=row.parent_uuid,
                name=row.parent_name,
            )
        else:
            parent = None

        department = Department(
            entity_id=row.uuid,
            entity_meta_summary=row.name,
            uuid=row.uuid,
            name=row.name,
            description=row.description or "",
            parent=parent,
        )
        return department
