# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
from ... import DatabaseRepositoryBase
from ..entities import CaseBasic
from minty.cqrs import UserInfo
from minty.exceptions import NotFound
from minty.repository import Repository
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)
from zsnl_domains.shared.repositories.custom_fields import (
    custom_fields_query,
    file_custom_fields_query,
)


class CaseBasicRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "CaseBasic"

    def find_case_basic_by_uuid(
        self, case_uuid: UUID, user_info: UserInfo, permission: str
    ) -> CaseBasic:
        """Return case_basic entity for given id by fetching case from database.

        :param case_uuid: case uuid
        :param user_uuid: UUID of the user performing the action
        :param permission: permission needed for the action ("search", "read",
            "write" or "manage")
        :return: case_basic entity
        """
        db = self._get_infrastructure("database")
        query_result = self._get_basic_case(
            case_uuid, user_info, permission, db
        )
        case_entity = self._transform_to_entity(query_result=query_result)

        return case_entity

    def _transform_to_entity(self, query_result) -> CaseBasic:
        new_basic_case = CaseBasic(
            entity_id=query_result.uuid,
            entity_meta_summary=None,
            number=query_result.id,
            uuid=query_result.uuid,
            case_type_uuid=query_result.case_type_uuid,
            case_type_version_uuid=query_result.case_type_version_uuid,
            custom_fields=self._translate_custom_fields(
                query_result.custom_fields, query_result.file_custom_fields
            ),
            _event_service=self.event_service,
        )

        return new_basic_case

    def _get_basic_case(
        self, case_uuid: UUID, user_info: UserInfo, permission: str, db
    ) -> dict:
        """Retrieve a specific case from the database with permission check.

        :param case_uuid: Case to retrieve
        :param user: User wanting to retrieve the case
        :param permission: Purpose the case is retrieved for. Can be one of
            "search", "read", "write" or "manage".
        :param db: SQLAlchemy session to use for retrieval
        :return: The basic_case; raises an exception if the case can't be retrieved
            (either because it doesn't exist, or the user doesn't have the specified
            permissions)
        """

        case_alias = sql.alias(schema.Case)

        query = (
            sql.select(
                [
                    case_alias.c.id,
                    case_alias.c.uuid,
                    custom_fields_query(case_alias.c).label("custom_fields"),
                    file_custom_fields_query(case_alias.c).label(
                        "file_custom_fields"
                    ),
                    schema.Zaaktype.uuid.label("case_type_uuid"),
                    schema.ZaaktypeNode.uuid.label("case_type_version_uuid"),
                ]
            )
            .select_from(
                sql.join(
                    case_alias,
                    schema.Zaaktype,
                    sql.and_(case_alias.c.zaaktype_id == schema.Zaaktype.id),
                ).join(
                    schema.ZaaktypeNode,
                    sql.and_(
                        case_alias.c.zaaktype_node_id == schema.ZaaktypeNode.id
                    ),
                )
            )
            .where(
                sql.and_(
                    case_alias.c.uuid == case_uuid,
                    user_allowed_cases_subquery(
                        user_info, permission, case_alias=case_alias.c
                    ),
                )
            )
        )

        basic_case = self.session.execute(query).fetchone()

        if not basic_case:
            raise NotFound(f"CaseBasic with uuid '{case_uuid}' not found.")

        return basic_case

    def _translate_custom_fields(self, custom_fields, file_custom_fields):
        valid_json_types = [
            "geojson",
            "relationship",
            "address_v2",
            "appointment_v2",
        ]
        custom_fields_list = {}
        for field in custom_fields:
            if field["type"] in valid_json_types and field["value"]:
                try:
                    value = json.loads(field["value"][0])
                except (json.decoder.JSONDecodeError, TypeError):
                    # field["value"] is invalid json, return emtpy json
                    value = {}

                custom_fields_list[field["magic_string"]] = {
                    "type": field["type"],
                    "value": value,
                }
            else:
                custom_fields_list[field["magic_string"]] = {
                    "type": field["type"],
                    "value": field["value"],
                }

        for field in file_custom_fields:
            custom_fields_list[field["magic_string"]] = {
                "type": field["type"],
                "value": field["value"],
            }
        return custom_fields_list
