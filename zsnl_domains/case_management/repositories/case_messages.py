# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities.case_message_list import CaseMessageList
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class CaseMessageListRepository(DatabaseRepositoryBase):
    def get_messages_for_case(self, case_uuid: UUID) -> CaseMessageList:
        """Get message list entity for given `case_uuid`.

        :param case_uuid: case_uuid
        :type case_uuid: UUID
        :return: message list entity
        :rtype: CaseMessageList
        """
        db = self._get_infrastructure("database")
        case_row = (
            db.query(schema.Case.id)
            .filter(schema.Case.uuid == case_uuid)
            .one()
        )
        case_message_list = CaseMessageList(
            case_uuid=case_uuid, case_id=case_row.id
        )
        case_message_list.event_service = self.event_service
        return case_message_list

    def save(self):
        """Save events to database."""
        events = [
            ev
            for ev in self.event_service.event_list
            if ev.entity_type == "CaseMessageList"
        ]
        for event in events:
            db = self._get_infrastructure("database")

            try:
                subject_uuid = event.changes[0]["new_value"]["entity_id"]
            except KeyError:
                subject_uuid = event.changes[0]["new_value"]["uuid"]

            message_update_stmt = (
                sql.update(schema.Message)
                .where(
                    schema.Message.logging_id.in_(
                        sql.select([schema.Logging.id]).where(
                            schema.Logging.zaak_id
                            == event.entity_data["case_id"]
                        )
                    )
                )
                .values(
                    subject_id=sql.func.concat(
                        "betrokkene-medewerker-",
                        sql.select([sql.literal_column("id")])
                        .select_from(schema.Subject)
                        .where(schema.Subject.uuid == subject_uuid)
                        .scalar_subquery(),
                    )
                )
                .execution_options(synchronize_session=False)
            )
            db.execute(message_update_stmt)
