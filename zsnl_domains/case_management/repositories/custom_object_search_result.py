# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from ... import DatabaseRepositoryBase
from ...shared.types import (
    ComparisonFilterCondition,
    ComparisonFilterConditionStr,
)
from ..entities import custom_object as object_entities
from ..entities import custom_object_search_result as search_entities
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from minty.repository import Repository
from sqlalchemy import sql
from typing import List, Optional, Set
from uuid import UUID
from zsnl_domains.case_management.entities.custom_object import (
    AuthorizationLevel,
)
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.object_acl import (
    allowed_object_v2_subquery,
)
from zsnl_domains.shared.util import TimedInMilliseconds, escape_term_for_like

co = sql.alias(schema.CustomObject, name="co")
ot = sql.alias(schema.CustomObjectType, name="ot")
otv = sql.alias(schema.CustomObjectTypeVersion, name="otv")
cov = sql.alias(schema.CustomObjectVersion, name="cov")
covc = sql.alias(schema.CustomObjectVersionContent, name="covc")

custom_object_search_query = sql.select(
    [
        otv.c.uuid.label("object_type_uuid"),
        otv.c.name.label("object_type_name"),
        co.c.uuid.label("version_independent_uuid"),
        cov.c.uuid,
        cov.c.title,
        cov.c.subtitle,
        cov.c.external_reference,
        cov.c.status,
        cov.c.version,
        cov.c.date_created,
        cov.c.last_modified,
        cov.c.date_deleted,
        covc.c.archive_status,
        covc.c.archive_ground,
        covc.c.archive_retention,
        covc.c.custom_fields,
    ]
).select_from(
    sql.join(
        co,
        cov,
        sql.and_(
            cov.c.id == co.c.custom_object_version_id,
            sql.or_(
                cov.c.date_deleted.is_(None),
                cov.c.date_deleted
                > datetime.datetime.now(datetime.timezone.utc),
            ),
        ),
    )
    .join(covc, cov.c.custom_object_version_content_id == covc.c.id)
    .join(otv, otv.c.id == cov.c.custom_object_type_version_id)
    .join(ot, ot.c.id == otv.c.custom_object_type_id)
)


SORT_ORDER = {
    search_entities.CustomObjectSearchOrder.title_asc: [
        sql.asc(cov.c.title),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.title_desc: [
        sql.desc(cov.c.title),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.subtitle_asc: [
        sql.asc(cov.c.subtitle),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.subtitle_desc: [
        sql.desc(cov.c.subtitle),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.external_reference_asc: [
        sql.asc(cov.c.external_reference),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.external_reference_desc: [
        sql.desc(cov.c.external_reference),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.date_created_asc: [
        sql.asc(cov.c.date_created),
    ],
    search_entities.CustomObjectSearchOrder.date_created_desc: [
        sql.desc(cov.c.date_created),
    ],
    search_entities.CustomObjectSearchOrder.last_modified_asc: [
        sql.asc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.last_modified_desc: [
        sql.desc(cov.c.last_modified),
    ],
}


class CustomObjectSearchResultRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "CustomObjectSearchResult"

    def search(
        self,
        page: int,
        page_size: int,
        sort: search_entities.CustomObjectSearchOrder,
        custom_object_type_uuid: UUID,
        filter_status: Optional[Set[object_entities.ValidObjectStatus]],
        filter_archive_status: Optional[
            Set[object_entities.ValidArchiveStatus]
        ],
        filter_last_modified: Optional[List[ComparisonFilterConditionStr]],
        filter_keyword: Optional[str],
        user_info: UserInfo,
    ) -> EntityCollection[search_entities.CustomObjectSearchResult]:
        """Get list of Custom objects based on given filter."""

        offset = self._calculate_offset(page, page_size)

        query = custom_object_search_query.where(
            allowed_object_v2_subquery(
                user_info=user_info,
                authorization=AuthorizationLevel.read,
                object_alias=co,
            )
        )

        query = query.where(ot.c.uuid == custom_object_type_uuid)

        query = self._apply_filters(
            query,
            filter_status=filter_status,
            filter_archive_status=filter_archive_status,
            filter_last_modified=filter_last_modified,
            filter_keyword=filter_keyword,
        )

        sort_order = SORT_ORDER[sort]

        with TimedInMilliseconds(
            "custom_object_search", self.MAX_QUERYTIME_COUNT_MS
        ) as executed_in_time:
            custom_objects_list = self.session.execute(
                query.order_by(*sort_order).limit(page_size).offset(offset)
            ).fetchall()

        total_results = None
        if executed_in_time():
            total_results = self._get_count(query)

        rows = [self._entity_from_row(row=row) for row in custom_objects_list]
        return EntityCollection[search_entities.CustomObjectSearchResult](
            rows, total_results=total_results
        )

    def _apply_filters(
        self,
        query,
        filter_status: Optional[Set[object_entities.ValidObjectStatus]],
        filter_archive_status: Optional[
            Set[object_entities.ValidArchiveStatus]
        ],
        filter_last_modified: Optional[List[ComparisonFilterConditionStr]],
        filter_keyword: Optional[str],
    ):
        if filter_status:
            query = query.where(cov.c.status.in_(filter_status))

        if filter_archive_status:
            query = query.where(
                covc.c.archive_status.in_(filter_archive_status)
            )

        if filter_last_modified:
            query = self.apply_comparison_filter(
                query=query,
                column=cov.c.last_modified,
                comparison_filters=[
                    ComparisonFilterCondition[datetime.datetime].from_str(
                        filter
                    )
                    for filter in filter_last_modified
                ],
            )

        if filter_keyword:
            escaped_keyword = escape_term_for_like(filter_keyword)
            query = query.where(
                sql.or_(
                    cov.c.title.ilike(f"%{escaped_keyword}%", escape="~"),
                    cov.c.subtitle.ilike(f"%{escaped_keyword}%", escape="~"),
                    cov.c.external_reference.ilike(
                        f"%{escaped_keyword}%", escape="~"
                    ),
                )
            )

        return query

    def _entity_from_row(
        self, row
    ) -> search_entities.CustomObjectSearchResult:

        mapping = {
            "version_independent_uuid": "version_independent_uuid",
            "uuid": "uuid",
            "name": "object_type_name",
            "title": "title",
            "subtitle": "subtitle",
            "external_reference": "external_reference",
            "custom_fields": "custom_fields",
            "date_created": "date_created",
            "last_modified": "last_modified",
            "date_deleted": "date_deleted",
            "archive_status": "archive_status",
            "status": "status",
            "entity_id": "uuid",
            "entity_meta_summary": "title",
        }

        object_type_mapping = {
            "uuid": "object_type_uuid",
            "entity_id": "object_type_uuid",
        }

        entity_obj = {}
        for key, objkey in mapping.items():
            entity_obj[key] = getattr(row, objkey)

        entity_obj["custom_object_type"] = {}
        for key, objkey in object_type_mapping.items():
            entity_obj["custom_object_type"][key] = getattr(row, objkey)

        return search_entities.CustomObjectSearchResult.parse_obj(
            {**entity_obj, "_event_service": self.event_service}
        )
