# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import datetime
import pytz
import re
from ... import DatabaseRepositoryBase
from ..constants import CASE_EVENTS, DOCUMENT_EVENTS
from ..entities import TimelineEntry
from datetime import timezone
from minty.cqrs import UserInfo
from minty.exceptions import NotFound
from minty.repository import Repository
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from typing import Dict, Iterable, List, Optional, Set
from uuid import UUID
from zsnl_domains.case_management.entities.case import ValidEventCategory
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)

EXPORT_PAGE_SIZE = 2500

custom_object_events_query = sql.select(
    [
        schema.Logging.uuid.label("uuid"),
        schema.Logging.event_type.label("type"),
        schema.Logging.onderwerp.label("description"),
        schema.Logging.created_by_name_cache.label("user"),
        schema.Logging.created.label("created"),
        schema.Logging.zaak_id,
    ]
).where(schema.Logging.component == "custom_object")


class TimelineEntryRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "TimelineEntry"
    _events_to_calls: Dict[str, str] = {}

    def get_custom_object_event_logs(
        self,
        page: int,
        page_size: int,
        period_start: Optional[datetime.datetime],
        period_end: Optional[datetime.datetime],
        custom_object_uuid: UUID,
    ) -> List[TimelineEntry]:
        """Get list of Custom object event logs."""

        offset = self._calculate_offset(page, page_size)

        query = custom_object_events_query.where(
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "custom_object_uuid"
            ].astext
            == str(custom_object_uuid)
        )

        if period_start:
            query = query.where(schema.Logging.created >= period_start)

        if period_end:
            query = query.where(schema.Logging.created <= period_end)

        events = self.session.execute(
            query.order_by(schema.Logging.created.desc())
            .limit(page_size)
            .offset(offset)
        ).fetchall()

        return [self._entity_from_row(row=event_row) for event_row in events]

    def _get_subject_query(self, contact_type: str, contact_uuid: str):
        if contact_type == "person":
            return sql.select(
                [
                    sql.literal("natuurlijk_persoon").label("type"),
                    schema.NatuurlijkPersoon.id,
                ]
            ).where(sql.and_(schema.NatuurlijkPersoon.uuid == contact_uuid))
        elif contact_type == "organization":
            return sql.select(
                [sql.literal("bedrijf").label("type"), schema.Bedrijf.id]
            ).where(sql.and_(schema.Bedrijf.uuid == contact_uuid))
        else:
            return sql.select(
                [sql.literal("medewerker").label("type"), schema.Subject.id]
            ).where(
                sql.and_(
                    schema.Subject.uuid == contact_uuid,
                    schema.Subject.subject_type == "employee",
                )
            )

    def get_contact_event_logs_generator(
        self,
        contact_type: str,
        contact_uuid: str,
        user_info: UserInfo,
        period_start: Optional[datetime.datetime] = None,
        period_end: Optional[datetime.datetime] = None,
    ) -> Iterable[TimelineEntry]:
        page = 1
        page_size = EXPORT_PAGE_SIZE

        while log_entries := self.get_contact_event_logs(
            contact_type=contact_type,
            contact_uuid=contact_uuid,
            user_info=user_info,
            period_start=period_start,
            period_end=period_end,
            page=page,
            page_size=page_size,
        ):
            self.logger.debug(
                "Exporting timeline entries for contact "
                f"{contact_uuid}; page {page}"
            )

            yield from log_entries
            page += 1

        self.logger.debug(
            f"Finished exporting timeline entries for contact {contact_uuid}"
        )

        return

    def get_contact_event_logs(
        self,
        contact_type: str,
        contact_uuid: str,
        user_info: UserInfo,
        period_start: Optional[datetime.datetime] = None,
        period_end: Optional[datetime.datetime] = None,
        page: Optional[int] = None,
        page_size: Optional[int] = None,
    ) -> List[TimelineEntry]:

        contact_query = self._get_subject_query(contact_type, contact_uuid)
        row = self.session.execute(contact_query).fetchone()

        if not row:
            raise NotFound(
                f"Contact of type '{contact_type}' not found for uuid '{contact_uuid}'",
                "contact/not_found",
            )

        contact_unique_name = "betrokkene-" + row.type + "-" + str(row.id)

        events = []

        contact_events_query = self._get_contact_events_query(
            contact_unique_name, contact_type, contact_uuid, user_info
        )

        if period_start:
            contact_events_query = contact_events_query.where(
                sql.literal_column("created") >= period_start
            )

        if period_end:
            contact_events_query = contact_events_query.where(
                sql.literal_column("created") <= period_end
            )

        if page and page_size:
            offset = self._calculate_offset(page, page_size)
            contact_events_query = contact_events_query.limit(
                page_size
            ).offset(offset)

        events = self.session.execute(contact_events_query).fetchall()

        return [self._entity_from_row(row=event_row) for event_row in events]

    def _entity_from_row(self, row) -> TimelineEntry:
        mapping = {
            "entity_id": "uuid",
            "uuid": "uuid",
            "type": "type",
            "date": "created",
            "description": "description",
            "entity_meta_summary": "description",
            "user": "user",
        }

        entity_data = {}
        for key, objkey in mapping.items():
            entity_data[key] = getattr(row, objkey)

        case_mapping = {
            "case_id": "zaak_id",
            "entity_id": "case_uuid",
            "entity_meta_summary": "zaak_id",
        }

        if row.zaak_id is not None:
            entity_data["case"] = {}
            for key, objkey in case_mapping.items():
                entity_data["case"][key] = getattr(row, objkey)

        if getattr(row, "comment", None):
            entity_data["description"] += ", reden: " + str(row.comment)

        if entity_data["date"] is not None:
            display_date = (
                entity_data["date"]
                .replace(tzinfo=timezone.utc)
                .astimezone(tz=pytz.timezone("Europe/Amsterdam"))
            )

            entity_data["date"] = display_date

        if entity_data["type"] == "case/attribute/update":
            entity_data["attribute_value"] = getattr(
                row, "attribute_value", None
            )
            entity_data[
                "description"
            ] = self._generate_description_for_case_events(entity_data)

        return TimelineEntry.parse_obj(
            {**entity_data, "_event_service": self.event_service}
        )

    def _generate_description_for_case_events(self, entity_data):

        description = entity_data["description"]
        attribute_value = entity_data["attribute_value"]

        if attribute_value and attribute_value != "":
            attribute_value = re.sub(re.compile("<.*?>"), " ", attribute_value)
            description = description + " : " + attribute_value
        return description

    def _get_contact_events_query(
        self,
        contact_unique_name: str,
        contact_type: str,
        contact_uuid: str,
        user_info: UserInfo,
    ):
        event_types = [
            "auth/alternative",
            "case/checklist/item/update",
            "case/close",
            "case/create",
            "case/document/assign",
            "case/document/create",
            "case/document/label",
            "case/document/metadata/update",
            "case/document/publish",
            "case/document/reject",
            "case/document/rename",
            "case/document/trash",
            "case/document/unpublish",
            "case/note/create",
            "case/pip/feedback",
            "case/update/purge_date",
            "case/update/registration_date",
            "case/update/status",
            "case/update/subject",
            "case/update/target_date",
            "case/update/allocation",
            "case/attribute/update",
            "case/relation/update",
            "document/assign",
            "document/create",
            "case/accept",
            "document/delete_document",
            "document/metadata/update",
            "email/send",
            "object/view",
            "subject/contactmoment/create",
            "subject/create",
            "subject/inspect",
            "subject/note/create",
            "subject/update",
            "subject/update_contact_data",
            "case/contact_moment/created",
            "case/note/created",
            "case/email/created",
        ]

        contact_entries = sql.select(
            [
                schema.Logging.uuid.label("uuid"),
                schema.Logging.event_type.label("type"),
                schema.Logging.onderwerp.label("description"),
                schema.Logging.created_by_name_cache.label("user"),
                schema.Logging.created.label("created"),
                schema.Logging.zaak_id,
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "comment"
                ].astext.label("comment"),
                sql.literal(None).label("case_uuid"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "attribute_value"
                ].astext.label("attribute_value"),
            ]
        ).where(
            sql.and_(
                schema.Logging.restricted.is_(False),
                schema.Logging.event_type.in_(event_types),
                sql.or_(
                    sql.cast(schema.Logging.event_data, postgresql.JSON)[
                        "subject_id"
                    ].astext
                    == contact_unique_name,
                    schema.Logging.created_for == contact_unique_name,
                ),
            )
        )

        natuurlijk_persoon_for_case_entries = (
            sql.select(
                [
                    schema.Logging.uuid.label("uuid"),
                    schema.Logging.event_type.label("type"),
                    schema.Logging.onderwerp.label("description"),
                    schema.Logging.created_by_name_cache.label("user"),
                    schema.Logging.created.label("created"),
                    schema.Logging.zaak_id,
                    sql.cast(schema.Logging.event_data, postgresql.JSON)[
                        "comment"
                    ].astext.label("comment"),
                    schema.Case.uuid.label("case_uuid"),
                    sql.cast(schema.Logging.event_data, postgresql.JSON)[
                        "attribute_value"
                    ].astext.label("attribute_value"),
                ]
            )
            .select_from(
                sql.join(
                    schema.Logging,
                    schema.Case,
                    schema.Logging.zaak_id == schema.Case.id,
                ).join(
                    schema.ZaakBetrokkenen,
                    schema.Case.aanvrager == schema.ZaakBetrokkenen.id,
                )
            )
            .where(
                sql.and_(
                    schema.Logging.restricted.is_(False),
                    schema.Logging.event_type.in_(event_types),
                    schema.ZaakBetrokkenen.subject_id == contact_uuid,
                    user_allowed_cases_subquery(user_info, "read"),
                )
            )
        )
        uni = sql.union(
            contact_entries, natuurlijk_persoon_for_case_entries
        ).cte()
        return sql.select([uni]).order_by(sql.literal_column("created").desc())

    def _get_case_events_query(
        self,
        case_uuid: UUID,
        user_info: UserInfo,
    ):

        case_events_query = (
            sql.select(
                [
                    schema.Logging.uuid.label("uuid"),
                    schema.Logging.event_type.label("type"),
                    schema.Logging.onderwerp.label("description"),
                    schema.Logging.created_by_name_cache.label("user"),
                    schema.Logging.created.label("created"),
                    schema.Logging.zaak_id,
                    sql.cast(schema.Logging.event_data, postgresql.JSON)[
                        "comment"
                    ].astext.label("comment"),
                    schema.Case.uuid.label("case_uuid"),
                    sql.cast(schema.Logging.event_data, postgresql.JSON)[
                        "attribute_value"
                    ].astext.label("attribute_value"),
                ]
            )
            .select_from(
                sql.join(
                    schema.Logging,
                    schema.Case,
                    schema.Logging.zaak_id == schema.Case.id,
                )
            )
            .where(
                sql.and_(
                    schema.Logging.restricted.is_(False),
                    schema.Case.uuid == case_uuid,
                    user_allowed_cases_subquery(user_info, "read"),
                )
            )
        )
        return case_events_query

    def get_case_event_logs_generator(
        self,
        period_start: Optional[datetime.datetime],
        period_end: Optional[datetime.datetime],
        case_uuid: UUID,
        user_info: UserInfo,
    ) -> Iterable[TimelineEntry]:
        page = 1
        page_size = EXPORT_PAGE_SIZE

        while log_entries := self.get_case_event_logs(
            period_start=period_start,
            period_end=period_end,
            case_uuid=case_uuid,
            user_info=user_info,
            page=page,
            page_size=page_size,
        ):
            self.logger.debug(
                f"Exporting timeline entries for case {case_uuid}; page {page}"
            )
            yield from log_entries
            page += 1

        self.logger.debug(
            f"Finished exporting timeline entries for case {case_uuid}"
        )
        return

    def get_case_event_logs(
        self,
        period_start: Optional[datetime.datetime],
        period_end: Optional[datetime.datetime],
        case_uuid: UUID,
        user_info: UserInfo,
        page: Optional[int] = None,
        page_size: Optional[int] = None,
        filter_attributes_category: Optional[Set[ValidEventCategory]] = None,
    ) -> List[TimelineEntry]:

        query = self._get_case_events_query(
            case_uuid=case_uuid, user_info=user_info
        )

        if filter_attributes_category:
            query = self._apply_attributes_category_filter(
                query, filter_attributes_category
            )
        if period_start:
            query = query.where(schema.Logging.created >= period_start)

        if period_end:
            query = query.where(schema.Logging.created <= period_end)

        query = query.order_by(schema.Logging.created.desc())
        if page and page_size:
            offset = self._calculate_offset(page, page_size)
            query = query.limit(page_size).offset(offset)

        events = self.session.execute(query).fetchall()

        return [self._entity_from_row(row=event_row) for event_row in events]

    def _apply_attributes_category_filter(
        self, query, filter_attributes_category: Set[ValidEventCategory]
    ):

        include_types = []
        if "document" in filter_attributes_category:
            include_types += DOCUMENT_EVENTS

        elif "case_update" in filter_attributes_category:
            include_types += CASE_EVENTS

        query = query.where(schema.Logging.event_type.in_(include_types))
        return query
