# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
from sqlalchemy import sql
from zsnl_domains.database import schema


def get_case_type_version_uuid(case_id, db):
    row = (
        db.query(schema.Case, schema.ZaaktypeNode)
        .join(
            schema.ZaaktypeNode,
            schema.ZaaktypeNode.id == schema.Case.zaaktype_node_id,
        )
        .filter(schema.Case.id == case_id)
    ).one()
    return str(row.ZaaktypeNode.uuid)


def get_requestor_by_id(id, db):
    requestor = (
        db.query(schema.ZaakBetrokkenen)
        .filter(schema.ZaakBetrokkenen.id == id)
        .one()
    )

    mapped_type = {
        "medewerker": "employee",
        "natuurlijk_persoon": "person",
        "bedrijf": "organization",
    }
    return {
        "type": mapped_type[requestor.betrokkene_type],
        "uuid": str(requestor.subject_id),
    }


def get_subject_by_id(id, db):
    """Get subject by id.

    :param id: subject id
    :type id: int
    :param db: database
    :type db: database
    :return: subject
    :rtype: schema.Subject
    """
    return db.query(schema.Subject).get(id)


def get_subject_by_uuid(uuid, db):
    """Get subject and group by uuid.

    :param uuid: subject uuid
    :type uuid: UUID
    :param db: database
    :type db: databse
    :return: subject
    :rtype: schem.Subject
    """
    query = (
        db.query(schema.Subject, schema.Group)
        .join(schema.Group, schema.Group.id == schema.Subject.group_ids[1])
        .filter(schema.Subject.uuid == uuid)
    )
    return query.one()


def get_involved_entity_by_id(id, db):
    """Get involved entity by id.

    :param id: entity id
    :type id: int
    :param db: database
    :type db: database
    :return: involved entity
    :rtype: schema.ZaakBetrokkenen
    """
    return (
        db.query(schema.ZaakBetrokkenen)
        .filter(schema.ZaakBetrokkenen.id == id)
        .one()
    )


def add_entity_from_subject(subject, case_id, role, db):
    """Add new row to ZaakBetrokkenen table from a subject.

    :param DatabaseRepositoryBase: DatabaseRepositoryBase
    :type DatabaseRepositoryBase: DatabaseRepositoryBase
    :param subject: involved subject
    :type subject: Subject
    :param case_id: case id
    :type case_id: int
    :param role: role of involved subject
    :type role: str
    :return: inserted row id
    :rtype: int
    """

    properties = json.loads(subject["properties"])
    insert_stmt = sql.insert(
        schema.ZaakBetrokkenen,
        values={
            "zaak_id": case_id,
            "subject_id": subject["uuid"],
            "betrokkene_id": subject["id"],
            "gegevens_magazijn_id": subject["id"],
            "betrokkene_type": "medewerker",
            "naam": properties.get("displayname", None),
            "rol": role,
        },
    )

    inserted_id = db.execute(insert_stmt).inserted_primary_key[0]
    return inserted_id


def add_entity_from_natural_person(
    person, person_snapshot_id, case_id, role, db
):
    """Add new row to ZaakBetrokkenen table from a natural_person.

    :param person: natural person
    :type person: NatuurlijkPersoon
    :param person_snapshot_id: id of snapshot of person in GmNatuurlijkPersoon table
    :type person_snapshot_id: int
    :param case_id: case id
    :type case_id: int
    :param role: role for involved entity
    :type role: str
    :param db: DatabaseRepositoryBase
    :type db: DatabaseRepositoryBase
    :return: inserted row id
    :rtype: int
    """
    name = person["naamgebruik"]
    if person["voornamen"]:
        name = person["voornamen"] + " " + name

    insert_stmt = sql.insert(
        schema.ZaakBetrokkenen,
        values={
            "zaak_id": case_id,
            "subject_id": person["uuid"],
            "betrokkene_id": person_snapshot_id,
            "gegevens_magazijn_id": person["id"],
            "betrokkene_type": "natuurlijk_persoon",
            "naam": name,
            "rol": role,
        },
    )
    inserted_id = db.execute(insert_stmt).inserted_primary_key[0]
    return inserted_id


def add_entity_from_organization(
    organization, organization_snapshot_id, case_id, role, db
):
    """Add new row to ZaakBetrokkenen table from an organization.

    :param organization: organization
    :type subject: Bedrijf
    :param organization_snapshot_id: id of snapshot for organization in GmBedrijf table.
    :type organization_snapshot_id: int
    :param case_id: case id
    :type case_id: int
    :param role: role for involved subject
    :type role: str
    :param db: DatabaseRepositoryBase
    :type db: DatabaseRepositoryBase
    :return: inserted row id
    :rtype: int
    """

    insert_stmt = sql.insert(
        schema.ZaakBetrokkenen,
        values={
            "zaak_id": case_id,
            "subject_id": organization["uuid"],
            "betrokkene_id": organization_snapshot_id,
            "gegevens_magazijn_id": organization["id"],
            "betrokkene_type": "bedrijf",
            "naam": organization["handelsnaam"],
            "rol": role,
        },
    )
    inserted_id = db.execute(insert_stmt).inserted_primary_key[0]
    return inserted_id
