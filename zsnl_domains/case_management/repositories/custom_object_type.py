# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from ... import DatabaseRepositoryBase
from .. import entities
from minty.cqrs import Event, UserInfo
from minty.entity import _reflect
from minty.exceptions import Conflict, NotFound
from minty.repository import Repository
from sqlalchemy import select, sql
from typing import Optional
from uuid import UUID, uuid4
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories import object_type_acl


def _authorizations_subquery(user_uuid: UUID, custom_object_type_alias):
    cota = sql.alias(schema.CustomObjectTypeAcl)
    cot = custom_object_type_alias
    spm = sql.alias(schema.SubjectPositionMatrix)

    return (
        sql.select(cota.c.authorization)
        .select_from(
            sql.join(
                cota,
                spm,
                sql.and_(
                    spm.c.group_id == cota.c.group_id,
                    spm.c.role_id == cota.c.role_id,
                    spm.c.subject_id
                    == (
                        sql.select(schema.Subject.id)
                        .where(schema.Subject.uuid == user_uuid)
                        .scalar_subquery()
                    ),
                ),
            )
        )
        .where(cot.c.id == cota.c.custom_object_type_id)
        .group_by(cota.c.authorization)
    ).scalar_subquery()


class CustomObjectTypeRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "CustomObjectType"
    _events_to_calls = {
        "CustomObjectTypeCreated": "_update_or_create_object_type",
        "CustomObjectTypeUpdated": "_update_or_create_object_type",
        "CustomObjectTypeDeleted": "_save_delete_object_type",
    }

    def create(self, **kwargs) -> entities.CustomObjectType:
        """Create a new object type for supplied case & phase.

        :return: CustomObjectType
        :rtype: entities.CustomObjectType
        """
        current_datetime = datetime.datetime.now(datetime.timezone.utc)
        params = kwargs.copy()

        catalog_folder_uuid = params.get("catalog_folder_uuid", None)
        if catalog_folder_uuid is not None:
            params["catalog_folder_id"] = self._get_catalog_folder_id(
                catalog_folder_uuid
            )
        object_type = entities.CustomObjectType.create(
            # Create defaults
            version=1,
            date_created=current_datetime,
            last_modified=current_datetime,
            is_active_version=True,
            # Override with given parameters
            **params,
            entity_id=params["uuid"],
            entity_meta_summary=params["name"],
            # Eventservice
            _event_service=self.event_service,
        )

        return object_type

    def load_catalog_attribute_into_input(self, custom_field_def_dict):
        if "custom_fields" not in custom_field_def_dict:
            return False

        cf_uuid_map = {}
        for cf in custom_field_def_dict["custom_fields"]:
            if "attribute_uuid" not in cf or not cf["attribute_uuid"]:
                raise NotFound(
                    "Could not find attribute in library with missing 'attribute_uuid'",
                    "case_management/object_type/attribute_uuid/missing",
                )

            cf_uuid_map[str(cf["attribute_uuid"])] = cf

        db_uuid_map = self._get_catalog_attributes_by_uuids(cf_uuid_map)

        for uuid in cf_uuid_map.keys():
            if uuid not in db_uuid_map.keys():
                raise NotFound(
                    f"Could not find attribute in library with UUID {uuid}",
                    "case_management/object_type/attribute_uuid/not_found",
                )

            cf_uuid_map[uuid]["custom_field_type"] = db_uuid_map[
                uuid
            ].value_type

            cf_uuid_map[uuid]["magic_string"] = db_uuid_map[uuid].magic_string

            cf_uuid_map[uuid]["name"] = db_uuid_map[uuid].naam

            cf_uuid_map[uuid]["options"] = db_uuid_map[uuid].options

            cf_uuid_map[uuid]["multiple_values"] = db_uuid_map[
                uuid
            ].type_multiple

            if db_uuid_map[uuid].value_type == "relationship":
                specification = {
                    "type": db_uuid_map[uuid].relationship_type,
                    "uuid": db_uuid_map[uuid].relationship_uuid,
                    "name": db_uuid_map[uuid].relationship_name,
                }
                if cf_uuid_map[uuid].get("custom_field_specification"):
                    cf_uuid_map[uuid]["custom_field_specification"].update(
                        specification
                    )
                else:
                    cf_uuid_map[uuid][
                        "custom_field_specification"
                    ] = specification

    def load_catalog_relationship_into_input(self, relationship_def_dict):
        list_of_uuids = []
        db_uuid_map = {}
        if not self._get_relationship_info_from_database(
            list_of_uuids, db_uuid_map, relationship_def_dict
        ):
            return None

        for relationship in relationship_def_dict["relationships"]:
            uuid = None
            if (
                "custom_object_type_uuid" in relationship
                and relationship["custom_object_type_uuid"]
            ):
                uuid = relationship["custom_object_type_uuid"]
            if (
                "case_type_uuid" in relationship
                and relationship["case_type_uuid"]
            ):
                uuid = relationship["case_type_uuid"]

            relationship["name"] = str(db_uuid_map[str(uuid)].name)

    def load_authorization_into_input(self, authorization_def_dict):
        auth_uuid_map = {"roles": {}, "departments": {}}
        db_uuid_map = {"roles": {}, "departments": {}}
        self._get_authorization_info_from_database(
            auth_uuid_map, db_uuid_map, authorization_def_dict
        )

        for authorization in authorization_def_dict["authorizations"]:
            role = db_uuid_map["roles"].get(authorization["role"]["uuid"])
            dept = db_uuid_map["departments"].get(
                authorization["department"]["uuid"]
            )

            if not role:
                raise NotFound(
                    f"Could not find role with UUID {authorization['role']['uuid']}",
                    "case_management/object_type/authorization_role/not_found",
                )

            if not dept:
                raise NotFound(
                    f"Could not find department with UUID {authorization['department']['uuid']}",
                    "case_management/object_type/authorization_department/not_found",
                )

            authorization["role"]["name"] = role.name
            authorization["department"]["name"] = dept.name

    def _get_catalog_attributes_by_uuids(self, custom_field_map):
        stmt = sql.select(
            [
                schema.BibliotheekKenmerk.uuid,
                schema.BibliotheekKenmerk.naam,
                schema.BibliotheekKenmerk.magic_string,
                schema.BibliotheekKenmerk.value_type,
                schema.BibliotheekKenmerk.value_default,
                schema.BibliotheekKenmerk.type_multiple,
                schema.BibliotheekKenmerk.relationship_type,
                schema.BibliotheekKenmerk.relationship_name,
                schema.BibliotheekKenmerk.relationship_uuid,
                sql.func.ARRAY(
                    sql.select([schema.BibliotheekKenmerkenValues.value])
                    .select_from(schema.BibliotheekKenmerkenValues)
                    .where(
                        sql.and_(
                            schema.BibliotheekKenmerkenValues.bibliotheek_kenmerken_id
                            == schema.BibliotheekKenmerk.id,
                            schema.BibliotheekKenmerkenValues.active.is_(True),
                        )
                    )
                    .scalar_subquery()
                ).label("options"),
            ]
        ).where(schema.BibliotheekKenmerk.uuid.in_(custom_field_map.keys()))

        rproxy = self.session.execute(stmt)
        return {str(row.uuid): row for row in rproxy.fetchall()}

    def _assert_catalog_attributes(self, custom_fields):
        cf_uuid_map = {}
        for cf in custom_fields:
            cf_uuid_map[str(cf.attribute_uuid)] = cf

        db_uuid_map = self._get_catalog_attributes_by_uuids(cf_uuid_map)

        for uuid in cf_uuid_map.keys():
            cf_uuid_map[uuid].custom_field_type = db_uuid_map[uuid].value_type

            cf_uuid_map[uuid].magic_string = db_uuid_map[uuid].magic_string

            cf_uuid_map[uuid].name = db_uuid_map[uuid].naam

            cf_uuid_map[uuid].options = db_uuid_map[uuid].options

            if db_uuid_map[uuid].value_type == "relationship":
                cf_uuid_map[uuid].custom_field_specification.parse_obj(
                    {
                        "type": db_uuid_map[uuid].relationship_type,
                        "uuid": db_uuid_map[uuid].relationship_uuid,
                        "name": db_uuid_map[uuid].relationship_name,
                    }
                )

    def _update_or_create_object_type(
        self, event, user_info=None, dry_run=None
    ):
        changes = event.format_changes()

        # Find object in database
        previous_uuid = event.previous_value("uuid")
        objecttype = None
        if previous_uuid is not None:
            objecttype = self.find_by_uuid(uuid=str(previous_uuid))

        # This is a fresh create, get object from scratch
        mark_create = False
        if not objecttype:
            objecttype = entities.CustomObjectType(**changes)
            mark_create = True
        else:
            # Apply changes over CustomObjectType:
            self._apply_changes(objecttype, changes)

        self.assert_custom_object_type_integrity(custom_object_type=objecttype)

        self._save_object_type_to_database(
            objecttype,
            create=mark_create,
            previous_uuid=str(event.previous_value("uuid")),
        )

    def assert_custom_object_type_integrity(
        self, custom_object_type: entities.CustomObjectType
    ):
        if custom_object_type.custom_field_definition:
            self._assert_catalog_attributes(
                custom_object_type.custom_field_definition.custom_fields
            )

        if custom_object_type.relationship_definition:
            self._assert_relationship_definition(
                definition=custom_object_type.relationship_definition
            )

    def _get_authorization_info_from_database(
        self, auth_uuid_map, db_uuid_map, definition
    ):
        if (
            "authorizations" not in definition
            or not definition["authorizations"]
        ):
            raise KeyError(
                "No authorizations given in authorization_definition"
            )

        for authorisation in definition["authorizations"]:
            auth_uuid_map["roles"][
                str(authorisation["role"]["uuid"])
            ] = authorisation
            auth_uuid_map["departments"][
                str(authorisation["department"]["uuid"])
            ] = authorisation

        departments = self.session.execute(
            sql.select([schema.Group.uuid, schema.Group.name]).where(
                schema.Group.uuid.in_(auth_uuid_map["departments"].keys())
            )
        ).fetchall()
        roles = self.session.execute(
            sql.select([schema.Role.uuid, schema.Role.name]).where(
                schema.Role.uuid.in_(auth_uuid_map["roles"].keys())
            )
        ).fetchall()

        db_uuid_map["departments"] = {
            str(row.uuid): row for row in departments
        }
        db_uuid_map["roles"] = {str(row.uuid): row for row in roles}

    def _get_relationship_info_from_database(
        self, list_of_uuids, db_uuid_map, definition
    ):
        if (
            "relationships" not in definition
            or not definition["relationships"]
        ):
            return

        relationship_uuid_map = {}
        for relationship in definition["relationships"]:
            if (
                "custom_object_type_uuid" in relationship
                and relationship["custom_object_type_uuid"]
            ):
                relationship_uuid_map[
                    relationship["custom_object_type_uuid"]
                ] = "custom_object_type"

            if (
                "case_type_uuid" in relationship
                and relationship["case_type_uuid"]
            ):
                relationship_uuid_map[
                    relationship["case_type_uuid"]
                ] = "case_type"

        # Do a union getting the primary database ids for this relationship
        for key in relationship_uuid_map.keys():
            list_of_uuids.append(key)

        dbresults = self.session.execute(
            sql.union_all(
                sql.select(
                    [
                        schema.Zaaktype.uuid.label("uuid"),
                        schema.ZaaktypeNode.titel.label("name"),
                        sql.literal("case_type").label("type"),
                    ]
                )
                .select_from(
                    sql.join(
                        schema.Zaaktype,
                        schema.ZaaktypeNode,
                        schema.Zaaktype.zaaktype_node_id
                        == schema.ZaaktypeNode.id,
                    )
                )
                .where(schema.Zaaktype.uuid.in_(list_of_uuids)),
                sql.select(
                    [
                        schema.CustomObjectType.uuid.label("uuid"),
                        schema.CustomObjectTypeVersion.name.label("name"),
                        sql.literal("custom_object_type").label("type"),
                    ]
                )
                .select_from(
                    sql.join(
                        schema.CustomObjectType,
                        schema.CustomObjectTypeVersion,
                        schema.CustomObjectType.custom_object_type_version_id
                        == schema.CustomObjectTypeVersion.id,
                    )
                )
                .where(schema.CustomObjectType.uuid.in_(list_of_uuids)),
            )
        ).fetchall()

        for row in dbresults:
            db_uuid_map[str(row.uuid)] = row

        for uuid in list_of_uuids:
            if str(uuid) not in db_uuid_map.keys():
                raise NotFound(
                    f"Could not find relationship {uuid} of type {relationship_uuid_map[uuid]}",
                    "case_management/custom_object_type/relationship/not_found",
                )

        return True

    def _assert_relationship_definition(self, definition):
        list_of_uuids = []
        db_uuid_map = {}
        self._get_relationship_info_from_database(
            list_of_uuids, db_uuid_map, definition.dict()
        )

    def _save_object_type_to_database(
        self, objecttype, create=False, previous_uuid=None
    ):
        entity_data = _reflect(objecttype)
        ot_table = {
            k: v
            for k, v in entity_data.items()
            if k
            in [
                "name",
                "status",
                "title",
                "subtitle",
                "external_reference",
                "date_created",
                "last_modified",
                "date_deleted",
                "custom_field_definition",
                "relationship_definition",
                "audit_log",
                "uuid",
            ]
        }

        if create is True:
            ot_table["version"] = 1

            # Set timestamps
            ot_table["date_created"] = ot_table[
                "last_modified"
            ] = datetime.datetime.now(datetime.timezone.utc)
        else:
            # Get latest version of this object
            stmt = (
                sql.select(
                    [
                        sql.expression.func.max(
                            schema.CustomObjectTypeVersion.version
                        ).label("max_version")
                    ]
                )
                .select_from(schema.CustomObjectTypeVersion)
                .where(schema.CustomObjectTypeVersion.uuid == previous_uuid)
            )
            rproxy = self.session.execute(stmt)
            maxrow = rproxy.fetchone()
            ot_table["version"] = maxrow.max_version + 1

            # Set timestamps and reset uuid
            ot_table["last_modified"] = datetime.datetime.now(
                datetime.timezone.utc
            )

        insert_stmt_otv = sql.insert(
            schema.CustomObjectTypeVersion, values=ot_table
        ).returning(schema.CustomObjectTypeVersion.id)

        otv_result = self.session.execute(insert_stmt_otv)
        objecttype_version_id = otv_result.fetchone().id
        objecttype_id = None

        if create is True:
            # Insert row in objecttype tabel
            insert_stmt_ot = sql.insert(
                schema.CustomObjectType,
                values={
                    "uuid": uuid4(),
                    "custom_object_type_version_id": objecttype_version_id,
                    "authorization_definition": entity_data[
                        "authorization_definition"
                    ],
                    "catalog_folder_id": entity_data["catalog_folder_id"],
                },
            ).returning(schema.CustomObjectType.id)

            # Make sure all references are set correctly
            ot_result = self.session.execute(insert_stmt_ot)
            objecttype_id = ot_result.fetchone().id
        else:
            stmt = sql.select([schema.CustomObjectType.id])

            rproxy = self.session.execute(
                stmt.where(
                    schema.CustomObjectType.uuid
                    == objecttype.version_independent_uuid
                )
            )
            otrow = rproxy.fetchone()
            objecttype_id = otrow.id

            self.session.execute(
                sql.update(schema.CustomObjectType)
                .values(
                    {
                        schema.CustomObjectType.custom_object_type_version_id: objecttype_version_id,
                        schema.CustomObjectType.authorization_definition: entity_data[
                            "authorization_definition"
                        ],
                        schema.CustomObjectType.catalog_folder_id: entity_data[
                            "catalog_folder_id"
                        ],
                    }
                )
                .where(schema.CustomObjectType.id == objecttype_id)
                .execution_options(synchronize_session=False)
            )
            self.session.commit()

        self.session.execute(
            sql.update(schema.CustomObjectTypeVersion)
            .values(
                {
                    schema.CustomObjectTypeVersion.custom_object_type_id: objecttype_id
                }
            )
            .where(schema.CustomObjectTypeVersion.id == objecttype_version_id)
            .execution_options(synchronize_session=False)
        )

    def _apply_changes(self, objecttype, changes):
        for k, v in changes.items():
            setattr(objecttype, k, v)

    def _save_delete_object_type(
        self, event: Event, user_info: UserInfo = None, dry_run: bool = False
    ):
        ## Get objecttype id from objecttype_uuid
        stmt = sql.select(
            [schema.CustomObjectTypeVersion.custom_object_type_id]
        )

        rproxy = self.session.execute(
            stmt.where(
                schema.CustomObjectTypeVersion.uuid == str(event.entity_id)
            )
        )
        otrow = rproxy.fetchone()
        objecttype_id = otrow.custom_object_type_id

        if not objecttype_id:
            raise NotFound(
                "Could not find objecttype_version with uuid {}".format(
                    str(event.entity_id)
                ),
                "case_management/custom_object_type/not_found",
            )

        ### TODO: check if there is any object created related to this version, if so:
        self.session.execute(
            sql.update(schema.CustomObjectTypeVersion)
            .values(
                {
                    schema.CustomObjectTypeVersion.date_deleted: datetime.datetime.now(
                        datetime.timezone.utc
                    )
                }
            )
            .where(
                schema.CustomObjectTypeVersion.custom_object_type_id
                == objecttype_id
            )
            .execution_options(synchronize_session=False)
        )

        self.session.commit()

    def find_by_uuid(
        self,
        uuid: UUID,
        user_info: Optional[UserInfo] = None,
        authorization: Optional[entities.AuthorizationLevel] = None,
    ) -> entities.CustomObjectType:
        stmt = self._select_stmt_objecttype(
            uuid=uuid, user_info=user_info, authorization=authorization
        )

        objecttype = self._load_from_stmt(stmt, uuid)

        return objecttype

    def find_by_version_independent_uuid(
        self,
        uuid: UUID,
        user_info: Optional[UserInfo] = None,
        authorization: Optional[entities.AuthorizationLevel] = None,
    ) -> entities.CustomObjectType:
        stmt = self._select_stmt_objecttype(
            version_independent_uuid=uuid,
            user_info=user_info,
            authorization=authorization,
        )

        objecttype = self._load_from_stmt(stmt, None, uuid)

        return objecttype

    def _entity_from_row(self, row, mapping=None) -> entities.CustomObjectType:
        if mapping is None:
            mapping = {}
        entity_keys = [
            "uuid",
            "name",
            "title",
            "subtitle",
            "external_reference",
            "audit_log",
            "status",
            "version",
            "date_created",
            "last_modified",
            "date_deleted",
            "catalog_folder_id",
            "version_independent_uuid",
            "is_active_version",
            "custom_field_definition",
            "authorization_definition",
            "relationship_definition",
            "entity_id",
            "entity_meta_summary",
            "entity_meta_authorizations",
        ]

        entity_obj = {}
        for key in entity_keys:
            dbkey = key
            if key in mapping.keys():
                dbkey = mapping[key]

            if not hasattr(row, dbkey):
                continue

            entity_obj[key] = getattr(row, dbkey)

        # Mark active version
        if "is_active_version" in entity_obj:
            entity_obj["is_active_version"] = bool(
                entity_obj["is_active_version"]
            )
        else:
            entity_obj["is_active_version"] = bool(0)

        return entities.CustomObjectType.parse_obj(
            {**entity_obj, "_event_service": self.event_service}
        )

    def _load_from_stmt(self, stmt, uuid, version_independent_uuid=None):
        if stmt is None:
            raise ValueError("Parameter stmt not given")

        qry = self.session.execute(stmt)

        row = qry.fetchone()

        if not row:
            return None

        objecttype = self._entity_from_row(
            row,
            {
                "uuid": "v_uuid",
                "name": "v_name",
                "title": "v_title",
                "subtitle": "v_subtitle",
                "external_reference": "v_external_reference",
                "status": "v_status",
                "version": "v_version",
                "date_created": "v_date_created",
                "last_modified": "v_last_modified",
                "date_deleted": "v_date_deleted",
                "catalog_folder_id": "catalog_folder_id",
                "version_independent_uuid": "version_independent_uuid",
                "is_active_version": "v_is_active_version",
                "custom_field_definition": "v_custom_field_definition",
                "relationship_definition": "v_relationship_definition",
                "authorization_definition": "authorization_definition",
                "audit_log": "v_audit_log",
                "entity_id": "v_uuid",
                "entity_meta_summary": "v_name",
                "entity_meta_authorizations": "authorizations",
            },
        )

        # Prevent setting the "latest_version" on the latest version, to
        # make sure we do not create "circular references"
        if str(row.lv_uuid) != str(row.v_uuid):
            latest_version = self._entity_from_row(
                row,
                {
                    "uuid": "lv_uuid",
                    "name": "lv_name",
                    "title": "lv_title",
                    "subtitle": "lv_subtitle",
                    "external_reference": "lv_external_reference",
                    "status": "lv_status",
                    "version": "lv_version",
                    "date_created": "lv_date_created",
                    "last_modified": "lv_last_modified",
                    "date_deleted": "lv_date_deleted",
                    "catalog_folder_id": "catalog_folder_id",
                    "version_independent_uuid": "version_independent_uuid",
                    "custom_field_definition": "lv_custom_field_definition",
                    "relationship_definition": "lv_relationship_definition",
                    "audit_log": "lv_audit_log",
                    "authorization_definition": "authorization_definition",
                    "entity_id": "lv_uuid",
                    "entity_meta_summary": "lv_name",
                    "entity_meta_authorizations": "authorizations",
                },
            )

            latest_version.is_active_version = True
            objecttype.latest_version = latest_version

        return objecttype

    def _get_db_query_components(self, user_info: Optional[UserInfo]):
        ot = sql.alias(schema.CustomObjectType, name="ot")  # ObjectType
        otv = sql.alias(schema.CustomObjectTypeVersion, name="otv")  # Version
        lv = sql.alias(
            schema.CustomObjectTypeVersion, name="lv"
        )  # LastVersion

        join = sql.join(
            sql.join(otv, ot, otv.c.custom_object_type_id == ot.c.id),
            lv,
            ot.c.custom_object_type_version_id == lv.c.id,
        )

        columns = [
            ot.c.uuid.label("version_independent_uuid"),
            ot.c.authorization_definition.label("authorization_definition"),
            ot.c.catalog_folder_id.label("catalog_folder_id"),
            sql.case(
                [
                    (
                        ot.c.custom_object_type_version_id == otv.c.id,
                        sql.literal_column("1"),
                    )
                ],
                else_=sql.literal_column("0"),
            ).label("v_is_active_version"),
            otv.c.uuid.label("v_uuid"),
            otv.c.name.label("v_name"),
            otv.c.title.label("v_title"),
            otv.c.subtitle.label("v_subtitle"),
            otv.c.external_reference.label("v_external_reference"),
            otv.c.status.label("v_status"),
            otv.c.version.label("v_version"),
            otv.c.custom_field_definition.label("v_custom_field_definition"),
            otv.c.audit_log.label("v_audit_log"),
            otv.c.relationship_definition.label("v_relationship_definition"),
            otv.c.date_created.label("v_date_created"),
            otv.c.last_modified.label("v_last_modified"),
            otv.c.date_deleted.label("v_date_deleted"),
            lv.c.uuid.label("lv_uuid"),
            lv.c.name.label("lv_name"),
            lv.c.title.label("lv_title"),
            lv.c.subtitle.label("lv_subtitle"),
            lv.c.external_reference.label("lv_external_reference"),
            lv.c.status.label("lv_status"),
            lv.c.version.label("lv_version"),
            lv.c.custom_field_definition.label("lv_custom_field_definition"),
            lv.c.relationship_definition.label("lv_relationship_definition"),
            lv.c.audit_log.label("lv_audit_log"),
            lv.c.date_created.label("lv_date_created"),
            lv.c.last_modified.label("lv_last_modified"),
            lv.c.date_deleted.label("lv_date_deleted"),
            sql.func.array(
                _authorizations_subquery(
                    user_uuid=user_info.user_uuid, custom_object_type_alias=ot
                )
            ).label("authorizations")
            if user_info
            else sql.literal(None).label("authorizations"),
        ]

        return {
            "columns": columns,
            "join": join,
            "aliases": {"ot": ot, "otv": otv, "lv": lv},
        }

    def _select_stmt_objecttype(
        self,
        uuid=None,
        version_independent_uuid=None,
        user_info=None,
        authorization: Optional[entities.AuthorizationLevel] = None,
    ):
        """Generate query to PGSQL to return not deleted rows related to the given UUID

        Or:

        SELECT
            ot.uuid AS version_independent_uuid,
            CASE WHEN (ot.custom_object_type_version_id = otv.id) THEN 1 ELSE 0 END AS v_is_active_version,
            otv.uuid AS v_uuid,
            otv.name AS v_name,
            otv.title AS v_title,
            otv.subtitle AS v_subtitle,
            otv.external_reference AS v_external_reference,
            otv.status AS v_status,
            otv.version AS v_version,
            otv.date_created AS v_date_created,
            otv.last_modified AS v_last_modified,
            otv.date_deleted AS v_date_deleted,
            lv.uuid AS lv_uuid,
            lv.name AS lv_name,
            lv.title AS lv_title,
            lv.subtitle AS lv_subtitle,
            lv.external_reference AS lv_external_reference,
            lv.status AS lv_status,
            lv.version AS lv_version,
            lv.date_created AS lv_date_created,
            lv.last_modified AS lv_last_modified,
            lv.date_deleted AS lv_date_deleted
        FROM
            custom_object_type_version AS otv
            JOIN custom_object_type AS ot ON otv.custom_object_type_id = ot.id
            JOIN custom_object_type_version AS lv ON ot.custom_object_type_version_id = lv.id
        WHERE
            otv.uuid = :uuid_1
            AND (otv.date_deleted < :date_deleted_1 OR otv.date_deleted IS NULL)

        """
        query_comp = self._get_db_query_components(user_info=user_info)

        ### Select all fields
        stmt = sql.select(query_comp["columns"]).select_from(
            query_comp["join"]
        )

        ### Filter on uuid and date
        uuid_select_clause = []
        if authorization:
            uuid_select_clause.append(
                object_type_acl.allowed_object_type_v2_subquery(
                    user_info=user_info,
                    authorization=authorization,
                    object_type_alias=query_comp["aliases"]["ot"],
                )
            )

        if uuid is not None:
            uuid_select_clause.append(
                query_comp["aliases"]["otv"].c.uuid == uuid
            )
        elif version_independent_uuid is not None:
            uuid_select_clause.append(
                query_comp["aliases"]["ot"].c.uuid == version_independent_uuid
            )
            uuid_select_clause.append(
                query_comp["aliases"]["ot"].c.custom_object_type_version_id
                == query_comp["aliases"]["otv"].c.id
            )
        else:
            return None

        where_clause = sql.and_(
            *uuid_select_clause,
            sql.or_(
                query_comp["aliases"]["otv"].c.date_deleted
                > datetime.datetime.now(datetime.timezone.utc),
                query_comp["aliases"]["otv"].c.date_deleted.is_(None),
            ),
        )

        return stmt.where(where_clause)

    def _get_catalog_folder_id(self, uuid: UUID) -> int:
        """
        Retrieve the catalog_folder_id by uuid. Raise error
        if no catalog_folder_id can be found for given UUID.

        :param uuid: The uuid of the catalog folder
        :type uuid: UUID
        :return: The id of the catalog folder or None
        :rtype: int
        """

        stmt = None

        if uuid is not None:
            stmt = select([schema.BibliotheekCategorie.id]).where(
                schema.BibliotheekCategorie.uuid == uuid
            )

        result = self.session.execute(stmt).fetchone()
        if result:
            folder_id = result.id
        else:
            raise Conflict(
                f"No catalog folder ID found for UUID: '{uuid}'",
                "case_management/custom_object_type/_get_catalog_folder_id",
            )

        return folder_id
