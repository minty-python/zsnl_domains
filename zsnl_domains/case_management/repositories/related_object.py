# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities import RelatedObject
from minty.repository import Repository
from sqlalchemy import sql
from typing import List
from uuid import UUID
from zsnl_domains.database import schema

custom_object_query = sql.select(
    [
        schema.CustomObject.uuid,
        schema.CustomObjectVersion.title,
        schema.CustomObjectType.uuid.label("object_type_uuid"),
        schema.CustomObjectTypeVersion.name.label("object_type_name"),
    ]
).select_from(
    sql.join(
        schema.CustomObject,
        schema.CustomObjectVersion,
        schema.CustomObject.custom_object_version_id
        == schema.CustomObjectVersion.id,
    )
    .join(
        schema.CustomObjectTypeVersion,
        schema.CustomObjectVersion.custom_object_type_version_id
        == schema.CustomObjectTypeVersion.id,
    )
    .join(
        schema.CustomObjectType,
        schema.CustomObjectTypeVersion.custom_object_type_id
        == schema.CustomObjectType.id,
    )
)


class RelatedObjectRepository(Repository, DatabaseRepositoryBase):
    def find_for_subject(self, subject_uuid: UUID) -> List[RelatedObject]:
        """
        Find related custom objects for a subject.

        :param subject_uuid: UUID of the contact whose relations to retrieve
        :return: custom_objects related to a custom_object
        """

        for_contact_query = custom_object_query.where(
            schema.CustomObjectVersion.id.in_(
                sql.select(
                    [schema.CustomObjectRelationship.custom_object_version_id]
                ).where(
                    sql.and_(
                        schema.CustomObjectRelationship.relationship_type
                        == "subject",
                        schema.CustomObjectRelationship.related_uuid
                        == subject_uuid,
                    )
                )
            )
        )
        result = self.session.execute(for_contact_query).fetchall()

        return [self._inflate_row_to_entity(row) for row in result]

    def find_for_custom_object(self, object_uuid: UUID) -> List[RelatedObject]:
        """
        Find related custom objects for custom_object.

        :param object_uuid: UUID of the custom_object
        :return: custom_objects related to a custom_object
        """

        select_stmt = custom_object_query

        custom_object = sql.alias(schema.CustomObject)

        objects_related_to_object = select_stmt.where(
            sql.and_(
                schema.CustomObjectRelationship.relationship_type
                == "custom_object",
                schema.CustomObjectRelationship.related_uuid
                == schema.CustomObject.uuid,
                custom_object.c.uuid == object_uuid,
                schema.CustomObjectRelationship.custom_object_id
                == custom_object.c.id,
            ),
        )

        objects_which_has_relation_to_object = select_stmt.where(
            sql.and_(
                schema.CustomObjectRelationship.relationship_type
                == "custom_object",
                schema.CustomObjectRelationship.related_uuid == object_uuid,
                schema.CustomObject.id
                == schema.CustomObjectRelationship.custom_object_id,
            ),
        )

        result = self.session.execute(
            sql.union(
                objects_related_to_object, objects_which_has_relation_to_object
            )
        ).fetchall()

        return [self._inflate_row_to_entity(row) for row in result]

    def _inflate_row_to_entity(self, row) -> RelatedObject:

        """
        Inflate database row to RelatedObject Entity.

        :param row: Database Row
        :type row: object
        :return: RelatedObjectEntity
        :rtype: RelatedObject
        """
        mapping = {
            "uuid": "uuid",
            "entity_id": "uuid",
            "entity_meta_summary": "title",
        }

        object_type_mapping = {
            "uuid": "object_type_uuid",
            "entity_id": "object_type_uuid",
            "entity_meta_summary": "object_type_name",
        }

        entity_obj = {}
        for key, objkey in mapping.items():
            entity_obj[key] = getattr(row, objkey)

        entity_obj["object_type"] = {}
        for key, objkey in object_type_mapping.items():
            entity_obj["object_type"][key] = getattr(row, objkey)

        return RelatedObject.parse_obj(
            {**entity_obj, "_event_service": self.event_service}
        )
