# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


def allowed_cases_subquery(user_uuid: UUID, case_alias=None):
    """ACL query to check if a pip user is allowed to access.

    A pip user can either be an 'aanvrager'(assignee) or 'gemachtigde'(authorized)
    on the case.

    `usage: query.where(
        allowed_cases_subquery(user_uuid,case_alias)
    )`

    :param user_uuid: pip_user uuid
    :type user_uuid: UUID
    :param case_alias: Alias to the case table to join on. Should be
        used if the main query uses an alias for `schema.Case`.
    :return: Subquery that can be used to restrict case queries to allowed
        cases.
    :rtype: sqlalchemy.sql.expression.Select
    """
    if case_alias is None:
        case_alias = schema.Case

    case_zaakbetrokkenen = sql.join(
        schema.ZaakBetrokkenen,
        case_alias,
        case_alias.id == schema.ZaakBetrokkenen.zaak_id,
        isouter=False,
    )
    joined_schema = case_zaakbetrokkenen.join(
        schema.ZaaktypeNode,
        schema.ZaaktypeNode.id == case_alias.zaaktype_node_id,
        isouter=False,
    )

    query = sql.exists(
        sql.select([1])
        .select_from(joined_schema)
        .where(
            sql.and_(
                schema.ZaakBetrokkenen.subject_id == user_uuid,
                case_alias.deleted.is_(None),
                case_alias.status.in_(["new", "open", "stalled", "resolved"]),
                schema.ZaaktypeNode.prevent_pip.isnot(True),
                sql.or_(
                    schema.ZaakBetrokkenen.pip_authorized.is_(True),
                    case_alias.aanvrager == schema.ZaakBetrokkenen.id,
                ),
            )
        )
    )
    return query
