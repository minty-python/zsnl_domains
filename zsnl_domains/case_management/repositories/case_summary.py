# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import typing
from ... import DatabaseRepositoryBase
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from uuid import UUID
from zsnl_domains.case_management.entities.case_summary import CaseSummary
from zsnl_domains.case_management.repositories import case_acl
from zsnl_domains.database import schema


class CaseSummaryRepository(DatabaseRepositoryBase):
    def _case_summary_query(self):
        """Query to retrive case_summary.

        IMPORTANT: No ACL check is performed in this query.

        :param DatabaseRepositoryBase: DatabaseRepositoryBase
        :type DatabaseRepositoryBase: DatabaseRepositoryBase
        :return: case_summary query
        :rtype: sql.select statament
        """
        case_join = sql.join(
            schema.Case,
            schema.ZaaktypeNode,
            schema.ZaaktypeNode.id == schema.Case.zaaktype_node_id,
            isouter=False,
        ).join(
            schema.Subject,
            schema.Subject.id == schema.Case.behandelaar_gm_id,
            isouter=True,
        )

        count_phases = (
            sql.select([sql.func.count(schema.ZaaktypeStatus.id)])
            .where(
                schema.ZaaktypeStatus.zaaktype_node_id
                == schema.ZaaktypeNode.id
            )
            .label("count_phases")
        )

        assignee = sql.case(
            [
                (
                    schema.Subject.uuid.isnot(None),
                    sql.func.json_build_object(
                        "uuid",
                        schema.Subject.uuid,
                        "name",
                        sql.cast(schema.Subject.properties, sqltypes.JSON)[
                            "displayname"
                        ],
                    ),
                )
            ],
            else_=None,
        )

        return sql.select(
            [
                schema.Case.uuid,
                schema.Case.id,
                schema.ZaaktypeNode.titel.label("casetype_title"),
                schema.Case.onderwerp.label("summary"),
                schema.Case.resultaat.label("result"),
                assignee.label("assignee"),
                (
                    sql.cast(schema.Case.milestone, sqltypes.Float)
                    / count_phases
                ).label("progress_status"),
            ]
        ).select_from(case_join)

    def get_case_summaries_by_uuid(
        self, uuids: typing.Iterable[UUID], user_uuid: UUID, check_acl=False
    ):
        """Retrieve the list of case_summaries for given uuids.

        :param case_uuids: List of UUID of the cases to retrieve summary of
        :type case_uuid: typing.List[UUID]
        :param user_uuid: UUID of the user
        :type case_uuid: UUID
        :param check_acl: Filed which enables acl check.
        :type check_acl: bool
        :return: List of CaseSummary entities
        :rtype: typing.List[CaseSummary]
        """

        query_stmt = self._case_summary_query().where(
            schema.Case.uuid.in_(uuids)
        )

        if check_acl:
            query_stmt = query_stmt.where(
                case_acl.allowed_cases_subquery(
                    self.session, user_uuid, "read"
                )
            )

        case_summaries = self.session.execute(query_stmt).fetchall()

        return [
            self._transform_to_entity(case_summary)
            for case_summary in case_summaries
        ]

    def _transform_to_entity(self, case_summary_row):
        """Transform database rwo to CaseSummaryEntity.

        :param case_summary_row: database row for case_summary
        :type case_summary_row: sqla
        :return: CaseSummary entity
        :rtype: CaseSummary
        """
        return CaseSummary(
            entity_id=case_summary_row.uuid,
            uuid=case_summary_row.uuid,
            number=case_summary_row.id,
            casetype_title=case_summary_row.casetype_title,
            progress_status=case_summary_row.progress_status,
            summary=case_summary_row.summary,
            result=case_summary_row.result,
            assignee=case_summary_row.assignee,
        )
