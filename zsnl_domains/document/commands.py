# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from minty.cqrs import CommandBase
from minty.exceptions import Conflict
from minty.validation import validate_with
from pkgutil import get_data
from typing import List, Optional, cast
from uuid import UUID, uuid4
from zsnl_domains.document.repositories import (
    DirectoryRepository,
    DocumentLabelRepository,
    DocumentRepository,
    FileRepository,
    FileStoreSource,
    MessageRepository,
)

logger = logging.getLogger(__name__)


class Commands(CommandBase):
    """Document domain commands."""

    @validate_with(get_data(__name__, "validation/create_document.json"))
    def create_document(
        self,
        document_uuid,
        filename,
        store_uuid,
        mimetype,
        size,
        storage_location,
        md5,
        magic_strings: Optional[List[str]],
        editor_update: Optional[bool] = False,
        directory_uuid=None,
        replaces=None,
        case_uuid=None,
        skip_intake=False,
        auto_accept=False,
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )

        if replaces:
            document_uuid = replaces
            auto_accept = True

        document = docu_repo.create_document(
            document_uuid=document_uuid,
            filename=filename,
            store_uuid=store_uuid,
            directory_uuid=directory_uuid,
            case_uuid=case_uuid,
            mimetype=mimetype,
            size=size,
            storage_location=storage_location,
            md5=md5,
            skip_intake=skip_intake,
            auto_accept=auto_accept,
            editor_update=editor_update,
        )

        if magic_strings:
            labels = label_repo.get_document_labels_for_case_magicstrings(
                case_uuid=case_uuid,
                user_uuid=self.user_uuid,
                magic_strings=magic_strings,
            )
            document.apply_labels(labels=labels)

        docu_repo.save()

    @validate_with(get_data(__name__, "validation/create_file.json"))
    def create_file(
        self, file_uuid, filename, mimetype, size, storage_location, md5
    ):
        file_repo = cast(FileRepository, self.get_repository("file"))
        file_repo.create_file(
            file_uuid, filename, mimetype, size, storage_location, md5
        )
        file_repo.save()

    @validate_with(
        get_data(__name__, "validation/create_document_from_attachment.json")
    )
    def create_document_from_attachment(self, attachment_uuid):
        document_repository = cast(
            DocumentRepository, self.get_repository("document")
        )
        document_repository.create_document_from_attachment(attachment_uuid)
        document_repository.save()

    @validate_with(
        get_data(__name__, "validation/create_document_from_message.json")
    )
    def create_document_from_message(
        self, message_uuid: str, output_format="pdf"
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        message_repo = cast(MessageRepository, self.get_repository("message"))
        file_repo = cast(FileRepository, self.get_repository("file"))

        document_uuid = uuid4()

        if output_format == "pdf":
            filestore_source: FileStoreSource = (
                message_repo.get_filestore_source_with_pdf_from_message_uuid(
                    message_uuid=message_uuid
                )
            )
        elif output_format == "original":
            filestore_source: FileStoreSource = message_repo.get_filestore_source_with_original_file_uuid_from_message_uuid(
                message_uuid=message_uuid
            )

        filename = (
            filestore_source.subject
            if filestore_source.subject
            else "Geen onderwerp"
        )

        file = file_repo.get_file_by_uuid(
            file_uuid=filestore_source.filestore_uuid
        )

        docu_repo.create_document(
            document_uuid=document_uuid,
            filename=filename + file.extension,
            store_uuid=filestore_source.filestore_uuid,
            directory_uuid=None,
            case_uuid=filestore_source.case_uuid,
            mimetype=file.mimetype,
            size=file.size,
            storage_location=file.storage_location,
            md5=file.md5,
        )

        docu_repo.save()

    @validate_with(
        get_data(__name__, "validation/create_document_from_file.json")
    )
    def create_document_from_file(
        self,
        document_uuid,
        file_uuid,
        case_uuid=None,
        directory_uuid=None,
        magic_strings: Optional[List[str]] = None,
        skip_intake=False,
    ):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )
        file_repo = cast(FileRepository, self.get_repository("file"))

        # If there is no user_info in the command_instance, we assume that
        # this command is being called from event_handler document_events_consumer
        # and the user will be always an employee (not pip).
        is_pip_user = False
        if self.user_info:
            is_pip_user = self.user_info.permissions.get("pip_user", False)

        file = file_repo.get_file_by_uuid(file_uuid=file_uuid)

        create_params = {
            "document_uuid": document_uuid,
            "filename": file.basename + file.extension,
            "store_uuid": file_uuid,
            "case_uuid": case_uuid,
            "mimetype": file.mimetype,
            "size": file.size,
            "storage_location": file.storage_location,
            "md5": file.md5,
            "is_pip_user": is_pip_user,
            "skip_intake": skip_intake,
        }

        if directory_uuid:
            create_params["directory_uuid"] = directory_uuid

        document = docu_repo.create_document(**create_params)

        if magic_strings:
            labels = label_repo.get_document_labels_for_case_magicstrings(
                case_uuid=case_uuid,
                user_uuid=self.user_uuid,
                magic_strings=magic_strings,
            )
            document.apply_labels(labels=labels)

        docu_repo.save()

    @validate_with(
        get_data(
            __name__, "validation/create_document_from_message_archive.json"
        )
    )
    def create_document_from_message_archive(self, message_uuid, file_uuid):
        docu_repo = cast(DocumentRepository, self.get_repository("document"))
        file_repo = cast(FileRepository, self.get_repository("file"))
        message_repo = cast(MessageRepository, self.get_repository("message"))

        message = message_repo.get_message_by_uuid(message_uuid=message_uuid)

        file = file_repo.get_file_by_uuid(file_uuid=file_uuid)

        document_uuid = uuid4()
        docu_repo.create_document(
            document_uuid=document_uuid,
            filename=file.basename + file.extension,
            store_uuid=file_uuid,
            directory_uuid=None,
            case_uuid=message.case_uuid,
            mimetype=file.mimetype,
            size=file.size,
            storage_location=file.storage_location,
            md5=file.md5,
        )

        docu_repo.save()

    @validate_with(get_data(__name__, "validation/add_document_to_case.json"))
    def add_document_to_case(
        self, document_uuid, case_uuid, document_label_uuids=[]
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        labels = []
        if document_label_uuids:
            document_label_repo = cast(
                DocumentLabelRepository, self.get_repository("document_label")
            )

            labels = document_label_repo.get_document_labels_by_uuid(
                uuids=document_label_uuids
            )

        document.add_to_case(case_uuid=case_uuid)
        document.apply_labels(labels=labels)
        document_repo.save()

    @validate_with(get_data(__name__, "validation/update_document.json"))
    def update_document(
        self,
        document_uuid,
        basename=None,
        description=None,
        document_category=None,
        origin=None,
        origin_date=None,
        confidentiality=None,
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.update(
            basename=basename,
            description=description,
            document_category=document_category,
            origin=origin,
            origin_date=origin_date,
            confidentiality=confidentiality,
        )
        document_repo.save()

    @validate_with(get_data(__name__, "validation/delete_document.json"))
    def delete_document(
        self, user_info: dict, document_uuid: UUID, reason: str = ""
    ):
        """
        Permanently delete a document.

        :param user_info: dict: The user_info dict
        :param document_uuid: UUID: The document_uuid to delete
        :param reason: str: Optional reason for deletion
        :return:

        :raises NotFound when document_uuid is not found (code: document/not_found)
        :raises Conflict when document_uuid is assigned to a case (code: document/delete_document/assigned_to_case)
        """

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid,
            user_info=user_info,
            integrity_check=True,
        )
        document.delete(reason=reason)
        document_repo.save()

    @validate_with(
        get_data(__name__, "validation/assign_document_to_user.json")
    )
    def assign_document_to_user(self, document_uuid: UUID, user_uuid: UUID):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.assign_document_to_user(intake_owner_uuid=user_uuid)
        document_repo.save()

    @validate_with(
        get_data(__name__, "validation/assign_document_to_role.json")
    )
    def assign_document_to_role(
        self, document_uuid: UUID, group_uuid: UUID, role_uuid: UUID
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.assign_document_to_role(
            intake_group_uuid=group_uuid, intake_role_uuid=role_uuid
        )
        document_repo.save()

    @validate_with(
        get_data(__name__, "validation/reject_assigned_document.json")
    )
    def reject_assigned_document(
        self, document_uuid: UUID, rejection_reason: str
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.reject_from_intake(rejection_reason=rejection_reason)
        document_repo.save(self.user_info)

    @validate_with(get_data(__name__, "validation/create_directory.json"))
    def create_directory(
        self,
        directory_uuid: UUID,
        case_uuid: UUID,
        name: str,
        parent_uuid: Optional[UUID] = None,
    ):
        """
        Create a Directory. A Directory entry is created by a given directory_uuid,
        a coupled case_uuid and a given name of the directory.
        It is possible to create a Directory entry within a directory hierarchy
        if an optional parent_uuid is given. When an optional parent_uuid is given,
        the case_id of the parent Directory must match the case_id of the directory
        being created, and the path of the parent Directory is copied into the
        Directory's path column and appended with it's parents ID. When no parent_uuid
        is given the Directory database path column will be filled with an empty list,
        which makes it a directory in the root in the case.

        :param directory_uuid UUID: The UUID for the new directory to be created.
        :param case_uuid UUID: The coupled case_uuid
        :param name: The given Directory name
        :param parent_uuid: An optional Directory parent_uuid
        :return:
        """
        case_repo = self.get_repository("case")
        directory_repo = cast(
            DirectoryRepository, self.get_repository("directory")
        )

        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid,
            user_info=self.user_info,
            permission="write",
        )

        if parent_uuid:
            parent_directory = directory_repo.get_directory_by_uuid(
                uuid=parent_uuid
            )
            if case.id != parent_directory.case_id:
                raise Conflict(
                    f"The parent directory's case_id does not match the given case_id ({case.id} != {parent_directory.case_id})",
                    "domains/document/create_directory/parent_case_id_mismatch",
                )

            parent_directory_id = directory_repo.get_directory_id_by_uuid(
                parent_directory.uuid
            )
            new_path = parent_directory.path
            new_path.append(parent_directory_id)
        else:
            new_path = []

        directory_repo.create_directory(
            directory_uuid,
            name,
            case.id,
            new_path,
        )

        directory_repo.save()

    def apply_labels_to_document(
        self, document_uuids: List[UUID], document_label_uuids: List[UUID]
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document_label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )
        labels = document_label_repo.get_document_labels_by_uuid(
            uuids=document_label_uuids
        )

        for document_uuid in document_uuids:
            document = document_repo.get_document_by_uuid(
                document_uuid=document_uuid, user_info=self.user_info
            )
            document.apply_labels(labels=labels)

        document_repo.save()

    def remove_labels_from_document(
        self, document_uuids: List[UUID], document_label_uuids: List[UUID]
    ):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document_label_repo = cast(
            DocumentLabelRepository, self.get_repository("document_label")
        )
        labels = document_label_repo.get_document_labels_by_uuid(
            uuids=document_label_uuids
        )

        for document_uuid in document_uuids:
            document = document_repo.get_document_by_uuid(
                document_uuid=document_uuid, user_info=self.user_info
            )
            document.remove_labels(labels=labels)

        document_repo.save()

    def move_to_directory(
        self,
        destination_directory_uuid: UUID = None,
        source_documents: List[UUID] = None,
        source_directories: List[UUID] = None,
    ):

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        directory_repo = cast(
            DirectoryRepository, self.get_repository("directory")
        )

        destination_directory = None
        destination_directory_id = None

        if destination_directory_uuid is not None:
            destination_directory = directory_repo.get_directory_by_uuid(
                destination_directory_uuid
            )
            destination_directory_id = directory_repo.get_directory_id_by_uuid(
                destination_directory.uuid
            )

        # Move documents
        if source_documents:
            for document_uuid in source_documents:
                document = document_repo.get_document_by_uuid(
                    document_uuid=document_uuid, user_info=self.user_info
                )
                if (
                    destination_directory
                    and document.case_display_number
                    != destination_directory.case_id
                ):
                    raise Conflict(
                        f"The destination directories case_id does not match the given documents case_id ({destination_directory.case_id} != {document.case_display_number})",
                        "domains/document/move_to_directory/document_case_id_mismatch",
                    )
                else:
                    document.move_to_directory(
                        directory_uuid=destination_directory_uuid
                    )

            document_repo.save()

        # move directories
        if source_directories:
            for directory_uuid in source_directories:
                directory = directory_repo.get_directory_by_uuid(
                    uuid=directory_uuid
                )
                directory_id = directory_repo.get_directory_id_by_uuid(
                    directory.uuid
                )

                if (
                    destination_directory
                    and directory.case_id != destination_directory.case_id
                ):
                    raise Conflict(
                        f"The destination directories case_id does not match the given directories case_id ({destination_directory.case_id} != {directory.case_id})",
                        "domains/document/move_to_directory/directory_case_id_mismatch",
                    )
                else:
                    if destination_directory_id and (
                        (directory_id in destination_directory.path)
                        or (directory_id == destination_directory_id)
                    ):
                        raise Conflict(
                            "Can not move directory to itself or its child directories",
                            "domains/document/move_to_directory/moving_to_self",
                        )
                    else:
                        new_path = []
                        if destination_directory_id is not None:
                            new_path.extend(destination_directory.path)
                            new_path.append(destination_directory_id)

                        directory.move(path=new_path)

            directory_repo.save()

    @validate_with(get_data(__name__, "validation/accept_document.json"))
    def accept_document(self, document_uuid):
        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        document.accept_document(accepted=True, explicit_accpet=True)
        document_repo.save()
