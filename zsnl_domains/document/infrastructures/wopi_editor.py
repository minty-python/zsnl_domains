# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import requests
from minty import Base


class WopiWrapper(Base):
    def __init__(self, api_key, base_url):
        self.base_url = base_url
        self.api_key = api_key

    def edit_document(self, options={}):
        """Get Wopi configuration to launch the hostpage"""

        url = self.base_url + "/api/document/edit"
        options["api_key"] = self.api_key

        response = requests.post(
            url=url,
            json=options,
            headers={"Content-Type": "application/json"},
            verify="/etc/ssl/certs",
        )
        if response.status_code != 200:
            response.raise_for_status()

        return response.json()


class WopiInfrastructure(Base):
    """Infrastructure for editing document with with Microsoft WOPI"""

    def __call__(self, config):
        """Initialize WopiWrapper"""
        return WopiWrapper(
            api_key=config["wopi_api_key"],
            base_url=config["wopi_base_url"],
        )
