# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import requests
from minty import Base, exceptions


class ZohoWrapper(Base):
    def __init__(self, api_key, base_url):
        self.base_url = base_url
        self.api_key = api_key

    def edit_document(self, options={}):
        """Provide URL to edit file using zoho editor.

        :param options: An object containing form data for zoho request.
        :type options: dict
        :rtype: URL to open document for editing
        """

        url = self.base_url
        options["apikey"] = self.api_key

        try:
            response = requests.request(
                "POST", url, data=options, verify="/etc/ssl/certs", timeout=100
            )
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            raise exceptions.Conflict(
                response.content.decode("utf8"), response.status_code
            )
        except requests.exceptions.Timeout:
            raise exceptions.Conflict("Timeout Error")
        except requests.exceptions.ConnectionError as e:
            raise exceptions.Conflict(str(e))
        except requests.exceptions.RequestException as e:
            raise exceptions.Conflict(str(e))

        result = response.json()["document_url"]
        return result


class ZohoInfrastructure(Base):
    """Infrastructure for online file editing"""

    def __call__(self, config):
        """Initialize ZohoWrapper"""

        return ZohoWrapper(
            api_key=config["zoho_api_key"],
            base_url=config["zoho_write_base_url"],
        )
