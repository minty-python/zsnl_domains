# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from .. import entities
from . import database_search_queries
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from minty.repository import Repository
from sqlalchemy import sql
from typing import Any, Iterable, Mapping, Optional

SEARCH_QUERY_MAPPING: Mapping[
    str, database_search_queries.SearchTypeCallable
] = {
    "case": database_search_queries.case_search_query,
    "custom_object_type": database_search_queries.custom_object_type_search_query,
    "custom_object": database_search_queries.custom_object_search_query,
    "document": database_search_queries.document_search_query,
    "employee": database_search_queries.employee_search_query,
    "object_v1": database_search_queries.object_v1_search_query,
    "organization": database_search_queries.organization_search_query,
    "person": database_search_queries.person_search_query,
    "case_type": database_search_queries.case_type_search_query,
    "saved_search": database_search_queries.saved_search_search_query,
}


class SearchResultRepository(DatabaseRepositoryBase, Repository):
    def search(
        self,
        user_info: UserInfo,
        types: Iterable[str],
        keyword: str,
        max_results: int,
        max_results_per_type: Optional[int] = None,
        filter_params: Optional[Mapping[str, Any]] = None,
    ) -> EntityCollection[entities.SearchResult]:
        """Search objects (custom_object,case_type,custom_object_type etc) by type and keyword.

        :param type: Type of object.
        :type type: str
        :param keyword: Keyword to search for object
        :type keyword: str
        :rtype: EntityCollection
        """

        if filter_params is None:
            filter_params = {}

        queries = []
        for search_type in types:
            subquery = SEARCH_QUERY_MAPPING[search_type](
                keyword=keyword,
                user_info=user_info,
                filter_params=filter_params,
            )
            if max_results_per_type is not None:
                subquery = subquery.limit(max_results_per_type)

            queries.append(subquery)

        search_query = sql.union_all(*queries).alias("search_query")
        full_query = sql.select(search_query.c).limit(max_results)

        search_result = self.session.execute(full_query).fetchall()

        rows = []
        for row in search_result:
            rows.append(self._inflate_row_to_entity(row))

        return EntityCollection[entities.SearchResult](rows)

    def _inflate_row_to_entity(self, row) -> entities.SearchResult:
        """Initialize Search_result Entity from a database row"""

        mapping = {
            "result_type": "type",
            "uuid": "uuid",
            "description": "description",
            "entity_id": "uuid",
            "entity_meta_summary": "summary",
        }

        entity_obj = {}
        for key, objkey in mapping.items():
            entity_obj[key] = getattr(row, objkey)

        # When the search result is of type custom_object, we include a relationship to custom_object_type,
        # that way we can filter custom_objects by object_type.
        if row.type == "custom_object":
            entity_obj["custom_object_type"] = {
                "uuid": row.parent_uuid,
                "entity_id": row.parent_uuid,
            }
        elif row.type == "document":
            entity_obj["case"] = {
                "uuid": row.parent_uuid,
                "entity_id": row.parent_uuid,
            }
        elif row.type == "case":
            entity_obj["case_type"] = {
                "uuid": row.parent_uuid,
                "entity_id": row.parent_uuid,
            }

        return entities.SearchResult.parse_obj(
            {**entity_obj, "_event_service": self.event_service}
        )
