# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from pydantic.generics import GenericModel
from pydantic.types import constr
from typing import Generic, TypeVar


class ComparisonFilterOperator(str, enum.Enum):
    lt = "lt"
    le = "le"
    gt = "gt"
    ge = "ge"
    eq = "eq"
    ne = "ne"


_comparison_filter_re = r"(" + r"|".join(list(ComparisonFilterOperator)) + r")"

ComparisonFilterConditionStr = constr(
    strip_whitespace=True, regex=rf"^{_comparison_filter_re} "
)

ComparisonT = TypeVar("ComparisonT")


class ComparisonFilterCondition(GenericModel, Generic[ComparisonT]):
    operator: ComparisonFilterOperator
    operand: ComparisonT

    @classmethod
    def from_str(
        cls, s: ComparisonFilterConditionStr
    ) -> "ComparisonFilterCondition[ComparisonT]":
        (operator, operand) = s.split(" ", maxsplit=1)
        return cls(operator=operator, operand=operand)
