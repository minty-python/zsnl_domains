# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import abc
from .jinja_template import process_template
from minty.entity import Entity


class TemplateService(abc.ABC):
    def render(self, template: str, variables: dict):
        return process_template(template, variables)

    def _prepare_entity(self, entity: Entity):
        attributes = entity.entity_dict()
        relationships = {}

        for relationship_key in entity.entity_relationships:
            relationship = attributes.pop(relationship_key)

            if isinstance(relationship, list):
                relationships[relationship_key] = relationship
            else:
                relationships[relationship_key] = [relationship]
        return {"attributes": attributes, "relationships": relationships}
