# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from babel import dates

DEFAULT_DATE_FORMAT = "d MMMM yyyy"
DEFAULT_TZ = "Europe/Amsterdam"


def __break_filter(value: str):
    if len(value) and len(value.strip()):
        value = value + "\n"
    return value


def __date_filter(value, date_format=DEFAULT_DATE_FORMAT):
    if isinstance(value, list):
        return [__date_filter(v, date_format) for v in value]

    if isinstance(value, datetime.datetime):
        return dates.format_datetime(
            value,
            date_format,
            tzinfo=DEFAULT_TZ,
            locale="nl",
        )
    elif isinstance(value, datetime.date):
        return dates.format_date(value, date_format, locale="nl")

    return value


ALL_FILTERS = {"date": __date_filter, "break": __break_filter}
