# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

"""Wrapping code to make Jinja2 work for Zaaksysteem templates"""

import datetime
import jinja2
import jinja2.exceptions
import re
from . import filters
from jinja2 import sandbox
from jsonpath2 import path as jpath
from uuid import UUID


def finalizer(template_value):
    if jinja2.is_undefined(template_value) or template_value is None:
        return ""
    elif isinstance(template_value, list):
        template_value_list = [
            final_value
            for final_value in (finalizer(value) for value in template_value)
            if final_value
        ]

        return ", ".join(template_value_list)
    elif isinstance(template_value, dict):
        final_value = None
        for value in template_value.values():
            final_value = finalizer(value)
        return final_value
    elif isinstance(template_value, (datetime.date, datetime.datetime)):
        return _format_date(template_value)
    elif isinstance(template_value, str):
        return _process_string(template_value)
    elif isinstance(template_value, (int, float, bool, UUID)):
        return str(template_value)
    return template_value


@jinja2.pass_context
def _jsonpath_handler(context, selector):
    jp = jpath.Path.parse_str(selector)
    matches = [m.current_value for m in jp.match(context)]

    if len(matches) == 0:
        return ""
    elif len(matches) == 1:
        return matches[0]
    else:
        return matches


TEMPLATE_TAG_PATTERN = re.compile(r"\[\[.*?\]\]")

J2_ENVIRONMENT = sandbox.SandboxedEnvironment(
    finalize=finalizer,
    loader=jinja2.BaseLoader(),
    variable_start_string="[[",
    variable_end_string="]]",
)
J2_ENVIRONMENT.globals = {"j": _jsonpath_handler}
J2_ENVIRONMENT.filters = filters.ALL_FILTERS


def process_template(template: str, variables: dict):
    result = template

    iterations_left = 10
    while TEMPLATE_TAG_PATTERN.search(result) and iterations_left > 0:
        iterations_left = iterations_left - 1

        try:
            j2_template = J2_ENVIRONMENT.from_string(result)
            result = j2_template.render(variables)
        except jinja2.exceptions.TemplateError as e:
            # Something is wrong in the template
            result = f"Error in template: {e}"

    return result


def _format_date(date_value):
    if isinstance(date_value, (datetime.datetime)):
        date_value = date_value.date()
    return date_value.strftime("%d-%m-%Y")


def _process_string(template_value):
    try:
        date_value = datetime.datetime.strptime(template_value, "%Y-%m-%d")
        if isinstance(date_value, datetime.datetime):
            return _format_date(date_value)
    except ValueError:
        return template_value
