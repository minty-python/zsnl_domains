# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from enum import Enum


class ValidCaseStatus(str, Enum):
    """Valid status for the case"""

    new = "new"
    stalled = "stalled"
    resolved = "resolved"
    open = "open"


class ValidCasePaymentStatus(str, Enum):
    success = "success"
    pending = "pending"
    failed = "failed"


class ValidContactChannel(str, Enum):
    behandelaar = "behandelaar"
    balie = "balie"
    telefoon = "telefoon"
    post = "post"
    email = "email"
    webformulier = "webformulier"
    sociale_media = "sociale media"
    externe_applicatie = "externe applicatie"


class ValidCaseConfidentiality(str, Enum):
    public = "public"
    internal = "internal"
    confidential = "confidential"


class ValidCaseArchivalState(str, Enum):
    vernietigen = "vernietigen"
    overdragen = "overdragen"


class ValidCaseRetentionPeriodSourceDate(str, Enum):
    vervallen = "vervallen"
    onherroepelijk = "onherroepelijk"
    verwerking = "verwerking"
    verleend = "verleend"
    einde_dienstverband = "einde dienstverband"
    afhandeling = "afhandeling"
    geboorte = "geboorte"
    geweigerd = "geweigerd"


class ValidCaseResult(str, Enum):
    aangegaan = "aangegaan"
    aangehouden = "aangehouden"
    aangekocht = "aangekocht"
    aangesteld = "aangesteld"
    aanvaard = "aanvaard"
    afgeboekt = "afgeboekt"
    afgebroken = "afgebroken"
    afgehandeld = "afgehandeld"
    afgesloten = "afgesloten"
    afgewezen = "afgewezen"
    akkoord = "akkoord"
    akkoord_met_wijzigingen = "akkoord met wijzigingen"
    beëindigd = "beëindigd"
    behaald = "behaald"
    betaald = "betaald"
    buiten_behandeling_gesteld = "buiten behandeling gesteld"
    definitief_toegekend = "definitief toegekend"
    geannuleerd = "geannuleerd"
    gedeeltelijk_gegrond = "gedeeltelijk gegrond"
    gedeeltelijk_verleend = "gedeeltelijk verleend"
    gedoogd = "gedoogd"
    gegrond = "gegrond"
    gegund = "gegund"
    geïnd = "geïnd"
    geleverd = "geleverd"
    geweigerd = "geweigerd"
    gewijzigd = "gewijzigd"
    handhaving_uitgevoerd = "handhaving uitgevoerd"
    ingericht = "ingericht"
    ingeschreven = "ingeschreven"
    ingesteld = "ingesteld"
    ingetrokken = "ingetrokken"
    ingewilligd = "ingewilligd"
    niet_aangekocht = "niet aangekocht"
    niet_aangesteld = "niet aangesteld"
    niet_akkoord = "niet akkoord"
    niet_behaald = "niet behaald"
    niet_betaald = "niet betaald"
    niet_doorgegaan = "niet doorgegaan"
    niet_gegund = "niet gegund"
    niet_geind = "niet geind"
    niet_geleverd = "niet geleverd"
    niet_gewijzigd = "niet gewijzigd"
    niet_ingesteld = "niet ingesteld"
    niet_ingetrokken = "niet ingetrokken"
    niet_nodig = "niet nodig"
    niet_ontvankelijk = "niet ontvankelijk"
    niet_opgelegd = "niet opgelegd"
    niet_opgeleverd = "niet opgeleverd"
    niet_toegekend = "niet toegekend"
    niet_uitgevoerd = "niet uitgevoerd"
    niet_vastgesteld = "niet vastgesteld"
    niet_verkregen = "niet verkregen"
    niet_verleend = "niet verleend"
    niet_verstrekt = "niet verstrekt"
    niet_verwerkt = "niet verwerkt"
    ongegrond = "ongegrond"
    ontvankelijk = "ontvankelijk"
    opgeheven = "opgeheven"
    opgelegd = "opgelegd"
    opgeleverd = "opgeleverd"
    opgelost = "opgelost"
    opgezegd = "opgezegd"
    toegekend = "toegekend"
    toezicht_uitgevoer = "toezicht uitgevoerd"
    uitgevoerd = "uitgevoerd"
    vastgesteld = "vastgesteld"
    verhuurd = "verhuurd"
    verkocht = "verkocht"
    verkregen = "verkregen"
    verleend = "verleend"
    vernietigd = "vernietigd"
    verstrekt = "verstrekt"
    verwerkt = "verwerkt"
    voorlopig_toegekend = "voorlopig toegekend"
    voorlopig_verleend = "voorlopig verleend"
