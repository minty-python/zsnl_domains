# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import EntityBase
from uuid import UUID


class Integration(EntityBase):
    """Data structure which hold information about an
    integration/interface used by zaaksysteem.
    """

    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self, id: int, uuid: UUID, name: str, active: bool, module: str
    ):
        self.id = id
        self.uuid = uuid
        self.name = name
        self.active = active
        self.module = module
