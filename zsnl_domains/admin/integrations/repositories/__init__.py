# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .integration import IntegrationRepository

__all__ = ["IntegrationRepository"]
