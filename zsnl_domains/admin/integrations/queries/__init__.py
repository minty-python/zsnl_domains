# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..constants import (
    APPOINTMENT_INTEGRATION_TYPES,
    APPOINTMENT_V2_INTEGRATION_TYPES,
    DOCUMENT_INTEGRATION_TYPES,
)
from ..entities import Integration
from ..repositories import IntegrationRepository
from minty.cqrs import QueryBase
from typing import List, cast


class Queries(QueryBase):
    """Queries class for the integrations"""

    @property
    def repository(self) -> IntegrationRepository:
        repo = cast(
            IntegrationRepository,
            self.repository_factory.get_repository(
                "integration", context=self.context, event_service=None
            ),
        )
        return repo

    def get_active_appointment_integrations(self) -> List[Integration]:
        """Get active appointment integrations.

        :return: List of active appointment integrations.
        """

        integrations = self.repository.get_active_integrations(
            integration_types=APPOINTMENT_INTEGRATION_TYPES
        )
        return integrations

    def get_active_appointment_v2_integrations(self) -> List[Integration]:
        """Get active appointment integrations.

        :return: List of active "v2" appointment integrations.
        """

        integrations = self.repository.get_active_integrations(
            integration_types=APPOINTMENT_V2_INTEGRATION_TYPES
        )
        return integrations

    def get_active_integrations_for_document(self) -> List[Integration]:
        """Retrieve a list of active integrations for document templating/creation.

        :return: List of active integrations for document.
        :rtype: list
        """

        document_integrations = self.repository.get_active_integrations(
            integration_types=DOCUMENT_INTEGRATION_TYPES
        )
        return document_integrations
