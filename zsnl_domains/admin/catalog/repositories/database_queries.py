# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from sqlalchemy.sql import (
    and_,
    cast,
    expression,
    func,
    join,
    or_,
    select,
    union_all,
)
from sqlalchemy.types import Boolean
from zsnl_domains.database import schema

folder_query = select(
    [
        expression.literal("folder").label("type"),
        schema.BibliotheekCategorie.uuid,
        schema.BibliotheekCategorie.naam.label("name"),
        expression.literal_column("true").label("active"),
        schema.BibliotheekCategorie.pid.label("parent_folder_id"),
        schema.BibliotheekCategorie.id.label("object_id"),
    ]
)

case_type_query = (
    select(
        [
            expression.literal("case_type").label("type"),
            schema.Zaaktype.uuid,
            schema.ZaaktypeNode.titel.label("name"),
            schema.Zaaktype.active,
            schema.Zaaktype.bibliotheek_categorie_id.label("parent_folder_id"),
            schema.Zaaktype.id.label("object_id"),
        ]
    )
    .select_from(schema.Zaaktype)
    .where(
        and_(
            schema.Zaaktype.deleted.is_(None),
            schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
        )
    )
)

attribute_query = select(
    [
        expression.literal("attribute").label("type"),
        schema.BibliotheekKenmerk.uuid,
        schema.BibliotheekKenmerk.naam.label("name"),
        expression.literal_column("true").label("active"),
        schema.BibliotheekKenmerk.bibliotheek_categorie_id.label(
            "parent_folder_id"
        ),
        schema.BibliotheekKenmerk.id.label("object_id"),
    ]
).where(schema.BibliotheekKenmerk.deleted.is_(None))


attribute_values_query = select(
    [
        schema.BibliotheekKenmerkenValues.id,
        schema.BibliotheekKenmerkenValues.value,
        schema.BibliotheekKenmerkenValues.active,
        schema.BibliotheekKenmerkenValues.bibliotheek_kenmerken_id,
        schema.BibliotheekKenmerkenValues.sort_order,
    ]
)


email_template_query = select(
    [
        expression.literal("email_template").label("type"),
        schema.BibliotheekNotificatie.uuid,
        schema.BibliotheekNotificatie.label.label("name"),
        expression.literal_column("true").label("active"),
        schema.BibliotheekNotificatie.bibliotheek_categorie_id.label(
            "parent_folder_id"
        ),
        schema.BibliotheekNotificatie.id.label("object_id"),
    ]
).where(schema.BibliotheekNotificatie.deleted.is_(None))

document_template_query = select(
    [
        expression.literal("document_template").label("type"),
        schema.BibliotheekSjabloon.uuid,
        schema.BibliotheekSjabloon.naam.label("name"),
        expression.literal_column("true").label("active"),
        schema.BibliotheekSjabloon.bibliotheek_categorie_id.label(
            "parent_folder_id"
        ),
        schema.BibliotheekSjabloon.id.label("object_id"),
    ]
).where(schema.BibliotheekSjabloon.deleted.is_(None))

object_type_query = select(
    [
        expression.literal("object_type").label("type"),
        schema.ObjectBibliotheekEntry.object_uuid.label("uuid"),
        schema.ObjectBibliotheekEntry.name,
        expression.literal_column("true").label("active"),
        schema.ObjectBibliotheekEntry.bibliotheek_categorie_id.label(
            "parent_folder_id"
        ),
        schema.ObjectBibliotheekEntry.id.label("object_id"),
    ]
)

custom_object_type_query = select(
    [
        expression.literal("custom_object_type").label("type"),
        schema.CustomObjectType.uuid,
        schema.CustomObjectTypeVersion.name,
        expression.literal_column("true").label("active"),
        schema.CustomObjectType.catalog_folder_id.label("parent_folder_id"),
        schema.CustomObjectTypeVersion.id.label("object_id"),
    ]
).where(
    and_(
        schema.CustomObjectTypeVersion.date_deleted.is_(None),
        schema.CustomObjectType.custom_object_type_version_id
        == schema.CustomObjectTypeVersion.id,
    )
)

attribute_detail_query = select(
    [
        schema.BibliotheekKenmerk.uuid,
        schema.BibliotheekKenmerk.id,
        schema.BibliotheekKenmerk.naam.label("name"),
        schema.BibliotheekKenmerk.magic_string,
        schema.BibliotheekKenmerk.value_type,
        schema.BibliotheekKenmerk.type_multiple,
        schema.BibliotheekKenmerk.file_metadata_id,
        schema.BibliotheekKenmerk.last_modified,
        schema.BibliotheekCategorie.uuid.label("folder_uuid"),
        schema.BibliotheekCategorie.naam.label("folder_name"),
    ]
).select_from(
    join(
        schema.BibliotheekKenmerk,
        schema.BibliotheekCategorie,
        schema.BibliotheekKenmerk.bibliotheek_categorie_id
        == schema.BibliotheekCategorie.id,
        isouter=True,
    )
)

case_type_detail_query = select(
    [
        schema.Zaaktype.uuid,
        schema.Zaaktype.id,
        schema.ZaaktypeNode.titel.label("name"),
        schema.ZaaktypeNode.code,
        schema.ZaaktypeNode.version.label("current_version"),
        schema.Zaaktype.active,
        schema.Zaaktype.last_modified,
    ]
).where(schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id)

object_type_detail = (
    select(
        [
            schema.ObjectBibliotheekEntry.object_uuid.label("uuid"),
            schema.ObjectBibliotheekEntry.name,
            schema.BibliotheekCategorie.uuid.label("folder_uuid"),
            schema.BibliotheekCategorie.naam.label("folder_name"),
        ]
    )
    .select_from(
        join(
            schema.ObjectBibliotheekEntry,
            schema.BibliotheekCategorie,
            schema.ObjectBibliotheekEntry.bibliotheek_categorie_id
            == schema.BibliotheekCategorie.id,
            isouter=True,
        )
    )
    .where(
        and_(
            schema.ObjectData.uuid
            == schema.ObjectBibliotheekEntry.object_uuid,
            schema.ObjectData.object_class == "type",
        )
    )
)

custom_object_type_detail = select(
    [
        schema.CustomObjectTypeVersion.uuid,
        schema.CustomObjectTypeVersion.name,
        schema.CustomObjectTypeVersion.title,
        schema.CustomObjectTypeVersion.external_reference,
        schema.CustomObjectTypeVersion.status,
        schema.CustomObjectTypeVersion.version,
        schema.CustomObjectTypeVersion.date_created,
        schema.CustomObjectTypeVersion.last_modified,
        schema.CustomObjectType.uuid.label("version_independent_uuid"),
        schema.BibliotheekCategorie.uuid.label("folder_uuid"),
        schema.BibliotheekCategorie.naam.label("folder_name"),
    ]
).select_from(
    join(
        join(
            schema.CustomObjectType,
            schema.CustomObjectTypeVersion,
            schema.CustomObjectType.custom_object_type_version_id
            == schema.CustomObjectTypeVersion.id,
        ),
        schema.BibliotheekCategorie,
        schema.CustomObjectType.catalog_folder_id
        == schema.BibliotheekCategorie.id,
        isouter=True,
    )
)

document_template_detail_query = select(
    [
        schema.BibliotheekSjabloon.id,
        schema.BibliotheekSjabloon.uuid,
        schema.BibliotheekSjabloon.naam.label("name"),
        schema.BibliotheekSjabloon.last_modified,
        schema.Filestore.original_name,
        schema.BibliotheekSjabloon.interface_id,
        schema.BibliotheekCategorie.uuid.label("folder_uuid"),
        schema.BibliotheekCategorie.naam.label("folder_name"),
    ]
).select_from(
    join(
        schema.BibliotheekSjabloon,
        schema.Filestore,
        schema.BibliotheekSjabloon.filestore_id == schema.Filestore.id,
        isouter=True,
    ).join(
        schema.BibliotheekCategorie,
        schema.BibliotheekSjabloon.bibliotheek_categorie_id
        == schema.BibliotheekCategorie.id,
        isouter=True,
    )
)

email_template_detail_query = select(
    [
        schema.BibliotheekNotificatie.id,
        schema.BibliotheekNotificatie.uuid,
        schema.BibliotheekNotificatie.label.label("name"),
        schema.BibliotheekNotificatie.last_modified,
        schema.BibliotheekCategorie.uuid.label("folder_uuid"),
        schema.BibliotheekCategorie.naam.label("folder_name"),
    ]
).select_from(
    join(
        schema.BibliotheekNotificatie,
        schema.BibliotheekCategorie,
        schema.BibliotheekNotificatie.bibliotheek_categorie_id
        == schema.BibliotheekCategorie.id,
        isouter=True,
    )
)


def get_folder_contents_query(folder_id):
    """Combine the generic folder content queries with a folder_id, and combine
    them with a "UNION ALL"

    :param folder_id: Database id of the folder to retrieve contents of.
    :type folder_id: int
    :return: SQLAlchemy query for folder contents
    """

    specific_folder_query = folder_query.where(
        schema.BibliotheekCategorie.pid == folder_id
    ).order_by("name")

    specific_case_type_query = case_type_query.where(
        schema.Zaaktype.bibliotheek_categorie_id == folder_id
    ).order_by("name")

    specific_attribute_query = attribute_query.where(
        schema.BibliotheekKenmerk.bibliotheek_categorie_id == folder_id
    ).order_by("name")

    specific_email_template_query = email_template_query.where(
        schema.BibliotheekNotificatie.bibliotheek_categorie_id == folder_id
    ).order_by("name")

    specific_document_template_query = document_template_query.where(
        schema.BibliotheekSjabloon.bibliotheek_categorie_id == folder_id
    ).order_by("name")

    specific_object_type_query = object_type_query.where(
        schema.ObjectBibliotheekEntry.bibliotheek_categorie_id == folder_id
    ).order_by("name")

    specific_custom_object_type_query = custom_object_type_query.where(
        schema.CustomObjectType.catalog_folder_id == folder_id
    ).order_by("name")

    return union_all(
        specific_folder_query,
        specific_case_type_query,
        specific_attribute_query,
        specific_document_template_query,
        specific_email_template_query,
        specific_object_type_query,
        specific_custom_object_type_query,
    )


def search_catalog_query(keyword: str = None, type: str = None):
    """Combine the generic search result content queries and combine
    them with a "UNION ALL"

    :param keyword: The search keyword to retrieve contents of the catalog.
    :type keyword: string
    :return: SQLAlchemy query for folder contents
    """

    specific_folder_query = folder_query.where(
        and_(schema.BibliotheekCategorie.naam.ilike(f"%{keyword}%"))
    ).order_by(schema.BibliotheekCategorie.naam)

    specific_case_type_query = case_type_query.where(
        or_(
            schema.ZaaktypeNode.titel.ilike(f"%{keyword}%"),
            schema.ZaaktypeNode.zaaktype_trefwoorden.ilike(f"%{keyword}%"),
        )
    ).order_by(schema.ZaaktypeNode.titel)

    specific_attribute_query = attribute_query.where(
        and_(
            schema.BibliotheekKenmerk.naam.ilike(f"%{keyword}%"),
            schema.BibliotheekKenmerk.deleted.is_(None),
        )
    ).order_by(schema.BibliotheekKenmerk.naam)

    specific_document_template_query = document_template_query.where(
        and_(schema.BibliotheekSjabloon.naam.ilike(f"%{keyword}%"))
    ).order_by(schema.BibliotheekSjabloon.naam)

    specific_email_template_query = email_template_query.where(
        and_(schema.BibliotheekNotificatie.label.ilike(f"%{keyword}%"))
    ).order_by(schema.BibliotheekNotificatie.label)

    specific_object_type_query = object_type_query.where(
        and_(schema.ObjectBibliotheekEntry.name.ilike(f"%{keyword}%"))
    ).order_by(schema.ObjectBibliotheekEntry.name)

    specific_custom_object_type_query = custom_object_type_query.where(
        and_(schema.CustomObjectTypeVersion.name.ilike(f"%{keyword}%"))
    ).order_by(schema.CustomObjectTypeVersion.name)

    type_query_map = {
        "folder": specific_folder_query,
        "case_type": specific_case_type_query,
        "attribute": specific_attribute_query,
        "document_template": specific_document_template_query,
        "email_template": specific_email_template_query,
        "object_type": specific_object_type_query,
        "custom_object_type": specific_custom_object_type_query,
    }

    if type and type in type_query_map:
        return type_query_map[type]

    return union_all(
        specific_folder_query,
        specific_case_type_query,
        specific_attribute_query,
        specific_document_template_query,
        specific_email_template_query,
        specific_object_type_query,
        specific_custom_object_type_query,
    )


def case_types_related_to_case_type(case_type_id):
    """Builds a SQL query to retrieve case types a case type is part of

    :param attribute_id: database id of the case type to retrieve the case type
        list for
    :type attribute_id: integer
    :return: SQL query for retrieving the case type list
    :rtype: sqlalchemy.sql.expression.Select
    """

    query = (
        select(
            [
                schema.ZaaktypeRelatie.zaaktype_node_id,
                schema.ZaaktypeNode.titel.label("name"),
                schema.ZaaktypeNode.version,
                schema.Zaaktype.active,
                cast(
                    schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
                    Boolean,
                ).label("is_current_version"),
                schema.Zaaktype.uuid,
                func.row_number()
                .over(
                    partition_by=schema.ZaaktypeNode.zaaktype_id,
                    order_by=schema.ZaaktypeNode.version.desc(),
                )
                .label("row_number"),
            ]
        )
        .select_from(join(schema.ZaaktypeKenmerk, schema.ZaaktypeNode))
        .where(
            and_(
                schema.ZaaktypeNode.id
                == schema.ZaaktypeRelatie.zaaktype_node_id,
                schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
                schema.ZaaktypeRelatie.relatie_zaaktype_id == case_type_id,
            )
        )
        .alias("ranked_case_types")
    )

    return (
        select(
            [
                query.c.uuid,
                query.c.active,
                query.c.version,
                query.c.name,
                query.c.is_current_version,
            ]
        )
        .where(query.c.row_number == 1)
        .order_by(query.c.name, query.c.uuid)
    )


def case_type_version(case_type_version_uuid):
    """Builds a SQL query to retrieve case type version record

    :param case_type_version_uuid: uuid of the case type version to retrieve record details.
    :type case_type_version_uuid: UUID
    :return: SQL query for retrieving one case type version by its uuid
    :rtype: sqlalchemy.sql.expression.Select
    """

    casetype_join = join(
        schema.ZaaktypeNode,
        schema.Zaaktype,
        schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
    )

    query = (
        select(
            [
                schema.ZaaktypeNode.id,
                schema.ZaaktypeNode.uuid,
                schema.ZaaktypeNode.titel.label("name"),
                schema.ZaaktypeNode.created,
                schema.ZaaktypeNode.last_modified,
                schema.ZaaktypeNode.version,
                cast(
                    schema.ZaaktypeNode.id == schema.Zaaktype.zaaktype_node_id,
                    Boolean,
                ).label("active"),
                schema.Zaaktype.uuid.label("case_type_uuid"),
            ]
        )
        .select_from(casetype_join)
        .where(and_(schema.ZaaktypeNode.uuid == case_type_version_uuid))
    )

    return query


def case_type_versions_history(case_type_uuid):
    """Builds a SQL query to retrieve case type versions history

    :param case_type_uuid: uuid of the case type to retrieve case type version history
    :type case_type_uuid: UUID
    :return: SQL query for retrieving the case type list
    :rtype: sqlalchemy.sql.expression.Select
    """

    casetype_join = join(
        schema.ZaaktypeNode,
        schema.Zaaktype,
        schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
    ).join(
        schema.Logging,
        schema.Logging.id == schema.ZaaktypeNode.logging_id,
        isouter=True,
    )
    query = (
        select(
            [
                schema.ZaaktypeNode.id,
                schema.ZaaktypeNode.uuid,
                schema.ZaaktypeNode.titel.label("name"),
                schema.ZaaktypeNode.created,
                schema.ZaaktypeNode.last_modified,
                schema.Zaaktype.uuid.label("case_type_uuid"),
                schema.ZaaktypeNode.version,
                cast(
                    schema.ZaaktypeNode.id == schema.Zaaktype.zaaktype_node_id,
                    Boolean,
                ).label("active"),
                schema.Logging.created_by.label("username"),
                schema.Logging.created_by_name_cache.label("display_name"),
                schema.Logging.onderwerp.label("reason"),
                schema.Logging.event_data,
            ]
        )
        .select_from(casetype_join)
        .where(and_(schema.Zaaktype.uuid == case_type_uuid))
    )

    return query.order_by(schema.ZaaktypeNode.id)


def case_types_related_to_attribute(attribute_id):
    """Builds a SQL query to retrieve case types an attribute is part of

    :param attribute_id: database id of the attribute to retrieve the case type
        list for
    :type attribute_id: integer
    :return: SQL query for retrieving the case type list
    :rtype: sqlalchemy.sql.expression.Select
    """

    query = (
        select(
            [
                schema.ZaaktypeKenmerk.zaaktype_node_id,
                schema.ZaaktypeNode.titel.label("name"),
                schema.ZaaktypeNode.version,
                schema.Zaaktype.active,
                cast(
                    schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
                    Boolean,
                ).label("is_current_version"),
                schema.Zaaktype.uuid,
                func.row_number()
                .over(
                    partition_by=schema.ZaaktypeNode.zaaktype_id,
                    order_by=schema.ZaaktypeNode.version.desc(),
                )
                .label("row_number"),
            ]
        )
        .select_from(join(schema.ZaaktypeKenmerk, schema.ZaaktypeNode))
        .where(
            and_(
                schema.ObjectData.object_class == "casetype",
                schema.ObjectData.object_id == schema.ZaaktypeNode.zaaktype_id,
                schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
                schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
                == attribute_id,
            )
        )
        .alias("ranked_case_types")
    )

    return (
        select(
            [
                query.c.uuid,
                query.c.active,
                query.c.version,
                query.c.name,
                query.c.is_current_version,
            ]
        )
        .where(query.c.row_number == 1)
        .order_by(query.c.name, query.c.uuid)
    )


def case_types_related_to_email_template(email_template_id):
    """Builds a SQL query to retrieve case types an email template is contained in

    :param attribute_id: database id of the email template to retrieve the case type
        list for
    :type attribute_id: integer
    :return: SQL query for retrieving the case type list
    :rtype: sqlalchemy.sql.expression.Select
    """

    query = (
        select(
            [
                schema.ZaaktypeNotificatie.zaaktype_node_id,
                schema.ZaaktypeNode.titel.label("name"),
                schema.ZaaktypeNode.version,
                schema.Zaaktype.active,
                cast(
                    schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
                    Boolean,
                ).label("is_current_version"),
                schema.Zaaktype.uuid,
                func.row_number()
                .over(
                    partition_by=schema.ZaaktypeNode.zaaktype_id,
                    order_by=schema.ZaaktypeNode.version.desc(),
                )
                .label("row_number"),
            ]
        )
        .select_from(join(schema.ZaaktypeNotificatie, schema.ZaaktypeNode))
        .where(
            and_(
                schema.ObjectData.object_class == "casetype",
                schema.ObjectData.object_id == schema.ZaaktypeNode.zaaktype_id,
                schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
                schema.ZaaktypeNotificatie.bibliotheek_notificatie_id
                == email_template_id,
            )
        )
        .alias("ranked_case_types")
    )

    return (
        select(
            [
                query.c.uuid,
                query.c.active,
                query.c.version,
                query.c.name,
                query.c.is_current_version,
            ]
        )
        .where(query.c.row_number == 1)
        .order_by(query.c.name, query.c.uuid)
    )


def case_types_related_to_document_template(document_template_id):
    """Builds a SQL query to retrieve case types an document template is contained in

    :param attribute_id: database id of the document template to retrieve the case type
        list for
    :type attribute_id: integer
    :return: SQL query for retrieving the case type list
    :rtype: sqlalchemy.sql.expression.Select
    """

    query = (
        select(
            [
                schema.ZaaktypeSjabloon.zaaktype_node_id,
                schema.ZaaktypeNode.titel.label("name"),
                schema.ZaaktypeNode.version,
                schema.Zaaktype.active,
                cast(
                    schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
                    Boolean,
                ).label("is_current_version"),
                schema.Zaaktype.uuid,
                func.row_number()
                .over(
                    partition_by=schema.ZaaktypeNode.zaaktype_id,
                    order_by=schema.ZaaktypeNode.version.desc(),
                )
                .label("row_number"),
            ]
        )
        .select_from(join(schema.ZaaktypeSjabloon, schema.ZaaktypeNode))
        .where(
            and_(
                schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
                schema.ZaaktypeSjabloon.bibliotheek_sjablonen_id
                == document_template_id,
            )
        )
        .alias("ranked_case_types")
    )

    return (
        select(
            [
                query.c.uuid,
                query.c.active,
                query.c.version,
                query.c.name,
                query.c.is_current_version,
            ]
        )
        .where(query.c.row_number == 1)
        .order_by(query.c.name, query.c.uuid)
    )
