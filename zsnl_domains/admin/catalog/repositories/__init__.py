# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .catalog import Catalog

__all__ = ["Catalog"]
