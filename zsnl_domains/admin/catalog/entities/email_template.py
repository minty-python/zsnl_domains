# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.cqrs.events import event
from minty.entity import EntityBase
from uuid import UUID


class EmailTemplate(EntityBase):
    """Data structure which holds detailed information about an email_template."""

    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        id: int,
        label: str,
        sender: str,
        sender_address: str,
        subject: str,
        message: str,
        category_uuid: UUID,
        attachments: list,
    ):
        self.uuid = uuid
        self.id = id
        self.label = label
        self.sender = sender
        self.sender_address = sender_address
        self.subject = subject
        self.message = message
        self.category_uuid = category_uuid
        self.attachments = attachments
        self.commit_message = None
        self.deleted = None

    @event("EmailTemplateCreated")
    def create(self, fields: dict):
        """Create email_template.

        :param fields: fields and fields to be updated
        :type fields: dict
        """
        allowed_fields = [
            "label",
            "sender",
            "sender_address",
            "subject",
            "message",
            "category_uuid",
            "attachments",
            "commit_message",
        ]

        for key, value in fields.items():
            if key in allowed_fields:
                setattr(self, key, value)

    @event("EmailTemplateEdited")
    def edit(self, fields: dict):
        """Update/edit email_template.

        :param fields: fields and fields to be updateds
        :type fields: dict
        """
        allowed_fields = [
            "label",
            "sender",
            "sender_address",
            "subject",
            "message",
            "category_uuid",
            "attachments",
            "commit_message",
        ]

        for key, value in fields.items():
            if key in allowed_fields:
                setattr(self, key, value)

    @event("EmailTemplateDeleted")
    def delete(self, reason: str):
        """Delete an email_template.

        :param reason: reason for delete
        :type reason: str
        """

        self.deleted = datetime.now()
        self.commit_message = reason
