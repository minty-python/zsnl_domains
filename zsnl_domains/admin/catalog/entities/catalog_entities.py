# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from dataclasses import dataclass
from datetime import datetime
from enum import Enum, auto
from minty.cqrs import event
from minty.entity import Entity, EntityBase, Field
from typing import List, Optional, Sequence
from uuid import UUID


class EntryType(Enum):
    folder = auto()
    case_type = auto()
    attribute = auto()
    email_template = auto()
    document_template = auto()
    object_type = auto()


class Folder(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        id,
        uuid,
        name,
        parent_uuid,
        parent_name,
        last_modified,
        parent_ids=None,
    ):
        self.id = id
        self.uuid = uuid
        self.name = name
        self.parent_uuid = parent_uuid
        self.parent_name = parent_name
        self.last_modified = last_modified
        self.parent_ids = parent_ids
        self.deleted = None
        self.commit_message = None

    @event("FolderRenamed")
    def rename(self, name: str):
        """Rename folder entry.

        Name can't be similar to a folder with the same parent id (pid) but this
        is a database constraint.

        :param name: new name of folder
        :type name: str
        """
        self.name = name

    @event("FolderCreated")
    def create(self, parent_uuid: UUID, name: str):
        """Create new folder entry from params."""
        self.parent_uuid = parent_uuid
        self.name = name

    @event("FolderDeleted", extra_fields=["name"])
    def delete(self, reason: str):
        """Delete a folder.

        :param reason: reason for delete
        :type reason: str
        """

        self.deleted = datetime.now()
        self.commit_message = reason


@dataclass
class RelatedCaseType:
    uuid: UUID
    name: str
    active: bool
    version: int
    is_current_version: bool


@dataclass
class ObjectTypeDetail:
    """An object type in the catalog."""

    uuid: UUID
    name: str
    used_in_case_types: Sequence[RelatedCaseType]
    used_in_object_types: Sequence
    folder: Optional[Folder]


@dataclass
class AttributeDetail:
    """An attribute detail in the catalog."""

    uuid: UUID
    name: str
    last_modified: datetime

    magic_string: str
    value_type: str
    is_multiple: bool

    used_in_case_types: List[RelatedCaseType]
    used_in_object_types: list
    folder: Optional[Folder]


@dataclass
class EmailTemplateDetail:
    """Data structure which holds basic details of an email_template."""

    uuid: UUID
    name: str
    last_modified: datetime

    used_in_case_types: List[RelatedCaseType]
    folder: Optional[Folder]


@dataclass
class DocumentTemplateDetail:
    """A document template in the catalog."""

    uuid: UUID
    name: str
    filename: str
    has_default_integration: bool
    last_modified: datetime

    used_in_case_types: List[RelatedCaseType]
    folder: Optional[Folder]


@dataclass
class CaseTypeVersion:
    """A Case type Version in the catalog."""

    uuid: UUID
    case_type_uuid: UUID
    name: str
    username: str
    display_name: str
    active: bool
    version: int
    created: datetime
    last_modified: datetime
    reason: str
    change_note: str
    modified_components: dict


class CustomObjectTypeDetail(Entity):
    """Custom Object Type"""

    class Config:
        arbitrary_types_allowed = True  # make sure "Folder" type is allowed

    entity_type = "custom_object_type_detail"

    name: str = Field(..., title="Name of this custom object type type")
    uuid: UUID = Field(
        ..., title="Unique identifier of this custom object type"
    )
    title: str = Field(
        ..., title="Title of generated objects from this custom object type"
    )
    status: str = Field(..., title="Current status of this custom object type")
    version: int = Field(
        ..., title="Current version of this custom object type"
    )
    external_reference: str = Field(
        None, title="External reference of this custom object type"
    )
    version_independent_uuid: UUID = Field(
        ..., title="Version independent identifier of this custom object"
    )

    date_created: datetime = Field(
        ..., title="Version independent identifier of this custom object"
    )
    last_modified: datetime = Field(
        ..., title="Version independent identifier of this custom object"
    )

    entity_relationships = ["folder"]
    folder: Optional[Folder] = Field(None, title="The folder of this entity")
