# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, ValueObject
from pydantic import Field
from typing import List, Optional
from uuid import UUID


class TransactionRecord(ValueObject):
    uuid: Optional[UUID] = Field(
        None, title="Internal Identifier this transaction record"
    )
    transaction_id: int = Field(None, title="Id of related transaction")
    is_error: Optional[bool] = Field(None, title="Boolean representing error")
    input_data: Optional[str] = Field(
        ..., title="Input data for the transaction"
    )
    output_data: Optional[str] = Field(
        ..., title="Output data for the transaction"
    )
    preview_string: Optional[str] = Field(
        ..., title="Text preview for transaction record"
    )


class Transaction(Entity):
    entity_type = "transaction"
    entity_id__fields = ["uuid"]

    uuid: Optional[UUID] = Field(
        ..., title="Internal Identifier for this role"
    )
    input_data: Optional[str] = Field(
        ..., title="Input data for the transaction"
    )
    integration_uuid: Optional[UUID] = Field(
        ..., title="Status of the transaction"
    )
    transaction_records: Optional[List[TransactionRecord]] = Field(
        default_factory=list, title="Detailed record of transaction"
    )

    @Entity.event("TransactionCreated")
    def create(self, integration_uuid: UUID, input_data: str):
        """Create Transaction entry"""
        self.integration_uuid = integration_uuid
        self.input_data = input_data

    @Entity.event("TransactionRecordAdded")
    def add_transaction_record(
        self,
        is_error: bool,
        input_data: str,
        output_data: str,
        preview_string: str,
    ):
        """Add transaction record for given transaction"""

        record = TransactionRecord(
            is_error=False,
            input_data=str(input_data),
            output_data=str(output_data),
            preview_string=preview_string,
        )

        self.transaction_records = [record]
