# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import os
import tempfile
from ... import DatabaseRepositoryBase
from ..entities import AttachedFile, MessageAttachment
from .database_queries import (
    get_file_by_attachment,
    get_file_by_attachment_without_acl_check,
)
from datetime import datetime, timezone
from io import SEEK_SET, BytesIO
from minty.exceptions import Conflict, NotFound
from minty_infra_storage import s3
from sqlalchemy import sql
from typing import Optional, cast
from uuid import UUID, uuid4
from zsnl_domains.database import schema
from zsnl_domains.document.infrastructures.converter import (
    ConverterInfrastructure,
)


class AttachmentRepository(DatabaseRepositoryBase):
    REQUIRED_INFRASTRUCTURE = {
        **DatabaseRepositoryBase.REQUIRED_INFRASTRUCTURE,
        "s3": s3.S3Infrastructure(),
        "converter": ConverterInfrastructure(),
    }

    def attach_file_to_message(
        self, filename: str, file_uuid: UUID, message_uuid: UUID
    ) -> MessageAttachment:
        # currently no acl checks are implemented here since this
        # method is only called on message create (ACL checks are already done)
        message_attachment = MessageAttachment(
            filename=None,
            file_uuid=file_uuid,
            message_uuid=None,
            attachment_uuid=None,
        )
        message_attachment.event_service = self.event_service
        message_attachment.attach_file(
            file_uuid=file_uuid,
            filename=filename,
            message_uuid=message_uuid,
            attachment_uuid=uuid4(),
        )
        return message_attachment

    def save(self):
        events = [
            e
            for e in self.event_service.event_list
            if e.entity_type in ["MessageAttachment", "AttachedFile"]
        ]
        for ev in events:
            if ev.event_name == "AttachedToMessage":
                self._insert_file_attachment(event=ev)
            if ev.event_name == "PdfDerivativeGenerated":
                try:
                    self._generate_pdf_derivative_for_attachment(event=ev)
                except Conflict:
                    self.logger.exception(
                        "Error while generating PDF derivative"
                    )

    def _insert_file_attachment(self, event):
        formatted_changes = {}
        for change in event.changes:
            formatted_changes[change["key"]] = change["new_value"]

        filestore_uuid = formatted_changes["file_uuid"]
        message_uuid = formatted_changes["message_uuid"]
        attachment_uuid = formatted_changes["attachment_uuid"]
        filename = formatted_changes["filename"]

        filestore_id_query = (
            sql.select([schema.Filestore.id])
            .where(schema.Filestore.uuid == filestore_uuid)
            .scalar_subquery()
        )
        message_id_query = (
            sql.select([schema.ThreadMessage.id])
            .where(schema.ThreadMessage.uuid == message_uuid)
            .scalar_subquery()
        )

        self.session.execute(
            sql.insert(
                schema.ThreadMessageAttachment,
                values={
                    "filestore_id": filestore_id_query,
                    "thread_message_id": message_id_query,
                    "uuid": attachment_uuid,
                    "filename": filename,
                },
            )
        )

    def generate_download_url(self, file: AttachedFile) -> str:
        """Generate temporary download url for given file.

        :param file: file Entity
        :type file: File
        :return:  url to download file
        :rtype: str
        """
        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        url = store.get_download_url(
            uuid=file.uuid,
            storage_location=file.storage_location[0],
            mime_type=file.mimetype,
            filename=file.filename,
            download=True,
        )
        return url

    def get_file_by_uuid(
        self, attachment_uuid: UUID, user_uuid: UUID, is_pip_user: bool
    ) -> AttachedFile:
        """Get file by attachment uuid, ACL checks included for pip users and normal users.

        :param attachment_uuid: attachment uuid
        :type attachment_uuid: UUID
        :param user_uuid: user_uuid
        :type user_uuid: UUID
        :param is_pip_user: is user a pip user
        :type is_pip_user: bool
        :return: file Entity
        :rtype: File
        """

        result = self.session.execute(
            get_file_by_attachment(
                attachment_uuid=attachment_uuid,
                db=self.session,
                user_uuid=user_uuid,
                is_pip=is_pip_user,
            )
        ).fetchone()
        if result is None:
            raise NotFound(
                "Attachment not found.",
                "communication/attachment/not_found",
            )
        return self._transform_to_file_entity(file_result=result)

    def _transform_to_file_entity(self, file_result) -> AttachedFile:
        file = AttachedFile(
            uuid=file_result.uuid,
            filename=file_result.original_name,
            size=file_result.size,
            mimetype=file_result.mimetype,
            md5=file_result.md5,
            date_created=file_result.date_created,
            storage_location=file_result.storage_location,
            attachment_uuid=file_result.attachment_uuid,
            preview_uuid=getattr(file_result, "preview_uuid", None),
            preview_mimetype=getattr(file_result, "preview_mimetype", None),
            preview_storage_location=getattr(
                file_result, "preview_storage_location", None
            ),
            preview_filename=getattr(file_result, "preview_filename", None),
        )
        file.event_service = self.event_service
        return file

    def _generate_pdf_derivative_for_attachment(self, event):
        """Generate pdf derivative for an attachment.

        :param event: PdfDerivativeGenerated
        :type event: minty.cqrs.Event
        """
        storage_infra = self._get_infrastructure("s3")
        converter = self._get_infrastructure("converter")

        file_uuid = event.entity_id
        storage_location = event.entity_data["storage_location"]
        original_filename = event.entity_data["filename"]
        attachment_uuid = event.entity_data["attachment_uuid"]

        # download attachment form s3.
        file_fh = BytesIO(initial_bytes=b"")
        storage_infra.download_file(
            destination=file_fh,
            file_uuid=file_uuid,
            storage_location=storage_location[0],
        )

        # convert attachment to pdf by using converter service.qq
        file_fh.seek(0, SEEK_SET)
        pdf_file = converter.convert(
            to_type="application/pdf",
            content=file_fh.read(),
            options={"force_from_type": "application/msword"},
        )

        # upload pdf to s3.
        file_handle = tempfile.SpooledTemporaryFile(max_size=5_000_000)
        file_handle.write(pdf_file)
        file_handle.seek(0, SEEK_SET)
        derivative_uuid = uuid4()
        upload_result = storage_infra.upload(
            file_handle=file_handle, uuid=derivative_uuid
        )

        basename = os.path.splitext(original_filename)[0]

        # insert details of uploaded pdf in filestore table.
        filestore_id = self.session.execute(
            sql.insert(
                schema.Filestore,
                values={
                    "uuid": derivative_uuid,
                    "original_name": basename + ".pdf",
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": upload_result["mime_type"],
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                },
            )
        ).inserted_primary_key[0]

        attachment_id = (
            sql.select([schema.ThreadMessageAttachment.id])
            .where(schema.ThreadMessageAttachment.uuid == attachment_uuid)
            .scalar_subquery()
        )

        # insert attachment_derivative row with link to filestore.
        self.session.execute(
            sql.insert(
                schema.ThreadMessageAttachmentDerivative,
                values={
                    "filestore_id": filestore_id,
                    "thread_message_attachment_id": attachment_id,
                    "max_width": 0,
                    "max_height": 0,
                    "date_generated": datetime.now(timezone.utc),
                    "type": "pdf",
                },
            )
        )

    def get_file_by_uuid_without_acl_check(
        self, attachment_uuid
    ) -> AttachedFile:
        """Get file by attachment_uuid,without ACL check..

        :param attachment_uuid: attachment uuid
        :type attachment_uuid: UUID
        :return: file Entity
        :rtype: AttachedFile
        """

        result = self.session.execute(
            get_file_by_attachment_without_acl_check(
                attachment_uuid=attachment_uuid
            )
        ).fetchone()

        if result is None:
            raise NotFound(
                f"Attachment with uuid {attachment_uuid} not found.",
                "communication/attachment/not_found",
            )
        return self._transform_to_file_entity(file_result=result)

    def generate_preview_url(
        self,
        preview_uuid: UUID,
        preview_mimetype: str,
        preview_storage_location: str,
        preview_filename: Optional[str] = None,
    ) -> str:
        """Generate temporary preview url for given attachment.

        :param preview_uuid: UUID of the filestore where preview is stored.
        :type preview_uuid: UUID
        :param preview_mimetype: mimetype of preview.
        :type preview_mimetype: str
        :param preview_storage_location: storage for the preview.
        :type preview_storage_location: str
        :return:  url to preview attachment
        :rtype: str
        """
        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        url = store.get_download_url(
            uuid=preview_uuid,
            storage_location=preview_storage_location[0],
            mime_type=preview_mimetype,
            filename=preview_filename,
            download=False,
        )
        return url
