# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import email
import email.message
import io
import logging
import re
import string
from ..document import entities as DocumentEntities
from .entities import Contact, ExternalMessage, Thread
from .repositories import FileRepository
from email import utils as email_utils
from minty.cqrs import CommandBase
from minty.exceptions import Conflict, Forbidden, NotFound
from minty.validation import validate_with
from pkgutil import get_data
from typing import Dict, List, Mapping, Optional
from uuid import UUID, uuid4
from zsnl_domains import document

logger = logging.getLogger(__name__)

OUTLOOK_MIME_TYPES = [
    # The "official" Outlook .msg MIME type
    "application/vnd.ms-outlook",
    # .msg files that have their parts in a non-default order get detected as
    # "uh yeah it's a file from MS Office but not sure which kind exactly"
    "application/vnd.ms-office",
    # Files >1MB are truncated before type detection, so they are detected as
    # "Composite Document Format v2"
    "application/CDFV2",
]


def _strip_nul_bytes(s: str):
    try:
        return s.translate({0: None})
    except AttributeError:
        return


class Commands(CommandBase):
    """Communication domain commands."""

    @validate_with(get_data(__name__, "validation/send_email.json"))
    def send_email(self, message_uuid):
        """Send the message indicated by message_uuid using the configured
        outgoing mail service

        :param message_uuid: UUID of the message to send out.
            Must be an external message.
        :param message_uuid: str
        """
        message_repo = self.get_repository("message")
        message = message_repo.get_message_by_uuid(message_uuid)
        message.send()
        message_repo.save()

    @validate_with(get_data(__name__, "validation/import_email.json"))
    def import_email(self, file_uuid: UUID):
        """Import an email from a file, given the file UUID

        :param file_uuid: UUID of the file to import
        :type file_uuid: str
        """
        file_repo = self.get_repository("file")
        case_repo = self.get_repository("case")
        thread_repo = self.get_repository("thread")
        message_repo = self.get_repository("message")
        document_file_repo = self.get_repository("document_file")
        attachment_repo = self.get_repository("attachment")

        email_config = message_repo.get_email_configuration()
        email_message = file_repo.parse_as_message(file_uuid)

        participants = _get_participants(email_message=email_message)
        subject = _strip_nul_bytes(email_message["subject"])
        content = _extract_message_body(email_message)

        # Get the UUID of the thread to attach the message to. Creates a new
        # "inbox" thread if no thread can be found, or if the case a thread is
        # linked to is closed.
        thread_info = _get_thread_from_message(
            subject_prefix=email_config["subject_prefix"],
            message=email_message,
            thread_repository=thread_repo,
            case_repository=case_repo,
        )
        thread = thread_info["thread"]
        thread.increment_unread_count()

        message_uuid = uuid4()

        message = message_repo.create_external_message(
            thread_uuid=thread.uuid,
            case_id=None,  # not used when creating an `incoming` message
            external_message_type="email",
            external_message_uuid=message_uuid,
            created_by=None,
            subject=subject,
            content=content,
            participants=participants,
            direction="incoming",
            original_message_file=file_uuid,
            creator_type=None,
        )

        for att in email_message.iter_attachments():
            try:
                # `get_content` raises KeyError if called on a "multipart" part
                #
                # This happens with multipart/signed (S/MIME) messages; if the
                # user wants to check digital signatures, they should download
                # the original source of the message.
                content = att.get_content()
            except KeyError:
                content = str(att)

            filename = att.get_filename()

            try:
                file_entity = _upload_and_create_attachment(
                    content=content,
                    filename=filename,
                    file_repository=file_repo,
                    document_file_repository=document_file_repo,
                )
            except Forbidden as e:
                self.logger.debug(f"Attachment not accepted: {e}")
                continue

            attachment_repo.attach_file_to_message(
                filename=file_entity.filename,
                file_uuid=file_entity.file_uuid,
                message_uuid=message_uuid,
            )
            _increment_attachment_count_for_message(
                message=message, thread=thread
            )

        # order of saving is essential
        document_file_repo.save()
        thread_repo.save()
        message_repo.save()
        attachment_repo.save()

    @validate_with(get_data(__name__, "validation/import_email_message.json"))
    def process_email_file(self, case_uuid: UUID, file_uuid: UUID):
        file_repo = self.get_repository("file")
        case_repo = self.get_repository("case")
        thread_repo = self.get_repository("thread")
        message_repo = self.get_repository("message")
        document_file_repo = self.get_repository("document_file")
        attachment_repo = self.get_repository("attachment")

        file = file_repo.get_file_by_uuid(file_uuid=file_uuid)
        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid, user_uuid=self.user_uuid, permission="write"
        )

        # Some .eml and .mime files are of mimetype 'text/plain' unfortunately
        if file.mimetype in ["text/plain", "message/rfc822"]:
            email_message = file_repo.parse_as_message(file_uuid)
        elif file.mimetype in OUTLOOK_MIME_TYPES:
            # We truncate the file to 1MB before MIME type detection, which
            # leads to .msg files being (mis)detected as "CDFV2".
            email_message = file_repo.parse_as_message_outlook(file_uuid)

        else:
            raise Conflict(
                f"Unable to process file with mimetype '{file.mimetype}'.",
                "communication/email/incorrect_filetype",
            )
        message_date = (
            datetime.datetime.strptime(
                email_message["date"] or "", "%a, %d %b %Y %H:%M:%S %z"
            )
            or None
        )

        if message_date:
            message_date = message_date.astimezone(tz=datetime.timezone.utc)

        subject = _strip_nul_bytes(email_message["subject"])
        content = _extract_message_body(email_message)
        participants = _get_participants(email_message=email_message)
        attachments = []

        for att in email_message.iter_attachments():
            try:
                attachment_content = att.get_content()
            except KeyError:
                # `get_content` raises KeyError if called on a "multipart" part
                #
                # This happens with multipart/signed (S/MIME) messages; if the
                # user wants to check digital signatures, they should download
                # the original source of the message.
                attachment_content = str(att)

            att_entry = {
                "content": attachment_content,
                "filename": att.get_filename(),
            }
            attachments.append(att_entry)

        thread_uuid = uuid4()
        thread = thread_repo.create(
            new_thread_uuid=thread_uuid,
            contact=None,
            case=case,
            thread_type="external",
        )

        thread.increment_unread_count()

        message_uuid = uuid4()

        message = message_repo.create_external_message(
            thread_uuid=thread_uuid,
            case_id=None,  # not used when creating an `incoming` message
            external_message_type="email",
            external_message_uuid=message_uuid,
            created_by=None,
            subject=subject,
            content=content,
            participants=participants,
            direction="incoming",
            original_message_file=file_uuid,
            message_date=message_date,
            is_imported=True,
            creator_type="employee",
        )

        for att in attachments:
            try:
                file_entity = _upload_and_create_attachment(
                    content=att["content"],
                    filename=att["filename"],
                    file_repository=file_repo,
                    document_file_repository=document_file_repo,
                )
            except Forbidden as e:
                self.logger.debug(f"Attachment not accepted: {e}")
                continue

            attachment_repo.attach_file_to_message(
                filename=file_entity.filename,
                file_uuid=file_entity.file_uuid,
                message_uuid=message_uuid,
            )
            _increment_attachment_count_for_message(
                message=message, thread=thread
            )

        # order of saving is essential for this to work
        document_file_repo.save()
        thread_repo.save()
        message_repo.save()
        attachment_repo.save()

    @validate_with(get_data(__name__, "validation/create_contact_moment.json"))
    def create_contact_moment(
        self,
        contact_moment_uuid: UUID,
        thread_uuid: UUID,
        contact_uuid: UUID,
        channel: str,
        content: str,
        direction: str,
        case_uuid: Optional[UUID],
    ):
        """Command to create a contact moment. For a contact or case or both contact and case.
        Creates a thread and a thread message belonging to the newly created contact moment.

        :param contact_moment_uuid:
        :param thread_uuid:
        :param case_uuid:
        :param contact_uuid:
        :param channel:
        :param content:
        :param direction:
        """
        contact_repo = self.get_repository("contact")
        contact = contact_repo.get_contact_by_uuid(contact_uuid=contact_uuid)
        created_by = contact_repo.get_contact_by_uuid(
            contact_uuid=self.user_uuid
        )

        case = None
        if case_uuid is not None:
            case_repo = self.get_repository("case")
            case = case_repo.find_case_by_uuid(
                case_uuid=case_uuid,
                user_uuid=created_by.uuid,
                permission="read",
            )

        """Create a Thread"""
        thread_repo = self.get_repository("thread")
        thread_repo.create(
            new_thread_uuid=thread_uuid,
            thread_type="contact_moment",
            contact=contact,
            case=case,
        )

        """Create a Message in relation to a thread."""
        message_repo = self.get_repository("message")
        message_repo.create_contact_moment(
            thread_uuid=thread_uuid,
            created_by=created_by,
            content=content,
            channel=channel,
            direction=direction,
            contact=contact,
            contact_moment_uuid=contact_moment_uuid,
        )

        thread_repo.save()
        message_repo.save()

    @validate_with(get_data(__name__, "validation/create_note.json"))
    def create_note(
        self,
        note_uuid: UUID,
        thread_uuid: UUID,
        content: str,
        contact_uuid: UUID,
        case_uuid: UUID,
    ):
        """Command to create a note.
        Creates a thread and a thread message belonging to the newly created note.

        :param note_uuid:
        :param thread_uuid:
        :param content:
        :param contact_uuid:
        :param case_uuid:
        """
        contact_repo = self.get_repository("contact")
        created_by = contact_repo.get_contact_by_uuid(
            contact_uuid=self.user_uuid
        )

        contact = None
        if contact_uuid:
            contact = contact_repo.get_contact_by_uuid(
                contact_uuid=contact_uuid
            )

        case = None
        if case_uuid:
            case_repo = self.get_repository("case")
            case = case_repo.find_case_by_uuid(
                case_uuid=case_uuid,
                user_uuid=created_by.uuid,
                permission="read",
            )

        """Create a Thread"""
        thread_repo = self.get_repository("thread")
        thread_repo.create(
            new_thread_uuid=thread_uuid,
            thread_type="note",
            contact=contact,
            case=case,
        )

        """Create a Message in relation to a thread."""
        message_repo = self.get_repository("message")
        message_repo.create_note(
            uuid=note_uuid,
            thread_uuid=thread_uuid,
            created_by=created_by,
            content=content,
        )

        thread_repo.save()
        message_repo.save()

    @validate_with(
        get_data(__name__, "validation/create_external_message.json")
    )
    def create_external_message(
        self,
        external_message_uuid: UUID,
        thread_uuid: UUID,
        subject: str,
        content: str,
        message_type: str,
        direction: str,
        case_uuid: UUID,
        attachments: List[Mapping[str, str]],
        participants: List[Dict[str, str]] = None,
    ):
        """Command to create an external message.
        Creates a thread and a thread message belonging to the newly created external message.

        :param external_message_uuid:
        :param thread_uuid:
        :param case_uuid:
        :param subject:
        :param content:
        :param external_message_type:
        :param direction:
        :param attachments:
        """
        if participants is None:
            participants = []

        _assert_create_external_message_parameters(
            message_type, direction, participants
        )

        thread_case = None
        thread_contact = None

        is_pip_user = self.user_info.permissions.get("pip_user", False)

        case_repo = self.get_repository("case")
        contact_repo = self.get_repository("contact")
        thread_repo = self.get_repository("thread")

        if is_pip_user:
            # pip user, apply pip user acl's
            if message_type != "pip":
                raise Forbidden(
                    f"No permission to create a message of type {message_type}",
                    "communication/external_message_type_not_allowed",
                )

            thread_case = case_repo.get_case_for_pip_user(
                user_uuid=self.user_uuid, case_uuid=case_uuid
            )
        else:
            # Not a pip user, apply regular acl's
            thread_case = case_repo.find_case_by_uuid(
                case_uuid=case_uuid,
                user_uuid=self.user_uuid,
                permission="read",
            )

        if thread_case.status == "resolved":
            raise Conflict(
                "Not allowed to create message for 'resolved' case.",
                "communication/case/case_resolved",
            )

        case_requestor = contact_repo.get_requestor_contact_from_case(
            case_uuid=case_uuid
        )

        if message_type == "pip":
            thread_contact = case_requestor
        elif len(participants):
            case_subject_relations = (
                contact_repo.get_subject_relation_contacts_from_case(
                    case_uuid=case_uuid
                )
            )
            thread_contact = _get_thread_contact_from_participants(
                participants, case_subject_relations, case_requestor
            )

        created_by = contact_repo.get_contact_by_uuid(
            contact_uuid=self.user_uuid
        )

        """Get a Thread"""
        thread = thread_repo.get_thread_by_uuid(
            thread_uuid=thread_uuid,
            check_acl=False,
            user_uuid=self.user_uuid,
            permission="view",
        )
        if thread is None:
            """Create a Thread"""
            thread = thread_repo.create(
                new_thread_uuid=thread_uuid,
                thread_type="external",
                contact=thread_contact,
                case=thread_case,
            )

        thread.increment_unread_count()

        """Create a Message in relation to a thread."""
        message_repo = self.get_repository("message")
        message = message_repo.create_external_message(
            external_message_uuid=external_message_uuid,
            case_id=thread_case.id,
            thread_uuid=thread_uuid,
            created_by=created_by,
            content=content,
            subject=subject,
            external_message_type=message_type,
            participants=participants,
            direction=direction,
            original_message_file=None,
            message_date=None,
            creator_type="pip" if is_pip_user else "employee",
        )
        if message.direction != "incoming":
            context = "pip" if is_pip_user else "employee"
            timestamp = datetime.datetime.now()
            _mark_message_read(
                message=message,
                thread=thread,
                context=context,
                timestamp=timestamp,
            )

        attachment_repo = self.get_repository("attachment")
        for att in attachments:
            attachment_repo.attach_file_to_message(
                filename=att["filename"],
                file_uuid=att["id"],
                message_uuid=external_message_uuid,
            )
            _increment_attachment_count_for_message(
                message=message, thread=thread
            )

        thread_repo.save()
        message_repo.save()
        attachment_repo.save()

    @validate_with(get_data(__name__, "validation/link_thread_to_case.json"))
    def link_thread_to_case(
        self, thread_uuid, case_uuid, external_message_type
    ):
        """Command to link a thread with a case, Case is acl-ed for current user before usage.

        :param thread_uuid: thread uuid to link.
        :type thread_uuid: UUID
        :param case_uuid: Case uuid to link thread to.
        :type case_uuid: UUID
        :param external_message_type: Message type of the thread. For now Email is the only type supported.
        :type external_message_type: str
        """
        case_repo = self.get_repository("case")
        case = case_repo.find_case_by_uuid(
            case_uuid=case_uuid, user_uuid=self.user_uuid, permission="read"
        )
        if case.status == "resolved":
            raise Conflict(
                "Not allowed to link thread to 'resolved' case.",
                "communication/case/case_resolved",
            )

        thread_repository = self.get_repository("thread")
        thread = thread_repository.get_thread_by_uuid(
            thread_uuid=thread_uuid,
            check_acl=False,
            user_uuid=self.user_uuid,
            permission="view",
        )
        if not thread:
            raise Conflict(
                f"Thread with uuid: '{thread_uuid}' is not found.",
                "communication/thread/not_found",
            )

        thread.link_thread_to_case(
            case=case, external_message_type=external_message_type
        )

        message_repo = self.get_repository("message")
        thread_messages = message_repo.get_messages_by_thread_uuid(
            uuid=thread_uuid, user_info=self.user_info, case_id=None
        )
        for message in thread_messages:
            if message.read_employee is not None:
                _mark_message_unread(
                    message=message, thread=thread, context="employee"
                )

        thread_repository.save()
        message_repo.save()

    @validate_with(get_data(__name__, "validation/delete_message.json"))
    def delete_message(self, message_uuid: UUID):
        message_repo = self.get_repository("message")
        message = message_repo.get_message_by_uuid(message_uuid=message_uuid)

        thread_repo = self.get_repository("thread")
        thread = thread_repo.get_thread_by_uuid(
            thread_uuid=message.thread_uuid,
            check_acl=True,
            user_uuid=self.user_uuid,
            permission="write",
        )

        if not thread:
            raise Conflict(
                f"No permission to delete message with uuid: '{message_uuid}'.",
                "communication/message/no_permission_for_delete",
            )
        if thread.case and thread.case.status == "resolved":
            raise Conflict(
                "Can not delete messages from case with status: 'resolved'.",
                "communication/case/case_resolved",
            )

        message.delete()

        if thread.number_of_messages <= 1:
            thread.delete()
        elif message.message_type == "external":
            if not message.read_pip:
                thread.decrement_unread_count(context="pip")

            if not message.read_employee:
                thread.decrement_unread_count(context="employee")

            for i in range(len(message.attachments)):
                thread.decrement_attachment_count()

        message_repo.save()
        thread_repo.save()

    @validate_with(get_data(__name__, "validation/mark_messages_unread.json"))
    def mark_messages_unread(self, message_uuids: List[UUID], context: str):
        """
        Mark messges ith given UUID's as unread.

        :param message_uuids: List of message uuid's to be marked as unread
        :type list of UUID
        :param context: context of the command (employee or PIP)
        :type: str
        :raises Conflict: when user has no permissions to the thread.
        """

        message_repo = self.get_repository("message")
        thread_repo = self.get_repository("thread")

        messages = message_repo.get_messages_by_uuid(
            uuids=message_uuids, user_info=self.user_info
        )

        if not messages:
            raise Conflict(
                "User does not have access to any of the messages",
                "communication/message/mark_unread/no_permission",
            )

        thread_uuid = messages[0].thread_uuid

        if context == "pip":
            thread = thread_repo.get_thread_for_pip_by_uuid(
                thread_uuid=thread_uuid, user_uuid=self.user_uuid
            )
        else:
            thread = thread_repo.get_thread_by_uuid(
                thread_uuid=thread_uuid,
                check_acl=True,
                user_uuid=self.user_uuid,
                permission="read",
            )

        if not thread:
            raise Conflict(
                f"No permission to mark messages unread for thread with uuid: '{thread_uuid}'.",
                "communication/message/mark_unread/no_permission",
            )

        for message in messages:

            if message.thread_uuid != thread_uuid:
                raise Conflict(
                    f"Message with uuid {message.uuid} does not belong to thread with uuid {thread_uuid}",
                    "communication/message/mark_unread/thread_id_mismatch",
                )

            _mark_message_unread(
                message=message, thread=thread, context=context
            )

        message_repo.save()
        thread_repo.save()

    @validate_with(get_data(__name__, "validation/mark_messages_read.json"))
    def mark_messages_read(
        self, message_uuids: UUID, context: str, timestamp: datetime.datetime
    ):
        """Mark messages with given uuids as read.

        :param message_uuids: List of uuids of message to be marked as read.
        :type message_uuids: UUID
        :param context: context of the command(pip/employee)
        :type context: str
        :param timestamp: time at which the message is read.
        :type timestamp: datetime.datetime
        :raises Conflict: When user has no permission to the thread.
        """
        message_repo = self.get_repository("message")
        thread_repo = self.get_repository("thread")

        messages = message_repo.get_messages_by_uuid(
            uuids=message_uuids, user_info=self.user_info
        )

        if messages:
            thread_uuid = messages[0].thread_uuid

            if context == "pip":
                thread = thread_repo.get_thread_for_pip_by_uuid(
                    thread_uuid=thread_uuid, user_uuid=self.user_uuid
                )
            else:
                thread = thread_repo.get_thread_by_uuid(
                    thread_uuid=thread_uuid,
                    check_acl=True,
                    user_uuid=self.user_uuid,
                    permission="read",
                )
            if not thread:
                raise Conflict(
                    f"No permission to mark messages read for thread with uuid: '{thread_uuid}'.",
                    "communication/message/no_permission_to_mark_read",
                )

            for message in messages:
                if message.thread_uuid != thread_uuid:
                    raise Conflict(
                        f"Message with uuid {message.uuid} does not belong to thread with uuid {thread_uuid}",
                        "communication/message/no_permission_to_mark_read",
                    )
                _mark_message_read(
                    message=message,
                    thread=thread,
                    context=context,
                    timestamp=timestamp,
                )

            thread_repo.save()
            message_repo.save()

        else:
            raise Conflict(
                "User do not have access to any of the messages",
                "communication/message/no_permission_to_mark_read",
            )

    @validate_with(
        get_data(__name__, "validation/generate_pdf_derivative.json")
    )
    def generate_pdf_derivative(self, attachment_uuid: UUID):
        """
        Generate an pdf derivative on S3 for an attachment.
        :param attachment_uuid: The UUID of the attachment to generate pdf derivative.
        :type: attachment_uuid: UUID
        """

        attachment_repo = self.get_repository("attachment")

        # pdf generation should happen for all attachments linked with messages(email and pip).
        # this should happen irrespective of the user. New method 'get_file_by_uuid_without_acl_check' for that.
        attached_file = attachment_repo.get_file_by_uuid_without_acl_check(
            attachment_uuid
        )
        _generate_pdf_derivative_for_attachment(attached_file)

        attachment_repo.save()


def _extract_message_body(email_message: email.message.EmailMessage):
    body = "<empty message: no text part found>"

    # Multipart message: check if it has a text part that's not an attachment
    # Assume that's the text body.
    if email_message.is_multipart():
        for part in email_message.walk():
            content_type = part.get_content_type()
            content_disposition = str(part.get("Content-Disposition"))

            if (
                content_type == "text/plain"
                and "attachment" not in content_disposition
            ):
                body = part.get_content()
                break
    else:
        # Not multipart. Assume the message body is the .
        body = email_message.get_content()

    return _strip_nul_bytes(body)


def _assert_create_external_message_parameters(
    message_type, direction, participants
):
    if message_type == "pip" and participants != []:
        raise Conflict(
            "Specifying participants is not possible for PIP messages",
            "communication/external_message/pip_participants/not_allowed",
        )

    if message_type == "pip" and direction != "unspecified":
        raise Conflict(
            "PIP messages do not have a specified direction",
            "communication/external_message/direction/not_allowed",
        )
    elif message_type == "email" and direction == "unspecified":
        raise Conflict(
            "Email messages must have a specified direction",
            "communication/external_message/direction/not_allowed",
        )
    return


def _get_thread_from_message(
    subject_prefix: str,
    message: email.message.EmailMessage,
    thread_repository,
    case_repository,
):
    case = None
    match = None
    if message["Subject"]:
        match = re.search(
            r"\[{}(?: [0-9]+)? ([{}]+)\]".format(
                re.escape(subject_prefix), string.ascii_letters + string.digits
            ),
            message["Subject"],
        )
        if match:
            logger.debug(
                f"Found partial UUID in subject: {match.group(1)}. Finding."
            )

            try:
                thread = thread_repository.get_thread_by_partial_uuid(
                    match.group(1)
                )

                return {
                    "thread": thread,
                    "case_id": thread.case.id if thread.case else None,
                }
            except (NotFound, Conflict):
                # When no thread, or multiple threads are found, a new one should be
                # created.
                pass
        else:
            case = _get_case_from_message_subject(
                subject=message["Subject"],
                subject_prefix=subject_prefix,
                case_repository=case_repository,
            )

        logger.debug(
            "No partial UUID found in message subject. Creating new thread."
        )

    thread_uuid = uuid4()

    thread = thread_repository.create(
        new_thread_uuid=thread_uuid,
        contact=None,
        case=case,
        thread_type="external",
    )

    return {"thread": thread, "case_id": case.id if case else None}


def _get_case_from_message_subject(
    subject: str, subject_prefix: str, case_repository
):
    # Old-style prefixes are of the form:
    # [PREFIX 123-abcdef] (case_number-case_uuid_part)

    match = re.search(
        r"\[{} ([{}]+)-([{}]+)\]".format(
            re.escape(subject_prefix), string.digits, string.hexdigits
        ),
        subject,
    )
    if match:
        logger.debug("Found old-style subject prefix. Finding case.")
        try:
            case = case_repository.get_case_by_id(case_id=match.group(1))

            # Only add it to the case if id and partial UUID match up
            if str(case.uuid).endswith(match.group(2)):
                logger.debug(f"Found case with id={case.id}")
                return case
        except NotFound:
            pass

    logger.debug("Old-style subject prefix, but no case found.")
    return


def _get_participants(email_message) -> List[Dict[str, str]]:
    participants_to = email_message.get_all("to", [])
    participants_cc = email_message.get_all("cc", [])
    participants_from = email_message.get_all("from", [])
    participants = [
        {
            "role": "to",
            "display_name": _strip_nul_bytes(addr[0]),
            "address": _strip_nul_bytes(addr[1]),
        }
        for addr in email_utils.getaddresses(participants_to)
        if addr[1] != ""
    ]
    participants.extend(
        [
            {
                "role": "cc",
                "display_name": _strip_nul_bytes(addr[0]),
                "address": _strip_nul_bytes(addr[1]),
            }
            for addr in email_utils.getaddresses(participants_cc)
            if addr[1] != ""
        ]
    )
    participants.extend(
        [
            {
                "role": "from",
                "display_name": _strip_nul_bytes(addr[0]),
                "address": _strip_nul_bytes(addr[1]),
            }
            for addr in email_utils.getaddresses(participants_from)
            if addr[1] != ""
        ]
    )
    return participants


def _upload_and_create_attachment(
    content,
    filename: str,
    file_repository: FileRepository,
    document_file_repository: document.repositories.FileRepository,
) -> DocumentEntities.File:
    """Upload and create attachment.

    :param content: content
    :type content: content
    :param filename: filename
    :type filename: str
    :param file_repository: file repository
    :type file_repository: FileRepository
    :param document_file_repository: document file repository
    :type document_file_repository: document.repositories.FileRepository
    :return: uuid of created attachment
    :rtype: UUID
    """
    file_uuid = uuid4()
    if isinstance(content, str):
        content = content.encode("utf-8")
    elif isinstance(content, bytes):
        pass
    elif isinstance(content, email.message.EmailMessage):
        content = bytes(content)

    file_handle = io.BytesIO(content)
    # upload to S3
    file = file_repository.upload(
        file_content=file_handle, file_uuid=file_uuid
    )
    # insert file-meta in database
    file_entity = document_file_repository.create_file(
        file_uuid=file_uuid,
        filename=filename,
        mimetype=file["mime_type"],
        size=file["size"],
        storage_location=file["storage_location"],
        md5=file["md5"],
    )
    return file_entity


def _mark_message_unread(
    message: ExternalMessage, thread: Thread, context: str
) -> None:

    message.mark_unread(context=context)
    thread.increment_unread_count(context=context)


def _mark_message_read(message, thread, context, timestamp):
    """Mark a message as read.Also decrement the unread count of message thread.

    :param message: message entity
    :type message: entities.ExternalMessage
    :param thread: thread entity
    :type thread: entities.thread
    :param is_pip_user: bool indicates if user is of type pip or employee.
    :type is_pip_user: bool
    """

    message.mark_read(context=context, timestamp=timestamp)
    if context == "pip":
        thread.decrement_unread_count(context=context)
    else:
        thread.decrement_unread_count(context=context)


def _increment_attachment_count_for_message(message, thread):
    """Increment attachment_count for message and thread when an attachment is linked with message.

    :param message: message entity
    :type message: Message
    :param thread: thread_entity
    :type thread: Thread
    """
    message.increment_attachment_count()
    thread.increment_attachment_count()


def _generate_pdf_derivative_for_attachment(attached_file):
    """Generate pdf derivative for an attachment.

    :param attached_file: Attached file
    :type attached_file: [AttachedFile]
    """
    if attached_file.mimetype in [
        "application/msword",
        "application/vnd.ms-word",
        "application/vnd.oasis.opendocument.text",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "text/plain",
    ]:
        attached_file.generate_pdf_derivative()


def _get_thread_contact_from_participants(
    participants: List, case_subject_relations: List, case_requestor: Contact
):
    """Check if the participants of email message has any of the subject_relations(Aanvrager,Behandlaar,coordinator, advocaat etc) of case in it.
    If the participants has any of the subject_relations return subject_relation.

    :param participants: participants of email message
    :type participants: List
    param case_subject_relations: List of subject relations
    :type case_subject_relations: List
    param case_requestor: Requestor of case.
    :type case_requestor: Contact
    """

    if len(participants) == 0:
        return None

    dict = {}
    for p in participants:
        if p.get("uuid"):
            dict[p["uuid"]] = p

    # if the participants has requestor, return requestor
    # if there is more than one participant and none of them is requestor return None
    if str(case_requestor.uuid) in dict:
        return case_requestor
    elif len(participants) > 1:
        return None

    for s in case_subject_relations:
        if str(s.uuid) in dict:
            return s

    return None
