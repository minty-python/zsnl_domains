# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .commands import Commands
from .queries import Queries
from .repositories import (
    AttachmentRepository,
    CaseRepository,
    ContactMomentOverviewRepository,
    ContactRepository,
    FileRepository,
    MessageRepository,
    ThreadRepository,
)
from zsnl_domains import document

REQUIRED_REPOSITORIES = {
    "attachment": AttachmentRepository,
    "case": CaseRepository,
    "contact": ContactRepository,
    "file": FileRepository,
    "message": MessageRepository,
    "thread": ThreadRepository,
    "document_file": document.repositories.FileRepository,
    "contact_moment_overview": ContactMomentOverviewRepository,
}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return Commands(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
    )
