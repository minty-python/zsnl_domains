# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .base import Base
from .types import GUID, UTCDateTime
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Enum, Integer, String


class CustomObjectType(Base):
    """Table pointing to the active custom object definition"""

    __tablename__ = "custom_object_type"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)

    catalog_folder_id = Column(
        Integer, ForeignKey("bibliotheek_categorie.id"), nullable=True
    )

    authorization_definition = Column(JSONB, nullable=True)

    custom_object_type_version_id = Column(
        Integer, ForeignKey("custom_object_type_version.id"), nullable=True
    )


class CustomObjectTypeVersion(Base):
    """Table containing custom object definitions"""

    __tablename__ = "custom_object_type_version"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)

    name = Column(String, nullable=True)
    title = Column(String, nullable=True)
    subtitle = Column(String, nullable=True)
    status = Column(Enum("active", "inactive"))
    version = Column(Integer, nullable=False)
    external_reference = Column(String, nullable=True)

    custom_object_type_id = Column(
        Integer, ForeignKey("custom_object_type.id"), nullable=True
    )

    custom_field_definition = Column(JSONB, nullable=True)

    authorizations = Column(JSONB, nullable=True)

    relationship_definition = Column(JSONB, nullable=True)

    audit_log = Column(JSONB, nullable=False)

    date_created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    date_deleted = Column(UTCDateTime)


class CustomObjectVersionContent(Base):
    __tablename__ = "custom_object_version_content"

    id = Column(Integer, primary_key=True)

    archive_status = Column(Enum("archived", "to destroy", "to preserve"))
    archive_ground = Column(String, nullable=True)
    archive_retention = Column(Integer, nullable=True)

    custom_fields = Column(JSONB, nullable=True)


class CustomObject(Base):
    __tablename__ = "custom_object"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)

    custom_object_version_id = Column(
        Integer, ForeignKey("custom_object_version.id"), nullable=True
    )


class CustomObjectVersion(Base):
    __tablename__ = "custom_object_version"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    title = Column(String, nullable=True)
    subtitle = Column(String, nullable=True)
    external_reference = Column(String, nullable=True)
    status = Column(Enum("active", "inactive", "draft"))
    version = Column(Integer, nullable=False)

    custom_object_id = Column(
        Integer, ForeignKey("custom_object.id"), nullable=True
    )

    custom_object_version_content_id = Column(
        Integer, ForeignKey("custom_object_version_content.id"), nullable=True
    )

    custom_object_type_version_id = Column(
        Integer, ForeignKey("custom_object_type_version.id"), nullable=True
    )

    date_created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    date_deleted = Column(UTCDateTime)


class CustomObjectRelationship(Base):
    __tablename__ = "custom_object_relationship"

    id = Column(
        Integer,
        primary_key=True,
        autoincrement=True,
    )
    custom_object_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
    )
    custom_object_version_id = Column(
        Integer,
        nullable=False,
    )

    # Relationship meta data
    relationship_type = Column(
        Enum("document", "case", "custom_object", "subject"), nullable=False
    )
    relationship_magic_string_prefix = Column(String, nullable=True)

    # Relationship "document"
    related_document_id = Column(Integer, ForeignKey("file.id"), nullable=True)

    # Relationship "case"
    related_case_id = Column(Integer, ForeignKey("zaak.id"), nullable=True)

    # Relationship "subject"
    related_employee_id = Column(
        Integer, ForeignKey("subject.id"), nullable=True
    )
    related_person_id = Column(
        Integer, ForeignKey("natuurlijk_persoon.id"), nullable=True
    )
    related_organization_id = Column(
        Integer, ForeignKey("bedrijf.id"), nullable=True
    )

    # Relationship "custom_object"
    related_custom_object_id = Column(
        Integer, ForeignKey("custom_object.id"), nullable=True
    )

    related_uuid = Column(GUID, nullable=False)

    # Relationship Attribute
    source_custom_field_type_id = Column(
        Integer, ForeignKey("bibliotheek_kenmerken.id"), nullable=True
    )

    uuid = Column(GUID, nullable=False)
