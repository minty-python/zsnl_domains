# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from sqlalchemy.orm import declarative_base

Base = declarative_base()
